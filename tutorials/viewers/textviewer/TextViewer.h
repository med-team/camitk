/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef TEXT_VIEWER_H
#define TEXT_VIEWER_H

#include "TextViewerAPI.h"

// -- Core stuff
#include <Viewer.h>
#include <PropertyObject.h>

// -- QT stuff
#include <QColor>
#include <QAction>
#include <QTextEdit>
#include <QMenu>

/**
  *    A simple text viewer...
  *    It just send a text message everytime a component is read/close and gives information
  *    about current number of components
  */
class TEXT_VIEWER_API TextViewer : public camitk::Viewer {
    Q_OBJECT

public:

    /** @name Constructors/Destructors
      */
    /// @{
    /** construtor. */
    Q_INVOKABLE TextViewer(QString name);

    /** destructor */
    virtual ~TextViewer();
    /// @}

    /** @name Viewer inherited
      */
    /// @{
    /// refresh the view (can be interesting to know which other viewer is calling this)
    virtual void refresh(Viewer* whoIsAsking = nullptr) override;

    /// get the viewer widget.
    virtual QWidget* getWidget() override;

    /// get the propertyObject (only the 3D Scene one)
    virtual camitk::PropertyObject* getPropertyObject() override;

    /// get the viewer menu
    virtual QMenu* getMenu() override;
    /// @}

public slots:
    /// invert background/foreground colors
    void invertColors(bool);

    /// this slot is only used to add a menu item in the textviewer tutorial application
    /// It creates a SettingsDialog to let the user modify the text/background colors.
    /// This is not needed in camitk-imp as it manages all viewer properties in the general settings dialog
    void editPreference();

private:

    /// The viewer's widget
    QWidget* myWidget;

    /// the text output
    QTextEdit* output;

    /// the viewer's menu
    QMenu* myMenu;

    /// inverse rendering
    QAction* inverse;

    /// number of top level component that are currently displayed
    unsigned int displayedTopLevelComponents;

    /// color properties are defined in the property object (see constructor)
    camitk::PropertyObject* propertyObject;

    /// color management
    void updateColors();

    /**
     * Event filter of this class instance to watch its properties instances.
     * Each time a property has dynamically changed, this method is called.
     */
    bool eventFilter(QObject* object, QEvent* event) override;
};

#endif // TEXT_VIEWER_H


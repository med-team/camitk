/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef TEXT_VIEWER_EXTENSION_H
#define TEXT_VIEWER_EXTENSION_H

#include <ViewerExtension.h>

class TextViewerExtension : public camitk::ViewerExtension {
    Q_OBJECT
    Q_INTERFACES(camitk::ViewerExtension);
    Q_PLUGIN_METADATA(IID "fr.imag.camitk.tutorials.viewer.textviewerextension")

public:
    /// Constructor
    TextViewerExtension();

    /// Destructor
    virtual ~TextViewerExtension() = default;

    /// Method returning the viewer extension name
    virtual QString getName() override {
        return "TextViewer Extension";
    };

    /// Method returning the viewer extension description
    virtual QString getDescription() override {
        return "An extension that brings a pure text viewer called... TextViewer (docked by default)";
    };

    /// initialize all the viewers
    virtual void init() override;

};

#endif // TEXT_VIEWER_EXTENSION_H



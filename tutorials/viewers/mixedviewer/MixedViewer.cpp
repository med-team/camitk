/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "MixedViewer.h"

// -- Core stuff
#include <Application.h>
#include <MixedComponent.h>
#include <InteractiveGeometryViewer.h>
#include <InteractiveSliceViewer.h>
#include <Log.h>

//-- Qt stuff
#include <QGridLayout>

using namespace camitk;

// -------------------- Constructor --------------------
MixedViewer::MixedViewer(QString name) : Viewer(name) {
    myWidget = nullptr;
    setComponentClassNames(QStringList() << "ImageComponent" << "MixedComponent");
    setDescription("A two panels viewer (left is axial, right is 3D), specialized in mixed component");
}

// -------------------- Destructor --------------------
MixedViewer::~MixedViewer() {
    delete myWidget;
}

// -------------------- refresh --------------------
void MixedViewer::refresh(Viewer*) {
    // nothing to do as everything is already done by the MedicalImageViewer!
    // just make sure the viewers are both visible (after all this is a mixed viewer)
    Application::getViewer("Axial Viewer")->setVisible(true);
    Application::getViewer("3D Viewer")->setVisible(true);
}

// -------------------- getWidget --------------------
QWidget* MixedViewer::getWidget() {
    if (myWidget == nullptr) {
        myWidget = new QWidget();

        //-- init layout
        auto* myWidgetLayout = new QGridLayout();
        myWidgetLayout->setSpacing(0);
        myWidgetLayout->setMargin(0);
        leftLayout = new QVBoxLayout();
        myWidgetLayout->addLayout(leftLayout, 0, 0);
        rightLayout = new QVBoxLayout();
        myWidgetLayout->addLayout(rightLayout, 0, 1);
        myWidget->setLayout(myWidgetLayout);
    }

    // always re-embed the viewers (in case another viewer embeded them somewhere else in the meanwhile)
    InteractiveGeometryViewer* default3DViewer = dynamic_cast<InteractiveGeometryViewer*>(Application::getViewer("3D Viewer"));
    if (default3DViewer != nullptr) {
        default3DViewer->setEmbedder(rightLayout);
    }
    else {
        CAMITK_WARNING(tr("Cannot find \"3D Viewer\". This viewer is mandatory for building \"Mixed Viewer\"."))
    }
    InteractiveSliceViewer* axialViewer = dynamic_cast<InteractiveSliceViewer*>(Application::getViewer("Axial Viewer"));
    if (default3DViewer != nullptr) {
        axialViewer->setEmbedder(leftLayout);
    }
    else {
        CAMITK_WARNING(tr("Cannot find \"Axial Viewer\". This viewer is mandatory for building \"Mixed Viewer\"."))
    }

    return myWidget;
}


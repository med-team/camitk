/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef WORKINGWHENSLEEPINGRANDOM_H
#define WORKINGWHENSLEEPINGRANDOM_H

#include <Action.h>

#include <ImageComponent.h>

class WorkingWhenSleepingRandom : public camitk::Action {

public:

    /// Default Constructor
    WorkingWhenSleepingRandom(camitk::ActionExtension*);

    /// Default Destructor
    virtual ~WorkingWhenSleepingRandom();

public slots:
    /** this method is automatically called when the action is triggered.
      * Call getTargets() method to get the list of components to use.
      * \note getTargets() is automatically filtered so that it only contains compatible components,
      * i.e., instances of WorkingWhenSleepingRandom (or a subclass).
      */
    virtual camitk::Action::ApplyStatus apply();

private:
    /// helper method to simplify the target component processing
    virtual void process(camitk::ImageComponent*);
    int numberOfWindow;


};

#endif // WORKINGWHENSLEEPINGRANDOM_H


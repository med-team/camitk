/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "ComputeSelectionBarycenter.h"

#include <Property.h>
using namespace camitk;

#include <QVector3D>

#include <vtkSelectionNode.h>
#include <vtkIdTypeArray.h>

// --------------- Constructor -------------------
ComputeSelectionBarycenter::ComputeSelectionBarycenter(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Compute Selection Barycenter");
    setDescription("This action demonstrates how to compute the barycenter of the currently selected mesh nodes");
    setComponentClassName("MeshComponent");

    // Setting classification family and tags
    setFamily("Tutorial");
    addTag("Selection");
    addTag("Points");

    // Setting the action's parameters
    Property* barycenter = new Property("Selection Barycenter", QVector3D(0.0, 0.0, 0.0), "Geometric barycenter of the currently selected mesh nodes", "same as the mesh");
    barycenter->setReadOnly(true);
    addParameter(barycenter);

}

// --------------- destructor -------------------
ComputeSelectionBarycenter::~ComputeSelectionBarycenter() {
    // Do not do anything yet.
    // Delete stuff if you create stuff
    // (except if you use smart pointers of course !!)
}

// --------------- apply -------------------
Action::ApplyStatus ComputeSelectionBarycenter::apply() {

    MeshComponent* input = dynamic_cast<MeshComponent*>(getTargets().last());
    process(input);

    return SUCCESS;
}

// --------------- process -------------------
void ComputeSelectionBarycenter::process(MeshComponent* comp) {
    QVector3D barycenter;

    //-- get selected points
    vtkSmartPointer<vtkSelectionNode> currentlySelected = comp->getSelection("Picked Selection");
    if (currentlySelected != NULL) {

        vtkSmartPointer<vtkIdTypeArray> idList = vtkIdTypeArray::SafeDownCast(currentlySelected->GetSelectionList());

        // if the selection is not empty
        if (idList != NULL) {

            //-- loop the array and compute the barycenter
            double* pos;
            for (vtkIdType i = 0; i < idList->GetNumberOfTuples(); i++) {
                // get the position of the i-th selected point (its index is the i-th value of the list)
                pos = comp->getPointSet()->GetPoints()->GetPoint(idList->GetValue(i));
                barycenter += QVector3D(pos[0], pos[1], pos[2]);
            }
            barycenter /= (double) idList->GetNumberOfTuples();

        }
    }

    //-- update the property
    setProperty("Selection Barycenter", barycenter);

    //-- update the widget (to show the new barycenter)
    getWidget()->update();

}



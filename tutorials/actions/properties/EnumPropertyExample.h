/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef ENUMPROPERTYEXAMPLE_H
#define ENUMPROPERTYEXAMPLE_H

#include <Action.h>

/// Demonstrates how to add a custom enum parameters to your action
class EnumPropertyExample : public camitk::Action {

    // DO NOT forget to add Q_OBJECT otherwise the enumeration cannot be registered in the Meta Object
    Q_OBJECT

public:
    /// Default Constructor
    EnumPropertyExample(camitk::ActionExtension*);

    /// Default Destructor
    virtual ~EnumPropertyExample() = default;

    /// Example for an Enumeration
    enum EnumerationExample {
        ACE,
        KING_OF_THE_WORLD,
        QUEEN_OF_THE_UNIVERSE,
        JACK_HIT_1_ROAD,
        TEN10,
        NINE,
        EIGHT,
        SEVEN
    };

    /// Transform this to Qt enumeration. Please note the Q_ENUM (without the "S")
    /// This can only be used with Qt >= 5.5, see https://woboq.com/blog/q_enum.html
    Q_ENUM(EnumerationExample)

public slots:
    /** this method is automatically called when the action is triggered.
      * Call getTargets() method to get the list of components to use.
      * \note getTargets() is automatically filtered so that it only contains compatible components,
      * i.e., any instances of Component (or a subclass).
      */
    virtual camitk::Action::ApplyStatus apply();

};

#endif // ENUMPROPERTYEXAMPLE_H


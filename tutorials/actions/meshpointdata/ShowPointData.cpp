/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "ShowPointData.h"

#include <Application.h>
#include <Property.h>

#include <vtkPointData.h>
#include <vtkDoubleArray.h>

using namespace camitk;


// --------------- Constructor -------------------
ShowPointData::ShowPointData(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Show Point Data");
    setDescription("This action demonstrates how to show the point data on a color scale");
    setComponentClassName("MeshComponent");

    // Setting classification family and tags
    setFamily("Tutorial");
    addTag("VTK Point Data");
    addTag("Color Scale");
    addTag("Color Bar");

    // Setting the action's parameters
    Property* generationMethodProp = new Property("Generation Method", ShowPointData::RANDOM_PERCENT, "How/What Point Data to Show. In this example, there are two ways to add synthetic point data: <ol><li>The generated point data have random values between 0 and 100</li><li>The generated point data have the values of their point index</li></ol>To show the point data, select the \"Data\" tab in the property editor and select the data you would like to display. Random generation point data are recreated each time.", "");
    generationMethodProp->setEnumTypeName("GenerationMethod");
    QStringList methodGUIName;
    methodGUIName << "Randomly Generated in [0..100]" << "Use Point Index";
    generationMethodProp->setAttribute("enumNames", methodGUIName);

    addParameter(generationMethodProp);

}

// --------------- destructor -------------------
ShowPointData::~ShowPointData() {
    // Do not do anything yet.
    // Delete stuff if you create stuff
    // (except if you use smart pointers of course !!)
}

// --------------- apply -------------------
Action::ApplyStatus ShowPointData::apply() {

    foreach (Component* comp, getTargets()) {
        MeshComponent* input = dynamic_cast<MeshComponent*>(comp);

        process(input);
    }

    Application::refresh();

    return SUCCESS;
}

// --------------- process -------------------
void ShowPointData::process(MeshComponent* comp) {
    // Get the parameter
    GenerationMethod displayMethod = (GenerationMethod) property("Generation Method").toInt();
    vtkSmartPointer<vtkDoubleArray> demoPointData = vtkSmartPointer<vtkDoubleArray>::New();

    int numberOfPoints = comp->getPointSet()->GetNumberOfPoints();
    demoPointData->SetNumberOfValues(numberOfPoints);

    switch (displayMethod) {
        case RANDOM_PERCENT:
            demoPointData->SetName("Random");
            for (vtkIdType i = 0; i < numberOfPoints; ++i) {
                demoPointData->SetValue(i, 100.0 * ((double)  rand()) / ((double) RAND_MAX));
            }
            break;
        case POINT_INDEX:
            demoPointData->SetName("Index");
            for (vtkIdType i = 0; i < numberOfPoints; ++i) {
                demoPointData->SetValue(i, i);
            }
            break;
    }

    // add the point data and show it
    comp->addPointData(demoPointData->GetName(), demoPointData);
}



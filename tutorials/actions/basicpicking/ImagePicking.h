/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef IMAGE_PICKING_H
#define IMAGE_PICKING_H

#include <Action.h>
#include <QFrame>
#include <QLabel>

/**
 * Demonstrates how to get information from image picking.
 * This is an embedded action (it is shown in the ActionViewer)
 */
class ImagePicking : public camitk::Action {

public:
    /// the constructor
    ImagePicking(camitk::ActionExtension*);

    /// the destructor
    virtual ~ImagePicking() = default;

    /// method called when the action when the action is triggered (i.e. started)
    virtual QWidget* getWidget();

public slots:
    /// method called when the action is applied
    virtual camitk::Action::ApplyStatus apply();

private:
    /// this action widget (to simplify, it is just a label that gives information + a button)
    QFrame* informationFrame;

    /// the information label (needed as an attributes to update the displayed text)
    QLabel* informationLabel;

    /// is the widget automatically connected to image picking
    bool isConnected;
};

#endif // IMAGE_PICKING_H

/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef BASIC_PICKING_EXTENSION_H
#define BASIC_PICKING_EXTENSION_H

#include <QObject>
#include <Action.h>
#include <ActionExtension.h>

/// a simple action extension containing one action to show how to add a vtk 3D widget in a viewer
class VtkWidgetExtension : public camitk::ActionExtension {
    Q_OBJECT
    Q_INTERFACES(camitk::ActionExtension)
    Q_PLUGIN_METADATA(IID "fr.imag.camitk.tutorials.action.vtkwidget")

public:
    /// the constructor
    VtkWidgetExtension() : ActionExtension() {};

    /// the destructor
    virtual ~VtkWidgetExtension() = default;

    /// initialize all the actions
    virtual void init() override;

    /// Method that return the action extension name
    virtual QString getName() override {
        return "Vtk Widget Tutorial";
    };

    /// Method that return the action extension description
    virtual QString getDescription() override {
        return "This is a a simple action extension containing one action to show how to add a vtk 3D widget in a viewer (a VTK Widget is an type of interaction directly available in a VTK renderer window).";
    };

};

#endif // BASIC_PICKING_EXTENSION_H

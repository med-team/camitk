/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "ComputeNeighborValueAction.h"

#include <Application.h>
#include <Property.h>
#include <Log.h>

using namespace camitk;


// --------------- Constructor -------------------
ComputeNeighborValueAction::ComputeNeighborValueAction(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName(tr("Compute Average Neighbor Value"));
    setDescription("Compute the average voxels value in the neiborhoods of the selected voxel. Show results on the log console.");
    setComponentClassName("ImageComponent");

    // Setting classification family and tags
    setFamily(tr("Tutorial"));
    addTag(tr("Neighborhood"));
    addTag(tr("Voxel Value"));

    // Setting the action's parameters
    addParameter(new Property(tr("Radius"), QVariant(5.0), tr("The radius that defines the distance around which the average value is computed"), tr("Number of Voxels")));

}

// --------------- destructor -------------------
ComputeNeighborValueAction::~ComputeNeighborValueAction() {
    // Do not do anything yet.
    // Delete stuff if you create stuff
    // (except if you use smart pointers of course !!)
}

// --------------- apply -------------------
Action::ApplyStatus ComputeNeighborValueAction::apply() {

    foreach (Component* comp, getTargets()) {
        ImageComponent* input = dynamic_cast<ImageComponent*>(comp);
        process(input);
    }

    return SUCCESS;
}

// --------------- process -------------------
void ComputeNeighborValueAction::process(ImageComponent* comp) {
    // Get the parameters
    double r = property("Radius").toDouble();

    // get the last picked pixel index
    int x0, y0, z0;
    comp->getLastPixelPicked(&x0, &y0, &z0);

    // get the image dimensions
    // Getting the input image
    vtkSmartPointer<vtkImageData> inputImage  = comp->getImageData();
    int dims[3];
    // int numberOfScalarComponents = inputImage->GetNumberOfScalarComponents();
    inputImage->GetDimensions(dims);

    double avgValue = 0.0;
    int nbPixel = 0;

    // check the pixel has been selected
    if (x0 != -1 && y0 != -1 && z0 != -1) {

        for (int z = 0; z < dims[2]; z++) {
            for (int y = 0; y < dims[1]; y++) {
                for (int x = 0; x < dims[0]; x++) {
                    // test pixel is in the neighborood of the selected pixel
                    if (distance(x0, x, y0, y, z0, z) < r) {
                        avgValue += comp->getImageData()->GetScalarComponentAsDouble(x, y, z, 0);
                        nbPixel++;
                    }
                }
            }
        }

        avgValue = avgValue / (double) nbPixel;

        // display the result on the console
        CAMITK_INFO(tr("Pixel selected: (%1,%2,%3)\n"
                       "Average pixel values in the neighborhood of %4 pixels: %5").arg(QString("%1").arg(x0, 3), QString("%1").arg(y0, 3), QString("%1").arg(z0, 3), QString::number(r), QString("%1").arg(avgValue, 5)))

    }
    Application::refresh();
}

// --------------- distance -------------------
double ComputeNeighborValueAction::distance(int x1, int x2, int y1, int y2, int z1, int z2) {
    return sqrt((double)((x2 - x1) * (x2 - x1)) +
                (double)((y2 - y1) * (y2 - y1)) +
                (double)((z2 - z1) * (z2 - z1))
               );
}


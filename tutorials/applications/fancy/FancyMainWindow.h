/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef FANCYAPP_H
#define FANCYAPP_H

// -- Application Fancy stuff
#include "ui_FancyMainWindow.h"

// -- Core stuff
#include <MainWindow.h>

namespace camitk {
class ImageComponent;
class InteractiveViewer;
}

/**
  * This Class describes the fancy bare application. It sets up the main
  * window and providing few buttons to demonstrates that CamiTK can
  * be very fancy!
  *
  */
class FancyMainWindow : public camitk::MainWindow {
    Q_OBJECT

public:
    /// \enum LayoutVisibility describes the possible currently displayed InteractiveViewer
    /// same as MedicalImageViewer::LayoutVisibility
    enum LayoutVisibility {
        VIEWER_ALL,        ///< All InteractiveViewer are visible
        VIEWER_3D,         ///< Only the 3D InteractiveViewer are visible
        VIEWER_AXIAL,      ///< Only the axial InteractiveViewer are visible
        VIEWER_CORONAL,    ///< Only the coronal InteractiveViewer are visible
        VIEWER_SAGITTAL,   ///< Only the sagittal InteractiveViewer are visible
        VIEWER_ARBITRARY   ///< Only the arbitrary InteractiveViewer are visible
    };
    Q_ENUM(LayoutVisibility);

    /// construtor
    FancyMainWindow();

    /// destructor
    ~FancyMainWindow();

public slots:

    /// inherited from QWidget, just to refresh all viewers
    void layoutChanged();
    void dialValueChanged(int value);

    /// Method that update the angle dialog slider (text + value)
    void xAngleDialValueChanged(int value);
    void yAngleDialValueChanged(int value);
    void zAngleDialValueChanged(int value);

    /// specific open slot: only one component can be opened at a time
    void fancyFileOpen();

    /// refresh everything!
    virtual void refresh() override final;

protected:

    /// overriden from MainWindow, just to unset the current comp and avoid crash
    void closeEvent(QCloseEvent*) override;

private:

    /// currently shown viewer
    LayoutVisibility visibility;

    /// manage Angle Dialog
    void showAngleDials(bool isShown);

    /// update the dialog slider (text + value)
    void updateDialSlider();

    /// update the angle dialog slider (text + value)
    void updateAngleSlider(QDial* dial, QLabel* label);

    /// Qt model designed by QDesigner
    Ui::ui_FancyMainWindow ui;

    /// the main widget containing the ui
    QWidget* mainWidget;

    /// get the currently visible viewer (axial, sagittal, coronal or arbitrary)
    camitk::InteractiveViewer* getVisibleViewer();

    /// update the comp
    void updateComponent();

    /// specific 3D viewer for fancy
    camitk::InteractiveViewer* viewer3D;

    /// current visible component
    camitk::ImageComponent* comp;
};

#endif // FANCYAPP_H

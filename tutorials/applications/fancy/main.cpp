/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

//-- Application Fancy stuff
#include "FancyMainWindow.h"
#include "style.qss"

//-- CamiTK stuff
#include <Application.h>
#include <Core.h>

using namespace camitk;

int main(int argc, char* argv[]) {
    Application a("fancy", argc, argv);

    // use a specific style sheet for this fancy application
    a.setStyleSheet(style);

    // set the main window using the Fancy class
    a.setMainWindow(new FancyMainWindow());

    // start by opening a mha
    Application::open(Core::getTestDataDir() + "/brain.mha");

    // let's go
    a.exec();
}

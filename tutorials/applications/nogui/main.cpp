/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// -- Core stuff
#include <ExtensionManager.h>
#include <QApplication>
#include <Core.h>
#include <Application.h>
#include <Action.h>
#include <ImageComponent.h>
#include <Log.h>

using namespace camitk;

int main(int argc, char* argv[]) {
    // Example of a non-gui program that
    // - open a MHA image file
    // - resample it to half its size using the "Resample" action
    // - save the resampled image in the current working directory
    //
    // note: it still requires a graphics renderer even if nothing is displayed
    // even if no event loop are started.
    //
    Log::getLogger()->setLogLevel(InterfaceLogger::INFO);
    Application app("nogui", argc, argv);

    // open an image volume
    ComponentExtension* cp = ExtensionManager::getComponentExtension("mha");
    Component* image = cp->open(QFileInfo(Core::getTestDataDir() + "/brain.mha").absoluteFilePath());

    // get the Resample action
    Action* resampleAction = Application::getAction("Resample");

    if (resampleAction != nullptr) {
        CAMITK_INFO_ALT(QString("Applying action: %1").arg(resampleAction->getName()))

        // set the input
        resampleAction->setInputComponent(image);

        //app.getMainWindow()->setCentralWidget(resampleAction->getWidget());
        //app.exec();

        // set the parameter
        ImageComponent* inputImage = dynamic_cast<ImageComponent*>(image);
        // Note: as the Resample action takes ImageComponent as inputs, the dynamic_cast is
        // guaranty to work perfectly well.
        int* dims = inputImage->getImageData()->GetDimensions();
        double factor = 0.5;
        resampleAction->setProperty("New Image X Dimension", int(dims[0]*factor));
        resampleAction->setProperty("New Image Y Dimension", int(dims[1]*factor));
        resampleAction->setProperty("New Image Z Dimension", std::max(1, int(dims[2]*factor)));

        // apply the action
        if (resampleAction->applyInPipeline() != Action::SUCCESS) {
            CAMITK_WARNING_ALT("Error while applying the Resample action");
            return EXIT_FAILURE;
        }

        // get the last instantiated top level component, i.e., the result of the resampleAction
        Component* resampled = Application::getTopLevelComponents().last();

        // save the resampled image into the current directory
        QString outputFileName = QDir::currentPath() + "/" + QFileInfo(image->getFileName()).completeBaseName() + "-output." + QFileInfo(image->getFileName()).suffix();

        resampled->setFileName(outputFileName);

        if (Application::save(resampled)) {
            CAMITK_INFO_ALT(QString("Resampled image succesfully saved to %1").arg(resampled->getFileName()))
            return EXIT_SUCCESS;
        }
        else {
            CAMITK_WARNING_ALT("Error while saving the resampled image");
            return EXIT_FAILURE;
        }
    }
    else {
        CAMITK_WARNING_ALT("Cannot find Action \"Resample\". Please make sure the \"ResampleExtension\" is present in your action extension installation path.")
        return EXIT_FAILURE;
    }
}


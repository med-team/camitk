/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef Mixed_COMPONENT_H
#define Mixed_COMPONENT_H

#include <Component.h>

/** The manager of the mixed data.
 *  A small extension that integrates mha and vtk together.
 *  .mixed files are just text files with two lines:
 *  - first line is the name of the mha file
 *  - second line is the name of the vtk file
 *  All files have to be in the CamiTK same directory
 *
 *  @author Emmanuel Promayon
 */
class MixedComponent : public camitk::Component {
public:
    /// default constructor
    /// This method may throw an AbortException if a problem occurs.
    MixedComponent(const QString& file);

    /// do nothing to init the representation, as all representation are done in the sub-component
    virtual void initRepresentation() {};
};

#endif

/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef PICKME_H
#define PICKME_H

#include <QObject>

#include <VtkMeshComponent.h>
#include <vtkDoubleArray.h>
#include <vtkSmartPointer.h>

class PickMe : public VtkMeshComponent {

    Q_OBJECT

public:
    /// Default Constructor
    /// This method may throw an AbortException if a problem occurs.
    PickMe(const QString& file);

    /// Default Destructor
    virtual ~PickMe() = default;

    /// reimplemented to visualize the picked points (demo)
    void pointPicked(vtkIdType pointId, bool) override;

    /// reimplemented to visualize the picked cells (demo)
    void cellPicked(vtkIdType cellId, bool) override;

private:
    /// the demo point data array (i.e the data value displayed when picked is the id of the point, in point picking, or the average of the point id, in cell picking)
    vtkSmartPointer<vtkDoubleArray> demoPointData;

    /// initialize the point data
    void initPointData();

    /// update the visualization of point data
    /// This is just needed for fluidify the visualization/interaction
    /// (avoid the need to select/unselect data in the "Data" tab of the property editor)
    void refreshPointData();
};

#endif // PICKME_H


/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef Abort_EXTENSION_H
#define Abort_EXTENSION_H

#include <QObject>
#include <ComponentExtension.h>

/** This Abort Component manages ".abort" files that always... aborts!
 */
class AbortExtension : public camitk::ComponentExtension {
    Q_OBJECT
    Q_INTERFACES(camitk::ComponentExtension)
    Q_PLUGIN_METADATA(IID "fr.imag.camitk.tutorials.component.Abort")

public:
    /// the constructor (do nothing really)
    AbortExtension() : ComponentExtension() {};

    /// get the plugin name
    virtual QString getName() const override;

    /// get the plugin description (can be html)
    virtual QString getDescription() const override;

    /// get the list of managed extensions (each file with an extension in the list can be loaded by this extension)
    virtual QStringList getFileExtensions() const override;

    /** get a new instance from data stored in a file (this is the most important method to redefine in your subclass)
     *  This method may throw an AbortException if a problem occurs.
     */
    virtual camitk::Component* open(const QString&) override;

protected:
    /// the destructor
    virtual ~AbortExtension() = default;
};

#endif //Abort_EXTENSION_H

/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include <QObject>
#include <QMap>
#include <Property.h>

#ifndef PMLITEMPROPERTY_H
#define PMLITEMPROPERTY_H

/** This class holds a list of CamiTK Properties and can be used as an editable QObject
 *  for the ObjectControler widget
 */
class CamiTKPropertyList : public QObject {
    Q_OBJECT

public:

    // destructor
    ~CamiTKPropertyList() override;

    /** Get a Property given its name
     *  @param name the property name
     *  @return nullptr if the name does not match any property name
     *
     *  @see Property
     */
    Q_INVOKABLE virtual camitk::Property* getProperty(QString name);

    /** Add a new CamiTK property to the component.
     * If the property already exist, it will just change its value.
     *
     * \note
     * The component takes ownership of the Property instance.
     *
     * @return false if the Qt Meta Object property was added by this method (otherwise the property was already defined and true is returned if it was successfully updated)
     */
    virtual bool addProperty(camitk::Property*);

    /// get the number of properties
    virtual unsigned int getNumberOfProperties() const;

    /// get the property stored at the given index
    virtual camitk::Property* getProperty(unsigned int);

private:
    /// list of CamiTK property decorating the dynamic properties
    QMap<QString, camitk::Property*> propertyMap;

};
#endif // PMLITEMPROPERTY_H

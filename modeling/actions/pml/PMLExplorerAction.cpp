/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "PMLExplorerAction.h"

// CamiTK includes
#include <PMLExplorerWidget.h>
#include <Application.h>
#include <PMLComponent.h>

using namespace camitk;

// --------------- Constructor -------------------
PMLExplorerAction::PMLExplorerAction(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Explore PML Content");
    setDescription("This action aims at exploring inner PML information (atoms, cells and components). Features a specific Qt explorer.");
    setComponentClassName("PMLComponent");

    // Setting classification family and tag
    setFamily("Physical Model");
    addTag("pml");
    addTag("physicalmodel");
    addTag("physical");
    addTag("physical model");

    // else getWidget seg fault !
    pmlExplorerWidget = NULL;

}

// --------------- destructor -------------------
PMLExplorerAction::~PMLExplorerAction() {
    if (pmlExplorerWidget) {
        delete pmlExplorerWidget;
    }
}

// --------------- apply -------------------
Action::ApplyStatus PMLExplorerAction::apply() {

    // The action itselft does not launch an algorithm, only display a widget to explore the PML component.
    // => return always success
    return SUCCESS;
}

// --------------- getWidget -------------------
QWidget* PMLExplorerAction::getWidget() {
    // The action only explores one PML component, here explore the last selected one
    // (in case of multi component selection)
    PMLComponent* comp = dynamic_cast<PMLComponent*>(getTargets().last());
    if (comp) {
        if (!pmlExplorerWidget) {
            pmlExplorerWidget = new PMLExplorerWidget(comp);
        }
        else {
            pmlExplorerWidget->updateTarget(comp);
        }
    }

    return pmlExplorerWidget;
}

/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef MMLACTIONEXTENSION_H
#define MMLACTIONEXTENSION_H

#include <QObject>
#include <Action.h>
#include <ActionExtension.h>
/**
 * @ingroup group_cepmodeling_actions_mml
 *
 * @brief
 * Manager for the MML action.
 *
 *
 **/
class MMLActionExtension : public camitk::ActionExtension {
    Q_OBJECT
    Q_INTERFACES(camitk::ActionExtension)
    Q_PLUGIN_METADATA(IID "fr.imag.camitk.imaging.action.mml")

public:
    /// the constructor
    MMLActionExtension() : ActionExtension() {};

    /// the destructor
    virtual ~MMLActionExtension() = default;

    /// Method must be reimplemented when an ActionExtension is done,
    /// the own actions of an extension are load there.
    /// It is necessary to use the method Application::addAction(Action) for each action added in this method.
    virtual void init() override;

    /// Method that return the action extension name
    virtual QString getName() override {
        return "MML";
    };

    /// Method that return the action extension description
    virtual QString getDescription() override {
        return "Various actions for MML";
    };

};



#endif // MMLACTIONEXTENSION_H

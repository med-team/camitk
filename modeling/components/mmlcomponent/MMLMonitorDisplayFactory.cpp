/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "MMLMonitorDisplayFactory.h"

MMLMonitorDisplayFactory* MMLMonitorDisplayFactory::instance = nullptr;

// -------------------- hasClass --------------------
bool MMLMonitorDisplayFactory::isRegistered(std::string id) {
    return mapObjectCreator.find(id) != mapObjectCreator.end();
}

// -------------------- createObject --------------------
MMLMonitorDisplay* MMLMonitorDisplayFactory::createMonitorDisplay(std::string id, Monitor* monitor, MMLComponent* manager) {
    std::map<std::string, CreateMonitorDisplayFunctionPointer>::iterator iter = mapObjectCreator.find(id);
    if (iter == mapObjectCreator.end()) {
        return nullptr;
    }
    return ((*iter).second)(monitor, manager);
}

// -------------------- getDisplayByType --------------------
std::string MMLMonitorDisplayFactory::getDisplayByType(Monitor::type type, const unsigned int i) {
    if (i < displaysByType[type].size()) {
        return displaysByType[type][i];
    }
    else {
        return nullptr;
    }
}

// -------------------- getNumberOfDisplaysByType --------------------
unsigned int MMLMonitorDisplayFactory::getNumberOfDisplaysByType(Monitor::type type) {
    return displaysByType[type].size();
}

// -------------------- getInstance --------------------
MMLMonitorDisplayFactory* MMLMonitorDisplayFactory::getInstance() {
    if (!instance) {
        instance = new MMLMonitorDisplayFactory();
    }
    return instance;
}


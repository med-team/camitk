/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "Colors.h"

#include <PMLComponent.h>

#include <vtkDoubleArray.h>
#include <vtkPointData.h>

#include "MMLMonitorDisplayFactory.h"
#include "MMLComponent.h"

bool colorsRegistered = MMLMonitorDisplayFactory::getInstance()->registerClass<Colors>("Colors", Monitor::SCALARSET);

// ---------------------- constructor ----------------------------
Colors::Colors(Monitor* monitor, MMLComponent* manager): MMLMonitorDisplay(monitor, manager) {

}

// ---------------------- update ----------------------------
void Colors::update() {

    // create the corresponding point data
    PMLComponent* pmlComp = manager->getPMLComponent();

    // Use MeshComponent DataArray
    vtkSmartPointer<vtkDoubleArray> monitorDataArray = vtkDoubleArray::New();
    monitorDataArray->SetNumberOfValues(pmlComp->getPhysicalModel()->getNumberOfAtoms());

    // init to zero
    for (vtkIdType i = 0; i < pmlComp->getPhysicalModel()->getNumberOfAtoms(); i++) {
        monitorDataArray->SetValue(i, 0.0);
    }

    // update the known data
    for (unsigned int i = 0; i < monitor->getNumberOfIndex(); i++) {
        Atom* a = pmlComp->getPhysicalModel()->getAtom(monitor->getIndexOfValues(i));
        monitorDataArray->SetValue(pmlComp->getPointId(a), monitor->getValue(i));
    }

    pmlComp->addDataArray(camitk::MeshComponent::POINTS, QString(monitor->getTypeName().c_str()), monitorDataArray);
}

// ---------------------- hide ----------------------------
void Colors::hide() {
    // remove the current colors
    manager->getPMLComponent()->setDataRepresentationOff();
}



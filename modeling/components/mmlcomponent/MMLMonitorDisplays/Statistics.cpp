/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "Statistics.h"

#include "MMLMonitorDisplayFactory.h"
#include "MMLDisplay.h"
#include "MMLComponent.h"

#include <monitoringgui/MonitoringGuiManager.h>
#include <monitoringgui/MonitoringDialog.h>

#include <QHeaderView>

bool statisticsRegistered = MMLMonitorDisplayFactory::getInstance()->registerClass<Statistics>("Statistics", Monitor::SCALARSET);

Statistics::Statistics(Monitor* monitor, MMLComponent* manager): MMLMonitorDisplay(monitor, manager) {
    QTableWidget* t = manager->getMonitoringGuiManager()->getDialog()->getMonitorsTableWidget();
    line = manager->getDisplay()->getDisplayedMonitorLine();
    int j = t->columnCount();
    oldRowSize = t->rowHeight(line);
    t->setColumnCount(j + 1);
    auto* item = new QTableWidgetItem();
    item->setText(("Statistics"));
    t->setHorizontalHeaderItem(j, item);
    table = new QTableWidget;
    table->setEditTriggers(QAbstractItemView::NoEditTriggers);
    t->setCellWidget(line, j, table);
}

Statistics::~Statistics() {
    //if (table)
    //  delete table;
}

// ---------------------- update ----------------------------
void Statistics::update() {
    table->clear();
    QTableWidget* t = manager->getMonitoringGuiManager()->getDialog()->getMonitorsTableWidget();
    QString s;
    table->setColumnCount(3);
    table->setRowCount(1);
    auto* item1 = new QTableWidgetItem();
    item1->setText(tr("minimum"));
    table->setHorizontalHeaderItem(0, item1);
    auto* item2 = new QTableWidgetItem();
    item2->setText(tr("maximum"));
    table->setHorizontalHeaderItem(1, item2);
    auto* item3 = new QTableWidgetItem();
    item3->setText(tr("average"));
    table->setHorizontalHeaderItem(2, item3);
    table->verticalHeader()->hide();
    t->setRowHeight(line, 3 * table->rowHeight(0));

    double min = monitor->getValue(0);
    double max = monitor->getValue(0);
    double sum = 0;
    for (unsigned int i = 0; i < monitor->getNumberOfValues(); i++) {
        double val = monitor->getValue(i);
        if (min > val) {
            min = val;
        }
        if (max < val) {
            max = val;
        }
        sum += val;
    }
    table->setItem(0, 0, new QTableWidgetItem(s.setNum(min)));
    table->setItem(0, 1, new QTableWidgetItem(s.setNum(max)));
    if (monitor->getNumberOfValues() > 0) {
        table->setItem(0, 2, new QTableWidgetItem(s.setNum(sum / monitor->getNumberOfValues())));
    }

}

// ---------------------- hide ----------------------------
void Statistics::hide() {
    table->clear();
    QTableWidget* t = manager->getMonitoringGuiManager()->getDialog()->getMonitorsTableWidget();
    int i = t->columnCount();
    t->removeColumn(i - 1);
    t->setColumnCount(i - 1);
    t->setRowHeight(line, oldRowSize);
}


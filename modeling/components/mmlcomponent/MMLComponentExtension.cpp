/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "MMLComponentExtension.h"
#include "MMLComponent.h"

using namespace camitk;

// --------------- getName -------------------
QString MMLComponentExtension::getName() const {
    return "MML Component";
}

// --------------- getDescription -------------------
QString MMLComponentExtension::getDescription() const {
    return "Manage <em>.mml</em> document in <b>CamiTK</b>.<br/>Lots of things are possible with MML!";
}

// --------------- getFileExtensions -------------------
QStringList MMLComponentExtension::getFileExtensions() const {
    QStringList ext;
    ext << "mml";
    ext << "scn";
    return ext;
}

// --------------- open -------------------
camitk::Component* MMLComponentExtension::open(const QString& fileName) {
    return new MMLComponent(fileName);
}

// -------------------- save --------------------
bool MMLComponentExtension::save(Component* component) const {
    MMLComponent* mmlComp = dynamic_cast<MMLComponent*>(component);
    bool saveStatus = true;
    if (mmlComp) {
        mmlComp->saveMML();
    }
    else {
        saveStatus = ComponentExtension::save(component);
    }
    return saveStatus;
}

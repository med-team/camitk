#if defined(_WIN32) // MSVC and mingw

#ifdef COMPILE_PML_COMPONENT_API
#define PML_COMPONENT_API __declspec(dllexport)
#else
#define PML_COMPONENT_API __declspec(dllimport)
#endif // COMPILE_PML_COMPONENT_API

#else // for all other platforms PML_COMPONENT_API is defined to be "nothing"

#ifndef PML_COMPONENT_API
#define PML_COMPONENT_API
#endif // COMPILE_PML_COMPONENT_API

#endif // MSVC and mingw
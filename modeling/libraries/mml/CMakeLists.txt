# Is this tool is compiled inside or outside CAMITK (as a standalone)?
if (NOT CEP_NAME STREQUAL "modeling")
  # if this tool is compiled independently from CamiTK
  set(MML_STANDALONE ON CACHE BOOL "ON only if MML compiled outside CAMITK SDK")
  message(SEND_ERROR "Compilation of MML outside CamiTK SDK not implemented yet. Please contact authors")
endif()

# for generating GUI
option(MML_GENERATE_GUI "Generates MML GUI for CamiTK action" ON)

add_subdirectory(monitoring)

if(MML_GENERATE_GUI)
    add_subdirectory(monitoringgui)
endif()

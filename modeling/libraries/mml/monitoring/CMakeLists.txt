# TODO properly define the path to the XML Schema at run time (in MonitoringManager and MonitoringManagerFactory)
# otherwise this generates an error when executing the application on a machine that have no or a different
# source dir path!
#add_definitions(-DMONITORIN_XSD="${CMAKE_CURRENT_SOURCE_DIR}/../schema/MonitorIn.xsd")
#add_definitions(-DMONITOROUT_XSD="${CMAKE_CURRENT_SOURCE_DIR}/../schema/MonitorOut.xsd")

# +-------------------------------------------------------+
# |  EXTERNAL SIMULATOR SUPPORT : SOFA, ANSYS, ARTISYNTH  |
# +-------------------------------------------------------+
include(ExternalSimulatorSupport.cmake)

# +---------------+
# |  GUI SUPPORT  |
# +---------------+
if(MML_GENERATE_GUI)
    add_definitions(-DMML_GENERATE_GUI)
    set(MML_GUI_MOC_SRCS SimulatorWidget.h          
                         Xmlhighlighter.h
    )
    # qt4_wrap_cpp(MML_GUI_MOCS ${MML_GUI_MOC_SRCS}) Not needed if AUTOMOC is set to ON (CMake therefore handle moc automatically)
    set(MML_GUI_HEADERS ${MML_GUI_MOC_SRCS})
    set(MML_GUI_SRC ${MML_GUI_MOCS}
                    SimulatorWidget.cpp    
                    Xmlhighlighter.cpp
    )
endif(MML_GENERATE_GUI)

# +-----------------------+
# |  SOURCES AND HEADERS  |
# +-----------------------+
set(monitoring_H        MMLAPI.h
                        StoppingCriterion.h
                        StoppingCriterionFactory.h
                        Criterion.h
                        CriterionFactory.h
                        MultipleCriterion.h
                        MultipleCriterionFactory.h
                        Method.h
                        MethodFactory.h
                        Force.h 
                        KineticEnergy.h
                        Position.h
                        TimeMonitoring.h
                        Velocity.h
                        AndMultipleCriterion.h
                        OrMultipleCriterion.h
                        MinThreshold.h
                        Threshold.h
                        TimePeriodThreshold.h
                        Simulator.h
                        NonInteractiveSimulator.h
                        InteractiveSimulator.h
                        SimulatorFactory.h
                        InitSimulators.h
                        ${MML_SOFA_HEADERS}
                        ${MML_ANSYS_HEADERS}
                        ${MML_ARTISYNTH_HEADERS}
                        ${MML_GUI_HEADERS}       
                        Monitor.h 
                        MonitorFactory.h
                        MonitorGeometricDeviation.h
                        MonitorPosition.h
                        MonitorRen.h
                        MonitorForce.h
                        MonitorPointSetDistance.h
                        MonitorPointFinalSetDistance.h
                        MonitorVolume.h
                        MonitorSurface.h
                        MonitorDisplacement.h
                        MonitorNormDisplacement.h
                        MonitorPointToTriangleMeshDistanceFinal.h
                        MonitorDistanceX.h
                        MonitorDistanceY.h
                        MonitorDistanceZ.h
                        MonitorComputingTime.h
                        Reference.h
                        Chrono.h
                        Tools.h
                        Macros.h
                        AtomIterator.h
                        Facet.h
                        SurfaceExtractor.h
                        MonitoringManager.h
                        MonitoringManagerFactory.h
                        InteractiveMonitoringManager.h
                        NonInteractiveMonitoringManager.h
)

set(monitoring_SRCS     ${monitoring_H}
                        StoppingCriterion.cpp
                        StoppingCriterionFactory.cpp
                        Criterion.cpp
                        CriterionFactory.cpp
                        MultipleCriterion.cpp
                        MultipleCriterionFactory.cpp
                        Method.cpp
                        MethodFactory.cpp
                        Force.cpp 
                        KineticEnergy.cpp
                        Position.cpp
                        TimeMonitoring.cpp
                        Velocity.cpp
                        AndMultipleCriterion.cpp
                        OrMultipleCriterion.cpp
                        MinThreshold.cpp
                        Threshold.cpp
                        TimePeriodThreshold.cpp
                        Simulator.cpp
                        NonInteractiveSimulator.cpp
                        InteractiveSimulator.cpp
                        SimulatorFactory.cpp
                        ${MML_SOFA_SRC}
                        ${MML_ANSYS_SRC}
                        ${MML_ARTISYNTH_SRC}
                        ${MML_GUI_SRC}
                        Monitor.cpp
                        MonitorFactory.cpp
                        MonitorGeometricDeviation.cpp
                        MonitorPosition.cpp
                        MonitorRen.cpp
                        MonitorForce.cpp
                        MonitorPointSetDistance.cpp
                        MonitorPointFinalSetDistance.cpp
                        MonitorVolume.cpp
                        MonitorSurface.cpp
                        MonitorDisplacement.cpp
                        MonitorNormDisplacement.cpp
                        MonitorDistanceX.cpp
                        MonitorDistanceY.cpp
                        MonitorDistanceZ.cpp
                        MonitorComputingTime.cpp
                        MonitorPointToTriangleMeshDistanceFinal.cpp
                        Reference.cpp
                        Chrono.cpp
                        Tools.cpp
                        AtomIterator.cpp
                        Facet.cpp
                        SurfaceExtractor.cpp
                        MonitoringManager.cpp
                        MonitoringManagerFactory.cpp
                        InteractiveMonitoringManager.cpp
                        NonInteractiveMonitoringManager.cpp
)

# Add all the include directories
set(monitoring_INCLUDE_DIRECTORIES  ${MMLSCHEMA_INCLUDE_DIR}
                           ${MML_GUI_INCLUDE_DIR} 
                           ${SOFA_INCLUDE_DIR}
                           ${MML_ANSYS_WORKING_DIR}
                           ${MML_ARTISYNTH_WORKING_DIR}
)
       
# add monitoring as a camitk library
camitk_library( SHARED
                SOURCES ${monitoring_SRCS}
                DEFINES COMPILE_MML_TOOL
                NEEDS_QT
                NEEDS_XERCESC
                NEEDS_CEP_LIBRARIES mmlschema pml pmlschema lml lmlschema
                PUBLIC #install directly in lib (or bin for Windows).
                NEEDS_XSD
                INCLUDE_DIRECTORIES ${monitoring_INCLUDE_DIRECTORIES}
                LINK_DIRECTORIES ${SOFA_LIB_DIR}
                # Caution: probably need here for Ansys and Artisynth library something like - LINK_DIRECTORIES ${MML_ANSYS_WORKING_DIR}/lib ${MML_ARTISYNTH_WORKING_DIR}/lib
                EXTERNAL_LIBRARIES ${SOFA_LIBS}
                CEP_NAME CEP_MODELING
                HEADERS_TO_INSTALL ${monitoring_H}
                DESCRIPTION "Tools to monitor a biomechanical deformation."
)

set_target_properties(${LIBRARY_TARGET_NAME} PROPERTIES ${${LIBRARY_TARGET_NAME}_LIBRARY_PROPERTIES} LINK_INTERFACE_LIBRARIES "")

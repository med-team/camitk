/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef MANAGER_NONINTERACTIVEMANAGER_H
#define MANAGER_NONINTERACTIVEMANAGER_H

#include <memory>   // std::auto_ptr
#include <vector>

// Monitor includes
#include <MonitorIn.hxx>
#include <MonitorOut.hxx>
#include "MonitoringManager.h"
#include "Monitor.h"

// Simulator includes
#include "NonInteractiveSimulator.h"
/**
 * @ingroup group_cepmodeling_libraries_mml
 *
 * @brief
 * NonInteractice managers are managers linked with an non interactive simulator
 *
 **/
class NonInteractiveMonitoringManager: public MonitoringManager {
public:
    /**
     * constructor
     *@param mml mml file name
     *@param sim simulator name (Sofa, Ansys...)
     */
    NonInteractiveMonitoringManager(const char* mml);

    /// destructor
    ~NonInteractiveMonitoringManager() override;

    /** make computation of all steps with the simulator
     * @return false if calculation failed
     */
    bool doCalc();
    /// Initialize manager
    bool init() override;
    /// End manager
    void end() override;
    /// postprocess a simulation step, doCalc must have been done before
    void doMove() override;
    /// Check if simulation is finished
    bool checkStop() override;

private:
    /// the number of step unsed for simulation
    int maxStep;
    /// the simulator used for simualtion
    NonInteractiveSimulator* simul;



};

#endif // MANAGER_NONINTERACTIVEMANAGER_H

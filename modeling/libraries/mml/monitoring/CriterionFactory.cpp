/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Stopping criteria includes
#include "CriterionFactory.h"

#include "iostream"

// -------------------- constructor --------------------
CriterionFactory::CriterionFactory() {}

// -------------------- destructor --------------------
CriterionFactory::~CriterionFactory() {}

// -------------------- createCriterion --------------------
Criterion* CriterionFactory::createCriterion(mml::Criteria* c, MonitoringManager* monitoringManager, MultipleCriterion* parent) {
    if (dynamic_cast<mml::KineticEnergy*>(c)) {
        return (new kineticEnergy((mml::KineticEnergy*)c, monitoringManager, parent));
    }
    else if (dynamic_cast<mml::Speed*>(c)) {
        return (new Velocity((mml::Speed*)c, monitoringManager, parent));
    }
    else if (dynamic_cast<mml::Position*>(c)) {
        return (new Position((mml::Position*)c, monitoringManager, parent));
    }
    else if (dynamic_cast<mml::Time*>(c)) {
        return (new Time((mml::Time*)c, monitoringManager, parent));
    }
    else if (dynamic_cast<mml::Force*>(c)) {
        return (new ForceCriterion((mml::Force*)c, monitoringManager, parent));
    }
    else {
        std::cerr << "Criteria type error" << std::endl;
        return nullptr;
    }
}

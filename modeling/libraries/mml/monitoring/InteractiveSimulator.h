/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef SIMULATOR_INTERACTIVESIMULATOR_H
#define SIMULATOR_INTERACTIVESIMULATOR_H
// Simulator includes
#include "Simulator.h"
/**
 *
 * @ingroup group_cepmodeling_libraries_mml
 *
 * @brief
 * An interactive simulator is a simulator that we can control step by step (ex: Sofa)
 */
class MML_API InteractiveSimulator: public Simulator {
public:
    /// constructor
    InteractiveSimulator(MonitoringManager* monitoringManager);
    /// constructor
    InteractiveSimulator(MonitoringManager* monitoringManager, const char* file);
    /// destructor
    ~InteractiveSimulator() override = default;

    /// initialize the simulator
    void init() override = 0;
    /// end simultor
    void end() override = 0;
    /// get current position for one atom
    void getPosition(int index, double position[3]) override = 0;

    /**
     * ask the simulator do to one step of the simulation
     * @param dt integration step
     */
    virtual void doMove(double dt) = 0;

    /// Create a pml file from an imput file
    void createPml(const char* inputFile, const char* pmlFile) override = 0;
};

#endif // SIMULATOR_INTERACTIVESIMULATOR_H

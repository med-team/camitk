/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef TOOLS_FACET_H
#define TOOLS_FACET_H

#include "PhysicalModel.h"
/**
 *
 * @ingroup group_cepmodeling_libraries_mml
 *
 * @brief
 * Class facet to hold and compare facet
 * This class is used for the extraction of external surface during importation of simulator specific files
 */
class Facet {
public:
    /// create a facet using size nodes and their indexes
    Facet(unsigned int size, unsigned int id[]);

    /// destructor
    virtual ~Facet();

    /// if it is the same (equivalent) facet, increment used (return true if equivalence)
    bool testEquivalence(unsigned int size, unsigned int id[]);

    /// return the corresponding PML cell
    Cell* getCell(PhysicalModel*) const;

    /// print on stdout
    void debug();

    /// get the number of time it is being used
    unsigned int getUsed() const;

private:
    /// is this atom index present in this facet (no check on the order)
    bool isIn(unsigned int) const;

    /// the facet atom indexes
    unsigned int* id;

    /// nr of atoms composing the facet (3 = triangle, 4 = quad)
    unsigned int size;

    /// nr of times the facet is used
    unsigned int used;
};

#endif // TOOLS_FACET_H
/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef TOOLS_CHRONO_H
#define TOOLS_CHRONO_H

#include <iostream>
/**
 *
 * @ingroup group_cepmodeling_libraries_mml
 *
 * @brief
 * Elapsed real-time timer.
 * Allows one to measure elapsed real time. You can start, stop, reset
 * and of course get and print the current values of this chronometer.
 */
class Chrono {
public:
    /// default constructor
    Chrono();
    /// new chrono that starts directly at the given time
    Chrono(double);

    ~Chrono() = default;

    /// start at zero
    void start();
    /// start at a given value
    void start(double);
    /// stop chrono
    double stop();

    /// pause the chrono if the parameter is true
    void hold(bool);

    /// reset to zero
    void reset();

    /** if running, get the value (start time - now) without stopping the chrono,
      * else get (start time - stop time) value
      * this method is declared to be const, because we need to call it in <<
      * (and of course, no data member is modified
      */
    double get() const;

    /// print the value of the chrono on the stream
    friend std::ostream& operator << (std::ostream&, const Chrono);

private:
    double startValue;
    double stopValue;
    bool   running;
    double accumulatedTime;

    /// in milliseconds
    double getTimeInMilliseconds() const;
};

#endif // TOOLS_CHRONO_H

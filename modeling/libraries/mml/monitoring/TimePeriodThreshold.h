/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef STOPPINGCRITERION_STOPPINGCRITERIA_METHODS_TIMEPERIODTHRESHOLD_H
#define STOPPINGCRITERION_STOPPINGCRITERIA_METHODS_TIMEPERIODTHRESHOLD_H

#include <MonitorIn.hxx>

// Stopping criteria includes
#include "Method.h"
/**
 *
 * @ingroup group_cepmodeling_libraries_mml
 *
 * @brief
 * A TimePeriodThreshold method is a method where individualTest is true when the tested double is true during a time period defined by a number of iterations
 */
class TimePeriodThreshold: public Method {
public:
    /**
     * constructor
     * @param the xsdcxx generated TimePeriodThreshold class
     */
    TimePeriodThreshold(mml::TimePeriodThreshold* m);

    /// destructor
    ~TimePeriodThreshold() override = default;

    /// return true if tested <= value
    bool individualTest(double tested) override;

    /// get Method name
    std::string toString() override;

private:
    /// value of the threshold
    double value;

    /// number of iterations defining the time period
    int iterations;

    /// Needed to keep an eye on consecutive win
    int increment;
};

#endif // STOPPINGCRITERION_STOPPINGCRITERIA_METHODS_TIMEPERIODTHRESHOLD_H

/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef MMLMonitorDisplayFactory_H
#define MMLMonitorDisplayFactory_H

// Monitor includes
#include <MonitorIn.hxx>
#include "MonitorGeometricDeviation.h"
#include "MonitorPosition.h"
#include "MonitorRen.h"
#include "MonitorForce.h"
#include "MonitorPointSetDistance.h"
#include "MonitorPointFinalSetDistance.h"
#include "MonitorVolume.h"
#include "MonitorSurface.h"
#include "MonitorDisplacement.h"
#include "MonitorNormDisplacement.h"
#include "MonitorPointToTriangleMeshDistanceFinal.h"
#include "MonitorComputingTime.h"
#include "MonitorDistanceX.h"
#include "MonitorDistanceY.h"
#include "MonitorDistanceZ.h"
/**
 * @ingroup group_cepmodeling_libraries_mml
 *
 * @brief
 * A factory to create monitors
 *
 **/
class MonitorFactory {
public:
    /// destructor
    ~MonitorFactory();

    /// create a monitor according to the xsdcxx object monitor
    static Monitor* createMonitor(mml::Monitor* m, MonitoringManager* monitoringManager);

private:
    /// constructor
    MonitorFactory();

};

#endif // MMLMonitorDisplayFactory_H

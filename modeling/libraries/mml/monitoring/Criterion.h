/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef STOPPINGCRITERION_STOPPINGCRITERIA_CRITERIA_H
#define STOPPINGCRITERION_STOPPINGCRITERIA_CRITERIA_H

#include "MMLAPI.h"

#include <vector>

#include <MonitorIn.hxx>

// Stopping criteria includes
#include "StoppingCriterion.h"
#include "Method.h"
/**
 *
 * @ingroup group_cepmodeling_libraries_mml
 *
 * @brief
 * Class which represents a simple criterion
 * A criterion can be checked using checkCriterion method to know if the criterion is reach
 * A criterion calculate a set of double (values) wich is checked by a method (method) to make the boolean assessment of checkCriterion
 */
class MML_API Criterion: public StoppingCriterion {
public:
    /**
    * constructor
    *@param c the xsdcxx generated criterion
    */
    Criterion(mml::Criteria* c, MonitoringManager* monitoringManager, MultipleCriterion* parent = nullptr);
    /// destructor
    ~Criterion() override;

    /// return true if the criterion is reach
    bool checkCriterion() override;

    /// get number of childre; return -1 for non multiple criteria
    int getNumberOfChildren() override;

    /// get the child repered by index, return null if no child
    StoppingCriterion* getChild(const unsigned int i) override;

    /// get stopping criterion name
    std::string getName() override = 0;

    /// get Method Name
    std::string getMethodString();

    /// get Method scope as string
    std::string scopeToString();

protected:
    /// the computationnal method used to check if criterion is true
    Method* meth;
    /// factor to multiplate for conversion to SI
    double factor;
    /// vector that stores current data of the stopping criteria
    std::vector<double> values;
    /// calculate stopping criteria data and store it into values vector
    virtual void calculate() = 0;
    /// write values in the mmlOut file
    void write();
    /// the xsdcxx object representing criterion, used for serialization
    mml::Criteria* mappedObject;
    /// unit
    std::string unit;

};

#endif // STOPPINGCRITERION_STOPPINGCRITERIA_CRITERIA_H

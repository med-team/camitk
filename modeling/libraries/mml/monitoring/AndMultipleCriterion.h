/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef STOPPINGCRITERION_STOPPINGCRITERIA_MULTIPLECRITERIA_ANDMULTIPLECRITERIA_H
#define STOPPINGCRITERION_STOPPINGCRITERIA_MULTIPLECRITERIA_ANDMULTIPLECRITERIA_H


#include <MonitorIn.hxx>
#include <MonitoringModel.hxx>

// Stopping criteria includes
#include "MultipleCriterion.h"

/**
 *
 * @ingroup group_cepmodeling_libraries_mml
 *
 * @brief
 * A AndMultipleCriterion is a MultipleCriterion where a call of checkCriteria methof is true
 * if every single Criterion of the Criteria vector is reached
 */
class AndMultipleCriterion: public MultipleCriterion {
public:
    /**
     * constructor
     * @param m the xsdcxx generated MultipleCriterion
     * @param monitoringManager the monitoring manager that holds this criteria
     * @param parent the parent multiple criteria
     */
    AndMultipleCriterion(mml::MultipleCriteria* m, MonitoringManager* monitoringManager, MultipleCriterion* parent = nullptr);
    /// destructor
    ~AndMultipleCriterion() override = default;

    /// return true if every single Criterion is reached
    bool checkCriterion() override;

    /// get stopping criterion name
    std::string getName() override;
};

#endif // STOPPINGCRITERION_STOPPINGCRITERIA_MULTIPLECRITERIA_ANDMULTIPLECRITERIA_H

/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef SIMULATOR_SIMULATOR_H
#define SIMULATOR_SIMULATOR_H

#include "MMLAPI.h"
// Monitor includes
#include "MonitoringManager.h"
// Tools includes
#include "Macros.h"

#ifdef MML_GENERATE_GUI
// Simulator includes
#include "SimulatorWidget.h"
#endif
/**
 *
 * @ingroup group_cepmodeling_libraries_mml
 *
 * @brief
 * A simulator engine is used to compute the displacements of all atoms of the model
 */
class MML_API Simulator {
public:
    /** default constructor
     *  build a simulation from context provided by monitoringManager
     *  @param monitoringManager manager provinding context of simulation
     */
    Simulator(MonitoringManager* monitoringManager);
    /** constructor with simulator file
     *  build a simulation from context provided by monitoringManager
     *  and physical model provided by a simulator specific input file
     *  @param monitoringManager manager provinding context of simulation
     *  @param file path to simulator specific file
     */
    Simulator(MonitoringManager* monitoringManager, const char* file);
    /// destructor
    virtual ~Simulator() = default;

    /// initialize the simulator
    virtual void init() = 0;
    /// end simultor
    virtual void end() = 0;
    /// update all current positions and store last positions
    void updatePositions();
    /// get current position for one atom
    virtual void getPosition(int index, double position[3]) = 0;
    /// get current force for one atom
    virtual void getForce(int index, double force[3]) = 0;

    /// Create a pml file from a simulator specific imput file
    virtual void createPml(const char* inputFile, const char* pmlFile) = 0;

#ifdef MML_GENERATE_GUI
    /// get simulator widget
    QWidget* getWidget();
#endif

protected:
    ///monitoring manager
    MonitoringManager* monitoringManager;

#ifdef MML_GENERATE_GUI
    /// widget of simulator
    SimulatorWidget* widget;
#endif

};

#endif // SIMULATOR_SIMULATOR_H

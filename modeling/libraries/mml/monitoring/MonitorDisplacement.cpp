/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
// Monitor includes
#include "MonitorDisplacement.h"

// Tools includes
#include "Tools.h"
#include "AtomIterator.h"

// -------------------- constructor --------------------
MonitorDisplacement::MonitorDisplacement(mml::Monitor* m, MonitoringManager* monitoringManager): Monitor(m, monitoringManager, VECTORSET) {}

// -------------------- calculate --------------------
void MonitorDisplacement::calculate() {
    values.clear();
    double posSimul[3];
    double posInit[3];

    AtomIterator it = AtomIterator(monitoringManager->getPml(), target);
    for (it.begin(); !it.end(); it.next()) {
        it.currentAtom()->getPosition(posSimul);
        monitoringManager->getInitPml()->getAtom(it.currentAtom()->getIndex())->getPosition(posInit);
        posSimul[0] = posSimul[0] + dx;
        posSimul[1] = posSimul[1] + dy;
        posSimul[2] = posSimul[2] + dz;
        for (int i = 0; i < 3; i++) {
            values.push_back(posSimul[i] - posInit[i]);
        }

    }

    write();
}

// -------------------- getType --------------------
std::string MonitorDisplacement::getTypeName() {
    return "Displacement";
}


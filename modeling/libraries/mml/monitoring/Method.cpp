/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#include <iostream>

#include "Method.h"

using namespace std;

// -------------------- constructor --------------------
Method::Method(mml::Method* m) {
    if (m->scope().present()) {
        switch (m->scope().get()) {
            case mml::Scope::Any:
                scope = Any;
                break;
            case mml::Scope::Average:
                scope = Average;
                break;
            case mml::Scope::Sum:
                scope = Sum;
                break;
            default:
                cerr << "Method's scope error" << endl;
        }
    }
    else {
        scope = None;
    }
}

// -------------------- test --------------------
bool Method::test(vector<double>& values) { // attention si values vide....
    switch (scope) {
        case Method::Any: {
            bool result = true;
            for (unsigned int i = 0; i < values.size(); i++) {
                result = result && individualTest(values[i]);
            }
            return result;
            break;
        }
        case Method::None: {
        }
        case Method::Average: {
            double moy = 0;
            for (unsigned int i = 0; i < values.size(); i++) {
                moy = moy + values[i];
            }
            moy = moy / (double)(values.size());
            return individualTest(moy);
            break;
        }
        case Method::Sum: {
            double sum = 0;
            for (unsigned int i = 0; i < values.size(); i++) {
                sum = sum + values[i];
            }
            return individualTest(sum);
            break;
        }
    }
    return true;
}

// -------------------- scopeTosString --------------------
std::string Method::scopeTosString() {
    switch (scope) {
        case Method::Any: {
            return "Any";
            break;
        }
        case Method::None: {
            return "None";
            break;
        }
        case Method::Average: {
            return "Average";
            break;
        }
        case Method::Sum: {
            return "Sum";
            break;
        }
        default:
            return "";
    }
}

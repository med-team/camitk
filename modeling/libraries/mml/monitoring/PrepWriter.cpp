/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "PrepWriter.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

// -------------------- constructor --------------------
PrepWriter::PrepWriter(std::string wd, MonitoringManager* monitoringManager): AnsysBatchWriter(monitoringManager),
    young{15000.0},
    poisson{0.4999},
    elem{"et,1,solid45"},
    fileName{"MMLBatchAnsys"},
    workingDir{wd} {
}

// -------------------- destructor --------------------
PrepWriter::~PrepWriter() {}

// -------------------- write --------------------
std::string PrepWriter::write() {
    std::ostringstream os;

    os << "/PREP7" << std::endl;
    // material caracteristics
    os << elem << std::endl;
    os << "mp,ex,1," << young << std::endl;
    os << "mp,nuxy,1," << poisson << std::endl;

    //create .node and .elem file in the working directory
    std::string str;
    str = workingDir + fileName;
    monitoringManager->getPml()->exportAnsysMesh(str);
    //read .node and .elem file
    os << "nread,'" << fileName << "','node'" << std::endl;
    os << "eread,'" << fileName << "','elem'" << std::endl;

    //write load
    if (monitoringManager->isLmlPresent()) {
        monitoringManager->getLml()->ansysPrint(os);    // TODO: several load steps
    }

    return os.str();

}

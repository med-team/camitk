/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
// Monitor includes
#include "MonitorForce.h"

// Simulator includes
#include "Simulator.h"


// -------------------- constructor --------------------
MonitorForce::MonitorForce(mml::Monitor* m, MonitoringManager* monitoringManager): Monitor(m, monitoringManager, VECTORSET) {}

// -------------------- destructor --------------------
MonitorForce::~MonitorForce() {}

// -------------------- calculate --------------------
void MonitorForce::calculate() {
    values.clear();
    Simulator* simul = monitoringManager->getSimulator();
    double fo[3];
    StructuralComponent* scSimul = dynamic_cast<StructuralComponent*>(monitoringManager->getPml()->getComponentByName(target));
    if (scSimul) {
        for (unsigned int i = 0; i < scSimul->getNumberOfStructures(); i++) {
            Cell* cSimul = dynamic_cast<Cell*>(scSimul->getStructure(i));
            if (cSimul) {
                for (unsigned int j = 0; j < cSimul->getNumberOfStructures(); j++) {
                    simul->getForce(dynamic_cast<Atom*>(cSimul->getStructure(j))->getIndex(), fo);
                    values.push_back(fo[0]);
                    values.push_back(fo[1]);
                    values.push_back(fo[2]);
                }
            }
            else {
                Atom* aSimul = dynamic_cast<Atom*>(scSimul->getStructure(i));
                if (aSimul) {
                    simul->getForce(aSimul->getIndex(), fo);
                    values.push_back(fo[0]);
                    values.push_back(fo[1]);
                    values.push_back(fo[2]);
                }
            }
        }
    }
    write();

}

// -------------------- getType --------------------
std::string MonitorForce::getTypeName() {
    return "Force";
}


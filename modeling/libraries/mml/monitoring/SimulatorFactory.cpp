/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
// Simulator includes
#include "SimulatorFactory.h"
#include "InitSimulators.h"

SimulatorFactory* SimulatorFactory::instance = nullptr;

// -------------------- hasClass --------------------
bool SimulatorFactory::isRegistered(std::string id) {
    return mapObjectCreator.find(id) != mapObjectCreator.end();
}

// -------------------- createObject --------------------
Simulator* SimulatorFactory::createSimulator(std::string id, MonitoringManager* monitoringManager) {
    std::map<std::string, CreateSimulatorFunctionPointer>::iterator iter = mapObjectCreator.find(id);
    if (iter == mapObjectCreator.end()) {
        return nullptr;
    }
    return ((*iter).second)(monitoringManager);
}

// -------------------- createObject --------------------
Simulator* SimulatorFactory::createSimulator(std::string id, MonitoringManager* monitoringManager, const char* file) {
    std::map<std::string, CreateSimulatorFunctionPointer2>::iterator iter = mapObjectCreator2.find(id);
    if (iter == mapObjectCreator2.end()) {
        return nullptr;
    }
    return ((*iter).second)(monitoringManager, file);
}


// -------------------- getInstance --------------------
SimulatorFactory* SimulatorFactory::getInstance() {
    if (!instance) {
        instance = new SimulatorFactory();
    }
    return instance;
}

// -------------------- getInteractiveSimulator --------------------
std::string SimulatorFactory::getInteractiveSimulator(const unsigned int index) {
    if (index < interactiveSimulators.size()) {
        return interactiveSimulators[index];
    }
    else {
        return nullptr;
    }
}

// -------------------- getNonInteractiveSimulator --------------------
std::string SimulatorFactory::getNonInteractiveSimulator(const unsigned int index) {
    if (index < nonInteractiveSimulators.size()) {
        return nonInteractiveSimulators[index];
    }
    else {
        return nullptr;
    }
}

// -------------------- getNumberOfInteractiveSimulators --------------------
int SimulatorFactory::getNumberOfInteractiveSimulators() {
    return interactiveSimulators.size();
}

// -------------------- getNumberOfNonInteractiveSimulators --------------------
int SimulatorFactory::getNumberOfNonInteractiveSimulators() {
    return nonInteractiveSimulators.size();
}

//
bool SimulatorFactory::isInteractive(std::string id) {
    std::map<std::string, bool>::iterator iter = mapInteractive.find(id);
    if (iter == mapInteractive.end()) {
        std::cerr << "Simulator Factory: no simulator " << id << " registered" << std::endl;
        return false;
    }
    return (*iter).second;
}






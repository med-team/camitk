/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef MONITOR_MONITORS_MONITORDISTANCEX_H
#define MONITOR_MONITORS_MONITORDISTANCEX_H

#include  <string>

// Monitor includes

#include <MonitorIn.hxx>
#include "Monitor.h"

/**
* A monitor that calculate the distance between two points
*/
class MonitorDistanceX: public Monitor {

public:
    /**
     * constructor
     * @param m the xsdcxx generated monitor
     */
    MonitorDistanceX(mml::Monitor* m, MonitoringManager* monitoringManager);
    /// destructor
    ~MonitorDistanceX() override = default;

    /// calculate current followed data and store them in values vector
    void calculate() override;

    /// return a string relative to monitor type
    std::string getTypeName() override;
};

#endif // MONITOR_MONITORS_MONITORDISTANCEX_H

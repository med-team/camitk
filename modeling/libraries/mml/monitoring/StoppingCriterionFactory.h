/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef STOPPINGCRITERION_STOPPINGCRITERIONFACTORY_H
#define STOPPINGCRITERION_STOPPINGCRITERIONFACTORY_H

#include <MonitorIn.hxx>

// Stopping criteria includes
#include "StoppingCriterion.h"
#include "MultipleCriterion.h"
#include "Criterion.h"
/**
 *
 * @ingroup group_cepmodeling_libraries_mml
 *
 * @brief
 * A factory to create stopping criterion
 */
class StoppingCriterionFactory {
public:
    /// destructor
    ~StoppingCriterionFactory() = default;

    /// create a stopping criterion according to xsdcxx generated stopping criterion
    static StoppingCriterion* createStoppingCriterion(mml::StoppingCriteria* s, MonitoringManager* monitoringManager, MultipleCriterion* parent = nullptr);

private:
    /// constructor
    StoppingCriterionFactory() = default;

};


#endif // STOPPINGCRITERION_STOPPINGCRITERIONFACTORY_H

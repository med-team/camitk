/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef MONITORINGGUIMANAGER_H
#define MONITORINGGUIMANAGER_H

#include "MMLMonitoringGUIAPI.h"

#include <QObject>

// Monitor includes
#include "MonitoringManagerFactory.h"

class MonitoringDialog;
class MonitoringDriver;
/**
 * @ingroup group_cepmodeling_libraries_mml
 *
 * @brief
 * A GUI to manipulate mml documents
 *
 **/
class /* needed if you monitoringgui is compiled as shared: MML_MONITORING_GUI_API*/ MonitoringGuiManager : public QObject {

    Q_OBJECT

public:

    /// constructor
    MonitoringGuiManager();

    /// destructor
    ~MonitoringGuiManager() override;

    /// get dialog
    MonitoringDialog* getDialog();
    /// get driver
    MonitoringDriver* getDriver();
    /// get Monitoring Manager
    MonitoringManager* getMonitoringManager();

    /// Load a MMLIn file
    bool loadMmlInFile(QString fileName);
    /// Save MMLout file
    bool saveMmlOutFile(QString fileName);
    /// Save MMLIn file
    bool saveMmlInFile(QString fileName);
    /// Save csv file
    bool saveCsvFile(QString fileName);

    ///update dt
    void updateDt(double dt);
    ///update refresh
    void updateRefresh(double refresh);
    /// update pml
    void updatePml(QString fileName);
    /// update lml
    void updateLml(QString fileName);

    /// pause simulation
    void pause();
    /// do simulation lopp
    void simulate();
    /// do one step of simulation
    void simulateOneStep();
    /// rewind simulation
    void rewind();

public  slots:

    /** do one step of simulation
      * @return true only if the step was done
      */
    bool doOneStep();
    /// reload simulation with current parameters
    void reload();

signals:
    /// emitted when one step of simulation is done, there was changes, so maybe there are some display to update
    void changed();
    ///
    void reconnectPml();

private :

    /// the dialog box
    MonitoringDialog* dialog;
    /// the simulation driver
    MonitoringDriver* driver;
    /// Monitoring manager
    MonitoringManager* monitoringManager;
    /// last refresh time
    double lastRefreshTime;


};

#endif // MONITORINGGUIMANAGER_H

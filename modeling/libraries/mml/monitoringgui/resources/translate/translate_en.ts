<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>MonitoringDialog</name>
    <message>
        <location filename="../../MonitoringDialog.ui" line="26"/>
        <location filename="../../MonitoringDialog.ui" line="57"/>
        <source>Simulation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.ui" line="65"/>
        <source>dt:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.ui" line="72"/>
        <source>refresh dt:</source>
        <translation>dt rafraichi:</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.ui" line="82"/>
        <source>time:</source>
        <translation>temps:</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.ui" line="89"/>
        <source>current fps:</source>
        <translation>fps courante:</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.ui" line="106"/>
        <source>average fps:</source>
        <translation>fps moyenne:</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.ui" line="218"/>
        <source>pml:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.ui" line="232"/>
        <location filename="../../MonitoringDialog.ui" line="253"/>
        <location filename="../../MonitoringDialog.ui" line="561"/>
        <source>Browse</source>
        <translation>Naviguer</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.ui" line="239"/>
        <source>lml:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.ui" line="276"/>
        <source>Monitors</source>
        <translation>Moniteurs</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.ui" line="307"/>
        <location filename="../../MonitoringDialog.ui" line="435"/>
        <source>Type</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.ui" line="312"/>
        <source>Target</source>
        <translation>Cible</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.ui" line="317"/>
        <source>Start At</source>
        <translation>Démarrer à</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.ui" line="322"/>
        <source>Stop At</source>
        <translation>Stopper à</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.ui" line="327"/>
        <source>Reference</source>
        <translation>Référence</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.ui" line="340"/>
        <location filename="../../MonitoringDialog.ui" line="458"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.ui" line="363"/>
        <location filename="../../MonitoringDialog.ui" line="481"/>
        <source>Edit</source>
        <translation>Editer</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.ui" line="373"/>
        <location filename="../../MonitoringDialog.ui" line="491"/>
        <source>Remove</source>
        <translation>Retirer</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.ui" line="422"/>
        <source>Stopping Criteria</source>
        <translation>Critère d&apos;arrêt</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.ui" line="440"/>
        <source>Condition</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.ui" line="445"/>
        <source>Scope</source>
        <translation>Etendue</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.ui" line="547"/>
        <source>mml in:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.ui" line="570"/>
        <source>Save mml in</source>
        <translation>Sauver mml in</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.ui" line="579"/>
        <source>Save mml out</source>
        <translation>Sauver mml out</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.ui" line="586"/>
        <source>Save output as .csv</source>
        <translation>Sauver sortie comme .csv</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.ui" line="604"/>
        <location filename="../../MonitoringDialog.ui" line="607"/>
        <source>rewind simulation</source>
        <translation>rembobiner simulation</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.ui" line="617"/>
        <source>Ctrl+R</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.ui" line="655"/>
        <location filename="../../MonitoringDialog.ui" line="658"/>
        <source>play simulation</source>
        <translation>jouer simulation</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.ui" line="668"/>
        <source>P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.ui" line="693"/>
        <location filename="../../MonitoringDialog.ui" line="696"/>
        <source>play one step</source>
        <translation>jouer une étape</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.ui" line="706"/>
        <source>S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.ui" line="728"/>
        <location filename="../../MonitoringDialog.ui" line="731"/>
        <source>pause simulation</source>
        <translation>simulation en pause</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.cpp" line="137"/>
        <source>Open MMLIn</source>
        <translation>Ouvrir MMLIn</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.cpp" line="137"/>
        <location filename="../../MonitoringDialog.cpp" line="146"/>
        <location filename="../../MonitoringDialog.cpp" line="162"/>
        <source>MML files (*.mml)</source>
        <translation>Fichiers MML (*.mml)</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.cpp" line="146"/>
        <source>Save MMLOut</source>
        <translation>Sauver MMLOut</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.cpp" line="154"/>
        <source>Save as Csv</source>
        <translation>Sauver comme Csv</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.cpp" line="154"/>
        <source>CSV files (*.csv)</source>
        <translation>Fichiers CSV (*.csv)</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.cpp" line="162"/>
        <source>Save MMLIn</source>
        <translation>Sauver MMLIn</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.cpp" line="171"/>
        <source>Changing Lml</source>
        <translation>Changement Lml</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.cpp" line="171"/>
        <source>Changing lml will reload simulation.
 Not saved data will be lost.</source>
        <translation>Le changement de lml rechargera la simulation.
Les données non sauvées seront perdues.</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.cpp" line="173"/>
        <source>Open Lml</source>
        <translation>Ouvrir Lml</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.cpp" line="173"/>
        <source>LML files (*.lml)</source>
        <translation>Fichiers LML (*.lml)</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.cpp" line="184"/>
        <source>Changing Pml</source>
        <translation>Changement Pml</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.cpp" line="184"/>
        <source>Changing pml will reload simulation.
 Not saved data will be lost.</source>
        <translation>Le changement de pml rechargera la simulation.
Les données non sauvées seront perdues.</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.cpp" line="186"/>
        <source>Open Pml</source>
        <translation>Ouvrir Pml</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.cpp" line="186"/>
        <source>PML files (*.pml)</source>
        <translation>Fichiers PML (*.pml)</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.cpp" line="207"/>
        <source>Rewind Simulation</source>
        <translation>Rembobiner la simulation</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.cpp" line="207"/>
        <source>Rewind simulation?
 Not saved data will be lost.</source>
        <translation>Rembobiner la simulation ?
Les données non sauvées seront perdues.</translation>
    </message>
    <message>
        <location filename="../../MonitoringDialog.cpp" line="321"/>
        <source>Simulator</source>
        <translation>Simulateur</translation>
    </message>
</context>
<context>
    <name>MonitoringGuiManager</name>
    <message>
        <location filename="../../MonitoringGuiManager.cpp" line="81"/>
        <source>Simulation finished</source>
        <translation>Simulation Terminée</translation>
    </message>
    <message>
        <location filename="../../MonitoringGuiManager.cpp" line="81"/>
        <source>The simulation is finished (Stopping Criterion reached).</source>
        <translation>La simulation est terminée (Critère d&apos;arrêt atteint).</translation>
    </message>
</context>
</TS>

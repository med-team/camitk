<?xml version="1.0" encoding="UTF-8"?>
<!--
/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
-->
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema">

	<!--########################## Monitoring In ##########################-->

	<xsd:complexType name="MonitoringIn">
		<xsd:annotation>
		<xsd:documentation>
		monitoring model is a generic representation for monitors, simulation parameters and stopping criteria
		</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="dt" type="TimeParameter" minOccurs="1" maxOccurs="1"/>
			<xsd:element name="refresh" type="TimeParameter" minOccurs="1" maxOccurs="1"/>
			<xsd:choice>
				<xsd:element name="pmlFile" type="xsd:string" maxOccurs="1"/>
				<xsd:element name="simulatorFile" type="xsd:string" maxOccurs="1"/>
			</xsd:choice>
			<xsd:element name="lmlFile" type="xsd:string" minOccurs="0" maxOccurs="1"/>
			<xsd:element name="simulator" type="xsd:string" minOccurs="1" maxOccurs="1"/>
			<xsd:element name="stoppingCriteria" type="StoppingCriteria" minOccurs="0" maxOccurs="1"/>
			<xsd:element name="monitors" type="Monitors" minOccurs="0" maxOccurs="1"/>
			
		</xsd:sequence>
	</xsd:complexType>

	<xsd:complexType name="TimeParameter">
		<xsd:complexContent>
			<xsd:restriction base="xsd:anyType">
				<xsd:attribute name="value" type="xsd:double" use="required"/>
				<xsd:attribute name="unit" type="TimeUnit" use="required"/>
			</xsd:restriction>
		</xsd:complexContent>
	</xsd:complexType>

	<xsd:simpleType name="TimeUnit">
		<xsd:restriction base="xsd:string">
			<xsd:enumeration value="ms"/>
			<xsd:enumeration value="s"/>
			<xsd:enumeration value="min"/>
		</xsd:restriction>
	</xsd:simpleType>

	<!--####### Stopping criteria #######-->

	<xsd:complexType name="StoppingCriteria">
		<xsd:choice>
			<xsd:element name="criteria" type="Criteria"/>
			<xsd:element name="multipleCriteria" type="MultipleCriteria"/>
		</xsd:choice>
	</xsd:complexType>

		<!--####### Criteria #######-->

    	<xsd:complexType name="Criteria" abstract="true">
		<xsd:sequence>
			<xsd:element name="method" type="Method"/>
			<xsd:element name="data" type="xsd:string" minOccurs="0"/>  <!-- only if used for outpout -->
		</xsd:sequence>
	</xsd:complexType>

	<xsd:complexType name="KineticEnergy">
		<xsd:complexContent>
			<xsd:extension base="Criteria">
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>

	<xsd:complexType name="Speed">
		<xsd:complexContent>
			<xsd:extension base="Criteria">
				<xsd:attribute name="unit" type="SpeedUnit" use="required"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>

	<xsd:simpleType name="SpeedUnit">
		<xsd:restriction base="xsd:string">
			<xsd:enumeration value="m/s"/>
		</xsd:restriction>
	</xsd:simpleType>

	<xsd:complexType name="Position">
		<xsd:complexContent>
			<xsd:extension base="Criteria">
				<xsd:attribute name="unit" type="PositionUnit" use="required"/>
                <xsd:attribute name="target" type="xsd:string" use="optional"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
    	
    <xsd:simpleType name="PositionUnit">
		<xsd:restriction base="xsd:string">
			<xsd:enumeration value="nm"/>
			<xsd:enumeration value="mm"/>
		</xsd:restriction>
	</xsd:simpleType>

	<xsd:complexType name="Time">
		<xsd:complexContent>
			<xsd:extension base="Criteria">
				<xsd:attribute name="unit" type="TimeUnit" use="required"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>

	<xsd:complexType name="Force">
		<xsd:complexContent>
			<xsd:extension base="Criteria">
				<xsd:attribute name="unit" type="ForceUnit" use="required"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>

	<xsd:simpleType name="ForceUnit">
		<xsd:restriction base="xsd:string">
			<xsd:enumeration value="mN"/>
			<xsd:enumeration value="N"/>
		</xsd:restriction>
	</xsd:simpleType>

	<xsd:complexType name="Method" abstract="true">
		<xsd:attribute name="scope" type="Scope"/> <!-- default value?... -->
	</xsd:complexType>

	<xsd:complexType name="Threshold">
		<xsd:complexContent>
			<xsd:extension base="Method">
				<xsd:attribute name="maxValue" type="xsd:double" use="required"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
    
    <xsd:complexType name="TimePeriodThreshold">
        <xsd:complexContent>
            <xsd:extension base="Method">
                <xsd:attribute name="minValue" type="xsd:double" use="required"/>
                <xsd:attribute name="iterations" type="xsd:int" use="required"/>
            </xsd:extension>
        </xsd:complexContent>
    </xsd:complexType>
    
        <xsd:complexType name="MinThreshold">
          <xsd:complexContent>
            <xsd:extension base="Method">
              <xsd:attribute name="minValue" type="xsd:double" use="required"/>
            </xsd:extension>
          </xsd:complexContent>
        </xsd:complexType>

        <xsd:simpleType name="Scope">
		<xsd:restriction base="xsd:string">
			<xsd:enumeration value="Any"/>
			<xsd:enumeration value="Average"/>
			<xsd:enumeration value="Sum"/>
		</xsd:restriction>
	</xsd:simpleType>

		<!--####### Multiple Criteria #######-->

	<xsd:complexType name="MultipleCriteria">
		<xsd:choice minOccurs="2" maxOccurs="unbounded">
			<xsd:element  name="multipleCriteria" type="MultipleCriteria"/>
			<xsd:element  name="criteria" type="Criteria"/>
		</xsd:choice>
		<xsd:attribute name="op" type="LogicalOp" use="required"/>
	</xsd:complexType>

	<xsd:simpleType name="LogicalOp">
		<xsd:restriction base="xsd:string">
			<xsd:enumeration value="And"/>
			<xsd:enumeration value="Or"/>
		</xsd:restriction>
	</xsd:simpleType>

	<!--####### Monitors #######-->

	<xsd:complexType name="Monitors">
		<xsd:sequence>
			<xsd:element name="monitor" type="Monitor" maxOccurs="unbounded"/>
		</xsd:sequence>
	</xsd:complexType>
	
	<xsd:complexType name="Reference">
		<xsd:complexContent>
			<xsd:restriction base="xsd:anyType">
				<xsd:attribute name="document" type="xsd:string" use="required"/>
				<xsd:attribute name="monitorIndex" type="xsd:nonNegativeInteger"/>
				<xsd:attribute name="target" type="xsd:string"/>
			</xsd:restriction>
		</xsd:complexContent>
	</xsd:complexType>

	<xsd:complexType name="Monitor">
		<xsd:sequence>
			<xsd:element name="startAt" type="TimeParameter"/>
			<xsd:element name="stopAt" type="TimeParameter"/>
			<xsd:element name="reference" type="Reference" minOccurs="0" maxOccurs="2" />
			<xsd:element name="indexes" type="xsd:string" minOccurs="0"/>  <!-- only if used for output -->
			<xsd:element name="data" type="xsd:string" minOccurs="0"/>  <!-- only if used for output -->
			
		</xsd:sequence>
		<xsd:attribute name="type" type="MonitorType" use="required"/>
		<xsd:attribute name="index" type="xsd:nonNegativeInteger" use="required"/>
		<xsd:attribute name="target" type="xsd:string" use="required"/>
		<xsd:attribute name="dx" type="xsd:double"/>
		<xsd:attribute name="dy" type="xsd:double"/>
		<xsd:attribute name="dz" type="xsd:double"/>
	</xsd:complexType>

	<xsd:simpleType name="MonitorType">
		<xsd:restriction base="xsd:string">
			<xsd:enumeration value="Position"/>
			<xsd:enumeration value="REN"/>
			<xsd:enumeration value="GeomDeviation"/>	
			<xsd:enumeration value="Force"/>
			<xsd:enumeration value="PointSetDistance"/>
			<xsd:enumeration value="PointFinalSetDistance"/>
			<xsd:enumeration value="Volume"/>
			<xsd:enumeration value="Surface"/>
			<xsd:enumeration value="Displacement"/>
			<xsd:enumeration value="NormDisplacement"/>
			<xsd:enumeration value="DistanceToTriangularMeshFinal"/>
			<xsd:enumeration value="ComputingTime"/>
			<xsd:enumeration value="DistanceX"/>
			<xsd:enumeration value="DistanceY"/>
			<xsd:enumeration value="DistanceZ"/>	
		</xsd:restriction>
	</xsd:simpleType>

	<!--########################## Monitoring Out ##########################-->

	<xsd:complexType name="MonitoringOut">
		<xsd:sequence>
			<xsd:element name="pmlFile" type="xsd:string" minOccurs="1" maxOccurs="1"/>
			<xsd:element name="time" type="TimeStep" maxOccurs="unbounded"/>
		</xsd:sequence>
	</xsd:complexType>

	<xsd:complexType name="TimeStep">
		<xsd:sequence>
			<xsd:element name="stoppingCriteria" type="StoppingCriteria" minOccurs="0" maxOccurs="1"/>
			<xsd:element name="monitor" type="Monitor" minOccurs="0" maxOccurs="unbounded"/>
		</xsd:sequence>
		<xsd:attribute name="value" type="xsd:double" use="required"/>
		<xsd:attribute name="unit" type="TimeUnit" use="required"/>
	</xsd:complexType>

</xsd:schema>

/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef PML_ABORT_EXCEPTION_H
#define PML_ABORT_EXCEPTION_H

#include <string>
#include <exception>
/**
  * @ingroup group_cepmodeling_libraries_pml
  *
  * @brief
  * Exception class to handle abortion in the xmlReading
  * Particularly useful to handle constructor's abortion.
  *
 **/
class PMLAbortException : public std::exception  {
public:
    /// default constructor: give the reason for the exception
    PMLAbortException(std::string s) : reason{s} {
    }

    ~PMLAbortException() = default;

    /// get the detailed reason from the exception
    const char* what() const noexcept {
        return reason.c_str();
    }
private:
    std::string reason;
};

#endif //PML_ABORT_EXCEPTION_H

project(PML)
    
# Is this tool is compiled inside or outside CAMITK (as a standalone)?
if (NOT CEP_NAME STREQUAL "modeling")
  # if this tool is compiled independently from CamiTK
  set(PML_STANDALONE ON CACHE BOOL "ON only if PML compiled outside CAMITK SDK")
  message(FATAL_ERROR "Compilation of PML outside CamiTK SDK not implemented yet. Please contact authors")
endif()

# Version (in CamiTK)
set(VERSION_STRING "pml - ${CAMITK_VERSION_STRING} (c) Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)")
configure_file(${PML_SOURCE_DIR}/PhysicalModelVersion.h.in ${PML_BINARY_DIR}/PhysicalModelVersion.h)

# Sets the variables for required include subdirectories :
set (PhysicalModelProperties_DIR ${PML_SOURCE_DIR}/PhysicalProperties)

set(    HEADERS
        Atom.h
        AtomProperties.h
        Cell.h
        CellProperties.h
        Component.h
        MultiComponent.h
        PhysicalModel.h
        ${PML_BINARY_DIR}/PhysicalModelVersion.h
        PhysicalModelIO.h
        Properties.h
        StructuralComponent.h
        StructuralComponentProperties.h
        Structure.h
        StructureProperties.h
        PMLAbortException.h
        PMLTransform.h
        RenderingMode.h
)

set(SRCS 
    Atom.cpp
    AtomProperties.cpp
    Cell.cpp
    Component.cpp
    CellProperties.cpp
    MultiComponent.cpp
    PhysicalModel.cpp
    StructuralComponent.cpp
    Structure.cpp
    StructureProperties.cpp
    StructuralComponentProperties.cpp
    PMLTransform.cpp
    Properties.cpp
    ${HEADERS}
)


camitk_library(STATIC
               SOURCES ${SRCS}
               INCLUDE_DIRECTORIES ${PhysicalModelProperties_DIR}
               NEEDS_XSD
               NEEDS_CEP_LIBRARIES pmlschema
               HEADERS_TO_INSTALL  ${HEADERS} 
               CEP_NAME CEP_MODELING
               DESCRIPTION "Tools to describe a biomechanical model."
)

#-------------------
# api documentation
#-------------------

option(APIDOC_GENERATE_FOR_PML "Build API documentation for PML" OFF)

if(APIDOC_GENERATE_FOR_PML)
  find_package(Doxygen REQUIRED)
  if (DOXYGEN)
      add_subdirectory(api-doc)
  else ()
      message(STATUS "WARNING: Doxygen not found - PML API documentation and reference manual will not be created")
  endif ()
endif()

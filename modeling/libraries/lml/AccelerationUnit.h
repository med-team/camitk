/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef ACCELERATIONUNIT_H
#define ACCELERATIONUNIT_H

#include "Unit.h"
/**
 * @ingroup group_cepmodeling_libraries_lml
 *
 * @brief
 * Class that defines the different units of a Load 'Acceleration'.
 *
 * @note
 * This class implements the type-safe design pattern.
 *
 **/
class AccelerationUnit : public Unit {

public:
    /// Standard SI unit for acceleration: m/s^2
    static AccelerationUnit& MSm2();
    /// cm/s^2
    static AccelerationUnit& CMSm2();
    /// mm/s^2
    static AccelerationUnit& MMSm2();
private:
    AccelerationUnit(std::string);
};


#endif //ACCELERATIONUNIT_H

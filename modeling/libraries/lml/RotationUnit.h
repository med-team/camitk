/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef ROTATIONUNIT_H
#define ROTATIONUNIT_H


#include "Unit.h"
/**
 * @ingroup group_cepmodeling_libraries_lml
 *
 * @brief
 * RotationUnit model the different values that can be taken by the unit
 * field of a rotation.
 *
 * @note
 * This class implements the type-safe design pattern.
 *
 **/
class RotationUnit : public Unit {

public:
    /// radians
    static RotationUnit& RAD();
    /// degrees
    static RotationUnit& DEG();

private:
    RotationUnit(std::string n) {
        unitString = n;
    }
};

#endif //ROTATIONUNIT_H



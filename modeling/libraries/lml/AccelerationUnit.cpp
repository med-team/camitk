/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "AccelerationUnit.h"

// initializing the static class member using a singleton hidden in the member
// (avoid multiple declaration of static members across multiple loaded dlls/shared object)
AccelerationUnit& AccelerationUnit::MSm2() {
    static AccelerationUnit MSm2("m/s^2");
    return MSm2;
}

AccelerationUnit& AccelerationUnit::CMSm2() {
    static AccelerationUnit CMSm2("cm/s^2");
    return CMSm2;

}

AccelerationUnit& AccelerationUnit::MMSm2() {
    static AccelerationUnit MMSm2("mm/s^2");
    return MMSm2;

}

AccelerationUnit::AccelerationUnit(std::string n) {
    unitString =  n;
}


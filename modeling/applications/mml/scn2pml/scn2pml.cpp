/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include <iostream>

#include <monitoring/SofaSimulator.h>

#ifdef MML_GENERATE_GUI
#include <QApplication>
#endif

using namespace std;

int
main(int argc, char* argv[]) {
    if (argc != 2) {
        cerr << "usage: " << argv[0] << " file.scn" << endl;
        return 1;
    }

    try {
#ifdef MML_GENERATE_GUI
        QApplication app(argc, argv);
#endif
        SofaSimulator* simul = new SofaSimulator(NULL, argv[1]);

        string pmlFileName = argv[1];
        string::size_type pLast = pmlFileName.rfind(".");
        if (pLast != string::npos) {
            pmlFileName.erase(pLast);
            pmlFileName += ".pml";
        }

        simul->createPml(argv[1], pmlFileName.c_str());

    }
    catch (const xml_schema::exception& e) {
        cerr << e << endl;
        return 1;
    }
}


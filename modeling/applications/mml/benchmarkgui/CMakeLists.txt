project(benchmarkgui)

if(MML_GENERATE_GUI)
    set(QT_COMPONENTS Core Gui Xml XmlPatterns Widgets Help UiTools OpenGL OpenGLExtensions)
    find_package(Qt5 COMPONENTS ${QT_COMPONENTS} REQUIRED)
    string(REGEX REPLACE "([^;]+)" "Qt5::\\1" QT_LIBRARIES "${QT_COMPONENTS}")
    if (Qt5_FOUND)
        # cmake_policy(SET CMP0020 NEW) # policy for Qt core linking to qtmain.lib
        message(STATUS "Modeling application benchmarkgui: found Qt ${Qt5_VERSION}.")
        set(QT_INCLUDE_DIRS ${Qt5Widgets_INCLUDE_DIRS} ${Qt5Core_INCLUDE_DIRS} ${Qt5Gui_INCLUDE_DIRS} ${Qt5Xml_INCLUDE_DIRS} ${Qt5XmlPatterns_INCLUDE_DIRS} ${Qt5Declarative_INCLUDE_DIRS} ${Qt5Help_INCLUDE_DIRS} ${Qt5UiTools_INCLUDE_DIRS} ${Qt5OpenGL_INCLUDE_DIRS} ${Qt5OpenGLExtensions_INCLUDE_DIRS})
        include_directories(${QT_INCLUDE_DIRS})
    else()
        message(SEND_ERROR "Modeling application benchmarkgui: Failed to find Qt 5.x. This is needed by ${LIBRARY_TARGET_NAME}.")
    endif()
    add_definitions(-DMML_GENERATE_GUI)
endif()

set(benchmarkgui_LIBRARIES
    ${MML_DEPENDENCY}
    ${MONITORING_GUI_DEPENDENCY}
)

set(HEADERS
)

set(SRCS
  benchmarkgui.cpp
  ${HEADERS}
)

include_directories(${MML_INCLUDE_DIRECTORIES})
link_directories(${MML_LINK_DIRECTORIES})
add_executable(benchmarkgui ${SRCS})
target_link_libraries(benchmarkgui ${QT_LIBRARIES})
target_link_libraries(benchmarkgui ${benchmarkgui_LIBRARIES})
add_dependencies(benchmarkgui ${MONITORING_DEPENDENCY})

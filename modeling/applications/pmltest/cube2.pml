<?xml version="1.0" encoding="UTF-8"?>
<!-- physical model (PML) is a generic representation for 3D physical model.
     PML supports not continuous indexes and multiple non-exclusive labelling.
  --> 
<physicalModel name="cube 2" nrOfAtoms="14"
 nrOfExclusiveComponents="1"
 nrOfInformativeComponents="2"
 nrOfCells="39"
>
<!-- list of atoms: -->
<atoms>
<structuralComponent name="element list">
<nrOfStructures value="14"/>
<atom>
<atomProperties index="0" x="-5" y="5" z="-5" mass="100"/>
</atom>
<atom>
<atomProperties index="1" x="5" y="5" z="-5" mass="100"/>
</atom>
<atom>
<atomProperties index="2" x="5" y="5" z="5" mass="100"/>
</atom>
<atom>
<atomProperties index="3" x="-5" y="5" z="5" mass="100"/>
</atom>
<atom>
<atomProperties index="4" x="-5" y="-5" z="-5" mass="100"/>
</atom>
<atom>
<atomProperties index="5" x="5" y="-5" z="-5" mass="100"/>
</atom>
<atom>
<atomProperties index="6" x="5" y="-5" z="5" mass="100"/>
</atom>
<atom>
<atomProperties index="7" x="-5" y="-5" z="5" mass="100"/>
</atom>
<atom>
<atomProperties index="8" x="0" y="5" z="0" mass="100"/>
</atom>
<atom>
<atomProperties index="9" x="0" y="-5" z="0" mass="100"/>
</atom>
<atom>
<atomProperties index="10" x="0" y="0" z="-5" mass="100"/>
</atom>
<atom>
<atomProperties index="11" x="0" y="0" z="5" mass="100"/>
</atom>
<atom>
<atomProperties index="12" x="5" y="0" z="0" mass="100"/>
</atom>
<atom>
<atomProperties index="13" x="-5" y="0" z="0" mass="100"/>
</atom>
</structuralComponent>
</atoms>
<!-- list of exclusive components : -->
<exclusiveComponents>
<multiComponent name="exclusive component">
<structuralComponent name="Atoms of the elastic region">
<color r="0.8" g="0.8" b="0.2" a="1"/>
<nrOfStructures value="1"/>
<cell>
<cellProperties index="0" type="POLY_VERTEX" phymulType="elastic"/>
<nrOfStructures value="14"/>
<atomRef index="0"/>
<atomRef index="1"/>
<atomRef index="2"/>
<atomRef index="3"/>
<atomRef index="4"/>
<atomRef index="5"/>
<atomRef index="6"/>
<atomRef index="7"/>
<atomRef index="8"/>
<atomRef index="9"/>
<atomRef index="10"/>
<atomRef index="11"/>
<atomRef index="12"/>
<atomRef index="13"/>
</cell>
</structuralComponent>
</multiComponent>
</exclusiveComponents>
<!-- list of informative components : -->
<informativeComponents>
<multiComponent name="Phymul Informative Component">
<structuralComponent name="Element Neighborhoods">
<color r="0.5" g="0.5" b="0.5" a="1"/>
<nrOfStructures value="14"/>
<cell>
<cellProperties index="0" type="POLY_VERTEX" name="E#0"/>
<nrOfStructures value="6"/>
<atomRef index="1"/>
<atomRef index="10"/>
<atomRef index="4"/>
<atomRef index="13"/>
<atomRef index="3"/>
<atomRef index="8"/>
</cell>
<cell>
<cellProperties index="1" type="POLY_VERTEX" name="E#1"/>
<nrOfStructures value="6"/>
<atomRef index="2"/>
<atomRef index="12"/>
<atomRef index="5"/>
<atomRef index="10"/>
<atomRef index="0"/>
<atomRef index="8"/>
</cell>
<cell>
<cellProperties index="2" type="POLY_VERTEX" name="E#2"/>
<nrOfStructures value="6"/>
<atomRef index="3"/>
<atomRef index="11"/>
<atomRef index="6"/>
<atomRef index="12"/>
<atomRef index="1"/>
<atomRef index="8"/>
</cell>
<cell>
<cellProperties index="3" type="POLY_VERTEX" name="E#3"/>
<nrOfStructures value="6"/>
<atomRef index="0"/>
<atomRef index="13"/>
<atomRef index="7"/>
<atomRef index="11"/>
<atomRef index="2"/>
<atomRef index="8"/>
</cell>
<cell>
<cellProperties index="4" type="POLY_VERTEX" name="E#4"/>
<nrOfStructures value="6"/>
<atomRef index="0"/>
<atomRef index="10"/>
<atomRef index="5"/>
<atomRef index="9"/>
<atomRef index="7"/>
<atomRef index="13"/>
</cell>
<cell>
<cellProperties index="5" type="POLY_VERTEX" name="E#5"/>
<nrOfStructures value="6"/>
<atomRef index="1"/>
<atomRef index="12"/>
<atomRef index="6"/>
<atomRef index="9"/>
<atomRef index="4"/>
<atomRef index="10"/>
</cell>
<cell>
<cellProperties index="6" type="POLY_VERTEX" name="E#6"/>
<nrOfStructures value="6"/>
<atomRef index="2"/>
<atomRef index="11"/>
<atomRef index="7"/>
<atomRef index="9"/>
<atomRef index="5"/>
<atomRef index="12"/>
</cell>
<cell>
<cellProperties index="7" type="POLY_VERTEX" name="E#7"/>
<nrOfStructures value="6"/>
<atomRef index="3"/>
<atomRef index="13"/>
<atomRef index="4"/>
<atomRef index="9"/>
<atomRef index="6"/>
<atomRef index="11"/>
</cell>
<cell>
<cellProperties index="8" type="POLY_VERTEX" name="E#8"/>
<nrOfStructures value="4"/>
<atomRef index="0"/>
<atomRef index="3"/>
<atomRef index="2"/>
<atomRef index="1"/>
</cell>
<cell>
<cellProperties index="9" type="POLY_VERTEX" name="E#9"/>
<nrOfStructures value="4"/>
<atomRef index="4"/>
<atomRef index="5"/>
<atomRef index="6"/>
<atomRef index="7"/>
</cell>
<cell>
<cellProperties index="10" type="POLY_VERTEX" name="E#10"/>
<nrOfStructures value="4"/>
<atomRef index="0"/>
<atomRef index="1"/>
<atomRef index="5"/>
<atomRef index="4"/>
</cell>
<cell>
<cellProperties index="11" type="POLY_VERTEX" name="E#11"/>
<nrOfStructures value="4"/>
<atomRef index="2"/>
<atomRef index="3"/>
<atomRef index="7"/>
<atomRef index="6"/>
</cell>
<cell>
<cellProperties index="12" type="POLY_VERTEX" name="E#12"/>
<nrOfStructures value="4"/>
<atomRef index="1"/>
<atomRef index="2"/>
<atomRef index="6"/>
<atomRef index="5"/>
</cell>
<cell>
<cellProperties index="13" type="POLY_VERTEX" name="E#13"/>
<nrOfStructures value="4"/>
<atomRef index="0"/>
<atomRef index="4"/>
<atomRef index="7"/>
<atomRef index="3"/>
</cell>
</structuralComponent>
<structuralComponent name="Facet of region #0 (cubeMaud)" mode="WIREFRAME_AND_SURFACE">
<color r="1" g="0.89" b="0.53" a="1"/>
<nrOfStructures value="24"/>
<cell>
<cellProperties index="14" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="0"/>
<atomRef index="1"/>
<atomRef index="10"/>
</cell>
<cell>
<cellProperties index="15" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="0"/>
<atomRef index="10"/>
<atomRef index="4"/>
</cell>
<cell>
<cellProperties index="16" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="0"/>
<atomRef index="4"/>
<atomRef index="13"/>
</cell>
<cell>
<cellProperties index="17" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="0"/>
<atomRef index="13"/>
<atomRef index="3"/>
</cell>
<cell>
<cellProperties index="18" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="0"/>
<atomRef index="3"/>
<atomRef index="8"/>
</cell>
<cell>
<cellProperties index="19" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="0"/>
<atomRef index="8"/>
<atomRef index="1"/>
</cell>
<cell>
<cellProperties index="20" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="1"/>
<atomRef index="2"/>
<atomRef index="12"/>
</cell>
<cell>
<cellProperties index="21" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="1"/>
<atomRef index="12"/>
<atomRef index="5"/>
</cell>
<cell>
<cellProperties index="22" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="1"/>
<atomRef index="5"/>
<atomRef index="10"/>
</cell>
<cell>
<cellProperties index="23" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="1"/>
<atomRef index="8"/>
<atomRef index="2"/>
</cell>
<cell>
<cellProperties index="24" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="2"/>
<atomRef index="3"/>
<atomRef index="11"/>
</cell>
<cell>
<cellProperties index="25" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="2"/>
<atomRef index="11"/>
<atomRef index="6"/>
</cell>
<cell>
<cellProperties index="26" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="2"/>
<atomRef index="6"/>
<atomRef index="12"/>
</cell>
<cell>
<cellProperties index="27" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="2"/>
<atomRef index="8"/>
<atomRef index="3"/>
</cell>
<cell>
<cellProperties index="28" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="3"/>
<atomRef index="13"/>
<atomRef index="7"/>
</cell>
<cell>
<cellProperties index="29" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="3"/>
<atomRef index="7"/>
<atomRef index="11"/>
</cell>
<cell>
<cellProperties index="30" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="4"/>
<atomRef index="10"/>
<atomRef index="5"/>
</cell>
<cell>
<cellProperties index="31" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="4"/>
<atomRef index="5"/>
<atomRef index="9"/>
</cell>
<cell>
<cellProperties index="32" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="4"/>
<atomRef index="9"/>
<atomRef index="7"/>
</cell>
<cell>
<cellProperties index="33" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="4"/>
<atomRef index="7"/>
<atomRef index="13"/>
</cell>
<cell>
<cellProperties index="34" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="5"/>
<atomRef index="12"/>
<atomRef index="6"/>
</cell>
<cell>
<cellProperties index="35" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="5"/>
<atomRef index="6"/>
<atomRef index="9"/>
</cell>
<cell>
<cellProperties index="36" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="6"/>
<atomRef index="11"/>
<atomRef index="7"/>
</cell>
<cell>
<cellProperties index="37" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="6"/>
<atomRef index="7"/>
<atomRef index="9"/>
</cell>
</structuralComponent>
</multiComponent>
</informativeComponents>
</physicalModel>

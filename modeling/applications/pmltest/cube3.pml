<?xml version="1.0" encoding="UTF-8"?>
<!-- physical model (PML) is a generic representation for 3D physical model.
     PML supports not continuous indexes and multiple non-exclusive labelling.
  --> 
<physicalModel name="simple cubic element" nrOfAtoms="8"
 nrOfExclusiveComponents="1"
 nrOfInformativeComponents="2"
 nrOfCells="7"
>
<!-- list of atoms: -->
<atoms>
<structuralComponent name="Atoms" externW="0" mass="0" viscosityW="0">
<color r="0.972549" g="0.788235" b="1" a="1"/>
<nrOfStructures value="8"/>
<atom>
<atomProperties index="0" x="-0.05" y="0.05" z="-0.05" mass="1"/>
</atom>
<atom>
<atomProperties index="1" x="0.05" y="0.05" z="-0.05" mass="1"/>
</atom>
<atom>
<atomProperties index="2" x="0.05" y="0.05" z="0.05" mass="1"/>
</atom>
<atom>
<atomProperties index="3" x="-0.05" y="0.05" z="0.05" mass="1"/>
</atom>
<atom>
<atomProperties index="4" x="-0.05" y="-0.05" z="-0.05" mass="1"/>
</atom>
<atom>
<atomProperties index="5" x="0.05" y="-0.05" z="-0.05" mass="1"/>
</atom>
<atom>
<atomProperties index="6" x="0.05" y="-0.05" z="0.05" mass="1"/>
</atom>
<atom>
<atomProperties index="7" x="-0.05" y="-0.05" z="0.05" mass="1"/>
</atom>
</structuralComponent>
</atoms>
<!-- list of exclusive components : -->
<exclusiveComponents>
<multiComponent name="Exclusive Component">
<structuralComponent name="Element list" externW="0" mass="0" viscosityW="0">
<color r="0.972549" g="0.788235" b="1" a="1"/>
<nrOfStructures value="1"/>
<cell>
<cellProperties index="0" type="POLY_VERTEX" aggregat="0" externW="0" mass="0" masterRegion="0" viscosityW="0"/>
<color r="0.972549" g="0.788235" b="1" a="1"/>
<nrOfStructures value="8"/>
<atomRef index="0"/>
<atomRef index="1"/>
<atomRef index="2"/>
<atomRef index="3"/>
<atomRef index="4"/>
<atomRef index="5"/>
<atomRef index="6"/>
<atomRef index="7"/>
</cell>
</structuralComponent>
</multiComponent>
</exclusiveComponents>
<!-- list of informative components : -->
<informativeComponents>
<multiComponent name="Informative Component">
<structuralComponent name="Facets" mode="WIREFRAME_AND_SURFACE">
<color r="0.972549" g="0.843137" b="1" a="0.607843"/>
<nrOfStructures value="6"/>
<cell>
<cellProperties index="1" type="QUAD"/>
<color r="0.972549" g="0.843137" b="1" a="0.607843"/>
<nrOfStructures value="4"/>
<atomRef index="0"/>
<atomRef index="1"/>
<atomRef index="2"/>
<atomRef index="3"/>
</cell>
<cell>
<cellProperties index="2" type="QUAD"/>
<color r="0.972549" g="0.843137" b="1" a="0.607843"/>
<nrOfStructures value="4"/>
<atomRef index="1"/>
<atomRef index="5"/>
<atomRef index="6"/>
<atomRef index="2"/>
</cell>
<cell>
<cellProperties index="3" type="QUAD"/>
<color r="0.972549" g="0.843137" b="1" a="0.607843"/>
<nrOfStructures value="4"/>
<atomRef index="2"/>
<atomRef index="6"/>
<atomRef index="7"/>
<atomRef index="3"/>
</cell>
<cell>
<cellProperties index="4" type="QUAD"/>
<color r="0.972549" g="0.843137" b="1" a="0.607843"/>
<nrOfStructures value="4"/>
<atomRef index="3"/>
<atomRef index="7"/>
<atomRef index="4"/>
<atomRef index="0"/>
</cell>
<cell>
<cellProperties index="5" type="QUAD"/>
<color r="0.972549" g="0.843137" b="1" a="0.607843"/>
<nrOfStructures value="4"/>
<atomRef index="0"/>
<atomRef index="4"/>
<atomRef index="5"/>
<atomRef index="1"/>
</cell>
<cell>
<cellProperties index="6" type="QUAD"/>
<color r="0.972549" g="0.843137" b="1" a="0.607843"/>
<nrOfStructures value="4"/>
<atomRef index="4"/>
<atomRef index="7"/>
<atomRef index="6"/>
<atomRef index="5"/>
</cell>
</structuralComponent>
<structuralComponent name="Cube base" mode="SURFACE" externW="0" mass="0" viscosityW="0">
<nrOfStructures value="4"/>
<atomRef index="4"/>
<atomRef index="5"/>
<atomRef index="6"/>
<atomRef index="7"/>
</structuralComponent>
</multiComponent>
</informativeComponents>
</physicalModel>

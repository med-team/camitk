#!/bin/sed -nf
# glue everything on one line.
# usage:
# sed -nf all-on-one-line.sed fileToTransform

H
$ {
  x
  s/\n/ /g
  p
}

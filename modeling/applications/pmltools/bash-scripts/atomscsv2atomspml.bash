#!/bin/bash
# Transform a csv into an atoms SC
# usage:
# atomscsv2atomspml.bash file.csv
in=$1
out=${1/.csv/-atoms.pml}

echo output: $out

echo "<atoms>" > $out
echo "<structuralComponent  mode=\"WIREFRAME_AND_SURFACE\" >" >> $out
sed "s/^\(.*\) \(.*\) \(.*\) \(.*\)/<atom>\n<atomProperties index=\"\1\" x=\"\2\" y=\"\3\" z=\"\4\"  \/>\n<\/atom>/" $1 >> $out
echo "</structuralComponent>" >> $out
echo "</atoms>"  >> $out


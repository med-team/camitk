#!/bin/bash
# Transform atoms of pml into a csv file (index x y z)
# usage:
# atomspml2atomscsv.bash file.pml
in=$1
out=${1/.pml/-atoms.csv}

echo output: $out

grep atomProperties $1 > $out
sed -i 's/^<atomProperties index=\"//' $out
sed -i 's/[a-z]=\"//g' $out
sed -i 's/\"//g' $out
sed -i 's/\/>//g' $out


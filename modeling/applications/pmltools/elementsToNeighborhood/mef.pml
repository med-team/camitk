<!-- physical model is a generic representation for 3D physical model (FEM, spring mass network, phymulob...) --> 
<physicalModel name="TC_ANSYS" nrOfAtoms="729"
 nrOfExclusiveComponents="3"
 nrOfInformativeComponents="0"
 nrOfCells="513"
>
<!-- list of atoms: -->
<atoms>
<structuralComponent  name="element list" >
<nrOfStructures value="729"/>
<atom>
<atomProperties index="1" x="0" y="0" z="0"  />
</atom>
<atom>
<atomProperties index="2" x="8" y="0" z="0"  />
</atom>
<atom>
<atomProperties index="3" x="1" y="0" z="0"  />
</atom>
<atom>
<atomProperties index="4" x="2" y="0" z="0"  />
</atom>
<atom>
<atomProperties index="5" x="3" y="0" z="0"  />
</atom>
<atom>
<atomProperties index="6" x="4" y="0" z="0"  />
</atom>
<atom>
<atomProperties index="7" x="5" y="0" z="0"  />
</atom>
<atom>
<atomProperties index="8" x="6" y="0" z="0"  />
</atom>
<atom>
<atomProperties index="9" x="7" y="0" z="0"  />
</atom>
<atom>
<atomProperties index="10" x="8" y="8" z="0"  />
</atom>
<atom>
<atomProperties index="11" x="8" y="1" z="0"  />
</atom>
<atom>
<atomProperties index="12" x="8" y="2" z="0"  />
</atom>
<atom>
<atomProperties index="13" x="8" y="3" z="0"  />
</atom>
<atom>
<atomProperties index="14" x="8" y="4" z="0"  />
</atom>
<atom>
<atomProperties index="15" x="8" y="5" z="0"  />
</atom>
<atom>
<atomProperties index="16" x="8" y="6" z="0"  />
</atom>
<atom>
<atomProperties index="17" x="8" y="7" z="0"  />
</atom>
<atom>
<atomProperties index="18" x="0" y="8" z="0"  />
</atom>
<atom>
<atomProperties index="19" x="7" y="8" z="0"  />
</atom>
<atom>
<atomProperties index="20" x="6" y="8" z="0"  />
</atom>
<atom>
<atomProperties index="21" x="5" y="8" z="0"  />
</atom>
<atom>
<atomProperties index="22" x="4" y="8" z="0"  />
</atom>
<atom>
<atomProperties index="23" x="3" y="8" z="0"  />
</atom>
<atom>
<atomProperties index="24" x="2" y="8" z="0"  />
</atom>
<atom>
<atomProperties index="25" x="1" y="8" z="0"  />
</atom>
<atom>
<atomProperties index="26" x="0" y="7" z="0"  />
</atom>
<atom>
<atomProperties index="27" x="0" y="6" z="0"  />
</atom>
<atom>
<atomProperties index="28" x="0" y="5" z="0"  />
</atom>
<atom>
<atomProperties index="29" x="0" y="4" z="0"  />
</atom>
<atom>
<atomProperties index="30" x="0" y="3" z="0"  />
</atom>
<atom>
<atomProperties index="31" x="0" y="2" z="0"  />
</atom>
<atom>
<atomProperties index="32" x="0" y="1" z="0"  />
</atom>
<atom>
<atomProperties index="33" x="1" y="1" z="0"  />
</atom>
<atom>
<atomProperties index="34" x="1" y="2" z="0"  />
</atom>
<atom>
<atomProperties index="35" x="1" y="3" z="0"  />
</atom>
<atom>
<atomProperties index="36" x="1" y="4" z="0"  />
</atom>
<atom>
<atomProperties index="37" x="1" y="5" z="0"  />
</atom>
<atom>
<atomProperties index="38" x="1" y="6" z="0"  />
</atom>
<atom>
<atomProperties index="39" x="1" y="7" z="0"  />
</atom>
<atom>
<atomProperties index="40" x="2" y="1" z="0"  />
</atom>
<atom>
<atomProperties index="41" x="2" y="2" z="0"  />
</atom>
<atom>
<atomProperties index="42" x="2" y="3" z="0"  />
</atom>
<atom>
<atomProperties index="43" x="2" y="4" z="0"  />
</atom>
<atom>
<atomProperties index="44" x="2" y="5" z="0"  />
</atom>
<atom>
<atomProperties index="45" x="2" y="6" z="0"  />
</atom>
<atom>
<atomProperties index="46" x="2" y="7" z="0"  />
</atom>
<atom>
<atomProperties index="47" x="3" y="1" z="0"  />
</atom>
<atom>
<atomProperties index="48" x="3" y="2" z="0"  />
</atom>
<atom>
<atomProperties index="49" x="3" y="3" z="0"  />
</atom>
<atom>
<atomProperties index="50" x="3" y="4" z="0"  />
</atom>
<atom>
<atomProperties index="51" x="3" y="5" z="0"  />
</atom>
<atom>
<atomProperties index="52" x="3" y="6" z="0"  />
</atom>
<atom>
<atomProperties index="53" x="3" y="7" z="0"  />
</atom>
<atom>
<atomProperties index="54" x="4" y="1" z="0"  />
</atom>
<atom>
<atomProperties index="55" x="4" y="2" z="0"  />
</atom>
<atom>
<atomProperties index="56" x="4" y="3" z="0"  />
</atom>
<atom>
<atomProperties index="57" x="4" y="4" z="0"  />
</atom>
<atom>
<atomProperties index="58" x="4" y="5" z="0"  />
</atom>
<atom>
<atomProperties index="59" x="4" y="6" z="0"  />
</atom>
<atom>
<atomProperties index="60" x="4" y="7" z="0"  />
</atom>
<atom>
<atomProperties index="61" x="5" y="1" z="0"  />
</atom>
<atom>
<atomProperties index="62" x="5" y="2" z="0"  />
</atom>
<atom>
<atomProperties index="63" x="5" y="3" z="0"  />
</atom>
<atom>
<atomProperties index="64" x="5" y="4" z="0"  />
</atom>
<atom>
<atomProperties index="65" x="5" y="5" z="0"  />
</atom>
<atom>
<atomProperties index="66" x="5" y="6" z="0"  />
</atom>
<atom>
<atomProperties index="67" x="5" y="7" z="0"  />
</atom>
<atom>
<atomProperties index="68" x="6" y="1" z="0"  />
</atom>
<atom>
<atomProperties index="69" x="6" y="2" z="0"  />
</atom>
<atom>
<atomProperties index="70" x="6" y="3" z="0"  />
</atom>
<atom>
<atomProperties index="71" x="6" y="4" z="0"  />
</atom>
<atom>
<atomProperties index="72" x="6" y="5" z="0"  />
</atom>
<atom>
<atomProperties index="73" x="6" y="6" z="0"  />
</atom>
<atom>
<atomProperties index="74" x="6" y="7" z="0"  />
</atom>
<atom>
<atomProperties index="75" x="7" y="1" z="0"  />
</atom>
<atom>
<atomProperties index="76" x="7" y="2" z="0"  />
</atom>
<atom>
<atomProperties index="77" x="7" y="3" z="0"  />
</atom>
<atom>
<atomProperties index="78" x="7" y="4" z="0"  />
</atom>
<atom>
<atomProperties index="79" x="7" y="5" z="0"  />
</atom>
<atom>
<atomProperties index="80" x="7" y="6" z="0"  />
</atom>
<atom>
<atomProperties index="81" x="7" y="7" z="0"  />
</atom>
<atom>
<atomProperties index="82" x="8" y="0" z="7.6"  />
</atom>
<atom>
<atomProperties index="83" x="8.0927" y="-0.092702" z="0.94001"  />
</atom>
<atom>
<atomProperties index="84" x="8.1079" y="-0.10792" z="1.8793"  />
</atom>
<atom>
<atomProperties index="85" x="8.121" y="-0.12097" z="2.8432"  />
</atom>
<atom>
<atomProperties index="86" x="8.1224" y="-0.12236" z="3.8"  />
</atom>
<atom>
<atomProperties index="87" x="8.121" y="-0.12097" z="4.7568"  />
</atom>
<atom>
<atomProperties index="88" x="8.1079" y="-0.10792" z="5.7207"  />
</atom>
<atom>
<atomProperties index="89" x="8.0927" y="-0.092702" z="6.66"  />
</atom>
<atom>
<atomProperties index="90" x="0" y="0" z="7.6"  />
</atom>
<atom>
<atomProperties index="91" x="7" y="0" z="7.6"  />
</atom>
<atom>
<atomProperties index="92" x="6" y="0" z="7.6"  />
</atom>
<atom>
<atomProperties index="93" x="5" y="0" z="7.6"  />
</atom>
<atom>
<atomProperties index="94" x="4" y="0" z="7.6"  />
</atom>
<atom>
<atomProperties index="95" x="3" y="0" z="7.6"  />
</atom>
<atom>
<atomProperties index="96" x="2" y="0" z="7.6"  />
</atom>
<atom>
<atomProperties index="97" x="1" y="0" z="7.6"  />
</atom>
<atom>
<atomProperties index="98" x="-0.092702" y="-0.092702" z="6.66"  />
</atom>
<atom>
<atomProperties index="99" x="-0.10792" y="-0.10792" z="5.7207"  />
</atom>
<atom>
<atomProperties index="100" x="-0.12097" y="-0.12097" z="4.7568"  />
</atom>
<atom>
<atomProperties index="101" x="-0.12236" y="-0.12236" z="3.8"  />
</atom>
<atom>
<atomProperties index="102" x="-0.12097" y="-0.12097" z="2.8432"  />
</atom>
<atom>
<atomProperties index="103" x="-0.10792" y="-0.10792" z="1.8793"  />
</atom>
<atom>
<atomProperties index="104" x="-0.092702" y="-0.092702" z="0.94001"  />
</atom>
<atom>
<atomProperties index="105" x="0.94725" y="-0.093574" z="0.95982"  />
</atom>
<atom>
<atomProperties index="106" x="0.91375" y="-0.1094" z="1.8977"  />
</atom>
<atom>
<atomProperties index="107" x="0.90475" y="-0.12254" z="2.8496"  />
</atom>
<atom>
<atomProperties index="108" x="0.90081" y="-0.12391" z="3.8"  />
</atom>
<atom>
<atomProperties index="109" x="0.90475" y="-0.12254" z="4.7504"  />
</atom>
<atom>
<atomProperties index="110" x="0.91375" y="-0.1094" z="5.7023"  />
</atom>
<atom>
<atomProperties index="111" x="0.94725" y="-0.093574" z="6.6402"  />
</atom>
<atom>
<atomProperties index="112" x="1.9674" y="-0.096069" z="0.95933"  />
</atom>
<atom>
<atomProperties index="113" x="1.9461" y="-0.11338" z="1.9066"  />
</atom>
<atom>
<atomProperties index="114" x="1.9334" y="-0.12705" z="2.8553"  />
</atom>
<atom>
<atomProperties index="115" x="1.9316" y="-0.12853" z="3.8"  />
</atom>
<atom>
<atomProperties index="116" x="1.9334" y="-0.12705" z="4.7447"  />
</atom>
<atom>
<atomProperties index="117" x="1.9461" y="-0.11338" z="5.6934"  />
</atom>
<atom>
<atomProperties index="118" x="1.9674" y="-0.096069" z="6.6407"  />
</atom>
<atom>
<atomProperties index="119" x="2.9846" y="-0.098136" z="0.96233"  />
</atom>
<atom>
<atomProperties index="120" x="2.973" y="-0.11667" z="1.9074"  />
</atom>
<atom>
<atomProperties index="121" x="2.9671" y="-0.13083" z="2.8582"  />
</atom>
<atom>
<atomProperties index="122" x="2.9644" y="-0.13241" z="3.8"  />
</atom>
<atom>
<atomProperties index="123" x="2.9671" y="-0.13083" z="4.7418"  />
</atom>
<atom>
<atomProperties index="124" x="2.973" y="-0.11667" z="5.6926"  />
</atom>
<atom>
<atomProperties index="125" x="2.9846" y="-0.098136" z="6.6377"  />
</atom>
<atom>
<atomProperties index="126" x="4" y="-0.098884" z="0.96084"  />
</atom>
<atom>
<atomProperties index="127" x="4" y="-0.11787" z="1.9092"  />
</atom>
<atom>
<atomProperties index="128" x="4" y="-0.13223" z="2.858"  />
</atom>
<atom>
<atomProperties index="129" x="4" y="-0.13385" z="3.8"  />
</atom>
<atom>
<atomProperties index="130" x="4" y="-0.13223" z="4.742"  />
</atom>
<atom>
<atomProperties index="131" x="4" y="-0.11787" z="5.6908"  />
</atom>
<atom>
<atomProperties index="132" x="4" y="-0.098884" z="6.6392"  />
</atom>
<atom>
<atomProperties index="133" x="5.0154" y="-0.098136" z="0.96233"  />
</atom>
<atom>
<atomProperties index="134" x="5.027" y="-0.11667" z="1.9074"  />
</atom>
<atom>
<atomProperties index="135" x="5.0329" y="-0.13083" z="2.8582"  />
</atom>
<atom>
<atomProperties index="136" x="5.0356" y="-0.13241" z="3.8"  />
</atom>
<atom>
<atomProperties index="137" x="5.0329" y="-0.13083" z="4.7418"  />
</atom>
<atom>
<atomProperties index="138" x="5.027" y="-0.11667" z="5.6926"  />
</atom>
<atom>
<atomProperties index="139" x="5.0154" y="-0.098136" z="6.6377"  />
</atom>
<atom>
<atomProperties index="140" x="6.0326" y="-0.096069" z="0.95933"  />
</atom>
<atom>
<atomProperties index="141" x="6.0539" y="-0.11338" z="1.9066"  />
</atom>
<atom>
<atomProperties index="142" x="6.0666" y="-0.12705" z="2.8553"  />
</atom>
<atom>
<atomProperties index="143" x="6.0684" y="-0.12853" z="3.8"  />
</atom>
<atom>
<atomProperties index="144" x="6.0666" y="-0.12705" z="4.7447"  />
</atom>
<atom>
<atomProperties index="145" x="6.0539" y="-0.11338" z="5.6934"  />
</atom>
<atom>
<atomProperties index="146" x="6.0326" y="-0.096069" z="6.6407"  />
</atom>
<atom>
<atomProperties index="147" x="7.0527" y="-0.093574" z="0.95982"  />
</atom>
<atom>
<atomProperties index="148" x="7.0862" y="-0.1094" z="1.8977"  />
</atom>
<atom>
<atomProperties index="149" x="7.0952" y="-0.12254" z="2.8496"  />
</atom>
<atom>
<atomProperties index="150" x="7.0992" y="-0.12391" z="3.8"  />
</atom>
<atom>
<atomProperties index="151" x="7.0952" y="-0.12254" z="4.7504"  />
</atom>
<atom>
<atomProperties index="152" x="7.0862" y="-0.1094" z="5.7023"  />
</atom>
<atom>
<atomProperties index="153" x="7.0527" y="-0.093574" z="6.6402"  />
</atom>
<atom>
<atomProperties index="154" x="8" y="8" z="7.6"  />
</atom>
<atom>
<atomProperties index="155" x="8.0927" y="8.0927" z="6.66"  />
</atom>
<atom>
<atomProperties index="156" x="8.1079" y="8.1079" z="5.7207"  />
</atom>
<atom>
<atomProperties index="157" x="8.121" y="8.121" z="4.7568"  />
</atom>
<atom>
<atomProperties index="158" x="8.1224" y="8.1224" z="3.8"  />
</atom>
<atom>
<atomProperties index="159" x="8.121" y="8.121" z="2.8432"  />
</atom>
<atom>
<atomProperties index="160" x="8.1079" y="8.1079" z="1.8793"  />
</atom>
<atom>
<atomProperties index="161" x="8.0927" y="8.0927" z="0.94001"  />
</atom>
<atom>
<atomProperties index="162" x="8" y="1" z="7.6"  />
</atom>
<atom>
<atomProperties index="163" x="8" y="2" z="7.6"  />
</atom>
<atom>
<atomProperties index="164" x="8" y="3" z="7.6"  />
</atom>
<atom>
<atomProperties index="165" x="8" y="4" z="7.6"  />
</atom>
<atom>
<atomProperties index="166" x="8" y="5" z="7.6"  />
</atom>
<atom>
<atomProperties index="167" x="8" y="6" z="7.6"  />
</atom>
<atom>
<atomProperties index="168" x="8" y="7" z="7.6"  />
</atom>
<atom>
<atomProperties index="169" x="8.0936" y="0.94725" z="0.95982"  />
</atom>
<atom>
<atomProperties index="170" x="8.1094" y="0.91375" z="1.8977"  />
</atom>
<atom>
<atomProperties index="171" x="8.1225" y="0.90475" z="2.8496"  />
</atom>
<atom>
<atomProperties index="172" x="8.1239" y="0.90081" z="3.8"  />
</atom>
<atom>
<atomProperties index="173" x="8.1225" y="0.90475" z="4.7504"  />
</atom>
<atom>
<atomProperties index="174" x="8.1094" y="0.91375" z="5.7023"  />
</atom>
<atom>
<atomProperties index="175" x="8.0936" y="0.94725" z="6.6402"  />
</atom>
<atom>
<atomProperties index="176" x="8.0961" y="1.9674" z="0.95933"  />
</atom>
<atom>
<atomProperties index="177" x="8.1134" y="1.9461" z="1.9066"  />
</atom>
<atom>
<atomProperties index="178" x="8.1271" y="1.9334" z="2.8553"  />
</atom>
<atom>
<atomProperties index="179" x="8.1285" y="1.9316" z="3.8"  />
</atom>
<atom>
<atomProperties index="180" x="8.1271" y="1.9334" z="4.7447"  />
</atom>
<atom>
<atomProperties index="181" x="8.1134" y="1.9461" z="5.6934"  />
</atom>
<atom>
<atomProperties index="182" x="8.0961" y="1.9674" z="6.6407"  />
</atom>
<atom>
<atomProperties index="183" x="8.0981" y="2.9846" z="0.96233"  />
</atom>
<atom>
<atomProperties index="184" x="8.1167" y="2.973" z="1.9074"  />
</atom>
<atom>
<atomProperties index="185" x="8.1308" y="2.9671" z="2.8582"  />
</atom>
<atom>
<atomProperties index="186" x="8.1324" y="2.9644" z="3.8"  />
</atom>
<atom>
<atomProperties index="187" x="8.1308" y="2.9671" z="4.7418"  />
</atom>
<atom>
<atomProperties index="188" x="8.1167" y="2.973" z="5.6926"  />
</atom>
<atom>
<atomProperties index="189" x="8.0981" y="2.9846" z="6.6377"  />
</atom>
<atom>
<atomProperties index="190" x="8.0989" y="4" z="0.96084"  />
</atom>
<atom>
<atomProperties index="191" x="8.1179" y="4" z="1.9092"  />
</atom>
<atom>
<atomProperties index="192" x="8.1322" y="4" z="2.858"  />
</atom>
<atom>
<atomProperties index="193" x="8.1338" y="4" z="3.8"  />
</atom>
<atom>
<atomProperties index="194" x="8.1322" y="4" z="4.742"  />
</atom>
<atom>
<atomProperties index="195" x="8.1179" y="4" z="5.6908"  />
</atom>
<atom>
<atomProperties index="196" x="8.0989" y="4" z="6.6392"  />
</atom>
<atom>
<atomProperties index="197" x="8.0981" y="5.0154" z="0.96233"  />
</atom>
<atom>
<atomProperties index="198" x="8.1167" y="5.027" z="1.9074"  />
</atom>
<atom>
<atomProperties index="199" x="8.1308" y="5.0329" z="2.8582"  />
</atom>
<atom>
<atomProperties index="200" x="8.1324" y="5.0356" z="3.8"  />
</atom>
<atom>
<atomProperties index="201" x="8.1308" y="5.0329" z="4.7418"  />
</atom>
<atom>
<atomProperties index="202" x="8.1167" y="5.027" z="5.6926"  />
</atom>
<atom>
<atomProperties index="203" x="8.0981" y="5.0154" z="6.6377"  />
</atom>
<atom>
<atomProperties index="204" x="8.0961" y="6.0326" z="0.95933"  />
</atom>
<atom>
<atomProperties index="205" x="8.1134" y="6.0539" z="1.9066"  />
</atom>
<atom>
<atomProperties index="206" x="8.1271" y="6.0666" z="2.8553"  />
</atom>
<atom>
<atomProperties index="207" x="8.1285" y="6.0684" z="3.8"  />
</atom>
<atom>
<atomProperties index="208" x="8.1271" y="6.0666" z="4.7447"  />
</atom>
<atom>
<atomProperties index="209" x="8.1134" y="6.0539" z="5.6934"  />
</atom>
<atom>
<atomProperties index="210" x="8.0961" y="6.0326" z="6.6407"  />
</atom>
<atom>
<atomProperties index="211" x="8.0936" y="7.0527" z="0.95982"  />
</atom>
<atom>
<atomProperties index="212" x="8.1094" y="7.0862" z="1.8977"  />
</atom>
<atom>
<atomProperties index="213" x="8.1225" y="7.0952" z="2.8496"  />
</atom>
<atom>
<atomProperties index="214" x="8.1239" y="7.0992" z="3.8"  />
</atom>
<atom>
<atomProperties index="215" x="8.1225" y="7.0952" z="4.7504"  />
</atom>
<atom>
<atomProperties index="216" x="8.1094" y="7.0862" z="5.7023"  />
</atom>
<atom>
<atomProperties index="217" x="8.0936" y="7.0527" z="6.6402"  />
</atom>
<atom>
<atomProperties index="218" x="0" y="8" z="7.6"  />
</atom>
<atom>
<atomProperties index="219" x="7" y="8" z="7.6"  />
</atom>
<atom>
<atomProperties index="220" x="6" y="8" z="7.6"  />
</atom>
<atom>
<atomProperties index="221" x="5" y="8" z="7.6"  />
</atom>
<atom>
<atomProperties index="222" x="4" y="8" z="7.6"  />
</atom>
<atom>
<atomProperties index="223" x="3" y="8" z="7.6"  />
</atom>
<atom>
<atomProperties index="224" x="2" y="8" z="7.6"  />
</atom>
<atom>
<atomProperties index="225" x="1" y="8" z="7.6"  />
</atom>
<atom>
<atomProperties index="226" x="-0.092702" y="8.0927" z="6.66"  />
</atom>
<atom>
<atomProperties index="227" x="-0.10792" y="8.1079" z="5.7207"  />
</atom>
<atom>
<atomProperties index="228" x="-0.12097" y="8.121" z="4.7568"  />
</atom>
<atom>
<atomProperties index="229" x="-0.12236" y="8.1224" z="3.8"  />
</atom>
<atom>
<atomProperties index="230" x="-0.12097" y="8.121" z="2.8432"  />
</atom>
<atom>
<atomProperties index="231" x="-0.10792" y="8.1079" z="1.8793"  />
</atom>
<atom>
<atomProperties index="232" x="-0.092702" y="8.0927" z="0.94001"  />
</atom>
<atom>
<atomProperties index="233" x="0.94725" y="8.0936" z="0.95982"  />
</atom>
<atom>
<atomProperties index="234" x="0.91375" y="8.1094" z="1.8977"  />
</atom>
<atom>
<atomProperties index="235" x="0.90475" y="8.1225" z="2.8496"  />
</atom>
<atom>
<atomProperties index="236" x="0.90081" y="8.1239" z="3.8"  />
</atom>
<atom>
<atomProperties index="237" x="0.90475" y="8.1225" z="4.7504"  />
</atom>
<atom>
<atomProperties index="238" x="0.91375" y="8.1094" z="5.7023"  />
</atom>
<atom>
<atomProperties index="239" x="0.94725" y="8.0936" z="6.6402"  />
</atom>
<atom>
<atomProperties index="240" x="1.9674" y="8.0961" z="0.95933"  />
</atom>
<atom>
<atomProperties index="241" x="1.9461" y="8.1134" z="1.9066"  />
</atom>
<atom>
<atomProperties index="242" x="1.9334" y="8.1271" z="2.8553"  />
</atom>
<atom>
<atomProperties index="243" x="1.9316" y="8.1285" z="3.8"  />
</atom>
<atom>
<atomProperties index="244" x="1.9334" y="8.1271" z="4.7447"  />
</atom>
<atom>
<atomProperties index="245" x="1.9461" y="8.1134" z="5.6934"  />
</atom>
<atom>
<atomProperties index="246" x="1.9674" y="8.0961" z="6.6407"  />
</atom>
<atom>
<atomProperties index="247" x="2.9846" y="8.0981" z="0.96233"  />
</atom>
<atom>
<atomProperties index="248" x="2.973" y="8.1167" z="1.9074"  />
</atom>
<atom>
<atomProperties index="249" x="2.9671" y="8.1308" z="2.8582"  />
</atom>
<atom>
<atomProperties index="250" x="2.9644" y="8.1324" z="3.8"  />
</atom>
<atom>
<atomProperties index="251" x="2.9671" y="8.1308" z="4.7418"  />
</atom>
<atom>
<atomProperties index="252" x="2.973" y="8.1167" z="5.6926"  />
</atom>
<atom>
<atomProperties index="253" x="2.9846" y="8.0981" z="6.6377"  />
</atom>
<atom>
<atomProperties index="254" x="4" y="8.0989" z="0.96084"  />
</atom>
<atom>
<atomProperties index="255" x="4" y="8.1179" z="1.9092"  />
</atom>
<atom>
<atomProperties index="256" x="4" y="8.1322" z="2.858"  />
</atom>
<atom>
<atomProperties index="257" x="4" y="8.1338" z="3.8"  />
</atom>
<atom>
<atomProperties index="258" x="4" y="8.1322" z="4.742"  />
</atom>
<atom>
<atomProperties index="259" x="4" y="8.1179" z="5.6908"  />
</atom>
<atom>
<atomProperties index="260" x="4" y="8.0989" z="6.6392"  />
</atom>
<atom>
<atomProperties index="261" x="5.0154" y="8.0981" z="0.96233"  />
</atom>
<atom>
<atomProperties index="262" x="5.027" y="8.1167" z="1.9074"  />
</atom>
<atom>
<atomProperties index="263" x="5.0329" y="8.1308" z="2.8582"  />
</atom>
<atom>
<atomProperties index="264" x="5.0356" y="8.1324" z="3.8"  />
</atom>
<atom>
<atomProperties index="265" x="5.0329" y="8.1308" z="4.7418"  />
</atom>
<atom>
<atomProperties index="266" x="5.027" y="8.1167" z="5.6926"  />
</atom>
<atom>
<atomProperties index="267" x="5.0154" y="8.0981" z="6.6377"  />
</atom>
<atom>
<atomProperties index="268" x="6.0326" y="8.0961" z="0.95933"  />
</atom>
<atom>
<atomProperties index="269" x="6.0539" y="8.1134" z="1.9066"  />
</atom>
<atom>
<atomProperties index="270" x="6.0666" y="8.1271" z="2.8553"  />
</atom>
<atom>
<atomProperties index="271" x="6.0684" y="8.1285" z="3.8"  />
</atom>
<atom>
<atomProperties index="272" x="6.0666" y="8.1271" z="4.7447"  />
</atom>
<atom>
<atomProperties index="273" x="6.0539" y="8.1134" z="5.6934"  />
</atom>
<atom>
<atomProperties index="274" x="6.0326" y="8.0961" z="6.6407"  />
</atom>
<atom>
<atomProperties index="275" x="7.0527" y="8.0936" z="0.95982"  />
</atom>
<atom>
<atomProperties index="276" x="7.0862" y="8.1094" z="1.8977"  />
</atom>
<atom>
<atomProperties index="277" x="7.0952" y="8.1225" z="2.8496"  />
</atom>
<atom>
<atomProperties index="278" x="7.0992" y="8.1239" z="3.8"  />
</atom>
<atom>
<atomProperties index="279" x="7.0952" y="8.1225" z="4.7504"  />
</atom>
<atom>
<atomProperties index="280" x="7.0862" y="8.1094" z="5.7023"  />
</atom>
<atom>
<atomProperties index="281" x="7.0527" y="8.0936" z="6.6402"  />
</atom>
<atom>
<atomProperties index="282" x="0" y="7" z="7.6"  />
</atom>
<atom>
<atomProperties index="283" x="0" y="6" z="7.6"  />
</atom>
<atom>
<atomProperties index="284" x="0" y="5" z="7.6"  />
</atom>
<atom>
<atomProperties index="285" x="0" y="4" z="7.6"  />
</atom>
<atom>
<atomProperties index="286" x="0" y="3" z="7.6"  />
</atom>
<atom>
<atomProperties index="287" x="0" y="2" z="7.6"  />
</atom>
<atom>
<atomProperties index="288" x="0" y="1" z="7.6"  />
</atom>
<atom>
<atomProperties index="289" x="-0.093574" y="0.94725" z="0.95982"  />
</atom>
<atom>
<atomProperties index="290" x="-0.1094" y="0.91375" z="1.8977"  />
</atom>
<atom>
<atomProperties index="291" x="-0.12254" y="0.90475" z="2.8496"  />
</atom>
<atom>
<atomProperties index="292" x="-0.12391" y="0.90081" z="3.8"  />
</atom>
<atom>
<atomProperties index="293" x="-0.12254" y="0.90475" z="4.7504"  />
</atom>
<atom>
<atomProperties index="294" x="-0.1094" y="0.91375" z="5.7023"  />
</atom>
<atom>
<atomProperties index="295" x="-0.093574" y="0.94725" z="6.6402"  />
</atom>
<atom>
<atomProperties index="296" x="-0.096069" y="1.9674" z="0.95933"  />
</atom>
<atom>
<atomProperties index="297" x="-0.11338" y="1.9461" z="1.9066"  />
</atom>
<atom>
<atomProperties index="298" x="-0.12705" y="1.9334" z="2.8553"  />
</atom>
<atom>
<atomProperties index="299" x="-0.12853" y="1.9316" z="3.8"  />
</atom>
<atom>
<atomProperties index="300" x="-0.12705" y="1.9334" z="4.7447"  />
</atom>
<atom>
<atomProperties index="301" x="-0.11338" y="1.9461" z="5.6934"  />
</atom>
<atom>
<atomProperties index="302" x="-0.096069" y="1.9674" z="6.6407"  />
</atom>
<atom>
<atomProperties index="303" x="-0.098136" y="2.9846" z="0.96233"  />
</atom>
<atom>
<atomProperties index="304" x="-0.11667" y="2.973" z="1.9074"  />
</atom>
<atom>
<atomProperties index="305" x="-0.13083" y="2.9671" z="2.8582"  />
</atom>
<atom>
<atomProperties index="306" x="-0.13241" y="2.9644" z="3.8"  />
</atom>
<atom>
<atomProperties index="307" x="-0.13083" y="2.9671" z="4.7418"  />
</atom>
<atom>
<atomProperties index="308" x="-0.11667" y="2.973" z="5.6926"  />
</atom>
<atom>
<atomProperties index="309" x="-0.098136" y="2.9846" z="6.6377"  />
</atom>
<atom>
<atomProperties index="310" x="-0.098884" y="4" z="0.96084"  />
</atom>
<atom>
<atomProperties index="311" x="-0.11787" y="4" z="1.9092"  />
</atom>
<atom>
<atomProperties index="312" x="-0.13223" y="4" z="2.858"  />
</atom>
<atom>
<atomProperties index="313" x="-0.13385" y="4" z="3.8"  />
</atom>
<atom>
<atomProperties index="314" x="-0.13223" y="4" z="4.742"  />
</atom>
<atom>
<atomProperties index="315" x="-0.11787" y="4" z="5.6908"  />
</atom>
<atom>
<atomProperties index="316" x="-0.098884" y="4" z="6.6392"  />
</atom>
<atom>
<atomProperties index="317" x="-0.098136" y="5.0154" z="0.96233"  />
</atom>
<atom>
<atomProperties index="318" x="-0.11667" y="5.027" z="1.9074"  />
</atom>
<atom>
<atomProperties index="319" x="-0.13083" y="5.0329" z="2.8582"  />
</atom>
<atom>
<atomProperties index="320" x="-0.13241" y="5.0356" z="3.8"  />
</atom>
<atom>
<atomProperties index="321" x="-0.13083" y="5.0329" z="4.7418"  />
</atom>
<atom>
<atomProperties index="322" x="-0.11667" y="5.027" z="5.6926"  />
</atom>
<atom>
<atomProperties index="323" x="-0.098136" y="5.0154" z="6.6377"  />
</atom>
<atom>
<atomProperties index="324" x="-0.096069" y="6.0326" z="0.95933"  />
</atom>
<atom>
<atomProperties index="325" x="-0.11338" y="6.0539" z="1.9066"  />
</atom>
<atom>
<atomProperties index="326" x="-0.12705" y="6.0666" z="2.8553"  />
</atom>
<atom>
<atomProperties index="327" x="-0.12853" y="6.0684" z="3.8"  />
</atom>
<atom>
<atomProperties index="328" x="-0.12705" y="6.0666" z="4.7447"  />
</atom>
<atom>
<atomProperties index="329" x="-0.11338" y="6.0539" z="5.6934"  />
</atom>
<atom>
<atomProperties index="330" x="-0.096069" y="6.0326" z="6.6407"  />
</atom>
<atom>
<atomProperties index="331" x="-0.093574" y="7.0527" z="0.95982"  />
</atom>
<atom>
<atomProperties index="332" x="-0.1094" y="7.0862" z="1.8977"  />
</atom>
<atom>
<atomProperties index="333" x="-0.12254" y="7.0952" z="2.8496"  />
</atom>
<atom>
<atomProperties index="334" x="-0.12391" y="7.0992" z="3.8"  />
</atom>
<atom>
<atomProperties index="335" x="-0.12254" y="7.0952" z="4.7504"  />
</atom>
<atom>
<atomProperties index="336" x="-0.1094" y="7.0862" z="5.7023"  />
</atom>
<atom>
<atomProperties index="337" x="-0.093574" y="7.0527" z="6.6402"  />
</atom>
<atom>
<atomProperties index="338" x="1" y="1" z="7.6"  />
</atom>
<atom>
<atomProperties index="339" x="1" y="2" z="7.6"  />
</atom>
<atom>
<atomProperties index="340" x="1" y="3" z="7.6"  />
</atom>
<atom>
<atomProperties index="341" x="1" y="4" z="7.6"  />
</atom>
<atom>
<atomProperties index="342" x="1" y="5" z="7.6"  />
</atom>
<atom>
<atomProperties index="343" x="1" y="6" z="7.6"  />
</atom>
<atom>
<atomProperties index="344" x="1" y="7" z="7.6"  />
</atom>
<atom>
<atomProperties index="345" x="2" y="1" z="7.6"  />
</atom>
<atom>
<atomProperties index="346" x="2" y="2" z="7.6"  />
</atom>
<atom>
<atomProperties index="347" x="2" y="3" z="7.6"  />
</atom>
<atom>
<atomProperties index="348" x="2" y="4" z="7.6"  />
</atom>
<atom>
<atomProperties index="349" x="2" y="5" z="7.6"  />
</atom>
<atom>
<atomProperties index="350" x="2" y="6" z="7.6"  />
</atom>
<atom>
<atomProperties index="351" x="2" y="7" z="7.6"  />
</atom>
<atom>
<atomProperties index="352" x="3" y="1" z="7.6"  />
</atom>
<atom>
<atomProperties index="353" x="3" y="2" z="7.6"  />
</atom>
<atom>
<atomProperties index="354" x="3" y="3" z="7.6"  />
</atom>
<atom>
<atomProperties index="355" x="3" y="4" z="7.6"  />
</atom>
<atom>
<atomProperties index="356" x="3" y="5" z="7.6"  />
</atom>
<atom>
<atomProperties index="357" x="3" y="6" z="7.6"  />
</atom>
<atom>
<atomProperties index="358" x="3" y="7" z="7.6"  />
</atom>
<atom>
<atomProperties index="359" x="4" y="1" z="7.6"  />
</atom>
<atom>
<atomProperties index="360" x="4" y="2" z="7.6"  />
</atom>
<atom>
<atomProperties index="361" x="4" y="3" z="7.6"  />
</atom>
<atom>
<atomProperties index="362" x="4" y="4" z="7.6"  />
</atom>
<atom>
<atomProperties index="363" x="4" y="5" z="7.6"  />
</atom>
<atom>
<atomProperties index="364" x="4" y="6" z="7.6"  />
</atom>
<atom>
<atomProperties index="365" x="4" y="7" z="7.6"  />
</atom>
<atom>
<atomProperties index="366" x="5" y="1" z="7.6"  />
</atom>
<atom>
<atomProperties index="367" x="5" y="2" z="7.6"  />
</atom>
<atom>
<atomProperties index="368" x="5" y="3" z="7.6"  />
</atom>
<atom>
<atomProperties index="369" x="5" y="4" z="7.6"  />
</atom>
<atom>
<atomProperties index="370" x="5" y="5" z="7.6"  />
</atom>
<atom>
<atomProperties index="371" x="5" y="6" z="7.6"  />
</atom>
<atom>
<atomProperties index="372" x="5" y="7" z="7.6"  />
</atom>
<atom>
<atomProperties index="373" x="6" y="1" z="7.6"  />
</atom>
<atom>
<atomProperties index="374" x="6" y="2" z="7.6"  />
</atom>
<atom>
<atomProperties index="375" x="6" y="3" z="7.6"  />
</atom>
<atom>
<atomProperties index="376" x="6" y="4" z="7.6"  />
</atom>
<atom>
<atomProperties index="377" x="6" y="5" z="7.6"  />
</atom>
<atom>
<atomProperties index="378" x="6" y="6" z="7.6"  />
</atom>
<atom>
<atomProperties index="379" x="6" y="7" z="7.6"  />
</atom>
<atom>
<atomProperties index="380" x="7" y="1" z="7.6"  />
</atom>
<atom>
<atomProperties index="381" x="7" y="2" z="7.6"  />
</atom>
<atom>
<atomProperties index="382" x="7" y="3" z="7.6"  />
</atom>
<atom>
<atomProperties index="383" x="7" y="4" z="7.6"  />
</atom>
<atom>
<atomProperties index="384" x="7" y="5" z="7.6"  />
</atom>
<atom>
<atomProperties index="385" x="7" y="6" z="7.6"  />
</atom>
<atom>
<atomProperties index="386" x="7" y="7" z="7.6"  />
</atom>
<atom>
<atomProperties index="387" x="0.94674" y="0.94674" z="0.97966"  />
</atom>
<atom>
<atomProperties index="388" x="0.9129" y="0.9129" z="1.9161"  />
</atom>
<atom>
<atomProperties index="389" x="0.9039" y="0.9039" z="2.8561"  />
</atom>
<atom>
<atomProperties index="390" x="0.89999" y="0.89999" z="3.8"  />
</atom>
<atom>
<atomProperties index="391" x="0.9039" y="0.9039" z="4.7439"  />
</atom>
<atom>
<atomProperties index="392" x="0.9129" y="0.9129" z="5.6839"  />
</atom>
<atom>
<atomProperties index="393" x="0.94674" y="0.94674" z="6.6203"  />
</atom>
<atom>
<atomProperties index="394" x="0.94486" y="1.9672" z="0.97919"  />
</atom>
<atom>
<atomProperties index="395" x="0.90987" y="1.9457" z="1.925"  />
</atom>
<atom>
<atomProperties index="396" x="0.90044" y="1.9331" z="2.8618"  />
</atom>
<atom>
<atomProperties index="397" x="0.89645" y="1.9314" z="3.8"  />
</atom>
<atom>
<atomProperties index="398" x="0.90044" y="1.9331" z="4.7382"  />
</atom>
<atom>
<atomProperties index="399" x="0.90987" y="1.9457" z="5.675"  />
</atom>
<atom>
<atomProperties index="400" x="0.94486" y="1.9672" z="6.6208"  />
</atom>
<atom>
<atomProperties index="401" x="0.94324" y="2.9845" z="0.98221"  />
</atom>
<atom>
<atomProperties index="402" x="0.90726" y="2.9729" z="1.9259"  />
</atom>
<atom>
<atomProperties index="403" x="0.89741" y="2.9671" z="2.8647"  />
</atom>
<atom>
<atomProperties index="404" x="0.89333" y="2.9644" z="3.8"  />
</atom>
<atom>
<atomProperties index="405" x="0.89741" y="2.9671" z="4.7353"  />
</atom>
<atom>
<atomProperties index="406" x="0.90726" y="2.9729" z="5.6741"  />
</atom>
<atom>
<atomProperties index="407" x="0.94324" y="2.9845" z="6.6178"  />
</atom>
<atom>
<atomProperties index="408" x="0.94264" y="4" z="0.98074"  />
</atom>
<atom>
<atomProperties index="409" x="0.90629" y="4" z="1.9277"  />
</atom>
<atom>
<atomProperties index="410" x="0.89628" y="4" z="2.8645"  />
</atom>
<atom>
<atomProperties index="411" x="0.89215" y="4" z="3.8"  />
</atom>
<atom>
<atomProperties index="412" x="0.89628" y="4" z="4.7355"  />
</atom>
<atom>
<atomProperties index="413" x="0.90629" y="4" z="5.6723"  />
</atom>
<atom>
<atomProperties index="414" x="0.94264" y="4" z="6.6193"  />
</atom>
<atom>
<atomProperties index="415" x="0.94324" y="5.0155" z="0.98221"  />
</atom>
<atom>
<atomProperties index="416" x="0.90726" y="5.0271" z="1.9259"  />
</atom>
<atom>
<atomProperties index="417" x="0.89741" y="5.0329" z="2.8647"  />
</atom>
<atom>
<atomProperties index="418" x="0.89333" y="5.0356" z="3.8"  />
</atom>
<atom>
<atomProperties index="419" x="0.89741" y="5.0329" z="4.7353"  />
</atom>
<atom>
<atomProperties index="420" x="0.90726" y="5.0271" z="5.6741"  />
</atom>
<atom>
<atomProperties index="421" x="0.94324" y="5.0155" z="6.6178"  />
</atom>
<atom>
<atomProperties index="422" x="0.94486" y="6.0328" z="0.97919"  />
</atom>
<atom>
<atomProperties index="423" x="0.90987" y="6.0543" z="1.925"  />
</atom>
<atom>
<atomProperties index="424" x="0.90044" y="6.0669" z="2.8618"  />
</atom>
<atom>
<atomProperties index="425" x="0.89645" y="6.0686" z="3.8"  />
</atom>
<atom>
<atomProperties index="426" x="0.90044" y="6.0669" z="4.7382"  />
</atom>
<atom>
<atomProperties index="427" x="0.90987" y="6.0543" z="5.675"  />
</atom>
<atom>
<atomProperties index="428" x="0.94486" y="6.0328" z="6.6208"  />
</atom>
<atom>
<atomProperties index="429" x="0.94674" y="7.0533" z="0.97966"  />
</atom>
<atom>
<atomProperties index="430" x="0.9129" y="7.0871" z="1.9161"  />
</atom>
<atom>
<atomProperties index="431" x="0.9039" y="7.0961" z="2.8561"  />
</atom>
<atom>
<atomProperties index="432" x="0.89999" y="7.1" z="3.8"  />
</atom>
<atom>
<atomProperties index="433" x="0.9039" y="7.0961" z="4.7439"  />
</atom>
<atom>
<atomProperties index="434" x="0.9129" y="7.0871" z="5.6839"  />
</atom>
<atom>
<atomProperties index="435" x="0.94674" y="7.0533" z="6.6203"  />
</atom>
<atom>
<atomProperties index="436" x="1.9672" y="0.94486" z="0.97919"  />
</atom>
<atom>
<atomProperties index="437" x="1.9457" y="0.90987" z="1.925"  />
</atom>
<atom>
<atomProperties index="438" x="1.9331" y="0.90044" z="2.8618"  />
</atom>
<atom>
<atomProperties index="439" x="1.9314" y="0.89645" z="3.8"  />
</atom>
<atom>
<atomProperties index="440" x="1.9331" y="0.90044" z="4.7382"  />
</atom>
<atom>
<atomProperties index="441" x="1.9457" y="0.90987" z="5.675"  />
</atom>
<atom>
<atomProperties index="442" x="1.9672" y="0.94486" z="6.6208"  />
</atom>
<atom>
<atomProperties index="443" x="1.966" y="1.966" z="0.97871"  />
</atom>
<atom>
<atomProperties index="444" x="1.9437" y="1.9437" z="1.9339"  />
</atom>
<atom>
<atomProperties index="445" x="1.9309" y="1.9309" z="2.8676"  />
</atom>
<atom>
<atomProperties index="446" x="1.9291" y="1.9291" z="3.8"  />
</atom>
<atom>
<atomProperties index="447" x="1.9309" y="1.9309" z="4.7324"  />
</atom>
<atom>
<atomProperties index="448" x="1.9437" y="1.9437" z="5.6661"  />
</atom>
<atom>
<atomProperties index="449" x="1.966" y="1.966" z="6.6213"  />
</atom>
<atom>
<atomProperties index="450" x="1.9649" y="2.9839" z="0.98173"  />
</atom>
<atom>
<atomProperties index="451" x="1.942" y="2.972" z="1.9348"  />
</atom>
<atom>
<atomProperties index="452" x="1.9288" y="2.966" z="2.8705"  />
</atom>
<atom>
<atomProperties index="453" x="1.927" y="2.9633" z="3.8"  />
</atom>
<atom>
<atomProperties index="454" x="1.9288" y="2.966" z="4.7295"  />
</atom>
<atom>
<atomProperties index="455" x="1.942" y="2.972" z="5.6652"  />
</atom>
<atom>
<atomProperties index="456" x="1.9649" y="2.9839" z="6.6183"  />
</atom>
<atom>
<atomProperties index="457" x="1.9645" y="4" z="0.98024"  />
</atom>
<atom>
<atomProperties index="458" x="1.9413" y="4" z="1.9366"  />
</atom>
<atom>
<atomProperties index="459" x="1.928" y="4" z="2.8703"  />
</atom>
<atom>
<atomProperties index="460" x="1.9262" y="4" z="3.8"  />
</atom>
<atom>
<atomProperties index="461" x="1.928" y="4" z="4.7297"  />
</atom>
<atom>
<atomProperties index="462" x="1.9413" y="4" z="5.6634"  />
</atom>
<atom>
<atomProperties index="463" x="1.9645" y="4" z="6.6198"  />
</atom>
<atom>
<atomProperties index="464" x="1.9649" y="5.0161" z="0.98173"  />
</atom>
<atom>
<atomProperties index="465" x="1.942" y="5.028" z="1.9348"  />
</atom>
<atom>
<atomProperties index="466" x="1.9288" y="5.034" z="2.8705"  />
</atom>
<atom>
<atomProperties index="467" x="1.927" y="5.0367" z="3.8"  />
</atom>
<atom>
<atomProperties index="468" x="1.9288" y="5.034" z="4.7295"  />
</atom>
<atom>
<atomProperties index="469" x="1.942" y="5.028" z="5.6652"  />
</atom>
<atom>
<atomProperties index="470" x="1.9649" y="5.0161" z="6.6183"  />
</atom>
<atom>
<atomProperties index="471" x="1.966" y="6.034" z="0.97871"  />
</atom>
<atom>
<atomProperties index="472" x="1.9437" y="6.0563" z="1.9339"  />
</atom>
<atom>
<atomProperties index="473" x="1.9309" y="6.0691" z="2.8676"  />
</atom>
<atom>
<atomProperties index="474" x="1.9291" y="6.0709" z="3.8"  />
</atom>
<atom>
<atomProperties index="475" x="1.9309" y="6.0691" z="4.7324"  />
</atom>
<atom>
<atomProperties index="476" x="1.9437" y="6.0563" z="5.6661"  />
</atom>
<atom>
<atomProperties index="477" x="1.966" y="6.034" z="6.6213"  />
</atom>
<atom>
<atomProperties index="478" x="1.9672" y="7.0551" z="0.97919"  />
</atom>
<atom>
<atomProperties index="479" x="1.9457" y="7.0901" z="1.925"  />
</atom>
<atom>
<atomProperties index="480" x="1.9331" y="7.0996" z="2.8618"  />
</atom>
<atom>
<atomProperties index="481" x="1.9314" y="7.1036" z="3.8"  />
</atom>
<atom>
<atomProperties index="482" x="1.9331" y="7.0996" z="4.7382"  />
</atom>
<atom>
<atomProperties index="483" x="1.9457" y="7.0901" z="5.675"  />
</atom>
<atom>
<atomProperties index="484" x="1.9672" y="7.0551" z="6.6208"  />
</atom>
<atom>
<atomProperties index="485" x="2.9845" y="0.94324" z="0.98221"  />
</atom>
<atom>
<atomProperties index="486" x="2.9729" y="0.90726" z="1.9259"  />
</atom>
<atom>
<atomProperties index="487" x="2.9671" y="0.89741" z="2.8647"  />
</atom>
<atom>
<atomProperties index="488" x="2.9644" y="0.89333" z="3.8"  />
</atom>
<atom>
<atomProperties index="489" x="2.9671" y="0.89741" z="4.7353"  />
</atom>
<atom>
<atomProperties index="490" x="2.9729" y="0.90726" z="5.6741"  />
</atom>
<atom>
<atomProperties index="491" x="2.9845" y="0.94324" z="6.6178"  />
</atom>
<atom>
<atomProperties index="492" x="2.9839" y="1.9649" z="0.98173"  />
</atom>
<atom>
<atomProperties index="493" x="2.972" y="1.942" z="1.9348"  />
</atom>
<atom>
<atomProperties index="494" x="2.966" y="1.9288" z="2.8705"  />
</atom>
<atom>
<atomProperties index="495" x="2.9633" y="1.927" z="3.8"  />
</atom>
<atom>
<atomProperties index="496" x="2.966" y="1.9288" z="4.7295"  />
</atom>
<atom>
<atomProperties index="497" x="2.972" y="1.942" z="5.6652"  />
</atom>
<atom>
<atomProperties index="498" x="2.9839" y="1.9649" z="6.6183"  />
</atom>
<atom>
<atomProperties index="499" x="2.9834" y="2.9834" z="0.98475"  />
</atom>
<atom>
<atomProperties index="500" x="2.9711" y="2.9711" z="1.9357"  />
</atom>
<atom>
<atomProperties index="501" x="2.965" y="2.965" z="2.8733"  />
</atom>
<atom>
<atomProperties index="502" x="2.9623" y="2.9623" z="3.8"  />
</atom>
<atom>
<atomProperties index="503" x="2.965" y="2.965" z="4.7267"  />
</atom>
<atom>
<atomProperties index="504" x="2.9711" y="2.9711" z="5.6643"  />
</atom>
<atom>
<atomProperties index="505" x="2.9834" y="2.9834" z="6.6153"  />
</atom>
<atom>
<atomProperties index="506" x="2.9832" y="4" z="0.98326"  />
</atom>
<atom>
<atomProperties index="507" x="2.9707" y="4" z="1.9374"  />
</atom>
<atom>
<atomProperties index="508" x="2.9646" y="4" z="2.8731"  />
</atom>
<atom>
<atomProperties index="509" x="2.9619" y="4" z="3.8"  />
</atom>
<atom>
<atomProperties index="510" x="2.9646" y="4" z="4.7269"  />
</atom>
<atom>
<atomProperties index="511" x="2.9707" y="4" z="5.6626"  />
</atom>
<atom>
<atomProperties index="512" x="2.9832" y="4" z="6.6167"  />
</atom>
<atom>
<atomProperties index="513" x="2.9834" y="5.0166" z="0.98475"  />
</atom>
<atom>
<atomProperties index="514" x="2.9711" y="5.0289" z="1.9357"  />
</atom>
<atom>
<atomProperties index="515" x="2.965" y="5.035" z="2.8733"  />
</atom>
<atom>
<atomProperties index="516" x="2.9623" y="5.0377" z="3.8"  />
</atom>
<atom>
<atomProperties index="517" x="2.965" y="5.035" z="4.7267"  />
</atom>
<atom>
<atomProperties index="518" x="2.9711" y="5.0289" z="5.6643"  />
</atom>
<atom>
<atomProperties index="519" x="2.9834" y="5.0166" z="6.6153"  />
</atom>
<atom>
<atomProperties index="520" x="2.9839" y="6.0351" z="0.98173"  />
</atom>
<atom>
<atomProperties index="521" x="2.972" y="6.058" z="1.9348"  />
</atom>
<atom>
<atomProperties index="522" x="2.966" y="6.0712" z="2.8705"  />
</atom>
<atom>
<atomProperties index="523" x="2.9633" y="6.073" z="3.8"  />
</atom>
<atom>
<atomProperties index="524" x="2.966" y="6.0712" z="4.7295"  />
</atom>
<atom>
<atomProperties index="525" x="2.972" y="6.058" z="5.6652"  />
</atom>
<atom>
<atomProperties index="526" x="2.9839" y="6.0351" z="6.6183"  />
</atom>
<atom>
<atomProperties index="527" x="2.9845" y="7.0568" z="0.98221"  />
</atom>
<atom>
<atomProperties index="528" x="2.9729" y="7.0927" z="1.9259"  />
</atom>
<atom>
<atomProperties index="529" x="2.9671" y="7.1026" z="2.8647"  />
</atom>
<atom>
<atomProperties index="530" x="2.9644" y="7.1067" z="3.8"  />
</atom>
<atom>
<atomProperties index="531" x="2.9671" y="7.1026" z="4.7353"  />
</atom>
<atom>
<atomProperties index="532" x="2.9729" y="7.0927" z="5.6741"  />
</atom>
<atom>
<atomProperties index="533" x="2.9845" y="7.0568" z="6.6178"  />
</atom>
<atom>
<atomProperties index="534" x="4" y="0.94264" z="0.98074"  />
</atom>
<atom>
<atomProperties index="535" x="4" y="0.90629" z="1.9277"  />
</atom>
<atom>
<atomProperties index="536" x="4" y="0.89628" z="2.8645"  />
</atom>
<atom>
<atomProperties index="537" x="4" y="0.89215" z="3.8"  />
</atom>
<atom>
<atomProperties index="538" x="4" y="0.89628" z="4.7355"  />
</atom>
<atom>
<atomProperties index="539" x="4" y="0.90629" z="5.6723"  />
</atom>
<atom>
<atomProperties index="540" x="4" y="0.94264" z="6.6193"  />
</atom>
<atom>
<atomProperties index="541" x="4" y="1.9645" z="0.98024"  />
</atom>
<atom>
<atomProperties index="542" x="4" y="1.9413" z="1.9366"  />
</atom>
<atom>
<atomProperties index="543" x="4" y="1.928" z="2.8703"  />
</atom>
<atom>
<atomProperties index="544" x="4" y="1.9262" z="3.8"  />
</atom>
<atom>
<atomProperties index="545" x="4" y="1.928" z="4.7297"  />
</atom>
<atom>
<atomProperties index="546" x="4" y="1.9413" z="5.6634"  />
</atom>
<atom>
<atomProperties index="547" x="4" y="1.9645" z="6.6198"  />
</atom>
<atom>
<atomProperties index="548" x="4" y="2.9832" z="0.98326"  />
</atom>
<atom>
<atomProperties index="549" x="4" y="2.9707" z="1.9374"  />
</atom>
<atom>
<atomProperties index="550" x="4" y="2.9646" z="2.8731"  />
</atom>
<atom>
<atomProperties index="551" x="4" y="2.9619" z="3.8"  />
</atom>
<atom>
<atomProperties index="552" x="4" y="2.9646" z="4.7269"  />
</atom>
<atom>
<atomProperties index="553" x="4" y="2.9707" z="5.6626"  />
</atom>
<atom>
<atomProperties index="554" x="4" y="2.9832" z="6.6167"  />
</atom>
<atom>
<atomProperties index="555" x="4" y="4" z="0.98178"  />
</atom>
<atom>
<atomProperties index="556" x="4" y="4" z="1.9392"  />
</atom>
<atom>
<atomProperties index="557" x="4" y="4" z="2.8729"  />
</atom>
<atom>
<atomProperties index="558" x="4" y="4" z="3.8"  />
</atom>
<atom>
<atomProperties index="559" x="4" y="4" z="4.7271"  />
</atom>
<atom>
<atomProperties index="560" x="4" y="4" z="5.6608"  />
</atom>
<atom>
<atomProperties index="561" x="4" y="4" z="6.6182"  />
</atom>
<atom>
<atomProperties index="562" x="4" y="5.0168" z="0.98326"  />
</atom>
<atom>
<atomProperties index="563" x="4" y="5.0293" z="1.9374"  />
</atom>
<atom>
<atomProperties index="564" x="4" y="5.0354" z="2.8731"  />
</atom>
<atom>
<atomProperties index="565" x="4" y="5.0381" z="3.8"  />
</atom>
<atom>
<atomProperties index="566" x="4" y="5.0354" z="4.7269"  />
</atom>
<atom>
<atomProperties index="567" x="4" y="5.0293" z="5.6626"  />
</atom>
<atom>
<atomProperties index="568" x="4" y="5.0168" z="6.6167"  />
</atom>
<atom>
<atomProperties index="569" x="4" y="6.0355" z="0.98024"  />
</atom>
<atom>
<atomProperties index="570" x="4" y="6.0587" z="1.9366"  />
</atom>
<atom>
<atomProperties index="571" x="4" y="6.072" z="2.8703"  />
</atom>
<atom>
<atomProperties index="572" x="4" y="6.0738" z="3.8"  />
</atom>
<atom>
<atomProperties index="573" x="4" y="6.072" z="4.7297"  />
</atom>
<atom>
<atomProperties index="574" x="4" y="6.0587" z="5.6634"  />
</atom>
<atom>
<atomProperties index="575" x="4" y="6.0355" z="6.6198"  />
</atom>
<atom>
<atomProperties index="576" x="4" y="7.0574" z="0.98074"  />
</atom>
<atom>
<atomProperties index="577" x="4" y="7.0937" z="1.9277"  />
</atom>
<atom>
<atomProperties index="578" x="4" y="7.1037" z="2.8645"  />
</atom>
<atom>
<atomProperties index="579" x="4" y="7.1078" z="3.8"  />
</atom>
<atom>
<atomProperties index="580" x="4" y="7.1037" z="4.7355"  />
</atom>
<atom>
<atomProperties index="581" x="4" y="7.0937" z="5.6723"  />
</atom>
<atom>
<atomProperties index="582" x="4" y="7.0574" z="6.6193"  />
</atom>
<atom>
<atomProperties index="583" x="5.0155" y="0.94324" z="0.98221"  />
</atom>
<atom>
<atomProperties index="584" x="5.0271" y="0.90726" z="1.9259"  />
</atom>
<atom>
<atomProperties index="585" x="5.0329" y="0.89741" z="2.8647"  />
</atom>
<atom>
<atomProperties index="586" x="5.0356" y="0.89333" z="3.8"  />
</atom>
<atom>
<atomProperties index="587" x="5.0329" y="0.89741" z="4.7353"  />
</atom>
<atom>
<atomProperties index="588" x="5.0271" y="0.90726" z="5.6741"  />
</atom>
<atom>
<atomProperties index="589" x="5.0155" y="0.94324" z="6.6178"  />
</atom>
<atom>
<atomProperties index="590" x="5.0161" y="1.9649" z="0.98173"  />
</atom>
<atom>
<atomProperties index="591" x="5.028" y="1.942" z="1.9348"  />
</atom>
<atom>
<atomProperties index="592" x="5.034" y="1.9288" z="2.8705"  />
</atom>
<atom>
<atomProperties index="593" x="5.0367" y="1.927" z="3.8"  />
</atom>
<atom>
<atomProperties index="594" x="5.034" y="1.9288" z="4.7295"  />
</atom>
<atom>
<atomProperties index="595" x="5.028" y="1.942" z="5.6652"  />
</atom>
<atom>
<atomProperties index="596" x="5.0161" y="1.9649" z="6.6183"  />
</atom>
<atom>
<atomProperties index="597" x="5.0166" y="2.9834" z="0.98475"  />
</atom>
<atom>
<atomProperties index="598" x="5.0289" y="2.9711" z="1.9357"  />
</atom>
<atom>
<atomProperties index="599" x="5.035" y="2.965" z="2.8733"  />
</atom>
<atom>
<atomProperties index="600" x="5.0377" y="2.9623" z="3.8"  />
</atom>
<atom>
<atomProperties index="601" x="5.035" y="2.965" z="4.7267"  />
</atom>
<atom>
<atomProperties index="602" x="5.0289" y="2.9711" z="5.6643"  />
</atom>
<atom>
<atomProperties index="603" x="5.0166" y="2.9834" z="6.6153"  />
</atom>
<atom>
<atomProperties index="604" x="5.0168" y="4" z="0.98326"  />
</atom>
<atom>
<atomProperties index="605" x="5.0293" y="4" z="1.9374"  />
</atom>
<atom>
<atomProperties index="606" x="5.0354" y="4" z="2.8731"  />
</atom>
<atom>
<atomProperties index="607" x="5.0381" y="4" z="3.8"  />
</atom>
<atom>
<atomProperties index="608" x="5.0354" y="4" z="4.7269"  />
</atom>
<atom>
<atomProperties index="609" x="5.0293" y="4" z="5.6626"  />
</atom>
<atom>
<atomProperties index="610" x="5.0168" y="4" z="6.6167"  />
</atom>
<atom>
<atomProperties index="611" x="5.0166" y="5.0166" z="0.98475"  />
</atom>
<atom>
<atomProperties index="612" x="5.0289" y="5.0289" z="1.9357"  />
</atom>
<atom>
<atomProperties index="613" x="5.035" y="5.035" z="2.8733"  />
</atom>
<atom>
<atomProperties index="614" x="5.0377" y="5.0377" z="3.8"  />
</atom>
<atom>
<atomProperties index="615" x="5.035" y="5.035" z="4.7267"  />
</atom>
<atom>
<atomProperties index="616" x="5.0289" y="5.0289" z="5.6643"  />
</atom>
<atom>
<atomProperties index="617" x="5.0166" y="5.0166" z="6.6153"  />
</atom>
<atom>
<atomProperties index="618" x="5.0161" y="6.0351" z="0.98173"  />
</atom>
<atom>
<atomProperties index="619" x="5.028" y="6.058" z="1.9348"  />
</atom>
<atom>
<atomProperties index="620" x="5.034" y="6.0712" z="2.8705"  />
</atom>
<atom>
<atomProperties index="621" x="5.0367" y="6.073" z="3.8"  />
</atom>
<atom>
<atomProperties index="622" x="5.034" y="6.0712" z="4.7295"  />
</atom>
<atom>
<atomProperties index="623" x="5.028" y="6.058" z="5.6652"  />
</atom>
<atom>
<atomProperties index="624" x="5.0161" y="6.0351" z="6.6183"  />
</atom>
<atom>
<atomProperties index="625" x="5.0155" y="7.0568" z="0.98221"  />
</atom>
<atom>
<atomProperties index="626" x="5.0271" y="7.0927" z="1.9259"  />
</atom>
<atom>
<atomProperties index="627" x="5.0329" y="7.1026" z="2.8647"  />
</atom>
<atom>
<atomProperties index="628" x="5.0356" y="7.1067" z="3.8"  />
</atom>
<atom>
<atomProperties index="629" x="5.0329" y="7.1026" z="4.7353"  />
</atom>
<atom>
<atomProperties index="630" x="5.0271" y="7.0927" z="5.6741"  />
</atom>
<atom>
<atomProperties index="631" x="5.0155" y="7.0568" z="6.6178"  />
</atom>
<atom>
<atomProperties index="632" x="6.0328" y="0.94486" z="0.97919"  />
</atom>
<atom>
<atomProperties index="633" x="6.0543" y="0.90987" z="1.925"  />
</atom>
<atom>
<atomProperties index="634" x="6.0669" y="0.90044" z="2.8618"  />
</atom>
<atom>
<atomProperties index="635" x="6.0686" y="0.89645" z="3.8"  />
</atom>
<atom>
<atomProperties index="636" x="6.0669" y="0.90044" z="4.7382"  />
</atom>
<atom>
<atomProperties index="637" x="6.0543" y="0.90987" z="5.675"  />
</atom>
<atom>
<atomProperties index="638" x="6.0328" y="0.94486" z="6.6208"  />
</atom>
<atom>
<atomProperties index="639" x="6.034" y="1.966" z="0.97871"  />
</atom>
<atom>
<atomProperties index="640" x="6.0563" y="1.9437" z="1.9339"  />
</atom>
<atom>
<atomProperties index="641" x="6.0691" y="1.9309" z="2.8676"  />
</atom>
<atom>
<atomProperties index="642" x="6.0709" y="1.9291" z="3.8"  />
</atom>
<atom>
<atomProperties index="643" x="6.0691" y="1.9309" z="4.7324"  />
</atom>
<atom>
<atomProperties index="644" x="6.0563" y="1.9437" z="5.6661"  />
</atom>
<atom>
<atomProperties index="645" x="6.034" y="1.966" z="6.6213"  />
</atom>
<atom>
<atomProperties index="646" x="6.0351" y="2.9839" z="0.98173"  />
</atom>
<atom>
<atomProperties index="647" x="6.058" y="2.972" z="1.9348"  />
</atom>
<atom>
<atomProperties index="648" x="6.0712" y="2.966" z="2.8705"  />
</atom>
<atom>
<atomProperties index="649" x="6.073" y="2.9633" z="3.8"  />
</atom>
<atom>
<atomProperties index="650" x="6.0712" y="2.966" z="4.7295"  />
</atom>
<atom>
<atomProperties index="651" x="6.058" y="2.972" z="5.6652"  />
</atom>
<atom>
<atomProperties index="652" x="6.0351" y="2.9839" z="6.6183"  />
</atom>
<atom>
<atomProperties index="653" x="6.0355" y="4" z="0.98024"  />
</atom>
<atom>
<atomProperties index="654" x="6.0587" y="4" z="1.9366"  />
</atom>
<atom>
<atomProperties index="655" x="6.072" y="4" z="2.8703"  />
</atom>
<atom>
<atomProperties index="656" x="6.0738" y="4" z="3.8"  />
</atom>
<atom>
<atomProperties index="657" x="6.072" y="4" z="4.7297"  />
</atom>
<atom>
<atomProperties index="658" x="6.0587" y="4" z="5.6634"  />
</atom>
<atom>
<atomProperties index="659" x="6.0355" y="4" z="6.6198"  />
</atom>
<atom>
<atomProperties index="660" x="6.0351" y="5.0161" z="0.98173"  />
</atom>
<atom>
<atomProperties index="661" x="6.058" y="5.028" z="1.9348"  />
</atom>
<atom>
<atomProperties index="662" x="6.0712" y="5.034" z="2.8705"  />
</atom>
<atom>
<atomProperties index="663" x="6.073" y="5.0367" z="3.8"  />
</atom>
<atom>
<atomProperties index="664" x="6.0712" y="5.034" z="4.7295"  />
</atom>
<atom>
<atomProperties index="665" x="6.058" y="5.028" z="5.6652"  />
</atom>
<atom>
<atomProperties index="666" x="6.0351" y="5.0161" z="6.6183"  />
</atom>
<atom>
<atomProperties index="667" x="6.034" y="6.034" z="0.97871"  />
</atom>
<atom>
<atomProperties index="668" x="6.0563" y="6.0563" z="1.9339"  />
</atom>
<atom>
<atomProperties index="669" x="6.0691" y="6.0691" z="2.8676"  />
</atom>
<atom>
<atomProperties index="670" x="6.0709" y="6.0709" z="3.8"  />
</atom>
<atom>
<atomProperties index="671" x="6.0691" y="6.0691" z="4.7324"  />
</atom>
<atom>
<atomProperties index="672" x="6.0563" y="6.0563" z="5.6661"  />
</atom>
<atom>
<atomProperties index="673" x="6.034" y="6.034" z="6.6213"  />
</atom>
<atom>
<atomProperties index="674" x="6.0328" y="7.0551" z="0.97919"  />
</atom>
<atom>
<atomProperties index="675" x="6.0543" y="7.0901" z="1.925"  />
</atom>
<atom>
<atomProperties index="676" x="6.0669" y="7.0996" z="2.8618"  />
</atom>
<atom>
<atomProperties index="677" x="6.0686" y="7.1036" z="3.8"  />
</atom>
<atom>
<atomProperties index="678" x="6.0669" y="7.0996" z="4.7382"  />
</atom>
<atom>
<atomProperties index="679" x="6.0543" y="7.0901" z="5.675"  />
</atom>
<atom>
<atomProperties index="680" x="6.0328" y="7.0551" z="6.6208"  />
</atom>
<atom>
<atomProperties index="681" x="7.0533" y="0.94674" z="0.97966"  />
</atom>
<atom>
<atomProperties index="682" x="7.0871" y="0.9129" z="1.9161"  />
</atom>
<atom>
<atomProperties index="683" x="7.0961" y="0.9039" z="2.8561"  />
</atom>
<atom>
<atomProperties index="684" x="7.1" y="0.89999" z="3.8"  />
</atom>
<atom>
<atomProperties index="685" x="7.0961" y="0.9039" z="4.7439"  />
</atom>
<atom>
<atomProperties index="686" x="7.0871" y="0.9129" z="5.6839"  />
</atom>
<atom>
<atomProperties index="687" x="7.0533" y="0.94674" z="6.6203"  />
</atom>
<atom>
<atomProperties index="688" x="7.0551" y="1.9672" z="0.97919"  />
</atom>
<atom>
<atomProperties index="689" x="7.0901" y="1.9457" z="1.925"  />
</atom>
<atom>
<atomProperties index="690" x="7.0996" y="1.9331" z="2.8618"  />
</atom>
<atom>
<atomProperties index="691" x="7.1036" y="1.9314" z="3.8"  />
</atom>
<atom>
<atomProperties index="692" x="7.0996" y="1.9331" z="4.7382"  />
</atom>
<atom>
<atomProperties index="693" x="7.0901" y="1.9457" z="5.675"  />
</atom>
<atom>
<atomProperties index="694" x="7.0551" y="1.9672" z="6.6208"  />
</atom>
<atom>
<atomProperties index="695" x="7.0568" y="2.9845" z="0.98221"  />
</atom>
<atom>
<atomProperties index="696" x="7.0927" y="2.9729" z="1.9259"  />
</atom>
<atom>
<atomProperties index="697" x="7.1026" y="2.9671" z="2.8647"  />
</atom>
<atom>
<atomProperties index="698" x="7.1067" y="2.9644" z="3.8"  />
</atom>
<atom>
<atomProperties index="699" x="7.1026" y="2.9671" z="4.7353"  />
</atom>
<atom>
<atomProperties index="700" x="7.0927" y="2.9729" z="5.6741"  />
</atom>
<atom>
<atomProperties index="701" x="7.0568" y="2.9845" z="6.6178"  />
</atom>
<atom>
<atomProperties index="702" x="7.0574" y="4" z="0.98074"  />
</atom>
<atom>
<atomProperties index="703" x="7.0937" y="4" z="1.9277"  />
</atom>
<atom>
<atomProperties index="704" x="7.1037" y="4" z="2.8645"  />
</atom>
<atom>
<atomProperties index="705" x="7.1078" y="4" z="3.8"  />
</atom>
<atom>
<atomProperties index="706" x="7.1037" y="4" z="4.7355"  />
</atom>
<atom>
<atomProperties index="707" x="7.0937" y="4" z="5.6723"  />
</atom>
<atom>
<atomProperties index="708" x="7.0574" y="4" z="6.6193"  />
</atom>
<atom>
<atomProperties index="709" x="7.0568" y="5.0155" z="0.98221"  />
</atom>
<atom>
<atomProperties index="710" x="7.0927" y="5.0271" z="1.9259"  />
</atom>
<atom>
<atomProperties index="711" x="7.1026" y="5.0329" z="2.8647"  />
</atom>
<atom>
<atomProperties index="712" x="7.1067" y="5.0356" z="3.8"  />
</atom>
<atom>
<atomProperties index="713" x="7.1026" y="5.0329" z="4.7353"  />
</atom>
<atom>
<atomProperties index="714" x="7.0927" y="5.0271" z="5.6741"  />
</atom>
<atom>
<atomProperties index="715" x="7.0568" y="5.0155" z="6.6178"  />
</atom>
<atom>
<atomProperties index="716" x="7.0551" y="6.0328" z="0.97919"  />
</atom>
<atom>
<atomProperties index="717" x="7.0901" y="6.0543" z="1.925"  />
</atom>
<atom>
<atomProperties index="718" x="7.0996" y="6.0669" z="2.8618"  />
</atom>
<atom>
<atomProperties index="719" x="7.1036" y="6.0686" z="3.8"  />
</atom>
<atom>
<atomProperties index="720" x="7.0996" y="6.0669" z="4.7382"  />
</atom>
<atom>
<atomProperties index="721" x="7.0901" y="6.0543" z="5.675"  />
</atom>
<atom>
<atomProperties index="722" x="7.0551" y="6.0328" z="6.6208"  />
</atom>
<atom>
<atomProperties index="723" x="7.0533" y="7.0533" z="0.97966"  />
</atom>
<atom>
<atomProperties index="724" x="7.0871" y="7.0871" z="1.9161"  />
</atom>
<atom>
<atomProperties index="725" x="7.0961" y="7.0961" z="2.8561"  />
</atom>
<atom>
<atomProperties index="726" x="7.1" y="7.1" z="3.8"  />
</atom>
<atom>
<atomProperties index="727" x="7.0961" y="7.0961" z="4.7439"  />
</atom>
<atom>
<atomProperties index="728" x="7.0871" y="7.0871" z="5.6839"  />
</atom>
<atom>
<atomProperties index="729" x="7.0533" y="7.0533" z="6.6203"  />
</atom>
</structuralComponent>
</atoms>
<!-- list of exclusive components : -->
<exclusiveComponents>
<multiComponent name="Exclusive Components " >
<structuralComponent  name="Regions"  mass="1"  viscosityW="1"  targetPosition="0 0 0" >
<nrOfStructures value="1"/>
<cell>
<cellProperties index="0" type="POLY_VERTEX"  name="Elements"  materialType="elastic"  shapeW="100" />
<color r="0.8" g="0.8" b="0.2" a="1" />
<nrOfStructures value="729"/>
<atomRef index="1" />
<atomRef index="2" />
<atomRef index="3" />
<atomRef index="4" />
<atomRef index="5" />
<atomRef index="6" />
<atomRef index="7" />
<atomRef index="8" />
<atomRef index="9" />
<atomRef index="10" />
<atomRef index="11" />
<atomRef index="12" />
<atomRef index="13" />
<atomRef index="14" />
<atomRef index="15" />
<atomRef index="16" />
<atomRef index="17" />
<atomRef index="18" />
<atomRef index="19" />
<atomRef index="20" />
<atomRef index="21" />
<atomRef index="22" />
<atomRef index="23" />
<atomRef index="24" />
<atomRef index="25" />
<atomRef index="26" />
<atomRef index="27" />
<atomRef index="28" />
<atomRef index="29" />
<atomRef index="30" />
<atomRef index="31" />
<atomRef index="32" />
<atomRef index="33" />
<atomRef index="34" />
<atomRef index="35" />
<atomRef index="36" />
<atomRef index="37" />
<atomRef index="38" />
<atomRef index="39" />
<atomRef index="40" />
<atomRef index="41" />
<atomRef index="42" />
<atomRef index="43" />
<atomRef index="44" />
<atomRef index="45" />
<atomRef index="46" />
<atomRef index="47" />
<atomRef index="48" />
<atomRef index="49" />
<atomRef index="50" />
<atomRef index="51" />
<atomRef index="52" />
<atomRef index="53" />
<atomRef index="54" />
<atomRef index="55" />
<atomRef index="56" />
<atomRef index="57" />
<atomRef index="58" />
<atomRef index="59" />
<atomRef index="60" />
<atomRef index="61" />
<atomRef index="62" />
<atomRef index="63" />
<atomRef index="64" />
<atomRef index="65" />
<atomRef index="66" />
<atomRef index="67" />
<atomRef index="68" />
<atomRef index="69" />
<atomRef index="70" />
<atomRef index="71" />
<atomRef index="72" />
<atomRef index="73" />
<atomRef index="74" />
<atomRef index="75" />
<atomRef index="76" />
<atomRef index="77" />
<atomRef index="78" />
<atomRef index="79" />
<atomRef index="80" />
<atomRef index="81" />
<atomRef index="82" />
<atomRef index="83" />
<atomRef index="84" />
<atomRef index="85" />
<atomRef index="86" />
<atomRef index="87" />
<atomRef index="88" />
<atomRef index="89" />
<atomRef index="90" />
<atomRef index="91" />
<atomRef index="92" />
<atomRef index="93" />
<atomRef index="94" />
<atomRef index="95" />
<atomRef index="96" />
<atomRef index="97" />
<atomRef index="98" />
<atomRef index="99" />
<atomRef index="100" />
<atomRef index="101" />
<atomRef index="102" />
<atomRef index="103" />
<atomRef index="104" />
<atomRef index="105" />
<atomRef index="106" />
<atomRef index="107" />
<atomRef index="108" />
<atomRef index="109" />
<atomRef index="110" />
<atomRef index="111" />
<atomRef index="112" />
<atomRef index="113" />
<atomRef index="114" />
<atomRef index="115" />
<atomRef index="116" />
<atomRef index="117" />
<atomRef index="118" />
<atomRef index="119" />
<atomRef index="120" />
<atomRef index="121" />
<atomRef index="122" />
<atomRef index="123" />
<atomRef index="124" />
<atomRef index="125" />
<atomRef index="126" />
<atomRef index="127" />
<atomRef index="128" />
<atomRef index="129" />
<atomRef index="130" />
<atomRef index="131" />
<atomRef index="132" />
<atomRef index="133" />
<atomRef index="134" />
<atomRef index="135" />
<atomRef index="136" />
<atomRef index="137" />
<atomRef index="138" />
<atomRef index="139" />
<atomRef index="140" />
<atomRef index="141" />
<atomRef index="142" />
<atomRef index="143" />
<atomRef index="144" />
<atomRef index="145" />
<atomRef index="146" />
<atomRef index="147" />
<atomRef index="148" />
<atomRef index="149" />
<atomRef index="150" />
<atomRef index="151" />
<atomRef index="152" />
<atomRef index="153" />
<atomRef index="154" />
<atomRef index="155" />
<atomRef index="156" />
<atomRef index="157" />
<atomRef index="158" />
<atomRef index="159" />
<atomRef index="160" />
<atomRef index="161" />
<atomRef index="162" />
<atomRef index="163" />
<atomRef index="164" />
<atomRef index="165" />
<atomRef index="166" />
<atomRef index="167" />
<atomRef index="168" />
<atomRef index="169" />
<atomRef index="170" />
<atomRef index="171" />
<atomRef index="172" />
<atomRef index="173" />
<atomRef index="174" />
<atomRef index="175" />
<atomRef index="176" />
<atomRef index="177" />
<atomRef index="178" />
<atomRef index="179" />
<atomRef index="180" />
<atomRef index="181" />
<atomRef index="182" />
<atomRef index="183" />
<atomRef index="184" />
<atomRef index="185" />
<atomRef index="186" />
<atomRef index="187" />
<atomRef index="188" />
<atomRef index="189" />
<atomRef index="190" />
<atomRef index="191" />
<atomRef index="192" />
<atomRef index="193" />
<atomRef index="194" />
<atomRef index="195" />
<atomRef index="196" />
<atomRef index="197" />
<atomRef index="198" />
<atomRef index="199" />
<atomRef index="200" />
<atomRef index="201" />
<atomRef index="202" />
<atomRef index="203" />
<atomRef index="204" />
<atomRef index="205" />
<atomRef index="206" />
<atomRef index="207" />
<atomRef index="208" />
<atomRef index="209" />
<atomRef index="210" />
<atomRef index="211" />
<atomRef index="212" />
<atomRef index="213" />
<atomRef index="214" />
<atomRef index="215" />
<atomRef index="216" />
<atomRef index="217" />
<atomRef index="218" />
<atomRef index="219" />
<atomRef index="220" />
<atomRef index="221" />
<atomRef index="222" />
<atomRef index="223" />
<atomRef index="224" />
<atomRef index="225" />
<atomRef index="226" />
<atomRef index="227" />
<atomRef index="228" />
<atomRef index="229" />
<atomRef index="230" />
<atomRef index="231" />
<atomRef index="232" />
<atomRef index="233" />
<atomRef index="234" />
<atomRef index="235" />
<atomRef index="236" />
<atomRef index="237" />
<atomRef index="238" />
<atomRef index="239" />
<atomRef index="240" />
<atomRef index="241" />
<atomRef index="242" />
<atomRef index="243" />
<atomRef index="244" />
<atomRef index="245" />
<atomRef index="246" />
<atomRef index="247" />
<atomRef index="248" />
<atomRef index="249" />
<atomRef index="250" />
<atomRef index="251" />
<atomRef index="252" />
<atomRef index="253" />
<atomRef index="254" />
<atomRef index="255" />
<atomRef index="256" />
<atomRef index="257" />
<atomRef index="258" />
<atomRef index="259" />
<atomRef index="260" />
<atomRef index="261" />
<atomRef index="262" />
<atomRef index="263" />
<atomRef index="264" />
<atomRef index="265" />
<atomRef index="266" />
<atomRef index="267" />
<atomRef index="268" />
<atomRef index="269" />
<atomRef index="270" />
<atomRef index="271" />
<atomRef index="272" />
<atomRef index="273" />
<atomRef index="274" />
<atomRef index="275" />
<atomRef index="276" />
<atomRef index="277" />
<atomRef index="278" />
<atomRef index="279" />
<atomRef index="280" />
<atomRef index="281" />
<atomRef index="282" />
<atomRef index="283" />
<atomRef index="284" />
<atomRef index="285" />
<atomRef index="286" />
<atomRef index="287" />
<atomRef index="288" />
<atomRef index="289" />
<atomRef index="290" />
<atomRef index="291" />
<atomRef index="292" />
<atomRef index="293" />
<atomRef index="294" />
<atomRef index="295" />
<atomRef index="296" />
<atomRef index="297" />
<atomRef index="298" />
<atomRef index="299" />
<atomRef index="300" />
<atomRef index="301" />
<atomRef index="302" />
<atomRef index="303" />
<atomRef index="304" />
<atomRef index="305" />
<atomRef index="306" />
<atomRef index="307" />
<atomRef index="308" />
<atomRef index="309" />
<atomRef index="310" />
<atomRef index="311" />
<atomRef index="312" />
<atomRef index="313" />
<atomRef index="314" />
<atomRef index="315" />
<atomRef index="316" />
<atomRef index="317" />
<atomRef index="318" />
<atomRef index="319" />
<atomRef index="320" />
<atomRef index="321" />
<atomRef index="322" />
<atomRef index="323" />
<atomRef index="324" />
<atomRef index="325" />
<atomRef index="326" />
<atomRef index="327" />
<atomRef index="328" />
<atomRef index="329" />
<atomRef index="330" />
<atomRef index="331" />
<atomRef index="332" />
<atomRef index="333" />
<atomRef index="334" />
<atomRef index="335" />
<atomRef index="336" />
<atomRef index="337" />
<atomRef index="338" />
<atomRef index="339" />
<atomRef index="340" />
<atomRef index="341" />
<atomRef index="342" />
<atomRef index="343" />
<atomRef index="344" />
<atomRef index="345" />
<atomRef index="346" />
<atomRef index="347" />
<atomRef index="348" />
<atomRef index="349" />
<atomRef index="350" />
<atomRef index="351" />
<atomRef index="352" />
<atomRef index="353" />
<atomRef index="354" />
<atomRef index="355" />
<atomRef index="356" />
<atomRef index="357" />
<atomRef index="358" />
<atomRef index="359" />
<atomRef index="360" />
<atomRef index="361" />
<atomRef index="362" />
<atomRef index="363" />
<atomRef index="364" />
<atomRef index="365" />
<atomRef index="366" />
<atomRef index="367" />
<atomRef index="368" />
<atomRef index="369" />
<atomRef index="370" />
<atomRef index="371" />
<atomRef index="372" />
<atomRef index="373" />
<atomRef index="374" />
<atomRef index="375" />
<atomRef index="376" />
<atomRef index="377" />
<atomRef index="378" />
<atomRef index="379" />
<atomRef index="380" />
<atomRef index="381" />
<atomRef index="382" />
<atomRef index="383" />
<atomRef index="384" />
<atomRef index="385" />
<atomRef index="386" />
<atomRef index="387" />
<atomRef index="388" />
<atomRef index="389" />
<atomRef index="390" />
<atomRef index="391" />
<atomRef index="392" />
<atomRef index="393" />
<atomRef index="394" />
<atomRef index="395" />
<atomRef index="396" />
<atomRef index="397" />
<atomRef index="398" />
<atomRef index="399" />
<atomRef index="400" />
<atomRef index="401" />
<atomRef index="402" />
<atomRef index="403" />
<atomRef index="404" />
<atomRef index="405" />
<atomRef index="406" />
<atomRef index="407" />
<atomRef index="408" />
<atomRef index="409" />
<atomRef index="410" />
<atomRef index="411" />
<atomRef index="412" />
<atomRef index="413" />
<atomRef index="414" />
<atomRef index="415" />
<atomRef index="416" />
<atomRef index="417" />
<atomRef index="418" />
<atomRef index="419" />
<atomRef index="420" />
<atomRef index="421" />
<atomRef index="422" />
<atomRef index="423" />
<atomRef index="424" />
<atomRef index="425" />
<atomRef index="426" />
<atomRef index="427" />
<atomRef index="428" />
<atomRef index="429" />
<atomRef index="430" />
<atomRef index="431" />
<atomRef index="432" />
<atomRef index="433" />
<atomRef index="434" />
<atomRef index="435" />
<atomRef index="436" />
<atomRef index="437" />
<atomRef index="438" />
<atomRef index="439" />
<atomRef index="440" />
<atomRef index="441" />
<atomRef index="442" />
<atomRef index="443" />
<atomRef index="444" />
<atomRef index="445" />
<atomRef index="446" />
<atomRef index="447" />
<atomRef index="448" />
<atomRef index="449" />
<atomRef index="450" />
<atomRef index="451" />
<atomRef index="452" />
<atomRef index="453" />
<atomRef index="454" />
<atomRef index="455" />
<atomRef index="456" />
<atomRef index="457" />
<atomRef index="458" />
<atomRef index="459" />
<atomRef index="460" />
<atomRef index="461" />
<atomRef index="462" />
<atomRef index="463" />
<atomRef index="464" />
<atomRef index="465" />
<atomRef index="466" />
<atomRef index="467" />
<atomRef index="468" />
<atomRef index="469" />
<atomRef index="470" />
<atomRef index="471" />
<atomRef index="472" />
<atomRef index="473" />
<atomRef index="474" />
<atomRef index="475" />
<atomRef index="476" />
<atomRef index="477" />
<atomRef index="478" />
<atomRef index="479" />
<atomRef index="480" />
<atomRef index="481" />
<atomRef index="482" />
<atomRef index="483" />
<atomRef index="484" />
<atomRef index="485" />
<atomRef index="486" />
<atomRef index="487" />
<atomRef index="488" />
<atomRef index="489" />
<atomRef index="490" />
<atomRef index="491" />
<atomRef index="492" />
<atomRef index="493" />
<atomRef index="494" />
<atomRef index="495" />
<atomRef index="496" />
<atomRef index="497" />
<atomRef index="498" />
<atomRef index="499" />
<atomRef index="500" />
<atomRef index="501" />
<atomRef index="502" />
<atomRef index="503" />
<atomRef index="504" />
<atomRef index="505" />
<atomRef index="506" />
<atomRef index="507" />
<atomRef index="508" />
<atomRef index="509" />
<atomRef index="510" />
<atomRef index="511" />
<atomRef index="512" />
<atomRef index="513" />
<atomRef index="514" />
<atomRef index="515" />
<atomRef index="516" />
<atomRef index="517" />
<atomRef index="518" />
<atomRef index="519" />
<atomRef index="520" />
<atomRef index="521" />
<atomRef index="522" />
<atomRef index="523" />
<atomRef index="524" />
<atomRef index="525" />
<atomRef index="526" />
<atomRef index="527" />
<atomRef index="528" />
<atomRef index="529" />
<atomRef index="530" />
<atomRef index="531" />
<atomRef index="532" />
<atomRef index="533" />
<atomRef index="534" />
<atomRef index="535" />
<atomRef index="536" />
<atomRef index="537" />
<atomRef index="538" />
<atomRef index="539" />
<atomRef index="540" />
<atomRef index="541" />
<atomRef index="542" />
<atomRef index="543" />
<atomRef index="544" />
<atomRef index="545" />
<atomRef index="546" />
<atomRef index="547" />
<atomRef index="548" />
<atomRef index="549" />
<atomRef index="550" />
<atomRef index="551" />
<atomRef index="552" />
<atomRef index="553" />
<atomRef index="554" />
<atomRef index="555" />
<atomRef index="556" />
<atomRef index="557" />
<atomRef index="558" />
<atomRef index="559" />
<atomRef index="560" />
<atomRef index="561" />
<atomRef index="562" />
<atomRef index="563" />
<atomRef index="564" />
<atomRef index="565" />
<atomRef index="566" />
<atomRef index="567" />
<atomRef index="568" />
<atomRef index="569" />
<atomRef index="570" />
<atomRef index="571" />
<atomRef index="572" />
<atomRef index="573" />
<atomRef index="574" />
<atomRef index="575" />
<atomRef index="576" />
<atomRef index="577" />
<atomRef index="578" />
<atomRef index="579" />
<atomRef index="580" />
<atomRef index="581" />
<atomRef index="582" />
<atomRef index="583" />
<atomRef index="584" />
<atomRef index="585" />
<atomRef index="586" />
<atomRef index="587" />
<atomRef index="588" />
<atomRef index="589" />
<atomRef index="590" />
<atomRef index="591" />
<atomRef index="592" />
<atomRef index="593" />
<atomRef index="594" />
<atomRef index="595" />
<atomRef index="596" />
<atomRef index="597" />
<atomRef index="598" />
<atomRef index="599" />
<atomRef index="600" />
<atomRef index="601" />
<atomRef index="602" />
<atomRef index="603" />
<atomRef index="604" />
<atomRef index="605" />
<atomRef index="606" />
<atomRef index="607" />
<atomRef index="608" />
<atomRef index="609" />
<atomRef index="610" />
<atomRef index="611" />
<atomRef index="612" />
<atomRef index="613" />
<atomRef index="614" />
<atomRef index="615" />
<atomRef index="616" />
<atomRef index="617" />
<atomRef index="618" />
<atomRef index="619" />
<atomRef index="620" />
<atomRef index="621" />
<atomRef index="622" />
<atomRef index="623" />
<atomRef index="624" />
<atomRef index="625" />
<atomRef index="626" />
<atomRef index="627" />
<atomRef index="628" />
<atomRef index="629" />
<atomRef index="630" />
<atomRef index="631" />
<atomRef index="632" />
<atomRef index="633" />
<atomRef index="634" />
<atomRef index="635" />
<atomRef index="636" />
<atomRef index="637" />
<atomRef index="638" />
<atomRef index="639" />
<atomRef index="640" />
<atomRef index="641" />
<atomRef index="642" />
<atomRef index="643" />
<atomRef index="644" />
<atomRef index="645" />
<atomRef index="646" />
<atomRef index="647" />
<atomRef index="648" />
<atomRef index="649" />
<atomRef index="650" />
<atomRef index="651" />
<atomRef index="652" />
<atomRef index="653" />
<atomRef index="654" />
<atomRef index="655" />
<atomRef index="656" />
<atomRef index="657" />
<atomRef index="658" />
<atomRef index="659" />
<atomRef index="660" />
<atomRef index="661" />
<atomRef index="662" />
<atomRef index="663" />
<atomRef index="664" />
<atomRef index="665" />
<atomRef index="666" />
<atomRef index="667" />
<atomRef index="668" />
<atomRef index="669" />
<atomRef index="670" />
<atomRef index="671" />
<atomRef index="672" />
<atomRef index="673" />
<atomRef index="674" />
<atomRef index="675" />
<atomRef index="676" />
<atomRef index="677" />
<atomRef index="678" />
<atomRef index="679" />
<atomRef index="680" />
<atomRef index="681" />
<atomRef index="682" />
<atomRef index="683" />
<atomRef index="684" />
<atomRef index="685" />
<atomRef index="686" />
<atomRef index="687" />
<atomRef index="688" />
<atomRef index="689" />
<atomRef index="690" />
<atomRef index="691" />
<atomRef index="692" />
<atomRef index="693" />
<atomRef index="694" />
<atomRef index="695" />
<atomRef index="696" />
<atomRef index="697" />
<atomRef index="698" />
<atomRef index="699" />
<atomRef index="700" />
<atomRef index="701" />
<atomRef index="702" />
<atomRef index="703" />
<atomRef index="704" />
<atomRef index="705" />
<atomRef index="706" />
<atomRef index="707" />
<atomRef index="708" />
<atomRef index="709" />
<atomRef index="710" />
<atomRef index="711" />
<atomRef index="712" />
<atomRef index="713" />
<atomRef index="714" />
<atomRef index="715" />
<atomRef index="716" />
<atomRef index="717" />
<atomRef index="718" />
<atomRef index="719" />
<atomRef index="720" />
<atomRef index="721" />
<atomRef index="722" />
<atomRef index="723" />
<atomRef index="724" />
<atomRef index="725" />
<atomRef index="726" />
<atomRef index="727" />
<atomRef index="728" />
<atomRef index="729" />
</cell>
</structuralComponent>
<structuralComponent  name="Elements"  mass="0"  viscosityW="0"  externW="0" >
<color r="0.8" g="0.8" b="0.2" a="1" />
<nrOfStructures value="512"/>
<cell>
<cellProperties index="1" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="1" />
<atomRef index="3" />
<atomRef index="33" />
<atomRef index="32" />
<atomRef index="104" />
<atomRef index="105" />
<atomRef index="387" />
<atomRef index="289" />
</cell>
<cell>
<cellProperties index="2" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="3" />
<atomRef index="4" />
<atomRef index="40" />
<atomRef index="33" />
<atomRef index="105" />
<atomRef index="112" />
<atomRef index="436" />
<atomRef index="387" />
</cell>
<cell>
<cellProperties index="3" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="4" />
<atomRef index="5" />
<atomRef index="47" />
<atomRef index="40" />
<atomRef index="112" />
<atomRef index="119" />
<atomRef index="485" />
<atomRef index="436" />
</cell>
<cell>
<cellProperties index="4" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="5" />
<atomRef index="6" />
<atomRef index="54" />
<atomRef index="47" />
<atomRef index="119" />
<atomRef index="126" />
<atomRef index="534" />
<atomRef index="485" />
</cell>
<cell>
<cellProperties index="5" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="6" />
<atomRef index="7" />
<atomRef index="61" />
<atomRef index="54" />
<atomRef index="126" />
<atomRef index="133" />
<atomRef index="583" />
<atomRef index="534" />
</cell>
<cell>
<cellProperties index="6" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="7" />
<atomRef index="8" />
<atomRef index="68" />
<atomRef index="61" />
<atomRef index="133" />
<atomRef index="140" />
<atomRef index="632" />
<atomRef index="583" />
</cell>
<cell>
<cellProperties index="7" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="8" />
<atomRef index="9" />
<atomRef index="75" />
<atomRef index="68" />
<atomRef index="140" />
<atomRef index="147" />
<atomRef index="681" />
<atomRef index="632" />
</cell>
<cell>
<cellProperties index="8" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="9" />
<atomRef index="2" />
<atomRef index="11" />
<atomRef index="75" />
<atomRef index="147" />
<atomRef index="83" />
<atomRef index="169" />
<atomRef index="681" />
</cell>
<cell>
<cellProperties index="9" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="32" />
<atomRef index="33" />
<atomRef index="34" />
<atomRef index="31" />
<atomRef index="289" />
<atomRef index="387" />
<atomRef index="394" />
<atomRef index="296" />
</cell>
<cell>
<cellProperties index="10" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="33" />
<atomRef index="40" />
<atomRef index="41" />
<atomRef index="34" />
<atomRef index="387" />
<atomRef index="436" />
<atomRef index="443" />
<atomRef index="394" />
</cell>
<cell>
<cellProperties index="11" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="40" />
<atomRef index="47" />
<atomRef index="48" />
<atomRef index="41" />
<atomRef index="436" />
<atomRef index="485" />
<atomRef index="492" />
<atomRef index="443" />
</cell>
<cell>
<cellProperties index="12" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="47" />
<atomRef index="54" />
<atomRef index="55" />
<atomRef index="48" />
<atomRef index="485" />
<atomRef index="534" />
<atomRef index="541" />
<atomRef index="492" />
</cell>
<cell>
<cellProperties index="13" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="54" />
<atomRef index="61" />
<atomRef index="62" />
<atomRef index="55" />
<atomRef index="534" />
<atomRef index="583" />
<atomRef index="590" />
<atomRef index="541" />
</cell>
<cell>
<cellProperties index="14" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="61" />
<atomRef index="68" />
<atomRef index="69" />
<atomRef index="62" />
<atomRef index="583" />
<atomRef index="632" />
<atomRef index="639" />
<atomRef index="590" />
</cell>
<cell>
<cellProperties index="15" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="68" />
<atomRef index="75" />
<atomRef index="76" />
<atomRef index="69" />
<atomRef index="632" />
<atomRef index="681" />
<atomRef index="688" />
<atomRef index="639" />
</cell>
<cell>
<cellProperties index="16" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="75" />
<atomRef index="11" />
<atomRef index="12" />
<atomRef index="76" />
<atomRef index="681" />
<atomRef index="169" />
<atomRef index="176" />
<atomRef index="688" />
</cell>
<cell>
<cellProperties index="17" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="31" />
<atomRef index="34" />
<atomRef index="35" />
<atomRef index="30" />
<atomRef index="296" />
<atomRef index="394" />
<atomRef index="401" />
<atomRef index="303" />
</cell>
<cell>
<cellProperties index="18" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="34" />
<atomRef index="41" />
<atomRef index="42" />
<atomRef index="35" />
<atomRef index="394" />
<atomRef index="443" />
<atomRef index="450" />
<atomRef index="401" />
</cell>
<cell>
<cellProperties index="19" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="41" />
<atomRef index="48" />
<atomRef index="49" />
<atomRef index="42" />
<atomRef index="443" />
<atomRef index="492" />
<atomRef index="499" />
<atomRef index="450" />
</cell>
<cell>
<cellProperties index="20" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="48" />
<atomRef index="55" />
<atomRef index="56" />
<atomRef index="49" />
<atomRef index="492" />
<atomRef index="541" />
<atomRef index="548" />
<atomRef index="499" />
</cell>
<cell>
<cellProperties index="21" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="55" />
<atomRef index="62" />
<atomRef index="63" />
<atomRef index="56" />
<atomRef index="541" />
<atomRef index="590" />
<atomRef index="597" />
<atomRef index="548" />
</cell>
<cell>
<cellProperties index="22" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="62" />
<atomRef index="69" />
<atomRef index="70" />
<atomRef index="63" />
<atomRef index="590" />
<atomRef index="639" />
<atomRef index="646" />
<atomRef index="597" />
</cell>
<cell>
<cellProperties index="23" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="69" />
<atomRef index="76" />
<atomRef index="77" />
<atomRef index="70" />
<atomRef index="639" />
<atomRef index="688" />
<atomRef index="695" />
<atomRef index="646" />
</cell>
<cell>
<cellProperties index="24" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="76" />
<atomRef index="12" />
<atomRef index="13" />
<atomRef index="77" />
<atomRef index="688" />
<atomRef index="176" />
<atomRef index="183" />
<atomRef index="695" />
</cell>
<cell>
<cellProperties index="25" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="30" />
<atomRef index="35" />
<atomRef index="36" />
<atomRef index="29" />
<atomRef index="303" />
<atomRef index="401" />
<atomRef index="408" />
<atomRef index="310" />
</cell>
<cell>
<cellProperties index="26" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="35" />
<atomRef index="42" />
<atomRef index="43" />
<atomRef index="36" />
<atomRef index="401" />
<atomRef index="450" />
<atomRef index="457" />
<atomRef index="408" />
</cell>
<cell>
<cellProperties index="27" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="42" />
<atomRef index="49" />
<atomRef index="50" />
<atomRef index="43" />
<atomRef index="450" />
<atomRef index="499" />
<atomRef index="506" />
<atomRef index="457" />
</cell>
<cell>
<cellProperties index="28" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="49" />
<atomRef index="56" />
<atomRef index="57" />
<atomRef index="50" />
<atomRef index="499" />
<atomRef index="548" />
<atomRef index="555" />
<atomRef index="506" />
</cell>
<cell>
<cellProperties index="29" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="56" />
<atomRef index="63" />
<atomRef index="64" />
<atomRef index="57" />
<atomRef index="548" />
<atomRef index="597" />
<atomRef index="604" />
<atomRef index="555" />
</cell>
<cell>
<cellProperties index="30" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="63" />
<atomRef index="70" />
<atomRef index="71" />
<atomRef index="64" />
<atomRef index="597" />
<atomRef index="646" />
<atomRef index="653" />
<atomRef index="604" />
</cell>
<cell>
<cellProperties index="31" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="70" />
<atomRef index="77" />
<atomRef index="78" />
<atomRef index="71" />
<atomRef index="646" />
<atomRef index="695" />
<atomRef index="702" />
<atomRef index="653" />
</cell>
<cell>
<cellProperties index="32" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="77" />
<atomRef index="13" />
<atomRef index="14" />
<atomRef index="78" />
<atomRef index="695" />
<atomRef index="183" />
<atomRef index="190" />
<atomRef index="702" />
</cell>
<cell>
<cellProperties index="33" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="29" />
<atomRef index="36" />
<atomRef index="37" />
<atomRef index="28" />
<atomRef index="310" />
<atomRef index="408" />
<atomRef index="415" />
<atomRef index="317" />
</cell>
<cell>
<cellProperties index="34" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="36" />
<atomRef index="43" />
<atomRef index="44" />
<atomRef index="37" />
<atomRef index="408" />
<atomRef index="457" />
<atomRef index="464" />
<atomRef index="415" />
</cell>
<cell>
<cellProperties index="35" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="43" />
<atomRef index="50" />
<atomRef index="51" />
<atomRef index="44" />
<atomRef index="457" />
<atomRef index="506" />
<atomRef index="513" />
<atomRef index="464" />
</cell>
<cell>
<cellProperties index="36" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="50" />
<atomRef index="57" />
<atomRef index="58" />
<atomRef index="51" />
<atomRef index="506" />
<atomRef index="555" />
<atomRef index="562" />
<atomRef index="513" />
</cell>
<cell>
<cellProperties index="37" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="57" />
<atomRef index="64" />
<atomRef index="65" />
<atomRef index="58" />
<atomRef index="555" />
<atomRef index="604" />
<atomRef index="611" />
<atomRef index="562" />
</cell>
<cell>
<cellProperties index="38" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="64" />
<atomRef index="71" />
<atomRef index="72" />
<atomRef index="65" />
<atomRef index="604" />
<atomRef index="653" />
<atomRef index="660" />
<atomRef index="611" />
</cell>
<cell>
<cellProperties index="39" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="71" />
<atomRef index="78" />
<atomRef index="79" />
<atomRef index="72" />
<atomRef index="653" />
<atomRef index="702" />
<atomRef index="709" />
<atomRef index="660" />
</cell>
<cell>
<cellProperties index="40" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="78" />
<atomRef index="14" />
<atomRef index="15" />
<atomRef index="79" />
<atomRef index="702" />
<atomRef index="190" />
<atomRef index="197" />
<atomRef index="709" />
</cell>
<cell>
<cellProperties index="41" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="28" />
<atomRef index="37" />
<atomRef index="38" />
<atomRef index="27" />
<atomRef index="317" />
<atomRef index="415" />
<atomRef index="422" />
<atomRef index="324" />
</cell>
<cell>
<cellProperties index="42" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="37" />
<atomRef index="44" />
<atomRef index="45" />
<atomRef index="38" />
<atomRef index="415" />
<atomRef index="464" />
<atomRef index="471" />
<atomRef index="422" />
</cell>
<cell>
<cellProperties index="43" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="44" />
<atomRef index="51" />
<atomRef index="52" />
<atomRef index="45" />
<atomRef index="464" />
<atomRef index="513" />
<atomRef index="520" />
<atomRef index="471" />
</cell>
<cell>
<cellProperties index="44" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="51" />
<atomRef index="58" />
<atomRef index="59" />
<atomRef index="52" />
<atomRef index="513" />
<atomRef index="562" />
<atomRef index="569" />
<atomRef index="520" />
</cell>
<cell>
<cellProperties index="45" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="58" />
<atomRef index="65" />
<atomRef index="66" />
<atomRef index="59" />
<atomRef index="562" />
<atomRef index="611" />
<atomRef index="618" />
<atomRef index="569" />
</cell>
<cell>
<cellProperties index="46" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="65" />
<atomRef index="72" />
<atomRef index="73" />
<atomRef index="66" />
<atomRef index="611" />
<atomRef index="660" />
<atomRef index="667" />
<atomRef index="618" />
</cell>
<cell>
<cellProperties index="47" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="72" />
<atomRef index="79" />
<atomRef index="80" />
<atomRef index="73" />
<atomRef index="660" />
<atomRef index="709" />
<atomRef index="716" />
<atomRef index="667" />
</cell>
<cell>
<cellProperties index="48" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="79" />
<atomRef index="15" />
<atomRef index="16" />
<atomRef index="80" />
<atomRef index="709" />
<atomRef index="197" />
<atomRef index="204" />
<atomRef index="716" />
</cell>
<cell>
<cellProperties index="49" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="27" />
<atomRef index="38" />
<atomRef index="39" />
<atomRef index="26" />
<atomRef index="324" />
<atomRef index="422" />
<atomRef index="429" />
<atomRef index="331" />
</cell>
<cell>
<cellProperties index="50" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="38" />
<atomRef index="45" />
<atomRef index="46" />
<atomRef index="39" />
<atomRef index="422" />
<atomRef index="471" />
<atomRef index="478" />
<atomRef index="429" />
</cell>
<cell>
<cellProperties index="51" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="45" />
<atomRef index="52" />
<atomRef index="53" />
<atomRef index="46" />
<atomRef index="471" />
<atomRef index="520" />
<atomRef index="527" />
<atomRef index="478" />
</cell>
<cell>
<cellProperties index="52" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="52" />
<atomRef index="59" />
<atomRef index="60" />
<atomRef index="53" />
<atomRef index="520" />
<atomRef index="569" />
<atomRef index="576" />
<atomRef index="527" />
</cell>
<cell>
<cellProperties index="53" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="59" />
<atomRef index="66" />
<atomRef index="67" />
<atomRef index="60" />
<atomRef index="569" />
<atomRef index="618" />
<atomRef index="625" />
<atomRef index="576" />
</cell>
<cell>
<cellProperties index="54" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="66" />
<atomRef index="73" />
<atomRef index="74" />
<atomRef index="67" />
<atomRef index="618" />
<atomRef index="667" />
<atomRef index="674" />
<atomRef index="625" />
</cell>
<cell>
<cellProperties index="55" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="73" />
<atomRef index="80" />
<atomRef index="81" />
<atomRef index="74" />
<atomRef index="667" />
<atomRef index="716" />
<atomRef index="723" />
<atomRef index="674" />
</cell>
<cell>
<cellProperties index="56" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="80" />
<atomRef index="16" />
<atomRef index="17" />
<atomRef index="81" />
<atomRef index="716" />
<atomRef index="204" />
<atomRef index="211" />
<atomRef index="723" />
</cell>
<cell>
<cellProperties index="57" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="26" />
<atomRef index="39" />
<atomRef index="25" />
<atomRef index="18" />
<atomRef index="331" />
<atomRef index="429" />
<atomRef index="233" />
<atomRef index="232" />
</cell>
<cell>
<cellProperties index="58" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="39" />
<atomRef index="46" />
<atomRef index="24" />
<atomRef index="25" />
<atomRef index="429" />
<atomRef index="478" />
<atomRef index="240" />
<atomRef index="233" />
</cell>
<cell>
<cellProperties index="59" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="46" />
<atomRef index="53" />
<atomRef index="23" />
<atomRef index="24" />
<atomRef index="478" />
<atomRef index="527" />
<atomRef index="247" />
<atomRef index="240" />
</cell>
<cell>
<cellProperties index="60" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="53" />
<atomRef index="60" />
<atomRef index="22" />
<atomRef index="23" />
<atomRef index="527" />
<atomRef index="576" />
<atomRef index="254" />
<atomRef index="247" />
</cell>
<cell>
<cellProperties index="61" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="60" />
<atomRef index="67" />
<atomRef index="21" />
<atomRef index="22" />
<atomRef index="576" />
<atomRef index="625" />
<atomRef index="261" />
<atomRef index="254" />
</cell>
<cell>
<cellProperties index="62" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="67" />
<atomRef index="74" />
<atomRef index="20" />
<atomRef index="21" />
<atomRef index="625" />
<atomRef index="674" />
<atomRef index="268" />
<atomRef index="261" />
</cell>
<cell>
<cellProperties index="63" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="74" />
<atomRef index="81" />
<atomRef index="19" />
<atomRef index="20" />
<atomRef index="674" />
<atomRef index="723" />
<atomRef index="275" />
<atomRef index="268" />
</cell>
<cell>
<cellProperties index="64" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="81" />
<atomRef index="17" />
<atomRef index="10" />
<atomRef index="19" />
<atomRef index="723" />
<atomRef index="211" />
<atomRef index="161" />
<atomRef index="275" />
</cell>
<cell>
<cellProperties index="65" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="104" />
<atomRef index="105" />
<atomRef index="387" />
<atomRef index="289" />
<atomRef index="103" />
<atomRef index="106" />
<atomRef index="388" />
<atomRef index="290" />
</cell>
<cell>
<cellProperties index="66" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="105" />
<atomRef index="112" />
<atomRef index="436" />
<atomRef index="387" />
<atomRef index="106" />
<atomRef index="113" />
<atomRef index="437" />
<atomRef index="388" />
</cell>
<cell>
<cellProperties index="67" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="112" />
<atomRef index="119" />
<atomRef index="485" />
<atomRef index="436" />
<atomRef index="113" />
<atomRef index="120" />
<atomRef index="486" />
<atomRef index="437" />
</cell>
<cell>
<cellProperties index="68" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="119" />
<atomRef index="126" />
<atomRef index="534" />
<atomRef index="485" />
<atomRef index="120" />
<atomRef index="127" />
<atomRef index="535" />
<atomRef index="486" />
</cell>
<cell>
<cellProperties index="69" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="126" />
<atomRef index="133" />
<atomRef index="583" />
<atomRef index="534" />
<atomRef index="127" />
<atomRef index="134" />
<atomRef index="584" />
<atomRef index="535" />
</cell>
<cell>
<cellProperties index="70" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="133" />
<atomRef index="140" />
<atomRef index="632" />
<atomRef index="583" />
<atomRef index="134" />
<atomRef index="141" />
<atomRef index="633" />
<atomRef index="584" />
</cell>
<cell>
<cellProperties index="71" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="140" />
<atomRef index="147" />
<atomRef index="681" />
<atomRef index="632" />
<atomRef index="141" />
<atomRef index="148" />
<atomRef index="682" />
<atomRef index="633" />
</cell>
<cell>
<cellProperties index="72" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="147" />
<atomRef index="83" />
<atomRef index="169" />
<atomRef index="681" />
<atomRef index="148" />
<atomRef index="84" />
<atomRef index="170" />
<atomRef index="682" />
</cell>
<cell>
<cellProperties index="73" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="289" />
<atomRef index="387" />
<atomRef index="394" />
<atomRef index="296" />
<atomRef index="290" />
<atomRef index="388" />
<atomRef index="395" />
<atomRef index="297" />
</cell>
<cell>
<cellProperties index="74" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="387" />
<atomRef index="436" />
<atomRef index="443" />
<atomRef index="394" />
<atomRef index="388" />
<atomRef index="437" />
<atomRef index="444" />
<atomRef index="395" />
</cell>
<cell>
<cellProperties index="75" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="436" />
<atomRef index="485" />
<atomRef index="492" />
<atomRef index="443" />
<atomRef index="437" />
<atomRef index="486" />
<atomRef index="493" />
<atomRef index="444" />
</cell>
<cell>
<cellProperties index="76" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="485" />
<atomRef index="534" />
<atomRef index="541" />
<atomRef index="492" />
<atomRef index="486" />
<atomRef index="535" />
<atomRef index="542" />
<atomRef index="493" />
</cell>
<cell>
<cellProperties index="77" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="534" />
<atomRef index="583" />
<atomRef index="590" />
<atomRef index="541" />
<atomRef index="535" />
<atomRef index="584" />
<atomRef index="591" />
<atomRef index="542" />
</cell>
<cell>
<cellProperties index="78" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="583" />
<atomRef index="632" />
<atomRef index="639" />
<atomRef index="590" />
<atomRef index="584" />
<atomRef index="633" />
<atomRef index="640" />
<atomRef index="591" />
</cell>
<cell>
<cellProperties index="79" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="632" />
<atomRef index="681" />
<atomRef index="688" />
<atomRef index="639" />
<atomRef index="633" />
<atomRef index="682" />
<atomRef index="689" />
<atomRef index="640" />
</cell>
<cell>
<cellProperties index="80" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="681" />
<atomRef index="169" />
<atomRef index="176" />
<atomRef index="688" />
<atomRef index="682" />
<atomRef index="170" />
<atomRef index="177" />
<atomRef index="689" />
</cell>
<cell>
<cellProperties index="81" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="296" />
<atomRef index="394" />
<atomRef index="401" />
<atomRef index="303" />
<atomRef index="297" />
<atomRef index="395" />
<atomRef index="402" />
<atomRef index="304" />
</cell>
<cell>
<cellProperties index="82" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="394" />
<atomRef index="443" />
<atomRef index="450" />
<atomRef index="401" />
<atomRef index="395" />
<atomRef index="444" />
<atomRef index="451" />
<atomRef index="402" />
</cell>
<cell>
<cellProperties index="83" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="443" />
<atomRef index="492" />
<atomRef index="499" />
<atomRef index="450" />
<atomRef index="444" />
<atomRef index="493" />
<atomRef index="500" />
<atomRef index="451" />
</cell>
<cell>
<cellProperties index="84" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="492" />
<atomRef index="541" />
<atomRef index="548" />
<atomRef index="499" />
<atomRef index="493" />
<atomRef index="542" />
<atomRef index="549" />
<atomRef index="500" />
</cell>
<cell>
<cellProperties index="85" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="541" />
<atomRef index="590" />
<atomRef index="597" />
<atomRef index="548" />
<atomRef index="542" />
<atomRef index="591" />
<atomRef index="598" />
<atomRef index="549" />
</cell>
<cell>
<cellProperties index="86" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="590" />
<atomRef index="639" />
<atomRef index="646" />
<atomRef index="597" />
<atomRef index="591" />
<atomRef index="640" />
<atomRef index="647" />
<atomRef index="598" />
</cell>
<cell>
<cellProperties index="87" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="639" />
<atomRef index="688" />
<atomRef index="695" />
<atomRef index="646" />
<atomRef index="640" />
<atomRef index="689" />
<atomRef index="696" />
<atomRef index="647" />
</cell>
<cell>
<cellProperties index="88" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="688" />
<atomRef index="176" />
<atomRef index="183" />
<atomRef index="695" />
<atomRef index="689" />
<atomRef index="177" />
<atomRef index="184" />
<atomRef index="696" />
</cell>
<cell>
<cellProperties index="89" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="303" />
<atomRef index="401" />
<atomRef index="408" />
<atomRef index="310" />
<atomRef index="304" />
<atomRef index="402" />
<atomRef index="409" />
<atomRef index="311" />
</cell>
<cell>
<cellProperties index="90" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="401" />
<atomRef index="450" />
<atomRef index="457" />
<atomRef index="408" />
<atomRef index="402" />
<atomRef index="451" />
<atomRef index="458" />
<atomRef index="409" />
</cell>
<cell>
<cellProperties index="91" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="450" />
<atomRef index="499" />
<atomRef index="506" />
<atomRef index="457" />
<atomRef index="451" />
<atomRef index="500" />
<atomRef index="507" />
<atomRef index="458" />
</cell>
<cell>
<cellProperties index="92" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="499" />
<atomRef index="548" />
<atomRef index="555" />
<atomRef index="506" />
<atomRef index="500" />
<atomRef index="549" />
<atomRef index="556" />
<atomRef index="507" />
</cell>
<cell>
<cellProperties index="93" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="548" />
<atomRef index="597" />
<atomRef index="604" />
<atomRef index="555" />
<atomRef index="549" />
<atomRef index="598" />
<atomRef index="605" />
<atomRef index="556" />
</cell>
<cell>
<cellProperties index="94" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="597" />
<atomRef index="646" />
<atomRef index="653" />
<atomRef index="604" />
<atomRef index="598" />
<atomRef index="647" />
<atomRef index="654" />
<atomRef index="605" />
</cell>
<cell>
<cellProperties index="95" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="646" />
<atomRef index="695" />
<atomRef index="702" />
<atomRef index="653" />
<atomRef index="647" />
<atomRef index="696" />
<atomRef index="703" />
<atomRef index="654" />
</cell>
<cell>
<cellProperties index="96" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="695" />
<atomRef index="183" />
<atomRef index="190" />
<atomRef index="702" />
<atomRef index="696" />
<atomRef index="184" />
<atomRef index="191" />
<atomRef index="703" />
</cell>
<cell>
<cellProperties index="97" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="310" />
<atomRef index="408" />
<atomRef index="415" />
<atomRef index="317" />
<atomRef index="311" />
<atomRef index="409" />
<atomRef index="416" />
<atomRef index="318" />
</cell>
<cell>
<cellProperties index="98" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="408" />
<atomRef index="457" />
<atomRef index="464" />
<atomRef index="415" />
<atomRef index="409" />
<atomRef index="458" />
<atomRef index="465" />
<atomRef index="416" />
</cell>
<cell>
<cellProperties index="99" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="457" />
<atomRef index="506" />
<atomRef index="513" />
<atomRef index="464" />
<atomRef index="458" />
<atomRef index="507" />
<atomRef index="514" />
<atomRef index="465" />
</cell>
<cell>
<cellProperties index="100" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="506" />
<atomRef index="555" />
<atomRef index="562" />
<atomRef index="513" />
<atomRef index="507" />
<atomRef index="556" />
<atomRef index="563" />
<atomRef index="514" />
</cell>
<cell>
<cellProperties index="101" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="555" />
<atomRef index="604" />
<atomRef index="611" />
<atomRef index="562" />
<atomRef index="556" />
<atomRef index="605" />
<atomRef index="612" />
<atomRef index="563" />
</cell>
<cell>
<cellProperties index="102" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="604" />
<atomRef index="653" />
<atomRef index="660" />
<atomRef index="611" />
<atomRef index="605" />
<atomRef index="654" />
<atomRef index="661" />
<atomRef index="612" />
</cell>
<cell>
<cellProperties index="103" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="653" />
<atomRef index="702" />
<atomRef index="709" />
<atomRef index="660" />
<atomRef index="654" />
<atomRef index="703" />
<atomRef index="710" />
<atomRef index="661" />
</cell>
<cell>
<cellProperties index="104" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="702" />
<atomRef index="190" />
<atomRef index="197" />
<atomRef index="709" />
<atomRef index="703" />
<atomRef index="191" />
<atomRef index="198" />
<atomRef index="710" />
</cell>
<cell>
<cellProperties index="105" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="317" />
<atomRef index="415" />
<atomRef index="422" />
<atomRef index="324" />
<atomRef index="318" />
<atomRef index="416" />
<atomRef index="423" />
<atomRef index="325" />
</cell>
<cell>
<cellProperties index="106" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="415" />
<atomRef index="464" />
<atomRef index="471" />
<atomRef index="422" />
<atomRef index="416" />
<atomRef index="465" />
<atomRef index="472" />
<atomRef index="423" />
</cell>
<cell>
<cellProperties index="107" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="464" />
<atomRef index="513" />
<atomRef index="520" />
<atomRef index="471" />
<atomRef index="465" />
<atomRef index="514" />
<atomRef index="521" />
<atomRef index="472" />
</cell>
<cell>
<cellProperties index="108" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="513" />
<atomRef index="562" />
<atomRef index="569" />
<atomRef index="520" />
<atomRef index="514" />
<atomRef index="563" />
<atomRef index="570" />
<atomRef index="521" />
</cell>
<cell>
<cellProperties index="109" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="562" />
<atomRef index="611" />
<atomRef index="618" />
<atomRef index="569" />
<atomRef index="563" />
<atomRef index="612" />
<atomRef index="619" />
<atomRef index="570" />
</cell>
<cell>
<cellProperties index="110" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="611" />
<atomRef index="660" />
<atomRef index="667" />
<atomRef index="618" />
<atomRef index="612" />
<atomRef index="661" />
<atomRef index="668" />
<atomRef index="619" />
</cell>
<cell>
<cellProperties index="111" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="660" />
<atomRef index="709" />
<atomRef index="716" />
<atomRef index="667" />
<atomRef index="661" />
<atomRef index="710" />
<atomRef index="717" />
<atomRef index="668" />
</cell>
<cell>
<cellProperties index="112" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="709" />
<atomRef index="197" />
<atomRef index="204" />
<atomRef index="716" />
<atomRef index="710" />
<atomRef index="198" />
<atomRef index="205" />
<atomRef index="717" />
</cell>
<cell>
<cellProperties index="113" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="324" />
<atomRef index="422" />
<atomRef index="429" />
<atomRef index="331" />
<atomRef index="325" />
<atomRef index="423" />
<atomRef index="430" />
<atomRef index="332" />
</cell>
<cell>
<cellProperties index="114" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="422" />
<atomRef index="471" />
<atomRef index="478" />
<atomRef index="429" />
<atomRef index="423" />
<atomRef index="472" />
<atomRef index="479" />
<atomRef index="430" />
</cell>
<cell>
<cellProperties index="115" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="471" />
<atomRef index="520" />
<atomRef index="527" />
<atomRef index="478" />
<atomRef index="472" />
<atomRef index="521" />
<atomRef index="528" />
<atomRef index="479" />
</cell>
<cell>
<cellProperties index="116" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="520" />
<atomRef index="569" />
<atomRef index="576" />
<atomRef index="527" />
<atomRef index="521" />
<atomRef index="570" />
<atomRef index="577" />
<atomRef index="528" />
</cell>
<cell>
<cellProperties index="117" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="569" />
<atomRef index="618" />
<atomRef index="625" />
<atomRef index="576" />
<atomRef index="570" />
<atomRef index="619" />
<atomRef index="626" />
<atomRef index="577" />
</cell>
<cell>
<cellProperties index="118" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="618" />
<atomRef index="667" />
<atomRef index="674" />
<atomRef index="625" />
<atomRef index="619" />
<atomRef index="668" />
<atomRef index="675" />
<atomRef index="626" />
</cell>
<cell>
<cellProperties index="119" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="667" />
<atomRef index="716" />
<atomRef index="723" />
<atomRef index="674" />
<atomRef index="668" />
<atomRef index="717" />
<atomRef index="724" />
<atomRef index="675" />
</cell>
<cell>
<cellProperties index="120" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="716" />
<atomRef index="204" />
<atomRef index="211" />
<atomRef index="723" />
<atomRef index="717" />
<atomRef index="205" />
<atomRef index="212" />
<atomRef index="724" />
</cell>
<cell>
<cellProperties index="121" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="331" />
<atomRef index="429" />
<atomRef index="233" />
<atomRef index="232" />
<atomRef index="332" />
<atomRef index="430" />
<atomRef index="234" />
<atomRef index="231" />
</cell>
<cell>
<cellProperties index="122" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="429" />
<atomRef index="478" />
<atomRef index="240" />
<atomRef index="233" />
<atomRef index="430" />
<atomRef index="479" />
<atomRef index="241" />
<atomRef index="234" />
</cell>
<cell>
<cellProperties index="123" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="478" />
<atomRef index="527" />
<atomRef index="247" />
<atomRef index="240" />
<atomRef index="479" />
<atomRef index="528" />
<atomRef index="248" />
<atomRef index="241" />
</cell>
<cell>
<cellProperties index="124" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="527" />
<atomRef index="576" />
<atomRef index="254" />
<atomRef index="247" />
<atomRef index="528" />
<atomRef index="577" />
<atomRef index="255" />
<atomRef index="248" />
</cell>
<cell>
<cellProperties index="125" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="576" />
<atomRef index="625" />
<atomRef index="261" />
<atomRef index="254" />
<atomRef index="577" />
<atomRef index="626" />
<atomRef index="262" />
<atomRef index="255" />
</cell>
<cell>
<cellProperties index="126" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="625" />
<atomRef index="674" />
<atomRef index="268" />
<atomRef index="261" />
<atomRef index="626" />
<atomRef index="675" />
<atomRef index="269" />
<atomRef index="262" />
</cell>
<cell>
<cellProperties index="127" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="674" />
<atomRef index="723" />
<atomRef index="275" />
<atomRef index="268" />
<atomRef index="675" />
<atomRef index="724" />
<atomRef index="276" />
<atomRef index="269" />
</cell>
<cell>
<cellProperties index="128" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="723" />
<atomRef index="211" />
<atomRef index="161" />
<atomRef index="275" />
<atomRef index="724" />
<atomRef index="212" />
<atomRef index="160" />
<atomRef index="276" />
</cell>
<cell>
<cellProperties index="129" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="103" />
<atomRef index="106" />
<atomRef index="388" />
<atomRef index="290" />
<atomRef index="102" />
<atomRef index="107" />
<atomRef index="389" />
<atomRef index="291" />
</cell>
<cell>
<cellProperties index="130" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="106" />
<atomRef index="113" />
<atomRef index="437" />
<atomRef index="388" />
<atomRef index="107" />
<atomRef index="114" />
<atomRef index="438" />
<atomRef index="389" />
</cell>
<cell>
<cellProperties index="131" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="113" />
<atomRef index="120" />
<atomRef index="486" />
<atomRef index="437" />
<atomRef index="114" />
<atomRef index="121" />
<atomRef index="487" />
<atomRef index="438" />
</cell>
<cell>
<cellProperties index="132" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="120" />
<atomRef index="127" />
<atomRef index="535" />
<atomRef index="486" />
<atomRef index="121" />
<atomRef index="128" />
<atomRef index="536" />
<atomRef index="487" />
</cell>
<cell>
<cellProperties index="133" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="127" />
<atomRef index="134" />
<atomRef index="584" />
<atomRef index="535" />
<atomRef index="128" />
<atomRef index="135" />
<atomRef index="585" />
<atomRef index="536" />
</cell>
<cell>
<cellProperties index="134" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="134" />
<atomRef index="141" />
<atomRef index="633" />
<atomRef index="584" />
<atomRef index="135" />
<atomRef index="142" />
<atomRef index="634" />
<atomRef index="585" />
</cell>
<cell>
<cellProperties index="135" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="141" />
<atomRef index="148" />
<atomRef index="682" />
<atomRef index="633" />
<atomRef index="142" />
<atomRef index="149" />
<atomRef index="683" />
<atomRef index="634" />
</cell>
<cell>
<cellProperties index="136" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="148" />
<atomRef index="84" />
<atomRef index="170" />
<atomRef index="682" />
<atomRef index="149" />
<atomRef index="85" />
<atomRef index="171" />
<atomRef index="683" />
</cell>
<cell>
<cellProperties index="137" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="290" />
<atomRef index="388" />
<atomRef index="395" />
<atomRef index="297" />
<atomRef index="291" />
<atomRef index="389" />
<atomRef index="396" />
<atomRef index="298" />
</cell>
<cell>
<cellProperties index="138" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="388" />
<atomRef index="437" />
<atomRef index="444" />
<atomRef index="395" />
<atomRef index="389" />
<atomRef index="438" />
<atomRef index="445" />
<atomRef index="396" />
</cell>
<cell>
<cellProperties index="139" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="437" />
<atomRef index="486" />
<atomRef index="493" />
<atomRef index="444" />
<atomRef index="438" />
<atomRef index="487" />
<atomRef index="494" />
<atomRef index="445" />
</cell>
<cell>
<cellProperties index="140" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="486" />
<atomRef index="535" />
<atomRef index="542" />
<atomRef index="493" />
<atomRef index="487" />
<atomRef index="536" />
<atomRef index="543" />
<atomRef index="494" />
</cell>
<cell>
<cellProperties index="141" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="535" />
<atomRef index="584" />
<atomRef index="591" />
<atomRef index="542" />
<atomRef index="536" />
<atomRef index="585" />
<atomRef index="592" />
<atomRef index="543" />
</cell>
<cell>
<cellProperties index="142" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="584" />
<atomRef index="633" />
<atomRef index="640" />
<atomRef index="591" />
<atomRef index="585" />
<atomRef index="634" />
<atomRef index="641" />
<atomRef index="592" />
</cell>
<cell>
<cellProperties index="143" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="633" />
<atomRef index="682" />
<atomRef index="689" />
<atomRef index="640" />
<atomRef index="634" />
<atomRef index="683" />
<atomRef index="690" />
<atomRef index="641" />
</cell>
<cell>
<cellProperties index="144" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="682" />
<atomRef index="170" />
<atomRef index="177" />
<atomRef index="689" />
<atomRef index="683" />
<atomRef index="171" />
<atomRef index="178" />
<atomRef index="690" />
</cell>
<cell>
<cellProperties index="145" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="297" />
<atomRef index="395" />
<atomRef index="402" />
<atomRef index="304" />
<atomRef index="298" />
<atomRef index="396" />
<atomRef index="403" />
<atomRef index="305" />
</cell>
<cell>
<cellProperties index="146" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="395" />
<atomRef index="444" />
<atomRef index="451" />
<atomRef index="402" />
<atomRef index="396" />
<atomRef index="445" />
<atomRef index="452" />
<atomRef index="403" />
</cell>
<cell>
<cellProperties index="147" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="444" />
<atomRef index="493" />
<atomRef index="500" />
<atomRef index="451" />
<atomRef index="445" />
<atomRef index="494" />
<atomRef index="501" />
<atomRef index="452" />
</cell>
<cell>
<cellProperties index="148" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="493" />
<atomRef index="542" />
<atomRef index="549" />
<atomRef index="500" />
<atomRef index="494" />
<atomRef index="543" />
<atomRef index="550" />
<atomRef index="501" />
</cell>
<cell>
<cellProperties index="149" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="542" />
<atomRef index="591" />
<atomRef index="598" />
<atomRef index="549" />
<atomRef index="543" />
<atomRef index="592" />
<atomRef index="599" />
<atomRef index="550" />
</cell>
<cell>
<cellProperties index="150" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="591" />
<atomRef index="640" />
<atomRef index="647" />
<atomRef index="598" />
<atomRef index="592" />
<atomRef index="641" />
<atomRef index="648" />
<atomRef index="599" />
</cell>
<cell>
<cellProperties index="151" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="640" />
<atomRef index="689" />
<atomRef index="696" />
<atomRef index="647" />
<atomRef index="641" />
<atomRef index="690" />
<atomRef index="697" />
<atomRef index="648" />
</cell>
<cell>
<cellProperties index="152" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="689" />
<atomRef index="177" />
<atomRef index="184" />
<atomRef index="696" />
<atomRef index="690" />
<atomRef index="178" />
<atomRef index="185" />
<atomRef index="697" />
</cell>
<cell>
<cellProperties index="153" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="304" />
<atomRef index="402" />
<atomRef index="409" />
<atomRef index="311" />
<atomRef index="305" />
<atomRef index="403" />
<atomRef index="410" />
<atomRef index="312" />
</cell>
<cell>
<cellProperties index="154" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="402" />
<atomRef index="451" />
<atomRef index="458" />
<atomRef index="409" />
<atomRef index="403" />
<atomRef index="452" />
<atomRef index="459" />
<atomRef index="410" />
</cell>
<cell>
<cellProperties index="155" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="451" />
<atomRef index="500" />
<atomRef index="507" />
<atomRef index="458" />
<atomRef index="452" />
<atomRef index="501" />
<atomRef index="508" />
<atomRef index="459" />
</cell>
<cell>
<cellProperties index="156" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="500" />
<atomRef index="549" />
<atomRef index="556" />
<atomRef index="507" />
<atomRef index="501" />
<atomRef index="550" />
<atomRef index="557" />
<atomRef index="508" />
</cell>
<cell>
<cellProperties index="157" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="549" />
<atomRef index="598" />
<atomRef index="605" />
<atomRef index="556" />
<atomRef index="550" />
<atomRef index="599" />
<atomRef index="606" />
<atomRef index="557" />
</cell>
<cell>
<cellProperties index="158" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="598" />
<atomRef index="647" />
<atomRef index="654" />
<atomRef index="605" />
<atomRef index="599" />
<atomRef index="648" />
<atomRef index="655" />
<atomRef index="606" />
</cell>
<cell>
<cellProperties index="159" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="647" />
<atomRef index="696" />
<atomRef index="703" />
<atomRef index="654" />
<atomRef index="648" />
<atomRef index="697" />
<atomRef index="704" />
<atomRef index="655" />
</cell>
<cell>
<cellProperties index="160" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="696" />
<atomRef index="184" />
<atomRef index="191" />
<atomRef index="703" />
<atomRef index="697" />
<atomRef index="185" />
<atomRef index="192" />
<atomRef index="704" />
</cell>
<cell>
<cellProperties index="161" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="311" />
<atomRef index="409" />
<atomRef index="416" />
<atomRef index="318" />
<atomRef index="312" />
<atomRef index="410" />
<atomRef index="417" />
<atomRef index="319" />
</cell>
<cell>
<cellProperties index="162" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="409" />
<atomRef index="458" />
<atomRef index="465" />
<atomRef index="416" />
<atomRef index="410" />
<atomRef index="459" />
<atomRef index="466" />
<atomRef index="417" />
</cell>
<cell>
<cellProperties index="163" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="458" />
<atomRef index="507" />
<atomRef index="514" />
<atomRef index="465" />
<atomRef index="459" />
<atomRef index="508" />
<atomRef index="515" />
<atomRef index="466" />
</cell>
<cell>
<cellProperties index="164" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="507" />
<atomRef index="556" />
<atomRef index="563" />
<atomRef index="514" />
<atomRef index="508" />
<atomRef index="557" />
<atomRef index="564" />
<atomRef index="515" />
</cell>
<cell>
<cellProperties index="165" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="556" />
<atomRef index="605" />
<atomRef index="612" />
<atomRef index="563" />
<atomRef index="557" />
<atomRef index="606" />
<atomRef index="613" />
<atomRef index="564" />
</cell>
<cell>
<cellProperties index="166" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="605" />
<atomRef index="654" />
<atomRef index="661" />
<atomRef index="612" />
<atomRef index="606" />
<atomRef index="655" />
<atomRef index="662" />
<atomRef index="613" />
</cell>
<cell>
<cellProperties index="167" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="654" />
<atomRef index="703" />
<atomRef index="710" />
<atomRef index="661" />
<atomRef index="655" />
<atomRef index="704" />
<atomRef index="711" />
<atomRef index="662" />
</cell>
<cell>
<cellProperties index="168" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="703" />
<atomRef index="191" />
<atomRef index="198" />
<atomRef index="710" />
<atomRef index="704" />
<atomRef index="192" />
<atomRef index="199" />
<atomRef index="711" />
</cell>
<cell>
<cellProperties index="169" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="318" />
<atomRef index="416" />
<atomRef index="423" />
<atomRef index="325" />
<atomRef index="319" />
<atomRef index="417" />
<atomRef index="424" />
<atomRef index="326" />
</cell>
<cell>
<cellProperties index="170" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="416" />
<atomRef index="465" />
<atomRef index="472" />
<atomRef index="423" />
<atomRef index="417" />
<atomRef index="466" />
<atomRef index="473" />
<atomRef index="424" />
</cell>
<cell>
<cellProperties index="171" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="465" />
<atomRef index="514" />
<atomRef index="521" />
<atomRef index="472" />
<atomRef index="466" />
<atomRef index="515" />
<atomRef index="522" />
<atomRef index="473" />
</cell>
<cell>
<cellProperties index="172" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="514" />
<atomRef index="563" />
<atomRef index="570" />
<atomRef index="521" />
<atomRef index="515" />
<atomRef index="564" />
<atomRef index="571" />
<atomRef index="522" />
</cell>
<cell>
<cellProperties index="173" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="563" />
<atomRef index="612" />
<atomRef index="619" />
<atomRef index="570" />
<atomRef index="564" />
<atomRef index="613" />
<atomRef index="620" />
<atomRef index="571" />
</cell>
<cell>
<cellProperties index="174" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="612" />
<atomRef index="661" />
<atomRef index="668" />
<atomRef index="619" />
<atomRef index="613" />
<atomRef index="662" />
<atomRef index="669" />
<atomRef index="620" />
</cell>
<cell>
<cellProperties index="175" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="661" />
<atomRef index="710" />
<atomRef index="717" />
<atomRef index="668" />
<atomRef index="662" />
<atomRef index="711" />
<atomRef index="718" />
<atomRef index="669" />
</cell>
<cell>
<cellProperties index="176" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="710" />
<atomRef index="198" />
<atomRef index="205" />
<atomRef index="717" />
<atomRef index="711" />
<atomRef index="199" />
<atomRef index="206" />
<atomRef index="718" />
</cell>
<cell>
<cellProperties index="177" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="325" />
<atomRef index="423" />
<atomRef index="430" />
<atomRef index="332" />
<atomRef index="326" />
<atomRef index="424" />
<atomRef index="431" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="178" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="423" />
<atomRef index="472" />
<atomRef index="479" />
<atomRef index="430" />
<atomRef index="424" />
<atomRef index="473" />
<atomRef index="480" />
<atomRef index="431" />
</cell>
<cell>
<cellProperties index="179" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="472" />
<atomRef index="521" />
<atomRef index="528" />
<atomRef index="479" />
<atomRef index="473" />
<atomRef index="522" />
<atomRef index="529" />
<atomRef index="480" />
</cell>
<cell>
<cellProperties index="180" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="521" />
<atomRef index="570" />
<atomRef index="577" />
<atomRef index="528" />
<atomRef index="522" />
<atomRef index="571" />
<atomRef index="578" />
<atomRef index="529" />
</cell>
<cell>
<cellProperties index="181" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="570" />
<atomRef index="619" />
<atomRef index="626" />
<atomRef index="577" />
<atomRef index="571" />
<atomRef index="620" />
<atomRef index="627" />
<atomRef index="578" />
</cell>
<cell>
<cellProperties index="182" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="619" />
<atomRef index="668" />
<atomRef index="675" />
<atomRef index="626" />
<atomRef index="620" />
<atomRef index="669" />
<atomRef index="676" />
<atomRef index="627" />
</cell>
<cell>
<cellProperties index="183" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="668" />
<atomRef index="717" />
<atomRef index="724" />
<atomRef index="675" />
<atomRef index="669" />
<atomRef index="718" />
<atomRef index="725" />
<atomRef index="676" />
</cell>
<cell>
<cellProperties index="184" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="717" />
<atomRef index="205" />
<atomRef index="212" />
<atomRef index="724" />
<atomRef index="718" />
<atomRef index="206" />
<atomRef index="213" />
<atomRef index="725" />
</cell>
<cell>
<cellProperties index="185" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="332" />
<atomRef index="430" />
<atomRef index="234" />
<atomRef index="231" />
<atomRef index="333" />
<atomRef index="431" />
<atomRef index="235" />
<atomRef index="230" />
</cell>
<cell>
<cellProperties index="186" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="430" />
<atomRef index="479" />
<atomRef index="241" />
<atomRef index="234" />
<atomRef index="431" />
<atomRef index="480" />
<atomRef index="242" />
<atomRef index="235" />
</cell>
<cell>
<cellProperties index="187" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="479" />
<atomRef index="528" />
<atomRef index="248" />
<atomRef index="241" />
<atomRef index="480" />
<atomRef index="529" />
<atomRef index="249" />
<atomRef index="242" />
</cell>
<cell>
<cellProperties index="188" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="528" />
<atomRef index="577" />
<atomRef index="255" />
<atomRef index="248" />
<atomRef index="529" />
<atomRef index="578" />
<atomRef index="256" />
<atomRef index="249" />
</cell>
<cell>
<cellProperties index="189" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="577" />
<atomRef index="626" />
<atomRef index="262" />
<atomRef index="255" />
<atomRef index="578" />
<atomRef index="627" />
<atomRef index="263" />
<atomRef index="256" />
</cell>
<cell>
<cellProperties index="190" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="626" />
<atomRef index="675" />
<atomRef index="269" />
<atomRef index="262" />
<atomRef index="627" />
<atomRef index="676" />
<atomRef index="270" />
<atomRef index="263" />
</cell>
<cell>
<cellProperties index="191" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="675" />
<atomRef index="724" />
<atomRef index="276" />
<atomRef index="269" />
<atomRef index="676" />
<atomRef index="725" />
<atomRef index="277" />
<atomRef index="270" />
</cell>
<cell>
<cellProperties index="192" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="724" />
<atomRef index="212" />
<atomRef index="160" />
<atomRef index="276" />
<atomRef index="725" />
<atomRef index="213" />
<atomRef index="159" />
<atomRef index="277" />
</cell>
<cell>
<cellProperties index="193" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="102" />
<atomRef index="107" />
<atomRef index="389" />
<atomRef index="291" />
<atomRef index="101" />
<atomRef index="108" />
<atomRef index="390" />
<atomRef index="292" />
</cell>
<cell>
<cellProperties index="194" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="107" />
<atomRef index="114" />
<atomRef index="438" />
<atomRef index="389" />
<atomRef index="108" />
<atomRef index="115" />
<atomRef index="439" />
<atomRef index="390" />
</cell>
<cell>
<cellProperties index="195" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="114" />
<atomRef index="121" />
<atomRef index="487" />
<atomRef index="438" />
<atomRef index="115" />
<atomRef index="122" />
<atomRef index="488" />
<atomRef index="439" />
</cell>
<cell>
<cellProperties index="196" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="121" />
<atomRef index="128" />
<atomRef index="536" />
<atomRef index="487" />
<atomRef index="122" />
<atomRef index="129" />
<atomRef index="537" />
<atomRef index="488" />
</cell>
<cell>
<cellProperties index="197" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="128" />
<atomRef index="135" />
<atomRef index="585" />
<atomRef index="536" />
<atomRef index="129" />
<atomRef index="136" />
<atomRef index="586" />
<atomRef index="537" />
</cell>
<cell>
<cellProperties index="198" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="135" />
<atomRef index="142" />
<atomRef index="634" />
<atomRef index="585" />
<atomRef index="136" />
<atomRef index="143" />
<atomRef index="635" />
<atomRef index="586" />
</cell>
<cell>
<cellProperties index="199" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="142" />
<atomRef index="149" />
<atomRef index="683" />
<atomRef index="634" />
<atomRef index="143" />
<atomRef index="150" />
<atomRef index="684" />
<atomRef index="635" />
</cell>
<cell>
<cellProperties index="200" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="149" />
<atomRef index="85" />
<atomRef index="171" />
<atomRef index="683" />
<atomRef index="150" />
<atomRef index="86" />
<atomRef index="172" />
<atomRef index="684" />
</cell>
<cell>
<cellProperties index="201" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="291" />
<atomRef index="389" />
<atomRef index="396" />
<atomRef index="298" />
<atomRef index="292" />
<atomRef index="390" />
<atomRef index="397" />
<atomRef index="299" />
</cell>
<cell>
<cellProperties index="202" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="389" />
<atomRef index="438" />
<atomRef index="445" />
<atomRef index="396" />
<atomRef index="390" />
<atomRef index="439" />
<atomRef index="446" />
<atomRef index="397" />
</cell>
<cell>
<cellProperties index="203" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="438" />
<atomRef index="487" />
<atomRef index="494" />
<atomRef index="445" />
<atomRef index="439" />
<atomRef index="488" />
<atomRef index="495" />
<atomRef index="446" />
</cell>
<cell>
<cellProperties index="204" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="487" />
<atomRef index="536" />
<atomRef index="543" />
<atomRef index="494" />
<atomRef index="488" />
<atomRef index="537" />
<atomRef index="544" />
<atomRef index="495" />
</cell>
<cell>
<cellProperties index="205" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="536" />
<atomRef index="585" />
<atomRef index="592" />
<atomRef index="543" />
<atomRef index="537" />
<atomRef index="586" />
<atomRef index="593" />
<atomRef index="544" />
</cell>
<cell>
<cellProperties index="206" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="585" />
<atomRef index="634" />
<atomRef index="641" />
<atomRef index="592" />
<atomRef index="586" />
<atomRef index="635" />
<atomRef index="642" />
<atomRef index="593" />
</cell>
<cell>
<cellProperties index="207" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="634" />
<atomRef index="683" />
<atomRef index="690" />
<atomRef index="641" />
<atomRef index="635" />
<atomRef index="684" />
<atomRef index="691" />
<atomRef index="642" />
</cell>
<cell>
<cellProperties index="208" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="683" />
<atomRef index="171" />
<atomRef index="178" />
<atomRef index="690" />
<atomRef index="684" />
<atomRef index="172" />
<atomRef index="179" />
<atomRef index="691" />
</cell>
<cell>
<cellProperties index="209" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="298" />
<atomRef index="396" />
<atomRef index="403" />
<atomRef index="305" />
<atomRef index="299" />
<atomRef index="397" />
<atomRef index="404" />
<atomRef index="306" />
</cell>
<cell>
<cellProperties index="210" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="396" />
<atomRef index="445" />
<atomRef index="452" />
<atomRef index="403" />
<atomRef index="397" />
<atomRef index="446" />
<atomRef index="453" />
<atomRef index="404" />
</cell>
<cell>
<cellProperties index="211" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="445" />
<atomRef index="494" />
<atomRef index="501" />
<atomRef index="452" />
<atomRef index="446" />
<atomRef index="495" />
<atomRef index="502" />
<atomRef index="453" />
</cell>
<cell>
<cellProperties index="212" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="494" />
<atomRef index="543" />
<atomRef index="550" />
<atomRef index="501" />
<atomRef index="495" />
<atomRef index="544" />
<atomRef index="551" />
<atomRef index="502" />
</cell>
<cell>
<cellProperties index="213" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="543" />
<atomRef index="592" />
<atomRef index="599" />
<atomRef index="550" />
<atomRef index="544" />
<atomRef index="593" />
<atomRef index="600" />
<atomRef index="551" />
</cell>
<cell>
<cellProperties index="214" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="592" />
<atomRef index="641" />
<atomRef index="648" />
<atomRef index="599" />
<atomRef index="593" />
<atomRef index="642" />
<atomRef index="649" />
<atomRef index="600" />
</cell>
<cell>
<cellProperties index="215" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="641" />
<atomRef index="690" />
<atomRef index="697" />
<atomRef index="648" />
<atomRef index="642" />
<atomRef index="691" />
<atomRef index="698" />
<atomRef index="649" />
</cell>
<cell>
<cellProperties index="216" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="690" />
<atomRef index="178" />
<atomRef index="185" />
<atomRef index="697" />
<atomRef index="691" />
<atomRef index="179" />
<atomRef index="186" />
<atomRef index="698" />
</cell>
<cell>
<cellProperties index="217" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="305" />
<atomRef index="403" />
<atomRef index="410" />
<atomRef index="312" />
<atomRef index="306" />
<atomRef index="404" />
<atomRef index="411" />
<atomRef index="313" />
</cell>
<cell>
<cellProperties index="218" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="403" />
<atomRef index="452" />
<atomRef index="459" />
<atomRef index="410" />
<atomRef index="404" />
<atomRef index="453" />
<atomRef index="460" />
<atomRef index="411" />
</cell>
<cell>
<cellProperties index="219" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="452" />
<atomRef index="501" />
<atomRef index="508" />
<atomRef index="459" />
<atomRef index="453" />
<atomRef index="502" />
<atomRef index="509" />
<atomRef index="460" />
</cell>
<cell>
<cellProperties index="220" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="501" />
<atomRef index="550" />
<atomRef index="557" />
<atomRef index="508" />
<atomRef index="502" />
<atomRef index="551" />
<atomRef index="558" />
<atomRef index="509" />
</cell>
<cell>
<cellProperties index="221" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="550" />
<atomRef index="599" />
<atomRef index="606" />
<atomRef index="557" />
<atomRef index="551" />
<atomRef index="600" />
<atomRef index="607" />
<atomRef index="558" />
</cell>
<cell>
<cellProperties index="222" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="599" />
<atomRef index="648" />
<atomRef index="655" />
<atomRef index="606" />
<atomRef index="600" />
<atomRef index="649" />
<atomRef index="656" />
<atomRef index="607" />
</cell>
<cell>
<cellProperties index="223" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="648" />
<atomRef index="697" />
<atomRef index="704" />
<atomRef index="655" />
<atomRef index="649" />
<atomRef index="698" />
<atomRef index="705" />
<atomRef index="656" />
</cell>
<cell>
<cellProperties index="224" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="697" />
<atomRef index="185" />
<atomRef index="192" />
<atomRef index="704" />
<atomRef index="698" />
<atomRef index="186" />
<atomRef index="193" />
<atomRef index="705" />
</cell>
<cell>
<cellProperties index="225" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="312" />
<atomRef index="410" />
<atomRef index="417" />
<atomRef index="319" />
<atomRef index="313" />
<atomRef index="411" />
<atomRef index="418" />
<atomRef index="320" />
</cell>
<cell>
<cellProperties index="226" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="410" />
<atomRef index="459" />
<atomRef index="466" />
<atomRef index="417" />
<atomRef index="411" />
<atomRef index="460" />
<atomRef index="467" />
<atomRef index="418" />
</cell>
<cell>
<cellProperties index="227" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="459" />
<atomRef index="508" />
<atomRef index="515" />
<atomRef index="466" />
<atomRef index="460" />
<atomRef index="509" />
<atomRef index="516" />
<atomRef index="467" />
</cell>
<cell>
<cellProperties index="228" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="508" />
<atomRef index="557" />
<atomRef index="564" />
<atomRef index="515" />
<atomRef index="509" />
<atomRef index="558" />
<atomRef index="565" />
<atomRef index="516" />
</cell>
<cell>
<cellProperties index="229" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="557" />
<atomRef index="606" />
<atomRef index="613" />
<atomRef index="564" />
<atomRef index="558" />
<atomRef index="607" />
<atomRef index="614" />
<atomRef index="565" />
</cell>
<cell>
<cellProperties index="230" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="606" />
<atomRef index="655" />
<atomRef index="662" />
<atomRef index="613" />
<atomRef index="607" />
<atomRef index="656" />
<atomRef index="663" />
<atomRef index="614" />
</cell>
<cell>
<cellProperties index="231" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="655" />
<atomRef index="704" />
<atomRef index="711" />
<atomRef index="662" />
<atomRef index="656" />
<atomRef index="705" />
<atomRef index="712" />
<atomRef index="663" />
</cell>
<cell>
<cellProperties index="232" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="704" />
<atomRef index="192" />
<atomRef index="199" />
<atomRef index="711" />
<atomRef index="705" />
<atomRef index="193" />
<atomRef index="200" />
<atomRef index="712" />
</cell>
<cell>
<cellProperties index="233" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="319" />
<atomRef index="417" />
<atomRef index="424" />
<atomRef index="326" />
<atomRef index="320" />
<atomRef index="418" />
<atomRef index="425" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties index="234" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="417" />
<atomRef index="466" />
<atomRef index="473" />
<atomRef index="424" />
<atomRef index="418" />
<atomRef index="467" />
<atomRef index="474" />
<atomRef index="425" />
</cell>
<cell>
<cellProperties index="235" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="466" />
<atomRef index="515" />
<atomRef index="522" />
<atomRef index="473" />
<atomRef index="467" />
<atomRef index="516" />
<atomRef index="523" />
<atomRef index="474" />
</cell>
<cell>
<cellProperties index="236" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="515" />
<atomRef index="564" />
<atomRef index="571" />
<atomRef index="522" />
<atomRef index="516" />
<atomRef index="565" />
<atomRef index="572" />
<atomRef index="523" />
</cell>
<cell>
<cellProperties index="237" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="564" />
<atomRef index="613" />
<atomRef index="620" />
<atomRef index="571" />
<atomRef index="565" />
<atomRef index="614" />
<atomRef index="621" />
<atomRef index="572" />
</cell>
<cell>
<cellProperties index="238" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="613" />
<atomRef index="662" />
<atomRef index="669" />
<atomRef index="620" />
<atomRef index="614" />
<atomRef index="663" />
<atomRef index="670" />
<atomRef index="621" />
</cell>
<cell>
<cellProperties index="239" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="662" />
<atomRef index="711" />
<atomRef index="718" />
<atomRef index="669" />
<atomRef index="663" />
<atomRef index="712" />
<atomRef index="719" />
<atomRef index="670" />
</cell>
<cell>
<cellProperties index="240" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="711" />
<atomRef index="199" />
<atomRef index="206" />
<atomRef index="718" />
<atomRef index="712" />
<atomRef index="200" />
<atomRef index="207" />
<atomRef index="719" />
</cell>
<cell>
<cellProperties index="241" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="326" />
<atomRef index="424" />
<atomRef index="431" />
<atomRef index="333" />
<atomRef index="327" />
<atomRef index="425" />
<atomRef index="432" />
<atomRef index="334" />
</cell>
<cell>
<cellProperties index="242" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="424" />
<atomRef index="473" />
<atomRef index="480" />
<atomRef index="431" />
<atomRef index="425" />
<atomRef index="474" />
<atomRef index="481" />
<atomRef index="432" />
</cell>
<cell>
<cellProperties index="243" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="473" />
<atomRef index="522" />
<atomRef index="529" />
<atomRef index="480" />
<atomRef index="474" />
<atomRef index="523" />
<atomRef index="530" />
<atomRef index="481" />
</cell>
<cell>
<cellProperties index="244" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="522" />
<atomRef index="571" />
<atomRef index="578" />
<atomRef index="529" />
<atomRef index="523" />
<atomRef index="572" />
<atomRef index="579" />
<atomRef index="530" />
</cell>
<cell>
<cellProperties index="245" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="571" />
<atomRef index="620" />
<atomRef index="627" />
<atomRef index="578" />
<atomRef index="572" />
<atomRef index="621" />
<atomRef index="628" />
<atomRef index="579" />
</cell>
<cell>
<cellProperties index="246" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="620" />
<atomRef index="669" />
<atomRef index="676" />
<atomRef index="627" />
<atomRef index="621" />
<atomRef index="670" />
<atomRef index="677" />
<atomRef index="628" />
</cell>
<cell>
<cellProperties index="247" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="669" />
<atomRef index="718" />
<atomRef index="725" />
<atomRef index="676" />
<atomRef index="670" />
<atomRef index="719" />
<atomRef index="726" />
<atomRef index="677" />
</cell>
<cell>
<cellProperties index="248" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="718" />
<atomRef index="206" />
<atomRef index="213" />
<atomRef index="725" />
<atomRef index="719" />
<atomRef index="207" />
<atomRef index="214" />
<atomRef index="726" />
</cell>
<cell>
<cellProperties index="249" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="333" />
<atomRef index="431" />
<atomRef index="235" />
<atomRef index="230" />
<atomRef index="334" />
<atomRef index="432" />
<atomRef index="236" />
<atomRef index="229" />
</cell>
<cell>
<cellProperties index="250" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="431" />
<atomRef index="480" />
<atomRef index="242" />
<atomRef index="235" />
<atomRef index="432" />
<atomRef index="481" />
<atomRef index="243" />
<atomRef index="236" />
</cell>
<cell>
<cellProperties index="251" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="480" />
<atomRef index="529" />
<atomRef index="249" />
<atomRef index="242" />
<atomRef index="481" />
<atomRef index="530" />
<atomRef index="250" />
<atomRef index="243" />
</cell>
<cell>
<cellProperties index="252" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="529" />
<atomRef index="578" />
<atomRef index="256" />
<atomRef index="249" />
<atomRef index="530" />
<atomRef index="579" />
<atomRef index="257" />
<atomRef index="250" />
</cell>
<cell>
<cellProperties index="253" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="578" />
<atomRef index="627" />
<atomRef index="263" />
<atomRef index="256" />
<atomRef index="579" />
<atomRef index="628" />
<atomRef index="264" />
<atomRef index="257" />
</cell>
<cell>
<cellProperties index="254" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="627" />
<atomRef index="676" />
<atomRef index="270" />
<atomRef index="263" />
<atomRef index="628" />
<atomRef index="677" />
<atomRef index="271" />
<atomRef index="264" />
</cell>
<cell>
<cellProperties index="255" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="676" />
<atomRef index="725" />
<atomRef index="277" />
<atomRef index="270" />
<atomRef index="677" />
<atomRef index="726" />
<atomRef index="278" />
<atomRef index="271" />
</cell>
<cell>
<cellProperties index="256" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="725" />
<atomRef index="213" />
<atomRef index="159" />
<atomRef index="277" />
<atomRef index="726" />
<atomRef index="214" />
<atomRef index="158" />
<atomRef index="278" />
</cell>
<cell>
<cellProperties index="257" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="101" />
<atomRef index="108" />
<atomRef index="390" />
<atomRef index="292" />
<atomRef index="100" />
<atomRef index="109" />
<atomRef index="391" />
<atomRef index="293" />
</cell>
<cell>
<cellProperties index="258" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="108" />
<atomRef index="115" />
<atomRef index="439" />
<atomRef index="390" />
<atomRef index="109" />
<atomRef index="116" />
<atomRef index="440" />
<atomRef index="391" />
</cell>
<cell>
<cellProperties index="259" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="115" />
<atomRef index="122" />
<atomRef index="488" />
<atomRef index="439" />
<atomRef index="116" />
<atomRef index="123" />
<atomRef index="489" />
<atomRef index="440" />
</cell>
<cell>
<cellProperties index="260" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="122" />
<atomRef index="129" />
<atomRef index="537" />
<atomRef index="488" />
<atomRef index="123" />
<atomRef index="130" />
<atomRef index="538" />
<atomRef index="489" />
</cell>
<cell>
<cellProperties index="261" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="129" />
<atomRef index="136" />
<atomRef index="586" />
<atomRef index="537" />
<atomRef index="130" />
<atomRef index="137" />
<atomRef index="587" />
<atomRef index="538" />
</cell>
<cell>
<cellProperties index="262" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="136" />
<atomRef index="143" />
<atomRef index="635" />
<atomRef index="586" />
<atomRef index="137" />
<atomRef index="144" />
<atomRef index="636" />
<atomRef index="587" />
</cell>
<cell>
<cellProperties index="263" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="143" />
<atomRef index="150" />
<atomRef index="684" />
<atomRef index="635" />
<atomRef index="144" />
<atomRef index="151" />
<atomRef index="685" />
<atomRef index="636" />
</cell>
<cell>
<cellProperties index="264" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="150" />
<atomRef index="86" />
<atomRef index="172" />
<atomRef index="684" />
<atomRef index="151" />
<atomRef index="87" />
<atomRef index="173" />
<atomRef index="685" />
</cell>
<cell>
<cellProperties index="265" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="292" />
<atomRef index="390" />
<atomRef index="397" />
<atomRef index="299" />
<atomRef index="293" />
<atomRef index="391" />
<atomRef index="398" />
<atomRef index="300" />
</cell>
<cell>
<cellProperties index="266" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="390" />
<atomRef index="439" />
<atomRef index="446" />
<atomRef index="397" />
<atomRef index="391" />
<atomRef index="440" />
<atomRef index="447" />
<atomRef index="398" />
</cell>
<cell>
<cellProperties index="267" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="439" />
<atomRef index="488" />
<atomRef index="495" />
<atomRef index="446" />
<atomRef index="440" />
<atomRef index="489" />
<atomRef index="496" />
<atomRef index="447" />
</cell>
<cell>
<cellProperties index="268" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="488" />
<atomRef index="537" />
<atomRef index="544" />
<atomRef index="495" />
<atomRef index="489" />
<atomRef index="538" />
<atomRef index="545" />
<atomRef index="496" />
</cell>
<cell>
<cellProperties index="269" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="537" />
<atomRef index="586" />
<atomRef index="593" />
<atomRef index="544" />
<atomRef index="538" />
<atomRef index="587" />
<atomRef index="594" />
<atomRef index="545" />
</cell>
<cell>
<cellProperties index="270" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="586" />
<atomRef index="635" />
<atomRef index="642" />
<atomRef index="593" />
<atomRef index="587" />
<atomRef index="636" />
<atomRef index="643" />
<atomRef index="594" />
</cell>
<cell>
<cellProperties index="271" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="635" />
<atomRef index="684" />
<atomRef index="691" />
<atomRef index="642" />
<atomRef index="636" />
<atomRef index="685" />
<atomRef index="692" />
<atomRef index="643" />
</cell>
<cell>
<cellProperties index="272" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="684" />
<atomRef index="172" />
<atomRef index="179" />
<atomRef index="691" />
<atomRef index="685" />
<atomRef index="173" />
<atomRef index="180" />
<atomRef index="692" />
</cell>
<cell>
<cellProperties index="273" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="299" />
<atomRef index="397" />
<atomRef index="404" />
<atomRef index="306" />
<atomRef index="300" />
<atomRef index="398" />
<atomRef index="405" />
<atomRef index="307" />
</cell>
<cell>
<cellProperties index="274" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="397" />
<atomRef index="446" />
<atomRef index="453" />
<atomRef index="404" />
<atomRef index="398" />
<atomRef index="447" />
<atomRef index="454" />
<atomRef index="405" />
</cell>
<cell>
<cellProperties index="275" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="446" />
<atomRef index="495" />
<atomRef index="502" />
<atomRef index="453" />
<atomRef index="447" />
<atomRef index="496" />
<atomRef index="503" />
<atomRef index="454" />
</cell>
<cell>
<cellProperties index="276" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="495" />
<atomRef index="544" />
<atomRef index="551" />
<atomRef index="502" />
<atomRef index="496" />
<atomRef index="545" />
<atomRef index="552" />
<atomRef index="503" />
</cell>
<cell>
<cellProperties index="277" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="544" />
<atomRef index="593" />
<atomRef index="600" />
<atomRef index="551" />
<atomRef index="545" />
<atomRef index="594" />
<atomRef index="601" />
<atomRef index="552" />
</cell>
<cell>
<cellProperties index="278" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="593" />
<atomRef index="642" />
<atomRef index="649" />
<atomRef index="600" />
<atomRef index="594" />
<atomRef index="643" />
<atomRef index="650" />
<atomRef index="601" />
</cell>
<cell>
<cellProperties index="279" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="642" />
<atomRef index="691" />
<atomRef index="698" />
<atomRef index="649" />
<atomRef index="643" />
<atomRef index="692" />
<atomRef index="699" />
<atomRef index="650" />
</cell>
<cell>
<cellProperties index="280" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="691" />
<atomRef index="179" />
<atomRef index="186" />
<atomRef index="698" />
<atomRef index="692" />
<atomRef index="180" />
<atomRef index="187" />
<atomRef index="699" />
</cell>
<cell>
<cellProperties index="281" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="306" />
<atomRef index="404" />
<atomRef index="411" />
<atomRef index="313" />
<atomRef index="307" />
<atomRef index="405" />
<atomRef index="412" />
<atomRef index="314" />
</cell>
<cell>
<cellProperties index="282" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="404" />
<atomRef index="453" />
<atomRef index="460" />
<atomRef index="411" />
<atomRef index="405" />
<atomRef index="454" />
<atomRef index="461" />
<atomRef index="412" />
</cell>
<cell>
<cellProperties index="283" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="453" />
<atomRef index="502" />
<atomRef index="509" />
<atomRef index="460" />
<atomRef index="454" />
<atomRef index="503" />
<atomRef index="510" />
<atomRef index="461" />
</cell>
<cell>
<cellProperties index="284" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="502" />
<atomRef index="551" />
<atomRef index="558" />
<atomRef index="509" />
<atomRef index="503" />
<atomRef index="552" />
<atomRef index="559" />
<atomRef index="510" />
</cell>
<cell>
<cellProperties index="285" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="551" />
<atomRef index="600" />
<atomRef index="607" />
<atomRef index="558" />
<atomRef index="552" />
<atomRef index="601" />
<atomRef index="608" />
<atomRef index="559" />
</cell>
<cell>
<cellProperties index="286" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="600" />
<atomRef index="649" />
<atomRef index="656" />
<atomRef index="607" />
<atomRef index="601" />
<atomRef index="650" />
<atomRef index="657" />
<atomRef index="608" />
</cell>
<cell>
<cellProperties index="287" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="649" />
<atomRef index="698" />
<atomRef index="705" />
<atomRef index="656" />
<atomRef index="650" />
<atomRef index="699" />
<atomRef index="706" />
<atomRef index="657" />
</cell>
<cell>
<cellProperties index="288" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="698" />
<atomRef index="186" />
<atomRef index="193" />
<atomRef index="705" />
<atomRef index="699" />
<atomRef index="187" />
<atomRef index="194" />
<atomRef index="706" />
</cell>
<cell>
<cellProperties index="289" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="313" />
<atomRef index="411" />
<atomRef index="418" />
<atomRef index="320" />
<atomRef index="314" />
<atomRef index="412" />
<atomRef index="419" />
<atomRef index="321" />
</cell>
<cell>
<cellProperties index="290" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="411" />
<atomRef index="460" />
<atomRef index="467" />
<atomRef index="418" />
<atomRef index="412" />
<atomRef index="461" />
<atomRef index="468" />
<atomRef index="419" />
</cell>
<cell>
<cellProperties index="291" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="460" />
<atomRef index="509" />
<atomRef index="516" />
<atomRef index="467" />
<atomRef index="461" />
<atomRef index="510" />
<atomRef index="517" />
<atomRef index="468" />
</cell>
<cell>
<cellProperties index="292" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="509" />
<atomRef index="558" />
<atomRef index="565" />
<atomRef index="516" />
<atomRef index="510" />
<atomRef index="559" />
<atomRef index="566" />
<atomRef index="517" />
</cell>
<cell>
<cellProperties index="293" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="558" />
<atomRef index="607" />
<atomRef index="614" />
<atomRef index="565" />
<atomRef index="559" />
<atomRef index="608" />
<atomRef index="615" />
<atomRef index="566" />
</cell>
<cell>
<cellProperties index="294" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="607" />
<atomRef index="656" />
<atomRef index="663" />
<atomRef index="614" />
<atomRef index="608" />
<atomRef index="657" />
<atomRef index="664" />
<atomRef index="615" />
</cell>
<cell>
<cellProperties index="295" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="656" />
<atomRef index="705" />
<atomRef index="712" />
<atomRef index="663" />
<atomRef index="657" />
<atomRef index="706" />
<atomRef index="713" />
<atomRef index="664" />
</cell>
<cell>
<cellProperties index="296" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="705" />
<atomRef index="193" />
<atomRef index="200" />
<atomRef index="712" />
<atomRef index="706" />
<atomRef index="194" />
<atomRef index="201" />
<atomRef index="713" />
</cell>
<cell>
<cellProperties index="297" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="320" />
<atomRef index="418" />
<atomRef index="425" />
<atomRef index="327" />
<atomRef index="321" />
<atomRef index="419" />
<atomRef index="426" />
<atomRef index="328" />
</cell>
<cell>
<cellProperties index="298" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="418" />
<atomRef index="467" />
<atomRef index="474" />
<atomRef index="425" />
<atomRef index="419" />
<atomRef index="468" />
<atomRef index="475" />
<atomRef index="426" />
</cell>
<cell>
<cellProperties index="299" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="467" />
<atomRef index="516" />
<atomRef index="523" />
<atomRef index="474" />
<atomRef index="468" />
<atomRef index="517" />
<atomRef index="524" />
<atomRef index="475" />
</cell>
<cell>
<cellProperties index="300" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="516" />
<atomRef index="565" />
<atomRef index="572" />
<atomRef index="523" />
<atomRef index="517" />
<atomRef index="566" />
<atomRef index="573" />
<atomRef index="524" />
</cell>
<cell>
<cellProperties index="301" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="565" />
<atomRef index="614" />
<atomRef index="621" />
<atomRef index="572" />
<atomRef index="566" />
<atomRef index="615" />
<atomRef index="622" />
<atomRef index="573" />
</cell>
<cell>
<cellProperties index="302" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="614" />
<atomRef index="663" />
<atomRef index="670" />
<atomRef index="621" />
<atomRef index="615" />
<atomRef index="664" />
<atomRef index="671" />
<atomRef index="622" />
</cell>
<cell>
<cellProperties index="303" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="663" />
<atomRef index="712" />
<atomRef index="719" />
<atomRef index="670" />
<atomRef index="664" />
<atomRef index="713" />
<atomRef index="720" />
<atomRef index="671" />
</cell>
<cell>
<cellProperties index="304" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="712" />
<atomRef index="200" />
<atomRef index="207" />
<atomRef index="719" />
<atomRef index="713" />
<atomRef index="201" />
<atomRef index="208" />
<atomRef index="720" />
</cell>
<cell>
<cellProperties index="305" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="327" />
<atomRef index="425" />
<atomRef index="432" />
<atomRef index="334" />
<atomRef index="328" />
<atomRef index="426" />
<atomRef index="433" />
<atomRef index="335" />
</cell>
<cell>
<cellProperties index="306" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="425" />
<atomRef index="474" />
<atomRef index="481" />
<atomRef index="432" />
<atomRef index="426" />
<atomRef index="475" />
<atomRef index="482" />
<atomRef index="433" />
</cell>
<cell>
<cellProperties index="307" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="474" />
<atomRef index="523" />
<atomRef index="530" />
<atomRef index="481" />
<atomRef index="475" />
<atomRef index="524" />
<atomRef index="531" />
<atomRef index="482" />
</cell>
<cell>
<cellProperties index="308" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="523" />
<atomRef index="572" />
<atomRef index="579" />
<atomRef index="530" />
<atomRef index="524" />
<atomRef index="573" />
<atomRef index="580" />
<atomRef index="531" />
</cell>
<cell>
<cellProperties index="309" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="572" />
<atomRef index="621" />
<atomRef index="628" />
<atomRef index="579" />
<atomRef index="573" />
<atomRef index="622" />
<atomRef index="629" />
<atomRef index="580" />
</cell>
<cell>
<cellProperties index="310" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="621" />
<atomRef index="670" />
<atomRef index="677" />
<atomRef index="628" />
<atomRef index="622" />
<atomRef index="671" />
<atomRef index="678" />
<atomRef index="629" />
</cell>
<cell>
<cellProperties index="311" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="670" />
<atomRef index="719" />
<atomRef index="726" />
<atomRef index="677" />
<atomRef index="671" />
<atomRef index="720" />
<atomRef index="727" />
<atomRef index="678" />
</cell>
<cell>
<cellProperties index="312" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="719" />
<atomRef index="207" />
<atomRef index="214" />
<atomRef index="726" />
<atomRef index="720" />
<atomRef index="208" />
<atomRef index="215" />
<atomRef index="727" />
</cell>
<cell>
<cellProperties index="313" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="334" />
<atomRef index="432" />
<atomRef index="236" />
<atomRef index="229" />
<atomRef index="335" />
<atomRef index="433" />
<atomRef index="237" />
<atomRef index="228" />
</cell>
<cell>
<cellProperties index="314" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="432" />
<atomRef index="481" />
<atomRef index="243" />
<atomRef index="236" />
<atomRef index="433" />
<atomRef index="482" />
<atomRef index="244" />
<atomRef index="237" />
</cell>
<cell>
<cellProperties index="315" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="481" />
<atomRef index="530" />
<atomRef index="250" />
<atomRef index="243" />
<atomRef index="482" />
<atomRef index="531" />
<atomRef index="251" />
<atomRef index="244" />
</cell>
<cell>
<cellProperties index="316" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="530" />
<atomRef index="579" />
<atomRef index="257" />
<atomRef index="250" />
<atomRef index="531" />
<atomRef index="580" />
<atomRef index="258" />
<atomRef index="251" />
</cell>
<cell>
<cellProperties index="317" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="579" />
<atomRef index="628" />
<atomRef index="264" />
<atomRef index="257" />
<atomRef index="580" />
<atomRef index="629" />
<atomRef index="265" />
<atomRef index="258" />
</cell>
<cell>
<cellProperties index="318" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="628" />
<atomRef index="677" />
<atomRef index="271" />
<atomRef index="264" />
<atomRef index="629" />
<atomRef index="678" />
<atomRef index="272" />
<atomRef index="265" />
</cell>
<cell>
<cellProperties index="319" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="677" />
<atomRef index="726" />
<atomRef index="278" />
<atomRef index="271" />
<atomRef index="678" />
<atomRef index="727" />
<atomRef index="279" />
<atomRef index="272" />
</cell>
<cell>
<cellProperties index="320" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="726" />
<atomRef index="214" />
<atomRef index="158" />
<atomRef index="278" />
<atomRef index="727" />
<atomRef index="215" />
<atomRef index="157" />
<atomRef index="279" />
</cell>
<cell>
<cellProperties index="321" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="100" />
<atomRef index="109" />
<atomRef index="391" />
<atomRef index="293" />
<atomRef index="99" />
<atomRef index="110" />
<atomRef index="392" />
<atomRef index="294" />
</cell>
<cell>
<cellProperties index="322" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="109" />
<atomRef index="116" />
<atomRef index="440" />
<atomRef index="391" />
<atomRef index="110" />
<atomRef index="117" />
<atomRef index="441" />
<atomRef index="392" />
</cell>
<cell>
<cellProperties index="323" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="116" />
<atomRef index="123" />
<atomRef index="489" />
<atomRef index="440" />
<atomRef index="117" />
<atomRef index="124" />
<atomRef index="490" />
<atomRef index="441" />
</cell>
<cell>
<cellProperties index="324" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="123" />
<atomRef index="130" />
<atomRef index="538" />
<atomRef index="489" />
<atomRef index="124" />
<atomRef index="131" />
<atomRef index="539" />
<atomRef index="490" />
</cell>
<cell>
<cellProperties index="325" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="130" />
<atomRef index="137" />
<atomRef index="587" />
<atomRef index="538" />
<atomRef index="131" />
<atomRef index="138" />
<atomRef index="588" />
<atomRef index="539" />
</cell>
<cell>
<cellProperties index="326" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="137" />
<atomRef index="144" />
<atomRef index="636" />
<atomRef index="587" />
<atomRef index="138" />
<atomRef index="145" />
<atomRef index="637" />
<atomRef index="588" />
</cell>
<cell>
<cellProperties index="327" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="144" />
<atomRef index="151" />
<atomRef index="685" />
<atomRef index="636" />
<atomRef index="145" />
<atomRef index="152" />
<atomRef index="686" />
<atomRef index="637" />
</cell>
<cell>
<cellProperties index="328" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="151" />
<atomRef index="87" />
<atomRef index="173" />
<atomRef index="685" />
<atomRef index="152" />
<atomRef index="88" />
<atomRef index="174" />
<atomRef index="686" />
</cell>
<cell>
<cellProperties index="329" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="293" />
<atomRef index="391" />
<atomRef index="398" />
<atomRef index="300" />
<atomRef index="294" />
<atomRef index="392" />
<atomRef index="399" />
<atomRef index="301" />
</cell>
<cell>
<cellProperties index="330" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="391" />
<atomRef index="440" />
<atomRef index="447" />
<atomRef index="398" />
<atomRef index="392" />
<atomRef index="441" />
<atomRef index="448" />
<atomRef index="399" />
</cell>
<cell>
<cellProperties index="331" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="440" />
<atomRef index="489" />
<atomRef index="496" />
<atomRef index="447" />
<atomRef index="441" />
<atomRef index="490" />
<atomRef index="497" />
<atomRef index="448" />
</cell>
<cell>
<cellProperties index="332" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="489" />
<atomRef index="538" />
<atomRef index="545" />
<atomRef index="496" />
<atomRef index="490" />
<atomRef index="539" />
<atomRef index="546" />
<atomRef index="497" />
</cell>
<cell>
<cellProperties index="333" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="538" />
<atomRef index="587" />
<atomRef index="594" />
<atomRef index="545" />
<atomRef index="539" />
<atomRef index="588" />
<atomRef index="595" />
<atomRef index="546" />
</cell>
<cell>
<cellProperties index="334" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="587" />
<atomRef index="636" />
<atomRef index="643" />
<atomRef index="594" />
<atomRef index="588" />
<atomRef index="637" />
<atomRef index="644" />
<atomRef index="595" />
</cell>
<cell>
<cellProperties index="335" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="636" />
<atomRef index="685" />
<atomRef index="692" />
<atomRef index="643" />
<atomRef index="637" />
<atomRef index="686" />
<atomRef index="693" />
<atomRef index="644" />
</cell>
<cell>
<cellProperties index="336" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="685" />
<atomRef index="173" />
<atomRef index="180" />
<atomRef index="692" />
<atomRef index="686" />
<atomRef index="174" />
<atomRef index="181" />
<atomRef index="693" />
</cell>
<cell>
<cellProperties index="337" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="300" />
<atomRef index="398" />
<atomRef index="405" />
<atomRef index="307" />
<atomRef index="301" />
<atomRef index="399" />
<atomRef index="406" />
<atomRef index="308" />
</cell>
<cell>
<cellProperties index="338" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="398" />
<atomRef index="447" />
<atomRef index="454" />
<atomRef index="405" />
<atomRef index="399" />
<atomRef index="448" />
<atomRef index="455" />
<atomRef index="406" />
</cell>
<cell>
<cellProperties index="339" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="447" />
<atomRef index="496" />
<atomRef index="503" />
<atomRef index="454" />
<atomRef index="448" />
<atomRef index="497" />
<atomRef index="504" />
<atomRef index="455" />
</cell>
<cell>
<cellProperties index="340" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="496" />
<atomRef index="545" />
<atomRef index="552" />
<atomRef index="503" />
<atomRef index="497" />
<atomRef index="546" />
<atomRef index="553" />
<atomRef index="504" />
</cell>
<cell>
<cellProperties index="341" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="545" />
<atomRef index="594" />
<atomRef index="601" />
<atomRef index="552" />
<atomRef index="546" />
<atomRef index="595" />
<atomRef index="602" />
<atomRef index="553" />
</cell>
<cell>
<cellProperties index="342" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="594" />
<atomRef index="643" />
<atomRef index="650" />
<atomRef index="601" />
<atomRef index="595" />
<atomRef index="644" />
<atomRef index="651" />
<atomRef index="602" />
</cell>
<cell>
<cellProperties index="343" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="643" />
<atomRef index="692" />
<atomRef index="699" />
<atomRef index="650" />
<atomRef index="644" />
<atomRef index="693" />
<atomRef index="700" />
<atomRef index="651" />
</cell>
<cell>
<cellProperties index="344" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="692" />
<atomRef index="180" />
<atomRef index="187" />
<atomRef index="699" />
<atomRef index="693" />
<atomRef index="181" />
<atomRef index="188" />
<atomRef index="700" />
</cell>
<cell>
<cellProperties index="345" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="307" />
<atomRef index="405" />
<atomRef index="412" />
<atomRef index="314" />
<atomRef index="308" />
<atomRef index="406" />
<atomRef index="413" />
<atomRef index="315" />
</cell>
<cell>
<cellProperties index="346" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="405" />
<atomRef index="454" />
<atomRef index="461" />
<atomRef index="412" />
<atomRef index="406" />
<atomRef index="455" />
<atomRef index="462" />
<atomRef index="413" />
</cell>
<cell>
<cellProperties index="347" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="454" />
<atomRef index="503" />
<atomRef index="510" />
<atomRef index="461" />
<atomRef index="455" />
<atomRef index="504" />
<atomRef index="511" />
<atomRef index="462" />
</cell>
<cell>
<cellProperties index="348" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="503" />
<atomRef index="552" />
<atomRef index="559" />
<atomRef index="510" />
<atomRef index="504" />
<atomRef index="553" />
<atomRef index="560" />
<atomRef index="511" />
</cell>
<cell>
<cellProperties index="349" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="552" />
<atomRef index="601" />
<atomRef index="608" />
<atomRef index="559" />
<atomRef index="553" />
<atomRef index="602" />
<atomRef index="609" />
<atomRef index="560" />
</cell>
<cell>
<cellProperties index="350" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="601" />
<atomRef index="650" />
<atomRef index="657" />
<atomRef index="608" />
<atomRef index="602" />
<atomRef index="651" />
<atomRef index="658" />
<atomRef index="609" />
</cell>
<cell>
<cellProperties index="351" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="650" />
<atomRef index="699" />
<atomRef index="706" />
<atomRef index="657" />
<atomRef index="651" />
<atomRef index="700" />
<atomRef index="707" />
<atomRef index="658" />
</cell>
<cell>
<cellProperties index="352" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="699" />
<atomRef index="187" />
<atomRef index="194" />
<atomRef index="706" />
<atomRef index="700" />
<atomRef index="188" />
<atomRef index="195" />
<atomRef index="707" />
</cell>
<cell>
<cellProperties index="353" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="314" />
<atomRef index="412" />
<atomRef index="419" />
<atomRef index="321" />
<atomRef index="315" />
<atomRef index="413" />
<atomRef index="420" />
<atomRef index="322" />
</cell>
<cell>
<cellProperties index="354" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="412" />
<atomRef index="461" />
<atomRef index="468" />
<atomRef index="419" />
<atomRef index="413" />
<atomRef index="462" />
<atomRef index="469" />
<atomRef index="420" />
</cell>
<cell>
<cellProperties index="355" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="461" />
<atomRef index="510" />
<atomRef index="517" />
<atomRef index="468" />
<atomRef index="462" />
<atomRef index="511" />
<atomRef index="518" />
<atomRef index="469" />
</cell>
<cell>
<cellProperties index="356" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="510" />
<atomRef index="559" />
<atomRef index="566" />
<atomRef index="517" />
<atomRef index="511" />
<atomRef index="560" />
<atomRef index="567" />
<atomRef index="518" />
</cell>
<cell>
<cellProperties index="357" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="559" />
<atomRef index="608" />
<atomRef index="615" />
<atomRef index="566" />
<atomRef index="560" />
<atomRef index="609" />
<atomRef index="616" />
<atomRef index="567" />
</cell>
<cell>
<cellProperties index="358" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="608" />
<atomRef index="657" />
<atomRef index="664" />
<atomRef index="615" />
<atomRef index="609" />
<atomRef index="658" />
<atomRef index="665" />
<atomRef index="616" />
</cell>
<cell>
<cellProperties index="359" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="657" />
<atomRef index="706" />
<atomRef index="713" />
<atomRef index="664" />
<atomRef index="658" />
<atomRef index="707" />
<atomRef index="714" />
<atomRef index="665" />
</cell>
<cell>
<cellProperties index="360" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="706" />
<atomRef index="194" />
<atomRef index="201" />
<atomRef index="713" />
<atomRef index="707" />
<atomRef index="195" />
<atomRef index="202" />
<atomRef index="714" />
</cell>
<cell>
<cellProperties index="361" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="321" />
<atomRef index="419" />
<atomRef index="426" />
<atomRef index="328" />
<atomRef index="322" />
<atomRef index="420" />
<atomRef index="427" />
<atomRef index="329" />
</cell>
<cell>
<cellProperties index="362" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="419" />
<atomRef index="468" />
<atomRef index="475" />
<atomRef index="426" />
<atomRef index="420" />
<atomRef index="469" />
<atomRef index="476" />
<atomRef index="427" />
</cell>
<cell>
<cellProperties index="363" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="468" />
<atomRef index="517" />
<atomRef index="524" />
<atomRef index="475" />
<atomRef index="469" />
<atomRef index="518" />
<atomRef index="525" />
<atomRef index="476" />
</cell>
<cell>
<cellProperties index="364" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="517" />
<atomRef index="566" />
<atomRef index="573" />
<atomRef index="524" />
<atomRef index="518" />
<atomRef index="567" />
<atomRef index="574" />
<atomRef index="525" />
</cell>
<cell>
<cellProperties index="365" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="566" />
<atomRef index="615" />
<atomRef index="622" />
<atomRef index="573" />
<atomRef index="567" />
<atomRef index="616" />
<atomRef index="623" />
<atomRef index="574" />
</cell>
<cell>
<cellProperties index="366" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="615" />
<atomRef index="664" />
<atomRef index="671" />
<atomRef index="622" />
<atomRef index="616" />
<atomRef index="665" />
<atomRef index="672" />
<atomRef index="623" />
</cell>
<cell>
<cellProperties index="367" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="664" />
<atomRef index="713" />
<atomRef index="720" />
<atomRef index="671" />
<atomRef index="665" />
<atomRef index="714" />
<atomRef index="721" />
<atomRef index="672" />
</cell>
<cell>
<cellProperties index="368" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="713" />
<atomRef index="201" />
<atomRef index="208" />
<atomRef index="720" />
<atomRef index="714" />
<atomRef index="202" />
<atomRef index="209" />
<atomRef index="721" />
</cell>
<cell>
<cellProperties index="369" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="328" />
<atomRef index="426" />
<atomRef index="433" />
<atomRef index="335" />
<atomRef index="329" />
<atomRef index="427" />
<atomRef index="434" />
<atomRef index="336" />
</cell>
<cell>
<cellProperties index="370" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="426" />
<atomRef index="475" />
<atomRef index="482" />
<atomRef index="433" />
<atomRef index="427" />
<atomRef index="476" />
<atomRef index="483" />
<atomRef index="434" />
</cell>
<cell>
<cellProperties index="371" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="475" />
<atomRef index="524" />
<atomRef index="531" />
<atomRef index="482" />
<atomRef index="476" />
<atomRef index="525" />
<atomRef index="532" />
<atomRef index="483" />
</cell>
<cell>
<cellProperties index="372" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="524" />
<atomRef index="573" />
<atomRef index="580" />
<atomRef index="531" />
<atomRef index="525" />
<atomRef index="574" />
<atomRef index="581" />
<atomRef index="532" />
</cell>
<cell>
<cellProperties index="373" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="573" />
<atomRef index="622" />
<atomRef index="629" />
<atomRef index="580" />
<atomRef index="574" />
<atomRef index="623" />
<atomRef index="630" />
<atomRef index="581" />
</cell>
<cell>
<cellProperties index="374" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="622" />
<atomRef index="671" />
<atomRef index="678" />
<atomRef index="629" />
<atomRef index="623" />
<atomRef index="672" />
<atomRef index="679" />
<atomRef index="630" />
</cell>
<cell>
<cellProperties index="375" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="671" />
<atomRef index="720" />
<atomRef index="727" />
<atomRef index="678" />
<atomRef index="672" />
<atomRef index="721" />
<atomRef index="728" />
<atomRef index="679" />
</cell>
<cell>
<cellProperties index="376" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="720" />
<atomRef index="208" />
<atomRef index="215" />
<atomRef index="727" />
<atomRef index="721" />
<atomRef index="209" />
<atomRef index="216" />
<atomRef index="728" />
</cell>
<cell>
<cellProperties index="377" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="335" />
<atomRef index="433" />
<atomRef index="237" />
<atomRef index="228" />
<atomRef index="336" />
<atomRef index="434" />
<atomRef index="238" />
<atomRef index="227" />
</cell>
<cell>
<cellProperties index="378" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="433" />
<atomRef index="482" />
<atomRef index="244" />
<atomRef index="237" />
<atomRef index="434" />
<atomRef index="483" />
<atomRef index="245" />
<atomRef index="238" />
</cell>
<cell>
<cellProperties index="379" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="482" />
<atomRef index="531" />
<atomRef index="251" />
<atomRef index="244" />
<atomRef index="483" />
<atomRef index="532" />
<atomRef index="252" />
<atomRef index="245" />
</cell>
<cell>
<cellProperties index="380" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="531" />
<atomRef index="580" />
<atomRef index="258" />
<atomRef index="251" />
<atomRef index="532" />
<atomRef index="581" />
<atomRef index="259" />
<atomRef index="252" />
</cell>
<cell>
<cellProperties index="381" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="580" />
<atomRef index="629" />
<atomRef index="265" />
<atomRef index="258" />
<atomRef index="581" />
<atomRef index="630" />
<atomRef index="266" />
<atomRef index="259" />
</cell>
<cell>
<cellProperties index="382" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="629" />
<atomRef index="678" />
<atomRef index="272" />
<atomRef index="265" />
<atomRef index="630" />
<atomRef index="679" />
<atomRef index="273" />
<atomRef index="266" />
</cell>
<cell>
<cellProperties index="383" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="678" />
<atomRef index="727" />
<atomRef index="279" />
<atomRef index="272" />
<atomRef index="679" />
<atomRef index="728" />
<atomRef index="280" />
<atomRef index="273" />
</cell>
<cell>
<cellProperties index="384" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="727" />
<atomRef index="215" />
<atomRef index="157" />
<atomRef index="279" />
<atomRef index="728" />
<atomRef index="216" />
<atomRef index="156" />
<atomRef index="280" />
</cell>
<cell>
<cellProperties index="385" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="99" />
<atomRef index="110" />
<atomRef index="392" />
<atomRef index="294" />
<atomRef index="98" />
<atomRef index="111" />
<atomRef index="393" />
<atomRef index="295" />
</cell>
<cell>
<cellProperties index="386" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="110" />
<atomRef index="117" />
<atomRef index="441" />
<atomRef index="392" />
<atomRef index="111" />
<atomRef index="118" />
<atomRef index="442" />
<atomRef index="393" />
</cell>
<cell>
<cellProperties index="387" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="117" />
<atomRef index="124" />
<atomRef index="490" />
<atomRef index="441" />
<atomRef index="118" />
<atomRef index="125" />
<atomRef index="491" />
<atomRef index="442" />
</cell>
<cell>
<cellProperties index="388" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="124" />
<atomRef index="131" />
<atomRef index="539" />
<atomRef index="490" />
<atomRef index="125" />
<atomRef index="132" />
<atomRef index="540" />
<atomRef index="491" />
</cell>
<cell>
<cellProperties index="389" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="131" />
<atomRef index="138" />
<atomRef index="588" />
<atomRef index="539" />
<atomRef index="132" />
<atomRef index="139" />
<atomRef index="589" />
<atomRef index="540" />
</cell>
<cell>
<cellProperties index="390" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="138" />
<atomRef index="145" />
<atomRef index="637" />
<atomRef index="588" />
<atomRef index="139" />
<atomRef index="146" />
<atomRef index="638" />
<atomRef index="589" />
</cell>
<cell>
<cellProperties index="391" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="145" />
<atomRef index="152" />
<atomRef index="686" />
<atomRef index="637" />
<atomRef index="146" />
<atomRef index="153" />
<atomRef index="687" />
<atomRef index="638" />
</cell>
<cell>
<cellProperties index="392" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="152" />
<atomRef index="88" />
<atomRef index="174" />
<atomRef index="686" />
<atomRef index="153" />
<atomRef index="89" />
<atomRef index="175" />
<atomRef index="687" />
</cell>
<cell>
<cellProperties index="393" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="294" />
<atomRef index="392" />
<atomRef index="399" />
<atomRef index="301" />
<atomRef index="295" />
<atomRef index="393" />
<atomRef index="400" />
<atomRef index="302" />
</cell>
<cell>
<cellProperties index="394" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="392" />
<atomRef index="441" />
<atomRef index="448" />
<atomRef index="399" />
<atomRef index="393" />
<atomRef index="442" />
<atomRef index="449" />
<atomRef index="400" />
</cell>
<cell>
<cellProperties index="395" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="441" />
<atomRef index="490" />
<atomRef index="497" />
<atomRef index="448" />
<atomRef index="442" />
<atomRef index="491" />
<atomRef index="498" />
<atomRef index="449" />
</cell>
<cell>
<cellProperties index="396" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="490" />
<atomRef index="539" />
<atomRef index="546" />
<atomRef index="497" />
<atomRef index="491" />
<atomRef index="540" />
<atomRef index="547" />
<atomRef index="498" />
</cell>
<cell>
<cellProperties index="397" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="539" />
<atomRef index="588" />
<atomRef index="595" />
<atomRef index="546" />
<atomRef index="540" />
<atomRef index="589" />
<atomRef index="596" />
<atomRef index="547" />
</cell>
<cell>
<cellProperties index="398" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="588" />
<atomRef index="637" />
<atomRef index="644" />
<atomRef index="595" />
<atomRef index="589" />
<atomRef index="638" />
<atomRef index="645" />
<atomRef index="596" />
</cell>
<cell>
<cellProperties index="399" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="637" />
<atomRef index="686" />
<atomRef index="693" />
<atomRef index="644" />
<atomRef index="638" />
<atomRef index="687" />
<atomRef index="694" />
<atomRef index="645" />
</cell>
<cell>
<cellProperties index="400" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="686" />
<atomRef index="174" />
<atomRef index="181" />
<atomRef index="693" />
<atomRef index="687" />
<atomRef index="175" />
<atomRef index="182" />
<atomRef index="694" />
</cell>
<cell>
<cellProperties index="401" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="301" />
<atomRef index="399" />
<atomRef index="406" />
<atomRef index="308" />
<atomRef index="302" />
<atomRef index="400" />
<atomRef index="407" />
<atomRef index="309" />
</cell>
<cell>
<cellProperties index="402" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="399" />
<atomRef index="448" />
<atomRef index="455" />
<atomRef index="406" />
<atomRef index="400" />
<atomRef index="449" />
<atomRef index="456" />
<atomRef index="407" />
</cell>
<cell>
<cellProperties index="403" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="448" />
<atomRef index="497" />
<atomRef index="504" />
<atomRef index="455" />
<atomRef index="449" />
<atomRef index="498" />
<atomRef index="505" />
<atomRef index="456" />
</cell>
<cell>
<cellProperties index="404" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="497" />
<atomRef index="546" />
<atomRef index="553" />
<atomRef index="504" />
<atomRef index="498" />
<atomRef index="547" />
<atomRef index="554" />
<atomRef index="505" />
</cell>
<cell>
<cellProperties index="405" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="546" />
<atomRef index="595" />
<atomRef index="602" />
<atomRef index="553" />
<atomRef index="547" />
<atomRef index="596" />
<atomRef index="603" />
<atomRef index="554" />
</cell>
<cell>
<cellProperties index="406" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="595" />
<atomRef index="644" />
<atomRef index="651" />
<atomRef index="602" />
<atomRef index="596" />
<atomRef index="645" />
<atomRef index="652" />
<atomRef index="603" />
</cell>
<cell>
<cellProperties index="407" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="644" />
<atomRef index="693" />
<atomRef index="700" />
<atomRef index="651" />
<atomRef index="645" />
<atomRef index="694" />
<atomRef index="701" />
<atomRef index="652" />
</cell>
<cell>
<cellProperties index="408" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="693" />
<atomRef index="181" />
<atomRef index="188" />
<atomRef index="700" />
<atomRef index="694" />
<atomRef index="182" />
<atomRef index="189" />
<atomRef index="701" />
</cell>
<cell>
<cellProperties index="409" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="308" />
<atomRef index="406" />
<atomRef index="413" />
<atomRef index="315" />
<atomRef index="309" />
<atomRef index="407" />
<atomRef index="414" />
<atomRef index="316" />
</cell>
<cell>
<cellProperties index="410" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="406" />
<atomRef index="455" />
<atomRef index="462" />
<atomRef index="413" />
<atomRef index="407" />
<atomRef index="456" />
<atomRef index="463" />
<atomRef index="414" />
</cell>
<cell>
<cellProperties index="411" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="455" />
<atomRef index="504" />
<atomRef index="511" />
<atomRef index="462" />
<atomRef index="456" />
<atomRef index="505" />
<atomRef index="512" />
<atomRef index="463" />
</cell>
<cell>
<cellProperties index="412" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="504" />
<atomRef index="553" />
<atomRef index="560" />
<atomRef index="511" />
<atomRef index="505" />
<atomRef index="554" />
<atomRef index="561" />
<atomRef index="512" />
</cell>
<cell>
<cellProperties index="413" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="553" />
<atomRef index="602" />
<atomRef index="609" />
<atomRef index="560" />
<atomRef index="554" />
<atomRef index="603" />
<atomRef index="610" />
<atomRef index="561" />
</cell>
<cell>
<cellProperties index="414" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="602" />
<atomRef index="651" />
<atomRef index="658" />
<atomRef index="609" />
<atomRef index="603" />
<atomRef index="652" />
<atomRef index="659" />
<atomRef index="610" />
</cell>
<cell>
<cellProperties index="415" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="651" />
<atomRef index="700" />
<atomRef index="707" />
<atomRef index="658" />
<atomRef index="652" />
<atomRef index="701" />
<atomRef index="708" />
<atomRef index="659" />
</cell>
<cell>
<cellProperties index="416" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="700" />
<atomRef index="188" />
<atomRef index="195" />
<atomRef index="707" />
<atomRef index="701" />
<atomRef index="189" />
<atomRef index="196" />
<atomRef index="708" />
</cell>
<cell>
<cellProperties index="417" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="315" />
<atomRef index="413" />
<atomRef index="420" />
<atomRef index="322" />
<atomRef index="316" />
<atomRef index="414" />
<atomRef index="421" />
<atomRef index="323" />
</cell>
<cell>
<cellProperties index="418" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="413" />
<atomRef index="462" />
<atomRef index="469" />
<atomRef index="420" />
<atomRef index="414" />
<atomRef index="463" />
<atomRef index="470" />
<atomRef index="421" />
</cell>
<cell>
<cellProperties index="419" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="462" />
<atomRef index="511" />
<atomRef index="518" />
<atomRef index="469" />
<atomRef index="463" />
<atomRef index="512" />
<atomRef index="519" />
<atomRef index="470" />
</cell>
<cell>
<cellProperties index="420" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="511" />
<atomRef index="560" />
<atomRef index="567" />
<atomRef index="518" />
<atomRef index="512" />
<atomRef index="561" />
<atomRef index="568" />
<atomRef index="519" />
</cell>
<cell>
<cellProperties index="421" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="560" />
<atomRef index="609" />
<atomRef index="616" />
<atomRef index="567" />
<atomRef index="561" />
<atomRef index="610" />
<atomRef index="617" />
<atomRef index="568" />
</cell>
<cell>
<cellProperties index="422" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="609" />
<atomRef index="658" />
<atomRef index="665" />
<atomRef index="616" />
<atomRef index="610" />
<atomRef index="659" />
<atomRef index="666" />
<atomRef index="617" />
</cell>
<cell>
<cellProperties index="423" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="658" />
<atomRef index="707" />
<atomRef index="714" />
<atomRef index="665" />
<atomRef index="659" />
<atomRef index="708" />
<atomRef index="715" />
<atomRef index="666" />
</cell>
<cell>
<cellProperties index="424" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="707" />
<atomRef index="195" />
<atomRef index="202" />
<atomRef index="714" />
<atomRef index="708" />
<atomRef index="196" />
<atomRef index="203" />
<atomRef index="715" />
</cell>
<cell>
<cellProperties index="425" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="322" />
<atomRef index="420" />
<atomRef index="427" />
<atomRef index="329" />
<atomRef index="323" />
<atomRef index="421" />
<atomRef index="428" />
<atomRef index="330" />
</cell>
<cell>
<cellProperties index="426" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="420" />
<atomRef index="469" />
<atomRef index="476" />
<atomRef index="427" />
<atomRef index="421" />
<atomRef index="470" />
<atomRef index="477" />
<atomRef index="428" />
</cell>
<cell>
<cellProperties index="427" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="469" />
<atomRef index="518" />
<atomRef index="525" />
<atomRef index="476" />
<atomRef index="470" />
<atomRef index="519" />
<atomRef index="526" />
<atomRef index="477" />
</cell>
<cell>
<cellProperties index="428" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="518" />
<atomRef index="567" />
<atomRef index="574" />
<atomRef index="525" />
<atomRef index="519" />
<atomRef index="568" />
<atomRef index="575" />
<atomRef index="526" />
</cell>
<cell>
<cellProperties index="429" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="567" />
<atomRef index="616" />
<atomRef index="623" />
<atomRef index="574" />
<atomRef index="568" />
<atomRef index="617" />
<atomRef index="624" />
<atomRef index="575" />
</cell>
<cell>
<cellProperties index="430" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="616" />
<atomRef index="665" />
<atomRef index="672" />
<atomRef index="623" />
<atomRef index="617" />
<atomRef index="666" />
<atomRef index="673" />
<atomRef index="624" />
</cell>
<cell>
<cellProperties index="431" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="665" />
<atomRef index="714" />
<atomRef index="721" />
<atomRef index="672" />
<atomRef index="666" />
<atomRef index="715" />
<atomRef index="722" />
<atomRef index="673" />
</cell>
<cell>
<cellProperties index="432" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="714" />
<atomRef index="202" />
<atomRef index="209" />
<atomRef index="721" />
<atomRef index="715" />
<atomRef index="203" />
<atomRef index="210" />
<atomRef index="722" />
</cell>
<cell>
<cellProperties index="433" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="329" />
<atomRef index="427" />
<atomRef index="434" />
<atomRef index="336" />
<atomRef index="330" />
<atomRef index="428" />
<atomRef index="435" />
<atomRef index="337" />
</cell>
<cell>
<cellProperties index="434" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="427" />
<atomRef index="476" />
<atomRef index="483" />
<atomRef index="434" />
<atomRef index="428" />
<atomRef index="477" />
<atomRef index="484" />
<atomRef index="435" />
</cell>
<cell>
<cellProperties index="435" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="476" />
<atomRef index="525" />
<atomRef index="532" />
<atomRef index="483" />
<atomRef index="477" />
<atomRef index="526" />
<atomRef index="533" />
<atomRef index="484" />
</cell>
<cell>
<cellProperties index="436" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="525" />
<atomRef index="574" />
<atomRef index="581" />
<atomRef index="532" />
<atomRef index="526" />
<atomRef index="575" />
<atomRef index="582" />
<atomRef index="533" />
</cell>
<cell>
<cellProperties index="437" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="574" />
<atomRef index="623" />
<atomRef index="630" />
<atomRef index="581" />
<atomRef index="575" />
<atomRef index="624" />
<atomRef index="631" />
<atomRef index="582" />
</cell>
<cell>
<cellProperties index="438" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="623" />
<atomRef index="672" />
<atomRef index="679" />
<atomRef index="630" />
<atomRef index="624" />
<atomRef index="673" />
<atomRef index="680" />
<atomRef index="631" />
</cell>
<cell>
<cellProperties index="439" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="672" />
<atomRef index="721" />
<atomRef index="728" />
<atomRef index="679" />
<atomRef index="673" />
<atomRef index="722" />
<atomRef index="729" />
<atomRef index="680" />
</cell>
<cell>
<cellProperties index="440" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="721" />
<atomRef index="209" />
<atomRef index="216" />
<atomRef index="728" />
<atomRef index="722" />
<atomRef index="210" />
<atomRef index="217" />
<atomRef index="729" />
</cell>
<cell>
<cellProperties index="441" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="336" />
<atomRef index="434" />
<atomRef index="238" />
<atomRef index="227" />
<atomRef index="337" />
<atomRef index="435" />
<atomRef index="239" />
<atomRef index="226" />
</cell>
<cell>
<cellProperties index="442" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="434" />
<atomRef index="483" />
<atomRef index="245" />
<atomRef index="238" />
<atomRef index="435" />
<atomRef index="484" />
<atomRef index="246" />
<atomRef index="239" />
</cell>
<cell>
<cellProperties index="443" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="483" />
<atomRef index="532" />
<atomRef index="252" />
<atomRef index="245" />
<atomRef index="484" />
<atomRef index="533" />
<atomRef index="253" />
<atomRef index="246" />
</cell>
<cell>
<cellProperties index="444" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="532" />
<atomRef index="581" />
<atomRef index="259" />
<atomRef index="252" />
<atomRef index="533" />
<atomRef index="582" />
<atomRef index="260" />
<atomRef index="253" />
</cell>
<cell>
<cellProperties index="445" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="581" />
<atomRef index="630" />
<atomRef index="266" />
<atomRef index="259" />
<atomRef index="582" />
<atomRef index="631" />
<atomRef index="267" />
<atomRef index="260" />
</cell>
<cell>
<cellProperties index="446" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="630" />
<atomRef index="679" />
<atomRef index="273" />
<atomRef index="266" />
<atomRef index="631" />
<atomRef index="680" />
<atomRef index="274" />
<atomRef index="267" />
</cell>
<cell>
<cellProperties index="447" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="679" />
<atomRef index="728" />
<atomRef index="280" />
<atomRef index="273" />
<atomRef index="680" />
<atomRef index="729" />
<atomRef index="281" />
<atomRef index="274" />
</cell>
<cell>
<cellProperties index="448" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="728" />
<atomRef index="216" />
<atomRef index="156" />
<atomRef index="280" />
<atomRef index="729" />
<atomRef index="217" />
<atomRef index="155" />
<atomRef index="281" />
</cell>
<cell>
<cellProperties index="449" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="98" />
<atomRef index="111" />
<atomRef index="393" />
<atomRef index="295" />
<atomRef index="90" />
<atomRef index="97" />
<atomRef index="338" />
<atomRef index="288" />
</cell>
<cell>
<cellProperties index="450" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="111" />
<atomRef index="118" />
<atomRef index="442" />
<atomRef index="393" />
<atomRef index="97" />
<atomRef index="96" />
<atomRef index="345" />
<atomRef index="338" />
</cell>
<cell>
<cellProperties index="451" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="118" />
<atomRef index="125" />
<atomRef index="491" />
<atomRef index="442" />
<atomRef index="96" />
<atomRef index="95" />
<atomRef index="352" />
<atomRef index="345" />
</cell>
<cell>
<cellProperties index="452" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="125" />
<atomRef index="132" />
<atomRef index="540" />
<atomRef index="491" />
<atomRef index="95" />
<atomRef index="94" />
<atomRef index="359" />
<atomRef index="352" />
</cell>
<cell>
<cellProperties index="453" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="132" />
<atomRef index="139" />
<atomRef index="589" />
<atomRef index="540" />
<atomRef index="94" />
<atomRef index="93" />
<atomRef index="366" />
<atomRef index="359" />
</cell>
<cell>
<cellProperties index="454" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="139" />
<atomRef index="146" />
<atomRef index="638" />
<atomRef index="589" />
<atomRef index="93" />
<atomRef index="92" />
<atomRef index="373" />
<atomRef index="366" />
</cell>
<cell>
<cellProperties index="455" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="146" />
<atomRef index="153" />
<atomRef index="687" />
<atomRef index="638" />
<atomRef index="92" />
<atomRef index="91" />
<atomRef index="380" />
<atomRef index="373" />
</cell>
<cell>
<cellProperties index="456" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="153" />
<atomRef index="89" />
<atomRef index="175" />
<atomRef index="687" />
<atomRef index="91" />
<atomRef index="82" />
<atomRef index="162" />
<atomRef index="380" />
</cell>
<cell>
<cellProperties index="457" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="295" />
<atomRef index="393" />
<atomRef index="400" />
<atomRef index="302" />
<atomRef index="288" />
<atomRef index="338" />
<atomRef index="339" />
<atomRef index="287" />
</cell>
<cell>
<cellProperties index="458" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="393" />
<atomRef index="442" />
<atomRef index="449" />
<atomRef index="400" />
<atomRef index="338" />
<atomRef index="345" />
<atomRef index="346" />
<atomRef index="339" />
</cell>
<cell>
<cellProperties index="459" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="442" />
<atomRef index="491" />
<atomRef index="498" />
<atomRef index="449" />
<atomRef index="345" />
<atomRef index="352" />
<atomRef index="353" />
<atomRef index="346" />
</cell>
<cell>
<cellProperties index="460" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="491" />
<atomRef index="540" />
<atomRef index="547" />
<atomRef index="498" />
<atomRef index="352" />
<atomRef index="359" />
<atomRef index="360" />
<atomRef index="353" />
</cell>
<cell>
<cellProperties index="461" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="540" />
<atomRef index="589" />
<atomRef index="596" />
<atomRef index="547" />
<atomRef index="359" />
<atomRef index="366" />
<atomRef index="367" />
<atomRef index="360" />
</cell>
<cell>
<cellProperties index="462" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="589" />
<atomRef index="638" />
<atomRef index="645" />
<atomRef index="596" />
<atomRef index="366" />
<atomRef index="373" />
<atomRef index="374" />
<atomRef index="367" />
</cell>
<cell>
<cellProperties index="463" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="638" />
<atomRef index="687" />
<atomRef index="694" />
<atomRef index="645" />
<atomRef index="373" />
<atomRef index="380" />
<atomRef index="381" />
<atomRef index="374" />
</cell>
<cell>
<cellProperties index="464" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="687" />
<atomRef index="175" />
<atomRef index="182" />
<atomRef index="694" />
<atomRef index="380" />
<atomRef index="162" />
<atomRef index="163" />
<atomRef index="381" />
</cell>
<cell>
<cellProperties index="465" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="302" />
<atomRef index="400" />
<atomRef index="407" />
<atomRef index="309" />
<atomRef index="287" />
<atomRef index="339" />
<atomRef index="340" />
<atomRef index="286" />
</cell>
<cell>
<cellProperties index="466" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="400" />
<atomRef index="449" />
<atomRef index="456" />
<atomRef index="407" />
<atomRef index="339" />
<atomRef index="346" />
<atomRef index="347" />
<atomRef index="340" />
</cell>
<cell>
<cellProperties index="467" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="449" />
<atomRef index="498" />
<atomRef index="505" />
<atomRef index="456" />
<atomRef index="346" />
<atomRef index="353" />
<atomRef index="354" />
<atomRef index="347" />
</cell>
<cell>
<cellProperties index="468" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="498" />
<atomRef index="547" />
<atomRef index="554" />
<atomRef index="505" />
<atomRef index="353" />
<atomRef index="360" />
<atomRef index="361" />
<atomRef index="354" />
</cell>
<cell>
<cellProperties index="469" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="547" />
<atomRef index="596" />
<atomRef index="603" />
<atomRef index="554" />
<atomRef index="360" />
<atomRef index="367" />
<atomRef index="368" />
<atomRef index="361" />
</cell>
<cell>
<cellProperties index="470" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="596" />
<atomRef index="645" />
<atomRef index="652" />
<atomRef index="603" />
<atomRef index="367" />
<atomRef index="374" />
<atomRef index="375" />
<atomRef index="368" />
</cell>
<cell>
<cellProperties index="471" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="645" />
<atomRef index="694" />
<atomRef index="701" />
<atomRef index="652" />
<atomRef index="374" />
<atomRef index="381" />
<atomRef index="382" />
<atomRef index="375" />
</cell>
<cell>
<cellProperties index="472" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="694" />
<atomRef index="182" />
<atomRef index="189" />
<atomRef index="701" />
<atomRef index="381" />
<atomRef index="163" />
<atomRef index="164" />
<atomRef index="382" />
</cell>
<cell>
<cellProperties index="473" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="309" />
<atomRef index="407" />
<atomRef index="414" />
<atomRef index="316" />
<atomRef index="286" />
<atomRef index="340" />
<atomRef index="341" />
<atomRef index="285" />
</cell>
<cell>
<cellProperties index="474" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="407" />
<atomRef index="456" />
<atomRef index="463" />
<atomRef index="414" />
<atomRef index="340" />
<atomRef index="347" />
<atomRef index="348" />
<atomRef index="341" />
</cell>
<cell>
<cellProperties index="475" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="456" />
<atomRef index="505" />
<atomRef index="512" />
<atomRef index="463" />
<atomRef index="347" />
<atomRef index="354" />
<atomRef index="355" />
<atomRef index="348" />
</cell>
<cell>
<cellProperties index="476" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="505" />
<atomRef index="554" />
<atomRef index="561" />
<atomRef index="512" />
<atomRef index="354" />
<atomRef index="361" />
<atomRef index="362" />
<atomRef index="355" />
</cell>
<cell>
<cellProperties index="477" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="554" />
<atomRef index="603" />
<atomRef index="610" />
<atomRef index="561" />
<atomRef index="361" />
<atomRef index="368" />
<atomRef index="369" />
<atomRef index="362" />
</cell>
<cell>
<cellProperties index="478" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="603" />
<atomRef index="652" />
<atomRef index="659" />
<atomRef index="610" />
<atomRef index="368" />
<atomRef index="375" />
<atomRef index="376" />
<atomRef index="369" />
</cell>
<cell>
<cellProperties index="479" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="652" />
<atomRef index="701" />
<atomRef index="708" />
<atomRef index="659" />
<atomRef index="375" />
<atomRef index="382" />
<atomRef index="383" />
<atomRef index="376" />
</cell>
<cell>
<cellProperties index="480" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="701" />
<atomRef index="189" />
<atomRef index="196" />
<atomRef index="708" />
<atomRef index="382" />
<atomRef index="164" />
<atomRef index="165" />
<atomRef index="383" />
</cell>
<cell>
<cellProperties index="481" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="316" />
<atomRef index="414" />
<atomRef index="421" />
<atomRef index="323" />
<atomRef index="285" />
<atomRef index="341" />
<atomRef index="342" />
<atomRef index="284" />
</cell>
<cell>
<cellProperties index="482" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="414" />
<atomRef index="463" />
<atomRef index="470" />
<atomRef index="421" />
<atomRef index="341" />
<atomRef index="348" />
<atomRef index="349" />
<atomRef index="342" />
</cell>
<cell>
<cellProperties index="483" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="463" />
<atomRef index="512" />
<atomRef index="519" />
<atomRef index="470" />
<atomRef index="348" />
<atomRef index="355" />
<atomRef index="356" />
<atomRef index="349" />
</cell>
<cell>
<cellProperties index="484" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="512" />
<atomRef index="561" />
<atomRef index="568" />
<atomRef index="519" />
<atomRef index="355" />
<atomRef index="362" />
<atomRef index="363" />
<atomRef index="356" />
</cell>
<cell>
<cellProperties index="485" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="561" />
<atomRef index="610" />
<atomRef index="617" />
<atomRef index="568" />
<atomRef index="362" />
<atomRef index="369" />
<atomRef index="370" />
<atomRef index="363" />
</cell>
<cell>
<cellProperties index="486" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="610" />
<atomRef index="659" />
<atomRef index="666" />
<atomRef index="617" />
<atomRef index="369" />
<atomRef index="376" />
<atomRef index="377" />
<atomRef index="370" />
</cell>
<cell>
<cellProperties index="487" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="659" />
<atomRef index="708" />
<atomRef index="715" />
<atomRef index="666" />
<atomRef index="376" />
<atomRef index="383" />
<atomRef index="384" />
<atomRef index="377" />
</cell>
<cell>
<cellProperties index="488" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="708" />
<atomRef index="196" />
<atomRef index="203" />
<atomRef index="715" />
<atomRef index="383" />
<atomRef index="165" />
<atomRef index="166" />
<atomRef index="384" />
</cell>
<cell>
<cellProperties index="489" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="323" />
<atomRef index="421" />
<atomRef index="428" />
<atomRef index="330" />
<atomRef index="284" />
<atomRef index="342" />
<atomRef index="343" />
<atomRef index="283" />
</cell>
<cell>
<cellProperties index="490" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="421" />
<atomRef index="470" />
<atomRef index="477" />
<atomRef index="428" />
<atomRef index="342" />
<atomRef index="349" />
<atomRef index="350" />
<atomRef index="343" />
</cell>
<cell>
<cellProperties index="491" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="470" />
<atomRef index="519" />
<atomRef index="526" />
<atomRef index="477" />
<atomRef index="349" />
<atomRef index="356" />
<atomRef index="357" />
<atomRef index="350" />
</cell>
<cell>
<cellProperties index="492" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="519" />
<atomRef index="568" />
<atomRef index="575" />
<atomRef index="526" />
<atomRef index="356" />
<atomRef index="363" />
<atomRef index="364" />
<atomRef index="357" />
</cell>
<cell>
<cellProperties index="493" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="568" />
<atomRef index="617" />
<atomRef index="624" />
<atomRef index="575" />
<atomRef index="363" />
<atomRef index="370" />
<atomRef index="371" />
<atomRef index="364" />
</cell>
<cell>
<cellProperties index="494" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="617" />
<atomRef index="666" />
<atomRef index="673" />
<atomRef index="624" />
<atomRef index="370" />
<atomRef index="377" />
<atomRef index="378" />
<atomRef index="371" />
</cell>
<cell>
<cellProperties index="495" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="666" />
<atomRef index="715" />
<atomRef index="722" />
<atomRef index="673" />
<atomRef index="377" />
<atomRef index="384" />
<atomRef index="385" />
<atomRef index="378" />
</cell>
<cell>
<cellProperties index="496" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="715" />
<atomRef index="203" />
<atomRef index="210" />
<atomRef index="722" />
<atomRef index="384" />
<atomRef index="166" />
<atomRef index="167" />
<atomRef index="385" />
</cell>
<cell>
<cellProperties index="497" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="330" />
<atomRef index="428" />
<atomRef index="435" />
<atomRef index="337" />
<atomRef index="283" />
<atomRef index="343" />
<atomRef index="344" />
<atomRef index="282" />
</cell>
<cell>
<cellProperties index="498" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="428" />
<atomRef index="477" />
<atomRef index="484" />
<atomRef index="435" />
<atomRef index="343" />
<atomRef index="350" />
<atomRef index="351" />
<atomRef index="344" />
</cell>
<cell>
<cellProperties index="499" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="477" />
<atomRef index="526" />
<atomRef index="533" />
<atomRef index="484" />
<atomRef index="350" />
<atomRef index="357" />
<atomRef index="358" />
<atomRef index="351" />
</cell>
<cell>
<cellProperties index="500" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="526" />
<atomRef index="575" />
<atomRef index="582" />
<atomRef index="533" />
<atomRef index="357" />
<atomRef index="364" />
<atomRef index="365" />
<atomRef index="358" />
</cell>
<cell>
<cellProperties index="501" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="575" />
<atomRef index="624" />
<atomRef index="631" />
<atomRef index="582" />
<atomRef index="364" />
<atomRef index="371" />
<atomRef index="372" />
<atomRef index="365" />
</cell>
<cell>
<cellProperties index="502" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="624" />
<atomRef index="673" />
<atomRef index="680" />
<atomRef index="631" />
<atomRef index="371" />
<atomRef index="378" />
<atomRef index="379" />
<atomRef index="372" />
</cell>
<cell>
<cellProperties index="503" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="673" />
<atomRef index="722" />
<atomRef index="729" />
<atomRef index="680" />
<atomRef index="378" />
<atomRef index="385" />
<atomRef index="386" />
<atomRef index="379" />
</cell>
<cell>
<cellProperties index="504" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="722" />
<atomRef index="210" />
<atomRef index="217" />
<atomRef index="729" />
<atomRef index="385" />
<atomRef index="167" />
<atomRef index="168" />
<atomRef index="386" />
</cell>
<cell>
<cellProperties index="505" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="337" />
<atomRef index="435" />
<atomRef index="239" />
<atomRef index="226" />
<atomRef index="282" />
<atomRef index="344" />
<atomRef index="225" />
<atomRef index="218" />
</cell>
<cell>
<cellProperties index="506" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="435" />
<atomRef index="484" />
<atomRef index="246" />
<atomRef index="239" />
<atomRef index="344" />
<atomRef index="351" />
<atomRef index="224" />
<atomRef index="225" />
</cell>
<cell>
<cellProperties index="507" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="484" />
<atomRef index="533" />
<atomRef index="253" />
<atomRef index="246" />
<atomRef index="351" />
<atomRef index="358" />
<atomRef index="223" />
<atomRef index="224" />
</cell>
<cell>
<cellProperties index="508" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="533" />
<atomRef index="582" />
<atomRef index="260" />
<atomRef index="253" />
<atomRef index="358" />
<atomRef index="365" />
<atomRef index="222" />
<atomRef index="223" />
</cell>
<cell>
<cellProperties index="509" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="582" />
<atomRef index="631" />
<atomRef index="267" />
<atomRef index="260" />
<atomRef index="365" />
<atomRef index="372" />
<atomRef index="221" />
<atomRef index="222" />
</cell>
<cell>
<cellProperties index="510" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="631" />
<atomRef index="680" />
<atomRef index="274" />
<atomRef index="267" />
<atomRef index="372" />
<atomRef index="379" />
<atomRef index="220" />
<atomRef index="221" />
</cell>
<cell>
<cellProperties index="511" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="680" />
<atomRef index="729" />
<atomRef index="281" />
<atomRef index="274" />
<atomRef index="379" />
<atomRef index="386" />
<atomRef index="219" />
<atomRef index="220" />
</cell>
<cell>
<cellProperties index="512" type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="729" />
<atomRef index="217" />
<atomRef index="155" />
<atomRef index="281" />
<atomRef index="386" />
<atomRef index="168" />
<atomRef index="154" />
<atomRef index="219" />
</cell>
</structuralComponent>
</multiComponent>
</exclusiveComponents>
<!-- list of informative components : -->
<informativeComponents>
</informativeComponents>
</physicalModel>

// Doxygen SDK groups creation
/**
 * @defgroup group_cepmodeling CEP Modeling
 * The CEP Modeling gathers CamiTK biomechanical / simulation extensions.
 *
 * @note
 * By default, this CEP is not configured using CMake, please select it at the configuration step if you wish to compile it.
 *
 *
 * @defgroup group_cepmodeling_actions Actions
 * @ingroup group_cepmodeling
 * Image processing actions.
 * The CEP Imaging actions are based on ITK.
 *
 *
 * @defgroup group_cepmodeling_actions_mml MML
 * @ingroup group_cepmodeling_actions
 * @ref camitk::Action "Action" to compare different biomechanical models / engine results.
 *
 *
 * @defgroup group_cepmodeling_components Components
 * @ingroup group_cepmodeling
 * @ref camitk::MeshComponent "MeshComponent" dedicated to biomechanical simulation.
 *
 *
 * @defgroup group_cepmodeling_components_mml MML
 * @ingroup group_cepmodeling_components
 * Structure to compare different biomechanical models / engine results.
 *
 *
 * @defgroup group_cepmodeling_components_physicalmodel Physical Model
 * @ingroup group_cepmodeling_components
 * Manage the physical model of a 3D mesh. It features a structure to handle the physical constraints applied on it.
 *
 *
 * @defgroup group_cepmodeling_libraries Libraries
 * @ingroup group_cepmodeling
 * Here you will find the MML, LML and PML libraries.
 *
 *
 * @defgroup group_cepmodeling_libraries_lml LML
 * @ingroup group_cepmodeling_libraries
 * Load Markup Language.
 * LML stands for Load Markup Language. It is a XML language designed to describe forces or constraints applied to one or many "points" for a given time and a given intensity.
 *
 * - To learn more about LML, go to the <a href="http://www-timc.imag.fr/Emmanuel.Promayon/PML">PML/LML website</a>
 * - If you would like to help please check the TODO
 * - To know the exact current LML library version use Loads::VERSION
 *
 *
 * @defgroup group_cepmodeling_libraries_mml MML
 * @ingroup group_cepmodeling_libraries
 * MML gathers the LML and PML information into one single XML file.
 *
 *
 * @defgroup group_cepmodeling_libraries_pml PML
 * @ingroup group_cepmodeling_libraries
 * Physical Model markup Language.
 * PML stands for Physical Model markup Language. It is an XML language designed to describe tissues and structures as physical objects using a model (discrete, continuous, ...) in order to perform a medical simulatio
 *
 * - To learn more about PML, go to the <a href="http://www-timc.imag.fr/Emmanuel.Promayon/PML">PML/LML website</a>
 * - If you would like to help please check the TODO
 * - To know the exact current PML library version use PhysicalModel::VERSION
 * n.
 *
 *
 *
 **/

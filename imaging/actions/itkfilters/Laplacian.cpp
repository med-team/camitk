/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "Laplacian.h"

// CamiTK includes
#include <Application.h>
#include <ItkProgressObserver.h>
#include <Property.h>

// Itk includes
#include <itkImageToVTKImageFilter.h>
#include <itkVTKImageToImageFilter.h>
#include <itkCastImageFilter.h>
#include <itkRescaleIntensityImageFilter.h>
#include <itkLaplacianImageFilter.h>

using namespace camitk;


// --------------- constructor -------------------
Laplacian::Laplacian(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Laplacian Filter");
    setDescription("<br/>This filter computes the <b>Laplacian of a scalar-valued image</b>.<br/><br/> \
                   The Laplacian is an isotropic measure of the second spatial derivative of an image. <br/><br/> \
                   The Laplacian of an image highlights regions of rapid intensity change and is therefore often used for edge detection. Often, the Laplacian is applied to an image that has first been smoothed with a Gaussian filter in order to reduce its sensitivity to noise.<br/><br/> \
				   <i>(source: ITK Developer's Guide)</i><br> \
				   ");
    setComponentClassName("ImageComponent");

    // Setting classification family and tags
    this->setFamily("ITK Filter");
    this->addTag("Derivative");
    this->addTag("Edge Detection");
    this->addTag("Contours");

    // Setting parameters default values
    Property* keepOrgVoxelTypeProperty = new Property(tr("Keep original voxel type?"), true, tr("Does the output image have the same voxel type as the input one?"), "");
    addParameter(keepOrgVoxelTypeProperty);
}

// --------------- destructor -------------------
Laplacian::~Laplacian() {
    // do not delete the widget has it might have been used in the ActionViewer (i.e. the ownership might have been taken by the stacked widget)
}

// --------------- apply -------------------
Action::ApplyStatus Laplacian::apply() {
    foreach (Component* comp, getTargets()) {
        ImageComponent* input = dynamic_cast<ImageComponent*>(comp);
        process(input);
    }
    return SUCCESS;
}

void Laplacian::process(ImageComponent* comp) {
    // Get the parameters
    this->keepOrgVoxelType = property("Keep original voxel type?").toBool();

    // ITK filter implementation using templates
    vtkSmartPointer<vtkImageData> inputImage = comp->getImageData();
    vtkSmartPointer<vtkImageData> outputImage = implementProcess(inputImage);
    ImageComponent* outputComp = new ImageComponent(outputImage, comp->getName() + "_laplacian");

    // consider frame policy on new image created
    Action::applyTargetPosition(comp, outputComp);

    Application::refresh();

}

#include "Laplacian.impl"

// ITK filter implementation
template <class InputPixelType, class OutputPixelType, const int dim>
vtkSmartPointer<vtkImageData> Laplacian::itkProcess(vtkSmartPointer<vtkImageData> img) {
    vtkSmartPointer<vtkImageData> result = vtkSmartPointer<vtkImageData>::New();
    vtkSmartPointer<vtkImageData> resultImage;

    // --------------------- Filters declaration and creation ----------------------
    // Define ITK input and output image types with respect to the instantiation
    //    types of the tamplate.
    typedef itk::Image< InputPixelType,  dim > InputImageType;
    typedef itk::Image< OutputPixelType, dim > OutputImageType;

    // Convert the image from CamiTK in VTK format to ITK format to use ITK filters.
    typedef itk::VTKImageToImageFilter<InputImageType> vtkToItkFilterType;
    typename vtkToItkFilterType::Pointer vtkToItkFilter = vtkToItkFilterType::New();

    // Declare and create your own private ITK filter here...
    typedef itk::CastImageFilter<InputImageType, OutputImageType> CastFilterType;
    typename CastFilterType::Pointer toDoubleFilter = CastFilterType::New();

    typedef itk::LaplacianImageFilter<OutputImageType, OutputImageType> LaplacianFilterType;
    typename LaplacianFilterType::Pointer laplacianFilter = LaplacianFilterType::New();

    typedef itk::RescaleIntensityImageFilter<OutputImageType, InputImageType> ToOrgFilterType;
    typename ToOrgFilterType::Pointer toOrgFilter = ToOrgFilterType::New();

    // In the same way, once the image is filtered, we need to convert it again to
    // VTK format to give it to CamiTK.
    typedef itk::ImageToVTKImageFilter<OutputImageType> ItkToVtkFloatFilterType;
    typename ItkToVtkFloatFilterType::Pointer itkToVtkFloatFilter = ItkToVtkFloatFilterType::New();

    typedef itk::ImageToVTKImageFilter<InputImageType> ItkToVtkOrgFilterType;
    typename ItkToVtkOrgFilterType::Pointer itkToVtkOrgFilter = ItkToVtkOrgFilterType::New();


    // To update CamiTK progress bar while filtering, add an ITK observer to the filters.
    ItkProgressObserver::Pointer observer = ItkProgressObserver::New();
    // ITK observers generally give values between 0 and 1, and CamiTK progress bar
    //    wants values between 0 and 100...
    observer->SetCoef(100.0);

    // --------------------- Plug filters and parameters ---------------------------
    // From VTK to ITK
    vtkToItkFilter->SetInput(img);
    vtkToItkFilter->SetInput(img);
    vtkToItkFilter->AddObserver(itk::ProgressEvent(), observer);
    vtkToItkFilter->Update();
    observer->Reset();

    toDoubleFilter->SetInput(vtkToItkFilter->GetOutput());
    toDoubleFilter->AddObserver(itk::ProgressEvent(), observer);
    toDoubleFilter->Update();
    observer->Reset();

    laplacianFilter->SetInput(toDoubleFilter->GetOutput());
    laplacianFilter->AddObserver(itk::ProgressEvent(), observer);
    laplacianFilter->Update();
    observer->Reset();

    if (keepOrgVoxelType) {
        toOrgFilter->SetInput(laplacianFilter->GetOutput());
        toOrgFilter->AddObserver(itk::ProgressEvent(), observer);
        toOrgFilter->Update();
        observer->Reset();

        itkToVtkOrgFilter->SetInput(toOrgFilter->GetOutput());
        itkToVtkOrgFilter->AddObserver(itk::ProgressEvent(), observer);
        itkToVtkOrgFilter->Update();
        observer->Reset();

        resultImage = itkToVtkOrgFilter->GetOutput();
    }
    else {
        itkToVtkFloatFilter->SetInput(laplacianFilter->GetOutput());
        itkToVtkFloatFilter->AddObserver(itk::ProgressEvent(), observer);
        itkToVtkFloatFilter->Update();
        observer->Reset();

        resultImage = itkToVtkFloatFilter->GetOutput();
    }

    // --------------------- Create and return a copy (the filters will be deleted)--
    int extent[6];
    resultImage->GetExtent(extent);
    result->SetExtent(extent);
    result->DeepCopy(resultImage);

    // Set CamiTK progress bar back to zero (the processing filter is over)
    observer->Reset();

    return result;
}


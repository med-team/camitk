/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
// CamiTK includes
#include "MedianFilter.h"
#include <Application.h>
#include <ItkProgressObserver.h>
#include <Property.h>

// Itk includes
#include <itkImageToVTKImageFilter.h>
#include <itkVTKImageToImageFilter.h>
#include <itkMedianImageFilter.h>

using namespace camitk;


// --------------- constructor -------------------
MedianFilter::MedianFilter(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Median Filter");
    setDescription("<br> \
The <b>median</b> filter is commonly used as a reobust approach for noise reduction.<br> \
This filter is particularly efficient against <i>salt-and-pepper</i> noise. In other words, it is robust to the presence of gray-level outliers. <br> \
This filter computes the value of each output pixel as the statistical median of the neighborhood of values around the corresponding input pixel. The following figure illustrates the local effect of this filter in 2D case.<br>\
_____________ <br> \
| 28 | 26 | 50 | <br> \
|----|-----|----| <br> \
| 27 | 25 | 29 |  -> 28<br> \
|----|-----|----| <br> \
| 25 | 30 | 32 | <br> \
----------------- <br> \
<br> \
The <b>parameters</b> are the size of the neighborhood along X, Y and Z directions. The value on each direction is used as the semi-size of a rectangular box. For example in <i>2D</i> a size of 1 in X direction and 2 in Y direction results in a 3x5 neighborhood.<br> \
");
    setComponentClassName("ImageComponent");

    // Setting classification family and tags
    this->setFamily("ITK Filter");
    this->addTag("Median");
    this->addTag("Smoothing");
    this->addTag("Neighborhood Filter");

    // Setting parameters default values
    Property* halfNeighborhoodSizeProp_X = new Property(tr("Half neighborhood size along X"), 1, tr("Half the size of the X direction of the neighborhood taken into account for the mean computation. \nFor instance, a value of 2 will create a windows of size 4 along the X direction."), "");
    halfNeighborhoodSizeProp_X->setAttribute("minimum", 1);
    halfNeighborhoodSizeProp_X->setAttribute("maximum", 100);
    halfNeighborhoodSizeProp_X->setAttribute("singleStep", 1);
    addParameter(halfNeighborhoodSizeProp_X);

    Property* halfNeighborhoodSizeProp_Y = new Property(tr("Half neighborhood size along Y"), 1, tr("Half the size of the Y direction of the neighborhood taken into account for the mean computation. \nFor instance, a value of 2 will create a windows of size 4 along the Y direction."), "");
    halfNeighborhoodSizeProp_Y->setAttribute("minimum", 1);
    halfNeighborhoodSizeProp_Y->setAttribute("maximum", 100);
    halfNeighborhoodSizeProp_Y->setAttribute("singleStep", 1);
    addParameter(halfNeighborhoodSizeProp_Y);

    Property* halfNeighborhoodSizeProp_Z = new Property(tr("Half neighborhood size along Z"), 1, tr("Half the size of the Z direction of the neighborhood taken into account for the mean computation. \nFor instance, a value of 2 will create a windows of size 4 along the Z direction."), "");
    halfNeighborhoodSizeProp_Z->setAttribute("minimum", 1);
    halfNeighborhoodSizeProp_Z->setAttribute("maximum", 100);
    halfNeighborhoodSizeProp_Z->setAttribute("singleStep", 1);
    addParameter(halfNeighborhoodSizeProp_Z);
}

// --------------- destructor -------------------
MedianFilter::~MedianFilter() {
    // do not delete the widget has it might have been used in the ActionViewer (i.e. the ownership might have been taken by the stacked widget)
}

// --------------- apply -------------------
Action::ApplyStatus MedianFilter::apply() {
    foreach (Component* comp, getTargets()) {
        ImageComponent* input = dynamic_cast<ImageComponent*>(comp);
        process(input);
    }
    return SUCCESS;
}

void MedianFilter::process(ImageComponent* comp) {
    // Get the parameters
    this->halfNeighborhoodSizeX = property("Half neighborhood size along X").toInt();
    this->halfNeighborhoodSizeY = property("Half neighborhood size along Y").toInt();
    this->halfNeighborhoodSizeZ = property("Half neighborhood size along Z").toInt();

    // ITK filter implementation using templates
    vtkSmartPointer<vtkImageData> inputImage = comp->getImageData();
    vtkSmartPointer<vtkImageData> outputImage = implementProcess(inputImage);

    ImageComponent* outputComp = new ImageComponent(outputImage, comp->getName() + "_median");

    // consider frame policy on new image created
    Action::applyTargetPosition(comp, outputComp);

    Application::refresh();

}

#include "MedianFilter.impl"

// ITK filter implementation
template <class InputPixelType, class OutputPixelType, const int dim>
vtkSmartPointer<vtkImageData> MedianFilter::itkProcess(vtkSmartPointer<vtkImageData> img) {
    vtkSmartPointer<vtkImageData> result = vtkSmartPointer<vtkImageData>::New();

    // --------------------- Filters declaration and creation ----------------------
    // Define ITK input and output image types with respect to the instantiation
    //    types of the tamplate.
    typedef itk::Image< InputPixelType,  dim > InputImageType;
    typedef itk::Image< OutputPixelType, dim > OutputImageType;

    // Convert the image from CamiTK in VTK format to ITK format to use ITK filters.
    typedef itk::VTKImageToImageFilter<InputImageType> vtkToItkFilterType;
    typename vtkToItkFilterType::Pointer vtkToItkFilter = vtkToItkFilterType::New();

    // Declare and create your own private ITK filter here...
    typedef itk::MedianImageFilter<InputImageType, OutputImageType> FilterType;
    typename FilterType::Pointer filter = FilterType::New();

    // In the same way, once the image is filtered, we need to convert it again to
    // VTK format to give it to CamiTK.
    typedef itk::ImageToVTKImageFilter<OutputImageType> itkToVtkFilterType;
    typename itkToVtkFilterType::Pointer itkToVtkFilter = itkToVtkFilterType::New();
// ------------------------- WRITE YOUR CODE HERE ----------------------------------

    // To update CamiTK progress bar while filtering, add an ITK observer to the filters.
    ItkProgressObserver::Pointer observer = ItkProgressObserver::New();
    // ITK observers generally give values between 0 and 1, and CamiTK progress bar
    //    wants values between 0 and 100...
    observer->SetCoef(100.0);

    // --------------------- Plug filters and parameters ---------------------------
    // From VTK to ITK
    vtkToItkFilter->SetInput(img);
    vtkToItkFilter->AddObserver(itk::ProgressEvent(), observer);
    vtkToItkFilter->Update();
    observer->Reset();

    typename InputImageType::SizeType indexRadius;
    indexRadius[0] = halfNeighborhoodSizeX;
    indexRadius[1] = halfNeighborhoodSizeY;

    if (dim == 3) {
        indexRadius[2] = halfNeighborhoodSizeZ;
    }

    filter->SetInput(vtkToItkFilter->GetOutput());
    filter->SetRadius(indexRadius);
    filter->AddObserver(itk::ProgressEvent(), observer);
    filter->Update();
    observer->Reset();

    // From ITK to VTK
    // Change the following line to put your filter instead of vtkToItkFilter
    // For example: itkToVtkFilter->SetInput(filter->GetOutput());
    itkToVtkFilter->SetInput(filter->GetOutput());

    // --------------------- Actually execute all filters parts --------------------
    itkToVtkFilter->Update();

    // --------------------- Create and return a copy (the filters will be deleted)--
    vtkSmartPointer<vtkImageData> resultImage = itkToVtkFilter->GetOutput();
    int extent[6];
    resultImage->GetExtent(extent);
    result->SetExtent(extent);
    result->DeepCopy(resultImage);

    // Set CamiTK progress bar back to zero (the processing filter is over)
    observer->Reset();

    return result;
}


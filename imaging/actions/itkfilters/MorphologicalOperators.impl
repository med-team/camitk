/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

vtkSmartPointer<vtkImageData> MorphologicalOperators::implementProcess(vtkSmartPointer<vtkImageData> img)
{ 
    vtkSmartPointer<vtkImageData> result = NULL; 
    if (img == NULL) 
        return result; 

    int * dims = img->GetDimensions(); 
    int dim = 0; 
    if (dims[2] == 0) 
        dim = 2; 
    else 
        dim = 3; 

    int scalarType = img->GetScalarType();  

    switch (scalarType) 
    { 
        case VTK_CHAR : 
            if (dim == 2) 
                result = itkProcess<char, char, 2>(img); 
            else // if dim == 3 
                result = itkProcess<char, char, 3>(img); 
            break; 

        case VTK_UNSIGNED_CHAR : 
            if (dim == 2) 
                result = itkProcess<unsigned char, unsigned char, 2>(img); 
            else // if dim == 3 
                result = itkProcess<unsigned char, unsigned char, 3>(img); 
            break; 

        case VTK_SIGNED_CHAR : 
            if (dim == 2) 
                result = itkProcess<signed char, signed char, 2>(img); 
            else // if dim == 3 
                result = itkProcess<signed char, signed char, 3>(img); 
            break; 

        case VTK_SHORT : 
            if (dim == 2) 
                result = itkProcess<short, short, 2>(img); 
            else // if dim == 3 
                result = itkProcess<short, short, 3>(img); 
            break; 

        case VTK_UNSIGNED_SHORT : 
            if (dim == 2) 
                result = itkProcess<unsigned short, unsigned short, 2>(img); 
            else // if dim == 3 
                result = itkProcess<unsigned short, unsigned short, 3>(img); 
            break; 

        case VTK_INT : 
            if (dim == 2) 
                result = itkProcess<int, int, 2>(img); 
            else // if dim == 3 
                result = itkProcess<int, int, 3>(img); 
            break; 

        case VTK_UNSIGNED_INT : 
            if (dim == 2) 
                result = itkProcess<unsigned int, unsigned int, 2>(img); 
            else // if dim == 3 
                result = itkProcess<unsigned int, unsigned int, 3>(img); 
            break; 

        case VTK_LONG : 
            if (dim == 2) 
                result = itkProcess<long, long, 2>(img); 
            else // if dim == 3 
                result = itkProcess<long, long, 3>(img); 
            break; 

        case VTK_UNSIGNED_LONG : 
            if (dim == 2) 
                result = itkProcess<unsigned long, unsigned long, 2>(img); 
            else // if dim == 3 
                result = itkProcess<unsigned long, unsigned long, 3>(img); 
            break; 

        case VTK_FLOAT : 
            if (dim == 2) 
                result = itkProcess<float, float, 2>(img); 
            else // if dim == 3 
                result = itkProcess<float, float, 3>(img); 
            break; 

        case VTK_DOUBLE : 
            if (dim == 2) 
                result = itkProcess<double, double, 2>(img); 
            else // if dim == 3 
                result = itkProcess<double, double, 3>(img); 
            break; 

        default : 
            break; 
        } 

    return result; 
} 

/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef DERIVATIVE_H
#define DERIVATIVE_H

#include <QObject>
#include <Action.h>
#include <ImageComponent.h>
#include <ActionWidget.h>

#include "ITKFilterAPI.h"

/**
 *
 * @ingroup group_cepimaging_actions_itkfilters
 *
 * @brief
 * Perform a derivative filter on the @ref camitk::ImageComponent "ImageComponent"
 *
 **/
class ITKFILTER_API Derivative : public camitk::Action {
    Q_OBJECT

public:
    /// The 3 different possible derivative orders
    enum derivativeOrder { ORDER_0, ORDER_1, ORDER_2 };
    Q_ENUM(derivativeOrder)

    /// The direction of the derivative
    enum derivativeDirection { DIRECTION_X, DIRECTION_Y, DIRECTION_Z };
    Q_ENUM(derivativeDirection)

    /// Default Constructor
    Derivative(camitk::ActionExtension*);

    /// Default Destructor
    virtual ~Derivative();

public slots:
    /** this method is automatically called when the action is triggered.
      * Use getTargets() QList to get the list of component to use.
      * \note getTargets() is automatically filtered so that it only contains compatible components,
      * i.e., instances of ImageComponent (or a subclass).
      */
    virtual camitk::Action::ApplyStatus apply();

private:
    /// helper method to simplify the target component processing
    virtual void process(camitk::ImageComponent*);

private:
    vtkSmartPointer<vtkImageData> implementProcess(vtkSmartPointer<vtkImageData> img);

    template <class InputPixelType, class OutputPixelType, const int dim>
    vtkSmartPointer<vtkImageData> itkProcess(vtkSmartPointer<vtkImageData> img);


protected:

    /** The derivation requires a signed type for voxel.
     *  With keepOrgVoxelType true, the result image is then re-casted
     *  to the original image voxel type.
     */
    bool keepOrgVoxelType;

    /** The direction of the derivative
     *   0-> derivate along x axis,
     *   1-> derivate along y axis,
     *   2-> derivate along z axis.
     */
    int direction;

    /// The order of the derivative.
    int order;
};

#endif // DERIVATIVE_H

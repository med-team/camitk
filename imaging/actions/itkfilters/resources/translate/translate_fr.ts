<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>AnisotropicDiffusion</name>
    <message>
        <location filename="../../AnisotropicDiffusion.cpp" line="67"/>
        <source>Keep original image voxel type</source>
        <translation>Garder le type image voxel original</translation>
    </message>
    <message>
        <location filename="../../AnisotropicDiffusion.cpp" line="68"/>
        <source>Keep the original image voxel type ?</source>
        <translation>Garder type de l&apos;image voxel original ?</translation>
    </message>
    <message>
        <location filename="../../AnisotropicDiffusion.cpp" line="69"/>
        <source>Number of iterations</source>
        <translation>Nombre d&apos;itérations</translation>
    </message>
    <message>
        <location filename="../../AnisotropicDiffusion.cpp" line="70"/>
        <source>The more iterations, the more smoothing. 
Each iteration takes the same amount of time. 
If it takes 10 seconds for one iteration, then it will take 100 seconds for 10 iterations. 
Note that the conductance controls how much each iteration smooths across edges. </source>
        <translation>Plus il y a d&apos;itérations, plus il y a lissage.
Chaque itération dure le même temps.
Si cela prend 10 secondes, alors cela prendra 100 secondes pour 10 itérations.
Note que la conductance controle combien chaque itération lisse les fronts. </translation>
    </message>
    <message>
        <location filename="../../AnisotropicDiffusion.cpp" line="76"/>
        <source>Conductance</source>
        <translation>Conductance</translation>
    </message>
    <message>
        <location filename="../../AnisotropicDiffusion.cpp" line="77"/>
        <source>Conductance controls the sensitivity of the conductance term. 
As a general rule, the lower the value, the more strongly the filter preserves edges. 
A high value will cause diffusion (smoothing) across edges. 
Note that the number of iterations controls how much smoothing is done within regions bounded by edges.</source>
        <translation>La conductance controle la sensibilité de la valeur conductance.
Régle générale, plus la valeur est petite, plus le filtre preserves les fronts.
Une valeur haute entrainera un lissage à travers les fronts.</translation>
    </message>
</context>
<context>
    <name>CannyEdgeDetection</name>
    <message>
        <location filename="../../CannyEdgeDetection.cpp" line="62"/>
        <source>Keep original voxel type?</source>
        <translation>Garder le type de voxel original ?</translation>
    </message>
    <message>
        <location filename="../../CannyEdgeDetection.cpp" line="62"/>
        <source>Does the output image have the same voxel type as the input one?</source>
        <translation>L&apos;image en sortie a t elle le même voxel que l&apos;image en entrée ?</translation>
    </message>
    <message>
        <location filename="../../CannyEdgeDetection.cpp" line="65"/>
        <source>Variance</source>
        <translation>Variance</translation>
    </message>
    <message>
        <location filename="../../CannyEdgeDetection.cpp" line="65"/>
        <source>The variance is used in the gaussian preprocessing step. 
The higher the variance is, the less the edges detection will be noise sensitive.</source>
        <translation>La variance est utilisée dans l&apos;étape de préprocessing gaussienne.
Plus grande est la variance, moins la détection des fronts sera sensible au bruit.</translation>
    </message>
    <message>
        <location filename="../../CannyEdgeDetection.cpp" line="71"/>
        <source>Upper threshold</source>
        <translation>Seuil supérieur</translation>
    </message>
    <message>
        <location filename="../../CannyEdgeDetection.cpp" line="71"/>
        <source>The upper threshold is used at step 3 to determine whether a voxel belongs an edge or not. 
Above, the voxel is detected as an edge.
Below and upper the low threshold, the voxel is accepted if it is connected to an edge voxel neighbor.</source>
        <translation>Le seuil supérieur est utilisé à l&apos;étape 3 pour déterminer si une voxel appartient à un front ou pas.
Au dessus le voxel est détecté comme un front.
En dessous et au dessus le seuil bas, le voxel est accepté si il est connecté à un voisin du front voxel.  </translation>
    </message>
    <message>
        <location filename="../../CannyEdgeDetection.cpp" line="75"/>
        <source>Lower threshold</source>
        <translation>Seuil bas</translation>
    </message>
    <message>
        <location filename="../../CannyEdgeDetection.cpp" line="75"/>
        <source>The lower threshold is used at step 3 to determine whether a voxel belongs an edge or not. 
Above, and under the upper threshold the voxel is detected as an edge if it is connected to an edge voxel neighbor. 
Below, the voxel is rejected.</source>
        <translation>Le seuil inférieur est utilisé à l&apos;étape 3 pour déterminer si une voxel appartient à un front ou pas.
Au dessus et dessous le seuil supérieur, le voxel est est détecté comme un front si il est connecté à un voisin du front voxel.
Au dessous le voxel est rejeté.
</translation>
    </message>
</context>
<context>
    <name>Derivative</name>
    <message>
        <location filename="../../Derivative.cpp" line="76"/>
        <source>Keep original voxel type?</source>
        <translation>Garder le type de voxel original ?</translation>
    </message>
    <message>
        <location filename="../../Derivative.cpp" line="76"/>
        <source>Does the output image have the same voxel type as the input one?</source>
        <translation>L&apos;image en sortie a t elle le même voxel que l&apos;image en entrée ?</translation>
    </message>
    <message>
        <location filename="../../Derivative.cpp" line="79"/>
        <source>Derivative order</source>
        <translation>Ordre de dérivation</translation>
    </message>
    <message>
        <location filename="../../Derivative.cpp" line="79"/>
        <source>The derivative order of the partial derivative along the selected direction (x, y, z).</source>
        <translation>L&apos;ordre de la dérivée de la dérivée partielle le long de la direction selectionnée (x,y,z).</translation>
    </message>
    <message>
        <location filename="../../Derivative.cpp" line="86"/>
        <source>Direction</source>
        <translation>Direction</translation>
    </message>
    <message>
        <location filename="../../Derivative.cpp" line="86"/>
        <source>The direction of the partial derivative.</source>
        <translation>La direction de la dérivée partielle.</translation>
    </message>
</context>
<context>
    <name>GaussianFilter</name>
    <message>
        <location filename="../../GaussianFilter.cpp" line="64"/>
        <source>Variance</source>
        <translation>Variance</translation>
    </message>
    <message>
        <location filename="../../GaussianFilter.cpp" line="64"/>
        <source>The variance is an input parameter of the gaussian kernel. 
The higher the variance is, the blurer the resulting image will be.</source>
        <translation>La variance est un paramêtre en entrée du noyau gaussien.
Plus grande est la variance, plus flou sera l&apos;image résultante.</translation>
    </message>
    <message>
        <location filename="../../GaussianFilter.cpp" line="70"/>
        <source>Gaussian Type</source>
        <translation>Type gaussien</translation>
    </message>
    <message>
        <location filename="../../GaussianFilter.cpp" line="70"/>
        <source>Choose the type of kernel for the gaussian filtering</source>
        <translation>Choisir le type du noyau pour le filtrage gaussien</translation>
    </message>
    <message>
        <location filename="../../GaussianFilter.cpp" line="73"/>
        <source>Standard</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../../GaussianFilter.cpp" line="73"/>
        <source>Recursive IIR</source>
        <translation>IIR récursif</translation>
    </message>
    <message>
        <location filename="../../../../src/imaging/actions/itkfilters/GaussianFilter.cpp" line="78"/>
        <source>Use Image Spacing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../src/imaging/actions/itkfilters/GaussianFilter.cpp" line="78"/>
        <source>The variance or standard deviation (sigma) will be evaluated as pixel units if this is off or as physical units if this is on.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GradientMagnitudeRecursiveGaussian</name>
    <message>
        <location filename="../../GradientMagnitudeRecursiveGaussian.cpp" line="68"/>
        <source>Standard deviation</source>
        <translation>Déviation standard</translation>
    </message>
    <message>
        <location filename="../../GradientMagnitudeRecursiveGaussian.cpp" line="68"/>
        <source>The standard deviation &lt;i&gt;(sigma)&lt;/i&gt; is used as a parameter of the Gaussian convolution kernel. 
The higher the deviation is, the blurer the resulting image will be.</source>
        <translation>La déviation standard  &lt;i&gt;(sigma)&lt;/i&gt; est utilisé comme un paramêtre de la convolution gaussienne.
Plus grande est la déviation, plus floue sera l&apos;image résultante.</translation>
    </message>
</context>
<context>
    <name>Laplacian</name>
    <message>
        <location filename="../../Laplacian.cpp" line="65"/>
        <source>Keep original voxel type?</source>
        <translation>Garder le type image voxel original ?</translation>
    </message>
    <message>
        <location filename="../../Laplacian.cpp" line="65"/>
        <source>Does the output image have the same voxel type as the input one?</source>
        <translation>L&apos;image en sortie a t elle le même voxel que l&apos;image en entrée ?</translation>
    </message>
</context>
<context>
    <name>LaplacianRecursiveGaussian</name>
    <message>
        <location filename="../../LaplacianRecursiveGaussian.cpp" line="61"/>
        <source>Keep original voxel type?</source>
        <translation>Garder le type image voxel original ?</translation>
    </message>
    <message>
        <location filename="../../LaplacianRecursiveGaussian.cpp" line="61"/>
        <source>Does the output image have the same voxel type as the input one?</source>
        <translation>L&apos;image en sortie a t elle le même voxel que l&apos;image en entrée ?</translation>
    </message>
    <message>
        <location filename="../../LaplacianRecursiveGaussian.cpp" line="64"/>
        <source>Standard deviation</source>
        <translation>Déviation standard</translation>
    </message>
    <message>
        <location filename="../../LaplacianRecursiveGaussian.cpp" line="64"/>
        <source>The standard deviation &lt;i&gt;(sigma)&lt;/i&gt; is used as a parameter of the Gaussian convolution kernel. 
The higher the deviation is, the blurer the resulting image will be.</source>
        <translation>La déviation standard  &lt;i&gt;(sigma)&lt;/i&gt; est utilisé comme un paramêtre de la convolution gaussienne.
Plus grande est la déviation, plus floue sera l&apos;image résultante.</translation>
    </message>
</context>
<context>
    <name>MeanFilter</name>
    <message>
        <location filename="../../MeanFilter.cpp" line="71"/>
        <source>Half neighborhood size along X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MeanFilter.cpp" line="71"/>
        <source>Half the size of the X direction of the neighborhood taken into account for the mean computation. 
For instance, a value of 2 will create a windows of size 4 along the X direction.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MeanFilter.cpp" line="77"/>
        <source>Half neighborhood size along Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MeanFilter.cpp" line="77"/>
        <source>Half the size of the Y direction of the neighborhood taken into account for the mean computation. 
For instance, a value of 2 will create a windows of size 4 along the Y direction.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MeanFilter.cpp" line="83"/>
        <source>Half neighborhood size along Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MeanFilter.cpp" line="83"/>
        <source>Half the size of the Z direction of the neighborhood taken into account for the mean computation. 
For instance, a value of 2 will create a windows of size 4 along the Z direction.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MedianFilter</name>
    <message>
        <location filename="../../MedianFilter.cpp" line="71"/>
        <source>Half neighborhood size along X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MedianFilter.cpp" line="71"/>
        <source>Half the size of the X direction of the neighborhood taken into account for the mean computation. 
For instance, a value of 2 will create a windows of size 4 along the X direction.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MedianFilter.cpp" line="77"/>
        <source>Half neighborhood size along Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MedianFilter.cpp" line="77"/>
        <source>Half the size of the Y direction of the neighborhood taken into account for the mean computation. 
For instance, a value of 2 will create a windows of size 4 along the Y direction.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MedianFilter.cpp" line="83"/>
        <source>Half neighborhood size along Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MedianFilter.cpp" line="83"/>
        <source>Half the size of the Z direction of the neighborhood taken into account for the mean computation. 
For instance, a value of 2 will create a windows of size 4 along the Z direction.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MorphologicalOperators</name>
    <message>
        <location filename="../../MorphologicalOperators.cpp" line="64"/>
        <source>Structuring element size</source>
        <translation>Taille de l&apos;élément structurant</translation>
    </message>
    <message>
        <location filename="../../MorphologicalOperators.cpp" line="64"/>
        <source>The structuring element size &apos;n&apos;&apos; represents the dimension of the n x n x n voxels square used by the operator.
For instance a 3 voxels size will indicate a 3x3x3 square.</source>
        <translation>La taille de l&apos;élément struturant &apos;n&apos; représente la dimension du voxels cubique nxnxn utilisé par l&apos;opérateur.
Par exemple un voxel de taille 3 indiquera un cube de 3x3x3.</translation>
    </message>
    <message>
        <location filename="../../MorphologicalOperators.cpp" line="70"/>
        <source>Image scalar type</source>
        <translation>Type de l&apos;image scalaire</translation>
    </message>
    <message>
        <location filename="../../MorphologicalOperators.cpp" line="70"/>
        <source>The image scalar type.</source>
        <translation>type de l&apos;image scalaire.</translation>
    </message>
    <message>
        <location filename="../../MorphologicalOperators.cpp" line="77"/>
        <source>Type of operation</source>
        <translation>Type d&apos;opération</translation>
    </message>
    <message>
        <location filename="../../MorphologicalOperators.cpp" line="77"/>
        <source>Select the desired morphological operation.</source>
        <translation>Sélectionner l&apos;opération morphologique désirée.</translation>
    </message>
</context>
<context>
    <name>SobelEdgeDetection</name>
    <message>
        <location filename="../../SobelEdgeDetection.cpp" line="61"/>
        <source>Keep original voxel type?</source>
        <translation>Garder type de l&apos;image voxel original ?</translation>
    </message>
    <message>
        <location filename="../../SobelEdgeDetection.cpp" line="61"/>
        <source>Does the output image have the same voxel type as the input one?</source>
        <translation>L&apos;image en sortie a t elle le même voxel que l&apos;image en entrée ?</translation>
    </message>
</context>
</TS>

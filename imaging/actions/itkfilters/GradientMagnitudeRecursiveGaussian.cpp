/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "GradientMagnitudeRecursiveGaussian.h"

// CamiTK includes
#include <Application.h>
#include <ItkProgressObserver.h>
#include <Property.h>

// ITK includes
#include <itkImageToVTKImageFilter.h>
#include <itkVTKImageToImageFilter.h>
#include <itkGradientMagnitudeRecursiveGaussianImageFilter.h>


using namespace camitk;


// --------------- constructor -------------------
GradientMagnitudeRecursiveGaussian::GradientMagnitudeRecursiveGaussian(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Gradient Magnitude With Smoothing");
    setDescription("<br>Differentiation is an ill-defined operation over digital data.<br> \
                   In practice it is convenient to define a scale in which the differentiation should be  performed. This is usually done by preprocessing the data with a smoothing filter. <br/><br/> \
                   It has been shown that a Gaussian kernel is the most choice for performing such smoothing. By choosing a particular value for the standard deviation <i>(sigma)</i> of the Gaussian, an associated scale is selected that ignores high frequency content, commonly considered image noise.	<br/><br/> \
                   This filter computes the magnitude of the image gradient at each pixel location.<br/><br/> \
                   <b>The computational process is equivalent to first smoothing the image by convolving it with a Gaussian kernel and then applying a differential operator.</b>  <br/><br/> \
                   The user selects the value of <i>sigma</i>.	Internally this is done by applying an IIR filter that approximates a convolution with the derivative of the Gaussian kernel.  Traditional convolution will produce a more accurate result, but the IIR approach is much faster, especially using large <i>sigma</i>s (Deriche1990,Deriche1993).<br/><br/> \
				   <i>(source: ITK Developer's Guide)</i><br>");
    setComponentClassName("ImageComponent");

    // Setting classification family and tags
    this->setFamily("ITK Filter");
    this->addTag("Gradient");
    this->addTag("Derivative");
    this->addTag("Edge Detection");
    this->addTag("Contours");
    this->addTag("Smoothing");

    // Setting parameters default values
    Property* standardDeviationProperty = new Property(tr("Standard deviation"), 3.0, tr("The standard deviation <i>(sigma)</i> is used as a parameter of the Gaussian convolution kernel. \nThe higher the deviation is, the blurer the resulting image will be."), "");
    standardDeviationProperty->setAttribute("minimum", 0);
    standardDeviationProperty->setAttribute("maximum", 100);
    standardDeviationProperty->setAttribute("singleStep", 0.1);
    addParameter(standardDeviationProperty);
}

// --------------- destructor -------------------
GradientMagnitudeRecursiveGaussian::~GradientMagnitudeRecursiveGaussian() {
    // do not delete the widget has it might have been used in the ActionViewer (i.e. the ownership might have been taken by the stacked widget)
}

// --------------- apply -------------------
Action::ApplyStatus GradientMagnitudeRecursiveGaussian::apply() {
    foreach (Component* comp, getTargets()) {
        ImageComponent* input = dynamic_cast<ImageComponent*>(comp);
        process(input);
    }
    return SUCCESS;
}

void GradientMagnitudeRecursiveGaussian::process(ImageComponent* comp) {
    // Get the parameters
    sigma = property("Standard deviation").toDouble();

    // ITK filter implementation using templates
    vtkSmartPointer<vtkImageData> inputImage = comp->getImageData();
    vtkSmartPointer<vtkImageData> outputImage = implementProcess(inputImage);

    ImageComponent* outputComp = new ImageComponent(outputImage, comp->getName() + "_smoothedGradient");

    // consider frame policy on new image created
    Action::applyTargetPosition(comp, outputComp);

    Application::refresh();

}

#include "GradientMagnitudeRecursiveGaussian.impl"

// ITK filter implementation
template <class InputPixelType, class OutputPixelType, const int dim>
vtkSmartPointer<vtkImageData> GradientMagnitudeRecursiveGaussian::itkProcess(vtkSmartPointer<vtkImageData> img) {
    vtkSmartPointer<vtkImageData> result = vtkSmartPointer<vtkImageData>::New();

    // --------------------- Filters declaration and creation ----------------------
    // Define ITK input and output image types with respect to the instantiation
    //    types of the tamplate.
    typedef itk::Image< InputPixelType,  dim > InputImageType;
    typedef itk::Image< OutputPixelType, dim > OutputImageType;

    // Convert the image from CamiTK in VTK format to ITK format to use ITK filters.
    typedef itk::VTKImageToImageFilter<InputImageType> vtkToItkFilterType;
    typename vtkToItkFilterType::Pointer vtkToItkFilter = vtkToItkFilterType::New();

    // Declare and create your own private ITK filter here...
    typedef itk::GradientMagnitudeRecursiveGaussianImageFilter <InputImageType, OutputImageType> GradientMagnitudeImageFilterType;
    typename GradientMagnitudeImageFilterType::Pointer gradientFilter = GradientMagnitudeImageFilterType::New();

    // In the same way, once the image is filtered, we need to convert it again to
    // VTK format to give it to CamiTK.
    typedef itk::ImageToVTKImageFilter<OutputImageType> itkToVtkFilterType;
    typename itkToVtkFilterType::Pointer itkToVtkFilter = itkToVtkFilterType::New();
// ------------------------- WRITE YOUR CODE HERE ----------------------------------

    // To update CamiTK progress bar while filtering, add an ITK observer to the filters.
    ItkProgressObserver::Pointer observer = ItkProgressObserver::New();
    // ITK observers generally give values between 0 and 1, and CamiTK progress bar
    //    wants values between 0 and 100...
    observer->SetCoef(100.0);

    // --------------------- Plug filters and parameters ---------------------------
    // From VTK to ITK
    vtkToItkFilter->SetInput(img);
    vtkToItkFilter->AddObserver(itk::ProgressEvent(), observer);
    vtkToItkFilter->Update();
    observer->Reset();

    gradientFilter->SetInput(vtkToItkFilter->GetOutput());
    gradientFilter->SetSigma(sigma);
    gradientFilter->AddObserver(itk::ProgressEvent(), observer);
    gradientFilter->Update();
    observer->Reset();

    // From ITK to VTK
    // Change the following line to put your filter instead of vtkToItkFilter
    // For example: itkToVtkFilter->SetInput(filter->GetOutput());
    itkToVtkFilter->SetInput(gradientFilter->GetOutput());

    // --------------------- Actually execute all filters parts --------------------
    itkToVtkFilter->Update();

    // --------------------- Create and return a copy (the filters will be deleted)--
    vtkSmartPointer<vtkImageData> resultImage = itkToVtkFilter->GetOutput();
    int extent[6];
    resultImage->GetExtent(extent);
    result->SetExtent(extent);
    result->DeepCopy(resultImage);

    // Set CamiTK progress bar back to zero (the processing filter is over)
    observer->Reset();

    return result;
}


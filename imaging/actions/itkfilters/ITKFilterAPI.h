#if defined(_WIN32) // MSVC and mingw
#ifdef COMPILE_ITKFILTER_API
#define ITKFILTER_API __declspec(dllexport)
#else
#define ITKFILTER_API __declspec(dllimport)
#endif // COMPILE_MY_COMPONENT_API
#else // for all other platforms ITKFILTER_API is defined to be "nothing"
#define ITKFILTER_API
#endif // MSVC and mingw
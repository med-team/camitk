/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

vtkSmartPointer<vtkImageData> SobelEdgeDetection::implementProcess(vtkSmartPointer<vtkImageData> img)
{ 
     vtkSmartPointer<vtkImageData> result = NULL; 
    if (img == NULL) 
        return result; 

    int * dims = img->GetDimensions(); 
    int dim = 0; 
    if ((dims[2] == 0) || (dims[2] == 1)) 
        dim = 2; 
    else 
        dim = 3; 

    int scalarType = img->GetScalarType();  

    switch (scalarType) 
    { 
        case VTK_CHAR : 
            if (dim == 2) 
                result = itkProcess<char, double, 2>(img); 
            else // if dim == 3 
                result = itkProcess<char, double, 3>(img); 
            break; 

        case VTK_UNSIGNED_CHAR : 
            if (dim == 2) 
                result = itkProcess<unsigned char, double, 2>(img); 
            else // if dim == 3 
                result = itkProcess<unsigned char, double, 3>(img); 
            break; 

        case VTK_SIGNED_CHAR : 
            if (dim == 2) 
                result = itkProcess<signed char, double, 2>(img); 
            else // if dim == 3 
                result = itkProcess<signed char, double, 3>(img); 
            break; 

        case VTK_SHORT : 
            if (dim == 2) 
                result = itkProcess<short, double, 2>(img); 
            else // if dim == 3 
                result = itkProcess<short, double, 3>(img); 
            break; 

        case VTK_UNSIGNED_SHORT : 
            if (dim == 2) 
                result = itkProcess<unsigned short, double, 2>(img); 
            else // if dim == 3 
                result = itkProcess<unsigned short, double, 3>(img); 
            break; 

        case VTK_INT : 
            if (dim == 2) 
                result = itkProcess<int, double, 2>(img); 
            else // if dim == 3 
                result = itkProcess<int, double, 3>(img); 
            break; 

        case VTK_UNSIGNED_INT : 
            if (dim == 2) 
                result = itkProcess<unsigned int, double, 2>(img); 
            else // if dim == 3 
                result = itkProcess<unsigned int, double, 3>(img); 
            break; 

        case VTK_LONG : 
            if (dim == 2) 
                result = itkProcess<long, double, 2>(img); 
            else // if dim == 3 
                result = itkProcess<long, double, 3>(img); 
            break; 

        case VTK_UNSIGNED_LONG : 
            if (dim == 2) 
                result = itkProcess<unsigned long, double, 2>(img); 
            else // if dim == 3 
                result = itkProcess<unsigned long, double, 3>(img); 
            break; 

        case VTK_FLOAT : 
            if (dim == 2) 
                result = itkProcess<float, double, 2>(img); 
            else // if dim == 3 
                result = itkProcess<float, double, 3>(img); 
            break; 

        case VTK_DOUBLE : 
            if (dim == 2) 
                result = itkProcess<double, double, 2>(img); 
            else // if dim == 3 
                result = itkProcess<double, double, 3>(img); 
            break; 

        default : 
            break; 
        } 

    return result; 
} 

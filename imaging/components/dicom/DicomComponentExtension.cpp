/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// CamiTK include
#include "DicomComponentExtension.h"
#include "DicomParser.h"
#include "DicomDialog.h"
#include "DicomDialogEntry.h"
#include "DicomSeries.h"
#include "DicomComponent.h"

// Qt stuff
#include <QFileInfo>

// change cursor
#include <QApplication>

using namespace camitk;

// --------------- GetFileExtensions -------------------
QStringList DicomComponentExtension::getFileExtensions() const {
    QStringList ext;
    ext << "[directory]";

    return ext;
}

// --------------- Open -------------------
Component* DicomComponentExtension::open(const QString& path) {

    seriesParsed = DicomParser::parseDirectory(path);

    // create a Dialog entry for each series parsed
    foreach (DicomSeries* dicomSeries, seriesParsed) {
        DicomDialogEntry* entry = new DicomDialogEntry();
        entry->setSelected(false);
        entry->setAcquisitionDate(dicomSeries->getAcquisitionDate());
        entry->setAcquisitionTime(dicomSeries->getAcquisitionTime());
        entry->setStudyName(dicomSeries->getStudyName());
        entry->setSeriesName(dicomSeries->getSeriesName());
        entry->setSeriesDescription(dicomSeries->getSeriesDescription());
        entry->setPatientName(dicomSeries->getPatientName());
        seriesDialogEntries.append(entry);
    }

    // Prompt the user which series he wishes to open
    QApplication::setOverrideCursor(QCursor(Qt::ArrowCursor));
    dialog = new DicomDialog(seriesDialogEntries);
    seriesDialogEntries = dialog->getSelectedDicomDialogEntries();
    QApplication::restoreOverrideCursor();

    // Open each selected Dicom as a component
    QSet<DicomSeries*> openedSeries;
    if (!seriesDialogEntries.isEmpty()) {
        foreach (DicomDialogEntry* entry, seriesDialogEntries) {
            // Find the corresponding Dicom Series in the List
            foreach (DicomSeries* series, seriesParsed) {
                if ((series->getStudyName() == entry->getStudyName()) &&
                        (series->getSeriesName() == entry->getSeriesName())) {
                    lastOpenedComponent = new DicomComponent(series);
                    openedSeries.insert(series);
                }
            }
        }
    }
    //Cleanup
    for (DicomSeries* series : seriesParsed) {
        if (!openedSeries.contains(series)) {
            delete series;
        }
    }
    seriesParsed.clear();

    for (DicomDialogEntry* entry : seriesDialogEntries) {
        delete entry;
    }
    seriesDialogEntries.clear();

    delete dialog;




    return lastOpenedComponent;
}

// --------------- Save --------------------
bool DicomComponentExtension::save(Component* component) const {
    // depending on the components managed by DicomComponentExtension, use
    // component->getPointSet() (for a MeshComponent derived class)
    // or component->getImageData() (for a ImageComponent derived class)
    // and save the data in the managed format in the file component->getFileName()

    return false;
}

// --------------- hasDataDirectory -------------------
bool DicomComponentExtension::hasDataDirectory() const {
    return true;
}


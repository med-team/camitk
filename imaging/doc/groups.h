// Doxygen SDK groups creation
/**
 * @defgroup group_cepimaging CEP Imaging
 * The CEP Imaging gathers CamiTK image processing extensions.
 * It requires ITK in order to work.
 * @note
 * By default, this CEP is not configured using CMake, please select it at the configuration step if you wish to compile it.
 *
 *
 * @defgroup group_cepimaging_actions Actions
 * @ingroup group_cepimaging
 * Image processing actions.
 * The CEP Imaging actions are based on ITK.
 *
 *
 * @defgroup group_cepimaging_actions_itkfilters ITK Filters
 * @ingroup group_cepimaging_actions
 * ITK filters based actions.
 * Those actions works on volumic images, i.e. @ref camitk::ImageComponent "ImageComponent".
 *
 * \image html actions/itkfilters.png "The ITK filters actions contextual menu."
 *
 *
 * @defgroup group_cepimaging_actions_itksegmentation ITK Segmentation
 * @ingroup group_cepimaging_actions
 * ITK segmentation filters based actions.
 * Those actions works on volumic images, i.e. @ref camitk::ImageComponent "ImageComponent".
 *
 * \image html actions/itksegmentation.png "The ITK segmentation filters actions contextual menu."
 *
 *
 * @defgroup group_cepimaging_components Components
 * @ingroup group_cepimaging
 * Specific @ref camitk::ImageComponent "ImageComponent"
 * The following @ref camitk::ImageComponent "ImageComponent" are derived classes, useful to handle specific type of volumic image.
 *
 * @defgroup group_cepimaging_components_dicom DICOM
 * @ingroup group_cepimaging_components
 * Old DICOM @ref camitk::Component "Component" to handle DICOM image opened from a directory using GDCM.
 *
 *
 * @defgroup group_cepimaging_components_itkimage ITK Image
 * @ingroup group_cepimaging_components
 * This @ref camitk::Component "Component" uses ITK to handle several volumic image (i.e @ref camitk::ImageComponent).
 *
 *
 **/

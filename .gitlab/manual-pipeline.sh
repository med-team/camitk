#!/bin/bash
export PROJECT_NAME="camitk"
export PROJECT_SOURCE_DIR=$(pwd)/CamiTK
export PROJECT_BUILD_DIR=$(pwd)/build
export PROJECT_LOG_DIR=$(pwd)/log
export CI_COMMIT_REF_NAME=$(cd CamiTK && git rev-parse --abbrev-ref HEAD)
export CAMITK_CI_MODE="Experimental"
export CAMITK_CI_STAGE=70
export CI_JOB_NAME="manual-$(date +%Y-%m-%d-%H:%M)"

if [[ "$OS" != "win10" ]]; then
    OS_FULL_NAME=$(grep PRETTY_NAME /etc/*rel* | cut -f2 -d\" )
else
    OS_FULL_NAME="Win10"
fi
export OS=$OS_FULL_NAME

export CDASH_SITE="[Manual] $OS"

# compiler-arch-buildtype string
export COMPILER_CONFIG="GCC-64bits-Debug"

echo
echo
echo
CamiTK/.gitlab/before.sh

echo
echo
echo
CamiTK/.gitlab/check.sh

echo
echo
echo

CamiTK/.gitlab/configure.sh

echo
echo
echo
CamiTK/.gitlab/build.sh

echo
echo
echo
CamiTK/.gitlab/test.sh 

echo
echo
echo
CamiTK/.gitlab/coverage.sh


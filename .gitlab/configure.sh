#!/bin/bash
# Uncomment next line to print each bash command before it is executed
#set -x

# Path needs to use forward slashes
# This is ok on Linux but since gitlab-runner 11.7 on windows all path variables use backward slash instead of forward slash
# → Replace all backslash to forward slash 
export PROJECT_SOURCE_DIR=${PROJECT_SOURCE_DIR//\\//}

echo "Job $CI_JOB_NAME"
echo "PROJECT_SOURCE_DIR=$PROJECT_SOURCE_DIR"

if ! grep -q TRIGGER_STAGE_CONFIGURE "${PROJECT_LOG_DIR}/trigger-stage.txt"; then
    echo "Job skipped as /configure flag not in commit message and CAMITK_CI_STAGE < $STAGE_CONFIGURE"; 
    exit 1;
fi

# Clean build directory
# note: cannot build outside the source tree otherwise artifacts cannot be collected
if [ "$CAMITK_CI_MODE" == "Nightly" ]; then 
    rm -rf ${PROJECT_BUILD_DIR}; 
    echo "Nightly Build, start from scratch"; 
fi

# build
if [[ "$OS" != "win10" ]]; then
    echo "===== Configuring xvfb ====="
    # Starts the server first (try to avoid unexpected and random "QXcbConnection: Could not connect to display :99")
    # see also https://doc.qt.io/qt-5/embedded-linux.html#linuxfb 
    export DISPLAY=":98"
    #-ac +extension GLX +render -noreset -v -fbdir $workingDir/ &> ${PROJECT_LOG_DIR}/test.log &
    Xvfb $DISPLAY -screen 0 1600x1200x24 -ac &> ${PROJECT_LOG_DIR}/test.log &
    trap "kill $! || true" EXIT
    sleep 10
    export XAUTHORITY=/dev/null
fi

echo "===== Running configure stage ====="
# there is no xvfb on windows
if [ "$OS" == "win10" ]; then
    ctest   -VV \
            -DCTEST_SITE="$CDASH_SITE" \
            -DCI_MODE="$CAMITK_CI_MODE" \
            -DCI_ID="P $CI_PIPELINE_ID - J $CI_BUILD_ID" \
            -DCI_BRANCH="$CI_COMMIT_REF_NAME" \
            -DCI_BUILD_SETTINGS="$COMPILER_CONFIG" \
            -DCTEST_SOURCE_DIRECTORY="$PROJECT_SOURCE_DIR" \
            -DCTEST_BINARY_DIRECTORY="$PROJECT_BUILD_DIR" \
            -S "$PROJECT_SOURCE_DIR/sdk/cmake/ctest/ci-configure.cmake" > >(tee ${PROJECT_LOG_DIR}/configure.log) 2>&1
else
    ctest   -VV \
            -DCTEST_SITE="$CDASH_SITE" \
            -DCI_MODE="$CAMITK_CI_MODE" \
            -DCI_ID="P $CI_PIPELINE_ID - J $CI_BUILD_ID" \
            -DCI_BRANCH="$CI_COMMIT_REF_NAME" \
            -DCI_BUILD_SETTINGS="$COMPILER_CONFIG" \
            -DCTEST_SOURCE_DIRECTORY="$PROJECT_SOURCE_DIR" \
            -DCTEST_BINARY_DIRECTORY="$PROJECT_BUILD_DIR" \
            -S $PROJECT_SOURCE_DIR/sdk/cmake/ctest/ci-configure.cmake > >(tee ${PROJECT_LOG_DIR}/configure.log) 2>&1
fi

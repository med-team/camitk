#!/bin/bash
# Uncomment next line to print each bash command before it is executed
#set -x

# Path needs to use forward slashes
# This is ok on Linux but since gitlab-runner 11.7 on windows all path variables use backward slash instead of forward slash
# → Replace all backslash to forward slash 
export PROJECT_SOURCE_DIR=${PROJECT_SOURCE_DIR//\\//}

echo "Job $CI_JOB_NAME"
echo "PROJECT_SOURCE_DIR=$PROJECT_SOURCE_DIR"

if ! grep -q TRIGGER_STAGE_BUILD "${PROJECT_LOG_DIR}/trigger-stage.txt"; then
    echo "Job skipped as /build flag not in commit message and CAMITK_CI_STAGE < $STAGE_CONFIGURE"; 
    exit 1;
fi

echo "===== build ====="

if [ "$OS" == "win10" ]; then
    # there is no xvfb on windows
    ctest   -VV \
            -DCTEST_SITE="$CDASH_SITE" \
            -DCI_MODE="$CAMITK_CI_MODE" \
            -DCI_ID="P $CI_PIPELINE_ID - J $CI_BUILD_ID" \
            -DCI_BRANCH="$CI_COMMIT_REF_NAME" \
            -DCI_BUILD_SETTINGS="$COMPILER_CONFIG" \
            -DCTEST_SOURCE_DIRECTORY="$PROJECT_SOURCE_DIR" \
            -DCTEST_BINARY_DIRECTORY="$PROJECT_BUILD_DIR" \
            -S $PROJECT_SOURCE_DIR/sdk/cmake/ctest/ci-build.cmake > >(tee ${PROJECT_LOG_DIR}/build.log | grep --line-buffered -e "- Building" -e "Compiler errors" -e "Compiler warnings") 2>&1
else
    # on Linux, xvfb is required to run the tests
    xvfb-run --auto-servernum --server-args="-screen 0 1024x768x24" \
    ctest   -VV \
            -DCTEST_SITE="$CDASH_SITE" \
            -DCI_MODE="$CAMITK_CI_MODE" \
            -DCI_ID="P $CI_PIPELINE_ID - J $CI_BUILD_ID" \
            -DCI_BRANCH="$CI_COMMIT_REF_NAME" \
            -DCI_BUILD_SETTINGS="$COMPILER_CONFIG" \
            -DCTEST_SOURCE_DIRECTORY="$PROJECT_SOURCE_DIR" \
            -DCTEST_BINARY_DIRECTORY="$PROJECT_BUILD_DIR" \
            -S $PROJECT_SOURCE_DIR/sdk/cmake/ctest/ci-build.cmake > >(tee ${PROJECT_LOG_DIR}/build.log | grep --line-buffered -e "\[100%] Built target") 2>&1
fi

#Specific to SDK: control the version (major, minor, default patch and )
set(CAMITK_VERSION_MAJOR "5")
set(CAMITK_VERSION_MINOR "2")
set(CAMITK_VERSION_NICKNAME "Kelly") # Green
set(CAMITK_VERSION_PATCH "0") # patch version for packaging, change when appropriate

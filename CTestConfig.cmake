## This file should be placed in the root directory of your project.
## Then modify the CMakeLists.txt file in the root directory of your
## project to incorporate the testing dashboard.
## # The following are required to uses Dart and the Cdash dashboard
set(CTEST_PROJECT_NAME "camitkcommunityedition")
set(CTEST_NIGHTLY_START_TIME "22:00:00 UTC")

set(CTEST_DROP_METHOD "https")
set(CTEST_DROP_SITE "cdash-timc.imag.fr")
set(CTEST_DROP_LOCATION "/submit.php?project=camitk-communityedition")
set(CTEST_DROP_SITE_CDASH TRUE)

## Disable CA certificates verification, until TIMC got valid certificate for this server
if (CTEST_DROP_METHOD STREQUAL "https")
    set(CTEST_CURL_OPTIONS
        "CURLOPT_SSL_VERIFYPEER_OFF"
        "CURLOPT_SSL_VERIFYHOST_OFF"
    )
endif()

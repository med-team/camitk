#!/usr/bin/make -f

# DH_VERBOSE := 1

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

export JAVA_HOME=/usr/lib/jvm/default-java

# CamiTK version from d/changelog)
include /usr/share/dpkg/pkg-info.mk
FULL_VERSION := $(shell echo '$(DEB_VERSION)' | sed -e 's/^[[:digit:]]*://' -e 's/[~-].*//')
VER_MAJOR := $(shell echo '$(FULL_VERSION)' | cut -f1 -d".")
VER_MINOR := $(shell echo '$(FULL_VERSION)' | cut -f2 -d".")
VER_SHORT=$(VER_MAJOR).$(VER_MINOR)

# package aliases (see d/control)
pkg_lib = libcamitk$(VER_MAJOR)
pkg_qpb = libqtpropertybrowser$(VER_MAJOR)
pkg_cfg = camitk-config
pkg_dev = libcamitk-dev
pkg_qpb_dev = libqtpropertybrowser-dev
pkg_data = libcamitk$(VER_MAJOR)-data
pkg_doc = libcamitk$(VER_MAJOR)-doc
pkg_imp = camitk-imp
pkg_asm = camitk-actionstatemachine
PKG_LIB_DIR := $(CURDIR)/debian/$(pkg_lib)/usr/lib/$(DEB_HOST_MULTIARCH)/camitk-$(VER_SHORT)
LIB_BUILD_DIR := $(CURDIR)/camitk-build/lib/$(DEB_HOST_MULTIARCH)
PRIVATE_LIB_BUILD_DIR := $(LIB_BUILD_DIR)/camitk-$(VER_SHORT)

# multi-arch support
include /usr/share/dpkg/architecture.mk

# CMake flags are of two types:
# - generic (to tell cmake to build proper binaries)
# - CamiTK specific options
# Notes:
# - if you would like a more verbose camitk at run-time,
#   you can add -DCAMITK_LOG_LEVEL=2
CMAKE_EXTRA_FLAGS = \
	-DCMAKE_SKIP_RPATH:BOOL=TRUE \
	-DCMAKE_INSTALL_RPATH_USE_LINK_PATH:BOOL=FALSE \
	-DCMAKE_BUILD_TYPE:STRING=None \
	\
	-DCEP_IMAGING:BOOL=TRUE \
	-DCEP_MODELING:BOOL=TRUE \
	-DCEP_TUTORIALS:BOOL=TRUE \
	\
	-DAPIDOC_SDK:BOOL=TRUE

%:
	dh $@ --builddirectory=camitk-build

override_dh_auto_configure:
	# modify libdir to include multiarch
	sed -i 's+libDir = "lib";+libDir = "lib/$(DEB_HOST_MULTIARCH)";+g' sdk/libraries/core/CamiTKVersion.h
	dh_auto_configure -- $(CMAKE_EXTRA_FLAGS)

# let's be smart with doc
override_dh_auto_build-indep:
	# Building API DOC
	dh_auto_build -- camitk-ce-api-doc
	# Building all (required to avoid cmake error)
	dh_auto_build

# No tests needed for docs
override_dh_auto_test-indep:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	echo "No tests availaible just for the docs"
endif

# run all the available tests (only for Archictecture: any
# as tests can only be run if and once the framework is build)
override_dh_auto_test-arch:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	# Use the CamiTK test suite
	# Note: all tests require an X server, xvfb-run is needed to have a virtual one
	# Another way: xvfb-run --auto-servernum $(MAKE) -C camitk-build ARGS="-V" test
	# e.g. to test config: (cd camitk-build && xvfb-run --auto-servernum --server-args="-screen 0 1024x768x24" ctest -VV --timeout 1800 -R application-config)
	# The build dir path is needed for extensions, public and private libraries during test
	# to know about all camitk extension directories and properly manage inner-dependencies
	# between extensions (i.e., mml component extension depends on physicalmodel component extensions)
	# Lib dependencies can be in any extensions (viewers, component and action extensions) lib directory
	export LD_LIBRARY_PATH=$(LIB_BUILD_DIR)/:$(PRIVATE_LIB_BUILD_DIR)/viewers/:$(PRIVATE_LIB_BUILD_DIR)/components/:$(PRIVATE_LIB_BUILD_DIR)/actions/:$(PRIVATE_LIB_BUILD_DIR)/:$(JAVA_HOME)/lib/:$(JAVA_HOME)/lib/server/ ; \
	(cd camitk-build && xvfb-run --auto-servernum --server-args="-screen 0 1024x768x24" ctest -V --timeout 1800)
endif

# packages for specific arch
override_dh_install:
	# $(CURDIR) is the current source dir
	# camitk dynamic library
	dh_install -p$(pkg_lib) --autodest debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/libcamitkcore.so.*
	dh_install -p$(pkg_lib) --autodest debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/libmonitoring.so.*
	dh_install -p$(pkg_lib) --autodest debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/camitk-$(VER_SHORT)/viewers/lib*.so.*
	dh_install -p$(pkg_lib) --autodest debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/camitk-$(VER_SHORT)/actions/lib*.so.*
	dh_install -p$(pkg_lib) --autodest debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/camitk-$(VER_SHORT)/components/lib*.so.*

	# camitk-config package contains only the app, man page and icon
	dh_install -p$(pkg_cfg) --autodest debian/tmp/usr/bin/camitk-config
	dh_install -p$(pkg_cfg) --autodest debian/tmp/usr/share/pixmaps/camitk-config.xpm
	dh_installman -p$(pkg_cfg) debian/tmp/usr/share/man/man1/camitk-config.1

	# separate packaging for qtpropertybrowser (can be used independently from camitk)
	dh_install -p$(pkg_qpb) --autodest debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/libqtpropertybrowser.so.*

	# camitk developer package
	# camitk so and headers and static libs
	dh_install -p$(pkg_dev) debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/libcamitkcore.so usr/lib/$(DEB_HOST_MULTIARCH)
	dh_install -p$(pkg_dev) debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/libmonitoring.so usr/lib/$(DEB_HOST_MULTIARCH)
	dh_install -p$(pkg_dev) --autodest debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/camitk-$(VER_SHORT)/viewers/lib*.so
	dh_install -p$(pkg_dev) --autodest debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/camitk-$(VER_SHORT)/actions/lib*.so
	dh_install -p$(pkg_dev) --autodest debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/camitk-$(VER_SHORT)/components/lib*.so
	dh_install -p$(pkg_dev) --autodest debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/camitk-$(VER_SHORT)/lib*.a
	dh_install -p$(pkg_dev) --autodest debian/tmp/usr/include/camitk-$(VER_SHORT)/libraries/camitkcore
	dh_install -p$(pkg_dev) --autodest debian/tmp/usr/include/camitk-$(VER_SHORT)/libraries/cepcoreschema
	dh_install -p$(pkg_dev) --autodest debian/tmp/usr/include/camitk-$(VER_SHORT)/libraries/cepgenerator
	dh_install -p$(pkg_dev) --autodest debian/tmp/usr/include/camitk-$(VER_SHORT)/viewers
	dh_install -p$(pkg_dev) --autodest debian/tmp/usr/include/camitk-$(VER_SHORT)/actions
	dh_install -p$(pkg_dev) --autodest debian/tmp/usr/include/camitk-$(VER_SHORT)/components
	dh_install -p$(pkg_dev) --autodest debian/tmp/usr/include/camitk-$(VER_SHORT)/libraries/pmlschema
	dh_install -p$(pkg_dev) --autodest debian/tmp/usr/include/camitk-$(VER_SHORT)/libraries/pml
	dh_install -p$(pkg_dev) --autodest debian/tmp/usr/include/camitk-$(VER_SHORT)/libraries/lmlschema
	dh_install -p$(pkg_dev) --autodest debian/tmp/usr/include/camitk-$(VER_SHORT)/libraries/lml
	dh_install -p$(pkg_dev) --autodest debian/tmp/usr/include/camitk-$(VER_SHORT)/libraries/mmlschema
	dh_install -p$(pkg_dev) --autodest debian/tmp/usr/include/camitk-$(VER_SHORT)/libraries/monitoring
	dh_install -p$(pkg_dev) --autodest debian/tmp/usr/include/camitk-$(VER_SHORT)/libraries/monitoringgui
	dh_install -p$(pkg_dev) --autodest debian/tmp/usr/share/camitk-$(VER_SHORT)/cmake/*.cmake
	dh_install -p$(pkg_dev) --autodest debian/tmp/usr/share/camitk-$(VER_SHORT)/cmake/macros/*.cmake
	dh_install -p$(pkg_dev) --autodest debian/tmp/usr/share/camitk-$(VER_SHORT)/cmake/macros/camitk/*.cmake
	dh_install -p$(pkg_dev) --autodest debian/tmp/usr/share/camitk-$(VER_SHORT)/cmake/macros/camitk/manifest/*.cmake
	dh_install -p$(pkg_dev) --autodest debian/tmp/usr/share/camitk-$(VER_SHORT)/cmake/macros/camitk/packaging/*.cmake
	dh_install -p$(pkg_dev) --autodest debian/tmp/usr/share/camitk-$(VER_SHORT)/cmake/macros/camitk/test/*.cmake
	dh_install -p$(pkg_dev) --autodest debian/tmp/usr/share/camitk-$(VER_SHORT)/cmake/macros/camitk/test/level/*.cmake
	# wizard
	dh_install -p$(pkg_dev) --autodest debian/tmp/usr/bin/camitk-wizard
	dh_installman -p$(pkg_dev) debian/tmp/usr/share/man/man1/camitk-wizard.1
	dh_install -p$(pkg_dev) --autodest debian/tmp/usr/share/applications/camitk-wizard.desktop
	dh_install -p$(pkg_dev) --autodest debian/tmp/usr/share/pixmaps/camitk-wizard.xpm
	# cepgenerator
	dh_install -p$(pkg_dev) --autodest debian/tmp/usr/bin/camitk-cepgenerator
	dh_installman -p$(pkg_dev) debian/tmp/usr/share/man/man1/camitk-cepgenerator.1
	# test programs
	dh_install -p$(pkg_dev) --autodest debian/tmp/usr/bin/camitk-testactions
	dh_installman -p$(pkg_dev) debian/tmp/usr/share/man/man1/camitk-testactions.1
	dh_install -p$(pkg_dev) --autodest debian/tmp/usr/bin/camitk-testcomponents
	dh_installman -p$(pkg_dev) debian/tmp/usr/share/man/man1/camitk-testcomponents.1

	# qtpropertybrowser so and headers (includes are put directly in usr/include as package should be generic/independant from CamiTK)
	dh_install -p$(pkg_qpb_dev) debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/libqtpropertybrowser.so usr/lib/$(DEB_HOST_MULTIARCH)
	dh_install -p$(pkg_qpb_dev) debian/tmp/usr/include/camitk-$(VER_SHORT)/libraries/qtpropertybrowser usr/include

	# imp
	dh_install -p$(pkg_imp) --autodest debian/tmp/usr/bin/camitk-imp
	dh_install -p$(pkg_imp) --autodest debian/tmp/usr/share/applications/camitk-imp.desktop
	dh_install -p$(pkg_imp) --autodest debian/tmp/usr/share/pixmaps/camitk-imp.xpm
	dh_installman -p$(pkg_imp) debian/tmp/usr/share/man/man1/camitk-imp.1

	# asm
	dh_install -p$(pkg_asm) --autodest debian/tmp/usr/bin/camitk-actionstatemachine
	dh_install -p$(pkg_asm) --autodest debian/tmp/usr/share/applications/camitk-actionstatemachine.desktop
	dh_install -p$(pkg_asm) --autodest debian/tmp/usr/share/pixmaps/camitk-actionstatemachine.xpm
	dh_installman -p$(pkg_asm) debian/tmp/usr/share/man/man1/camitk-actionstatemachine.1

	# packages for indep arch
	# api doc
	dh_install -p$(pkg_doc) debian/tmp/usr/share/camitk-$(VER_SHORT)/apidoc usr/share/doc/camitk-$(VER_SHORT)/apidoc

	# all test data excluding extra licence files (everything is already specified in d/c)
	dh_install -p$(pkg_data) --exclude=LICENSE --autodest debian/tmp/usr/share/camitk-$(VER_SHORT)/testdata

# required to avoid FTBFS for architecture build all or any
override_dh_missing:
	dh_missing --list-missing

# dpkg-shlibdeps needs to know about camitk extension directories to manage inner-dependencies
# between extensions (i.e., mml component extension depends on physicalmodel component extensions)
# Lib dependencies can be in any extensions lib directory (viewers, component and action extensions)
override_dh_shlibdeps:
	dh_shlibdeps -l$(PKG_LIB_DIR)/viewers/:$(PKG_LIB_DIR)/components/:$(PKG_LIB_DIR)/actions/:$(PKG_LIB_DIR)/

# see http://lists.debian.org/debian-mentors/2012/07/msg00124.html
get-orig-source:
	mkdir -p ../tarballs
	PERL_LWP_SSL_VERIFY_HOSTNAME=0 uscan --rename --verbose --force-download --destdir=../tarballs

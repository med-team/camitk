/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef SHOWFRAME_H
#define SHOWFRAME_H

#include <Action.h>

/**
 * This action allows you to show/hide the 3D axes representation of the component's frame
 */
class ShowFrame : public camitk::Action {
    Q_OBJECT

public:
    /// the constructor
    ShowFrame(camitk::ActionExtension*);

    /// Destructor
    virtual ~ShowFrame() = default;

    /// update the property visibility
    virtual QWidget* getWidget() override;

    /// manage change immediately
    virtual bool event(QEvent* e) override;

    /// Get the corresponding QAction (overriden to update the checked status)
    QAction* getQAction(camitk::Component* target = nullptr) override;

public slots:
    /// show/hide the frame depending on the user choice
    virtual camitk::Action::ApplyStatus apply() override;

private:
    // block auto update during UI refresh
    bool blockAutoUpdate;

    // currently selected/active component
    camitk::Component* comp;

    // get size in world coordinates for the frame representation depending on the component bounding radius and percentage
    double getFrameSize();
};

#endif // SHOWFRAME_H

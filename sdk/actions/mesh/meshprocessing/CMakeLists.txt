camitk_extension(ACTION_EXTENSION
                 NEEDS_COMPONENT_EXTENSION vtkmesh
                 DEFINES COMPILE_MESHPROCESSING_ACTION_API
                 CEP_NAME SDK
                 DESCRIPTION "Features some nice algorithms for mesh processing"
                 NEEDS_COMPONENT_EXTENSION vtkmesh
                 NEEDS_VIEWER_EXTENSION interactivegeometryviewer
                 INSTALL_ALL_HEADERS
                 ENABLE_AUTO_TEST
                 TEST_FILES brain.mha bassin.msh head1.STL liver-smooth.obj Mesh.off robot.wrl scissors.obj simple.vtk sinus_skin.vtk sinus_skull.vtk skull1.stl structured.vtk pointcloud-with-data.vtk polydata-with-data.vtk cube-with-point-and-cell-data.vtk fieldfile.vtk check-with-color-pointdata.vtk plate-with-data.vtk female.vtk brainImageSmooth.vtk structuredgrid-with-data.vtk male.vtk cube-with-tensors.vtk imageBoundingBox.vtk binary-mesh-with-pointdata.vtk unstructured_binary_with_celldata.vtk
)

# Recursively update the shiboken path variable containing the CamiTK SDK tree structure
set(SHIBOKEN_CAMITK_SDK_PATH ${SHIBOKEN_CAMITK_SDK_PATH}:${CMAKE_CURRENT_SOURCE_DIR} CACHE INTERNAL "") 

# "Append Meshes" action will always abort as only one mesh is given in input by testaction
# "Append Meshes" needs two meshes (appends first mesh to second mesh)
# TODO write a auto test using the asm

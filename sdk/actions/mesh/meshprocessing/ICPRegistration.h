/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef ICP_REGISTRATION_H
#define ICP_REGISTRATION_H

#include <QObject>

#include <Action.h>

/**
 * @ingroup group_sdk_actions_mesh_meshprocessing
 *
 * @brief
 * Perform an Iterative Closest Point registration between two meshes.
 *
 */
class ICPRegistration : public camitk::Action {

    Q_OBJECT

public:

    /// \enum DistanceMeasureType specify the mean distance mode
    enum DistanceMeasureType {
        RMS, ///< The RMS mode is the square root of the average of the sum of squares of the closest point distances
        ABS  ///< The Absolute Value mode is the mean of the sum of absolute values of the closest point distances.
    };
    Q_ENUM(DistanceMeasureType)

    /// \enum LandmarkTransformType Set the number of degrees of freedom to constrain the solution to.
    enum LandmarkTransformType {
        Rigidbody,  ///< rotation and translation only.
        Similarity, ///< rotation, translation and isotropic scaling.
        Affine      ///< collinearity is preserved, ratios of distances along a line are preserved
    };
    Q_ENUM(LandmarkTransformType)

public:

    /// the constructor
    ICPRegistration(camitk::ActionExtension* ext);

    /// the destructor
    virtual ~ICPRegistration() = default;

public slots :

    /// method applied when the action is called
    virtual camitk::Action::ApplyStatus apply();

};

#endif // ICP_REGISTRATION_H

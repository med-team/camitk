/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "CleanPolyData.h"

#include <Application.h>
#include <MeshComponent.h>
#include <Log.h>
#include <VtkMeshUtil.h>
using namespace camitk;

#include <vtkCleanPolyData.h>
#include <vtkCallbackCommand.h>

#include <Property.h>

// -------------------- CleanPolyData --------------------
CleanPolyData::CleanPolyData(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Clean Mesh");
    setDescription(tr("Merge duplicate points, and/or remove unused points and/or remove degenerate cells."));
    setComponentClassName("MeshComponent");

    // Setting classification family and tags
    setFamily("Mesh Processing");
    addTag(tr("vtkCleanPolyData"));
    addTag(tr("Clean vtkPolyData"));

    Property* tolerance = new Property("Tolerance", QVariant(1.0), "Tolerance is given as a percentage of the bounding box<br/>E.g. for a Tolerance value of <tt>1.0</tt>, if points are closer than <i>1.0%</i> of the bounding box dimensions, then they are merged.", "%");
    tolerance->setAttribute("minimum",  QVariant(0.0));
    tolerance->setAttribute("maximum",  QVariant(100.0));
    tolerance->setAttribute("singleStep", QVariant(0.5));
    addParameter(tolerance);
}


// --------------- apply -------------------
Action::ApplyStatus CleanPolyData::apply() {
    // set waiting cursor and status bar
    QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
    Application::showStatusBarMessage("Cleaning poly data...");
    Application::resetProgressBar();

    // use the last target
    MeshComponent* targetMesh = dynamic_cast<MeshComponent*>(getTargets().last());

    //-- check if this is a polydata and if this is an unstructured grid, extract the surface
    vtkSmartPointer<vtkPolyData> targetPolyData = VtkMeshUtil::vtkPointSetToVtkPolyData(targetMesh->getPointSet());

    //-- clean the polydata
    // remove redundant points
    vtkSmartPointer<vtkCleanPolyData> cleaner = vtkSmartPointer<vtkCleanPolyData>::New();
    cleaner->AddInputData(targetPolyData);
    cleaner->PointMergingOn();
    cleaner->SetTolerance(property("Tolerance").toDouble() * 0.01);    // % of the Bounding Box
    cleaner->ConvertLinesToPointsOn();
    cleaner->ConvertPolysToLinesOn();
    cleaner->ConvertStripsToPolysOn();

    vtkSmartPointer<vtkCallbackCommand> progressCallback = vtkSmartPointer<vtkCallbackCommand>::New();
    progressCallback->SetCallback(&Application::vtkProgressFunction);
    cleaner->AddObserver(vtkCommand::ProgressEvent, progressCallback);

    // lets do it!
    cleaner->Update();

    // Create a mesh Component
    vtkSmartPointer<vtkPointSet> resultPointSet = cleaner->GetOutput();
    new MeshComponent(resultPointSet, targetMesh->getName() + " clean");
    Application::refresh();

    // restore the normal cursor and progress bar
    Application::resetProgressBar();
    QApplication::restoreOverrideCursor();

    return SUCCESS;
}

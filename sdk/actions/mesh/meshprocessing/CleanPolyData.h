/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef CLEANPOLYDATA_H
#define CLEANPOLYDATA_H

#include <Action.h>
/**
 * @ingroup group_sdk_actions_mesh_meshprocessing
 *
 * @brief
 * Using vtkCleanPolyData for example to clean nodes that are too close.
 *
 */
class CleanPolyData : public camitk::Action {
    Q_OBJECT

public:
    /// the constructor
    CleanPolyData(camitk::ActionExtension*);

    /// the destructor
    virtual ~CleanPolyData() = default;

public slots:
    /// method called when the action is applied
    virtual camitk::Action::ApplyStatus apply();

};

#endif // CLEANPOLYDATA_H

/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
// CamiTK includes
#include "AppendMeshes.h"
#include <Application.h>
#include <MeshComponent.h>
#include <Log.h>
#include <Property.h>

// Vtk includes
#include <vtkAppendFilter.h>
#include <vtkUnstructuredGrid.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkLandmarkTransform.h>
#include <vtkCallbackCommand.h>

using namespace camitk;

AppendMeshes::AppendMeshes(camitk::ActionExtension* ext) : Action(ext) {

    this->setName("Append Meshes");
    this->setDescription(QString(tr("Append several meshes in one mesh.")));
    this->setComponentClassName("MeshComponent");
    this->setFamily("Mesh Processing");

    Property* mergePointsProperty = new Property(tr("Merge Points"), true, tr("Define if close points are merged"), "");
    addParameter(mergePointsProperty);
}

camitk::Action::ApplyStatus AppendMeshes::apply() {
    // set waiting cursor and status bar
    QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
    Application::showStatusBarMessage("Registration...");
    Application::resetProgressBar();
    vtkSmartPointer<vtkCallbackCommand> progressCallback = vtkSmartPointer<vtkCallbackCommand>::New();
    progressCallback->SetCallback(&Application::vtkProgressFunction);

    // at least 2 targets must be selected
    if (getTargets().size() < 2) {
        QApplication::restoreOverrideCursor();
        CAMITK_WARNING(tr("At least 2 mesh components must be selected. Action aborted."))
        return ABORTED;
    }

    MeshComponent* meshComp;
    vtkSmartPointer< vtkAppendFilter > appendFilter = vtkSmartPointer< vtkAppendFilter >::New();
    appendFilter->SetMergePoints(property("Merge Points").toInt());

    for (int i = 0; i < getTargets().size(); i++) {
        meshComp = dynamic_cast<MeshComponent*>(getTargets().at(i));
        if (meshComp == nullptr) {
            QApplication::restoreOverrideCursor();
            CAMITK_WARNING(tr("Component \"%1\" is not a MeshComponent. Action aborted.").arg(getTargets().at(i)->getName()))
            return ABORTED;
        }
        appendFilter->AddInputData(meshComp->getPointSet());
    }

    CAMITK_TRACE(tr("Append meshes"))

    appendFilter->AddObserver(vtkCommand::ProgressEvent, progressCallback);
    appendFilter->Modified();
    appendFilter->Update();

    new MeshComponent(appendFilter->GetOutput(), "merged meshes");

    // refresh restore the normal cursor and progress bar
    Application::refresh();
    Application::resetProgressBar();
    Application::showStatusBarMessage("");
    QApplication::restoreOverrideCursor();
    return SUCCESS;
}

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>SphereTopology</name>
    <message>
        <location filename="../../SphereTopology.cpp" line="56"/>
        <source>Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../SphereTopology.cpp" line="56"/>
        <source>This coordinate represents the center of the sphere. &lt;br /&gt;By default this value is (0,0,0).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../SphereTopology.cpp" line="59"/>
        <source>Radius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../SphereTopology.cpp" line="59"/>
        <source> This is the radius of the sphere. &lt;br /&gt;The default value is 0.5.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../SphereTopology.cpp" line="64"/>
        <source>Phi resolution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../SphereTopology.cpp" line="64"/>
        <source>This number represents the number of division between start phi and end phi on the sphere. &lt;br /&gt;The phi divisions are similar to latitude lines on the earth. &lt;br /&gt;&lt;br /&gt;The default phi resolution is 8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../SphereTopology.cpp" line="69"/>
        <source>Theta resolution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../SphereTopology.cpp" line="69"/>
        <source>This number represents the number of division between start theta and end theta around the sphere. &lt;br /&gt;The theta divisions are similar to longitude lines on the earth. &lt;br /&gt;The higher the resolution the closer the approximation will come to a sphere and the more polygons there will be. &lt;br /&gt;&lt;br /&gt;The default theta resolution is 8.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>

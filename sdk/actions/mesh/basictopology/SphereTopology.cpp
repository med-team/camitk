/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
// CamiTK includes
#include "SphereTopology.h"
#include <MeshComponent.h>
#include <Application.h>
#include <MainWindow.h>
#include <Property.h>

// Qt includes
#include <QVector3D>

// Vtk includes
#include <vtkCell.h>
#include <vtkPoints.h>
#include <vtkSmartPointer.h>
#include <vtkHexahedron.h>
#include <vtkUnstructuredGrid.h>
#include <vtkCellArray.h>
#include <vtkSphereSource.h>

using namespace camitk;

SphereTopology::SphereTopology(ActionExtension* extension) : Action(extension) {
    setName("Sphere Topology");
    setEmbedded(true);
    setDescription("Build a sphere");
    setComponentClassName(""); // empty component: it does not need any data to be triggered
    setFamily("Mesh Processing");
    addTag("Build Sphere");

    // parameters
    Property* centerProperty = new Property(tr("Center"), QVector3D(0.0, 0.0, 0.0), tr("This coordinate represents the center of the sphere. <br />By default this value is (0,0,0)."), "");
    addParameter(centerProperty);

    Property* radiusProperty = new Property(tr("Radius"), 0.5, tr(" This is the radius of the sphere. <br />The default value is 0.5."), "");
    radiusProperty->setAttribute("minimum", 0.1);
    radiusProperty->setAttribute("singleStep", 0.1);
    addParameter(radiusProperty);

    Property* phiProperty = new Property(tr("Phi resolution"), 8, tr("This number represents the number of division between start phi and end phi on the sphere. <br />The phi divisions are similar to latitude lines on the earth. <br /><br />The default phi resolution is 8"), "");
    phiProperty->setAttribute("minimum", 1);
    phiProperty->setAttribute("singleStep", 1);
    addParameter(phiProperty);

    Property* thetaProperty = new Property(tr("Theta resolution"), 8, tr("This number represents the number of division between start theta and end theta around the sphere. <br />The theta divisions are similar to longitude lines on the earth. <br />The higher the resolution the closer the approximation will come to a sphere and the more polygons there will be. <br /><br />The default theta resolution is 8."), "");
    thetaProperty->setAttribute("minimum", 1);
    thetaProperty->setAttribute("singleStep", 1);
    addParameter(thetaProperty);
}

Action::ApplyStatus SphereTopology::apply() {
    QVector3D center = property("Center").value<QVector3D>();
    double radius = property("Radius").toDouble();
    int phiRes = property("Phi resolution").toDouble();
    int thetaRes = property("Theta resolution").toDouble();

    vtkSmartPointer<vtkSphereSource> sphere = vtkSmartPointer<vtkSphereSource>::New();
    sphere->SetCenter(center.x(), center.y(), center.z());
    sphere->SetRadius(radius);
    sphere->SetPhiResolution(phiRes);
    sphere->SetThetaResolution(thetaRes);
    sphere->Update();

    vtkSmartPointer<vtkPolyData> pData = sphere->GetOutput();

    new MeshComponent(pData, "Sphere");

    Application::refresh();
    Application::resetProgressBar();
    Application::showStatusBarMessage("");

    return SUCCESS;
}

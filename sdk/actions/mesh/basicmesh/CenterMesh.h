/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef CENTERMESH_H
#define CENTERMESH_H

#include <Action.h>

namespace camitk {
class MeshComponent;
}

/**
 * @ingroup group_sdk_actions_mesh_basicmesh
 *
 * @brief
 * Center the current @ref camitk::MeshComponent in the center of the space coordinate.
 *
 * @note
 * At the end of the translation, the barycenter of the mesh is in (0,0,0).
 *
 *
 */
class CenterMesh : public camitk::Action {
    Q_OBJECT

public:
    /// the constructor
    CenterMesh(camitk::ActionExtension*);

    /// Destructor
    virtual ~CenterMesh() = default;

    /// just update the barycenter
    virtual QWidget* getWidget() override;

public slots:
    /// method called when the action is applied
    virtual camitk::Action::ApplyStatus apply() override;

    /// compute barycenter of targetMesh
    void updateMeshBarycenter(camitk::MeshComponent* targetMesh, double center[3]);
};

#endif // CENTERMESH_H

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>ChangeColor</name>
    <message>
        <location filename="../../ChangeColor.cpp" line="38"/>
        <source>Change the surface, wireframe or points colors of objects</source>
        <translation>Changer la couleur surface, fil de fer ou points desz objets</translation>
    </message>
    <message>
        <location filename="../../ChangeColor.cpp" line="42"/>
        <source>Color</source>
        <translation>Couleur</translation>
    </message>
</context>
<context>
    <name>ComputeNormals</name>
    <message>
        <location filename="../../ComputeNormals.cpp" line="19"/>
        <source>Compute normals of surface mesh.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ComputeNormals.cpp" line="22"/>
        <source>normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ComputeNormals.cpp" line="23"/>
        <source>surface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ComputeNormals.cpp" line="33"/>
        <source>Compute Curvatures...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExtractSelection</name>
    <message>
        <location filename="../../ExtractSelection.cpp" line="43"/>
        <source>Extract the current selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ExtractSelection.cpp" line="46"/>
        <source>selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ExtractSelection.cpp" line="53"/>
        <source>Extracting Surface...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MeshQuality</name>
    <message>
        <location filename="../../MeshQuality.cpp" line="140"/>
        <source>Triangles</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../MeshQuality.cpp" line="145"/>
        <source>Tetrahedra</source>
        <translation>Tetrahédre</translation>
    </message>
    <message>
        <location filename="../../MeshQuality.cpp" line="150"/>
        <source>Hexahedra</source>
        <translatorcomment>Quadrilatères</translatorcomment>
        <translation>Hexahédre</translation>
    </message>
    <message>
        <location filename="../../MeshQuality.cpp" line="155"/>
        <source>Quads</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MeshToImageStencil</name>
    <message>
        <location filename="../../MeshToImageStencil.cpp" line="54"/>
        <source>This action generates a new volume image and converts the mesh into a volume representation (vtkImageData) where the foreground voxels are colored and the background voxels are black.</source>
        <translation>Cette action génère une nouvelle image volumique et convertit le maillage en une représentation volumique (vtkImageData) où les sommets du premier plan sont de couleur et les sommets d&apos;arrière plan sont noirs.</translation>
    </message>
    <message>
        <location filename="../../MeshToImageStencil.cpp" line="59"/>
        <source>STE</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../MeshToImageStencil.cpp" line="65"/>
        <source>Dimension</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../MeshToImageStencil.cpp" line="65"/>
        <source>The dimension of the output volumic image</source>
        <translation>La dimension de l&apos;image volumique de sortie</translation>
    </message>
    <message>
        <location filename="../../MeshToImageStencil.cpp" line="68"/>
        <source>Origin</source>
        <translation>Origine</translation>
    </message>
    <message>
        <location filename="../../MeshToImageStencil.cpp" line="68"/>
        <source>The origin frame position</source>
        <translation>La Position du cadre d&apos;origine</translation>
    </message>
    <message>
        <location filename="../../MeshToImageStencil.cpp" line="71"/>
        <source>Spacing</source>
        <translation>Espacement</translation>
    </message>
    <message>
        <location filename="../../MeshToImageStencil.cpp" line="71"/>
        <source>The spacing between each voxel</source>
        <translation>L&apos;espacement entre chaque voxel</translation>
    </message>
    <message>
        <location filename="../../MeshToImageStencil.cpp" line="74"/>
        <source>Output file</source>
        <translation>Fichier de sortie</translation>
    </message>
    <message>
        <location filename="../../MeshToImageStencil.cpp" line="74"/>
        <source>The output filename</source>
        <translation>Le nom du fichier en sortie</translation>
    </message>
</context>
<context>
    <name>RenderingOption</name>
    <message>
        <location filename="../../RenderingOption.cpp" line="54"/>
        <source>Surface representation?</source>
        <translation>Représentation de la surface?</translation>
    </message>
    <message>
        <location filename="../../RenderingOption.cpp" line="54"/>
        <source>Allow the selected mesh(es) to be representated as a surface?</source>
        <translation>Permettre au(x) maillage(s) sélectionné(s) d&apos;être représentés comme une surface? </translation>
    </message>
    <message>
        <location filename="../../RenderingOption.cpp" line="57"/>
        <source>Wireframe representation?</source>
        <translation>Représentation fil de fer?</translation>
    </message>
    <message>
        <location filename="../../RenderingOption.cpp" line="57"/>
        <source>Allow the selected mesh(es) to be representated as wireframes?</source>
        <translation>Permettre au(x) maillage(s) sélectionné(s) d&apos;être représenté(s)sous forme de fil de fer?</translation>
    </message>
    <message>
        <location filename="../../RenderingOption.cpp" line="60"/>
        <source>Points representation?</source>
        <translation>Représentation en points?</translation>
    </message>
    <message>
        <location filename="../../RenderingOption.cpp" line="60"/>
        <source>Allow the selected mesh(es) to be representated as with points?</source>
        <translation>Permettre au(x) maillage(s) sélectionné(s) d&apos;être représenté(s) sous formle de points?</translation>
    </message>
    <message>
        <location filename="../../RenderingOption.cpp" line="63"/>
        <source>Label representation?</source>
        <translation>Représentation du label?</translation>
    </message>
    <message>
        <location filename="../../RenderingOption.cpp" line="63"/>
        <source>Allow the selected mesh(es) to be representated with its label?</source>
        <translation>Permettre au(x) maillage(s) sélectionné(s) d&apos;être représenté(s) avec son label?</translation>
    </message>
    <message>
        <location filename="../../RenderingOption.cpp" line="66"/>
        <source>Glyph representation?</source>
        <translation>Représentation de symbole? </translation>
    </message>
    <message>
        <location filename="../../RenderingOption.cpp" line="66"/>
        <source>Allow the selected mesh(es) to have glyphes representation?</source>
        <translation>Permettre au(x) maillage(s) sélectionné(s) d&apos;être représenté(s) sous forme de symbole?</translation>
    </message>
    <message>
        <location filename="../../RenderingOption.cpp" line="69"/>
        <source>Normals representation?</source>
        <translation>Représentation des normes?</translation>
    </message>
    <message>
        <location filename="../../RenderingOption.cpp" line="69"/>
        <source>Allow the selected mesh(es) to be representated with its normals?</source>
        <translation>Permettre au(x) maillage(s) sélectionné(s) d&apos;être représenté(s) avec ses normes?</translation>
    </message>
</context>
<context>
    <name>RigidTransformDialog</name>
    <message>
        <location filename="../../RigidTransformDialog.ui" line="23"/>
        <source>Rigid Transformation</source>
        <translation>Tranformation rigide</translation>
    </message>
    <message>
        <location filename="../../RigidTransformDialog.ui" line="26"/>
        <source>TransformDialog</source>
        <translation>Boite de dialogue Transforme</translation>
    </message>
    <message>
        <location filename="../../RigidTransformDialog.ui" line="39"/>
        <source>Translation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../RigidTransformDialog.ui" line="57"/>
        <source>Rotation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../RigidTransformDialog.ui" line="75"/>
        <source>Scaling</source>
        <translation>Echelonner</translation>
    </message>
    <message>
        <location filename="../../RigidTransformDialog.ui" line="97"/>
        <source>Automatically Refresh</source>
        <translation>Rafraichir automatiquement</translation>
    </message>
    <message>
        <location filename="../../RigidTransformDialog.ui" line="107"/>
        <source>Concatenate Transformations</source>
        <translation>Concatener les transformations</translation>
    </message>
    <message>
        <location filename="../../RigidTransformDialog.ui" line="114"/>
        <source>Uniform Scaling</source>
        <translation>Echelonnement uniforme</translation>
    </message>
    <message>
        <location filename="../../RigidTransformDialog.ui" line="123"/>
        <source>Reset</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../RigidTransformDialog.ui" line="149"/>
        <source>&amp;Load</source>
        <translation>&amp;Charger</translation>
    </message>
    <message>
        <location filename="../../RigidTransformDialog.ui" line="162"/>
        <source>&amp;Save</source>
        <translation>&amp;Sauver</translation>
    </message>
    <message>
        <location filename="../../RigidTransformDialog.ui" line="185"/>
        <source>Preview</source>
        <translation>Aperçu</translation>
    </message>
    <message>
        <location filename="../../RigidTransformDialog.ui" line="195"/>
        <source>Apply</source>
        <translation>Appliquer</translation>
    </message>
    <message>
        <location filename="../../RigidTransformDialog.ui" line="208"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
</context>
</TS>

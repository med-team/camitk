<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>CloseAction</name>
    <message>
        <location filename="../../CloseAction.cpp" line="36"/>
        <source>Close the currently selected components</source>
        <translation>Fermer les components couramment sélectionnés</translation>
    </message>
    <message>
        <location filename="../../CloseAction.cpp" line="42"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
</context>
<context>
    <name>CloseAllAction</name>
    <message>
        <location filename="../../CloseAllAction.cpp" line="59"/>
        <source>Closing all the documents...</source>
        <translation>Fermeture de tous les documents ...</translation>
    </message>
    <message>
        <source>Close all components, prompting for additional information if needed</source>
        <translation type="obsolete">Fermer tous les components </translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Fermer</translation>
    </message>
</context>
<context>
    <name>OpenAction</name>
    <message>
        <location filename="../../OpenAction.cpp" line="41"/>
        <source>Open data (component) from a file</source>
        <translation>Ouvrir les données (component) à partir d&apos;un fichier</translation>
    </message>
    <message>
        <location filename="../../OpenAction.cpp" line="48"/>
        <source>Open</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../../OpenAction.cpp" line="68"/>
        <source>Opening file...</source>
        <translation>Ouverture du fichier ...</translation>
    </message>
    <message>
        <location filename="../../OpenAction.cpp" line="83"/>
        <source>Select One or More Files to Open</source>
        <translation>Sélectionner Un ou Plusieurs Fichiers à Ouvrir</translation>
    </message>
    <message>
        <location filename="../../OpenAction.cpp" line="91"/>
        <source>All files loaded.</source>
        <translation>Tous les fichiers sont chargés.</translation>
    </message>
    <message>
        <location filename="../../OpenAction.cpp" line="94"/>
        <source>Error loading files: </source>
        <translation>Erreur au chargement des fichiers :</translation>
    </message>
    <message>
        <location filename="../../OpenAction.cpp" line="98"/>
        <source>Open aborted.</source>
        <translation>Ouverture abandonnée.</translation>
    </message>
</context>
<context>
    <name>OpenFile</name>
    <message>
        <location filename="../../OpenFile.cpp" line="45"/>
        <source>Open data (component) from a given file</source>
        <translation>Ouvrir les données (component) à partir d&apos;un fichier donné</translation>
    </message>
    <message>
        <location filename="../../OpenFile.cpp" line="53"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="../../OpenFile.cpp" line="62"/>
        <source>File name</source>
        <translation>Nom du fichier</translation>
    </message>
    <message>
        <location filename="../../OpenFile.cpp" line="62"/>
        <source>The name of the file to open.</source>
        <translation>Le nom du fichier à ouvrir.</translation>
    </message>
    <message>
        <location filename="../../OpenFile.cpp" line="85"/>
        <source>File Name:</source>
        <translation>Nom du Fichier:</translation>
    </message>
    <message>
        <location filename="../../OpenFile.cpp" line="89"/>
        <source>Browse</source>
        <translation>Naviguer</translation>
    </message>
    <message>
        <location filename="../../OpenFile.cpp" line="100"/>
        <source>Open Selected File</source>
        <translation>Ouvrir le Fichier Sélectionné</translation>
    </message>
    <message>
        <location filename="../../OpenFile.cpp" line="143"/>
        <source>Select One File to Open</source>
        <translation>Sélectionner Un Fichier à Ouvrir</translation>
    </message>
</context>
<context>
    <name>QuitAction</name>
    <message>
        <location filename="../../QuitAction.cpp" line="39"/>
        <source>Exit the application, prompting for additional information if needed</source>
        <translation>Sortir de l&apos;application </translation>
    </message>
    <message>
        <location filename="../../QuitAction.cpp" line="45"/>
        <source>Quit Application</source>
        <translation>Quitter l&apos;Application</translation>
    </message>
    <message>
        <location filename="../../QuitAction.cpp" line="46"/>
        <source>Exit Application</source>
        <translation>Sortir de l&apos;Application</translation>
    </message>
</context>
<context>
    <name>SaveAction</name>
    <message>
        <location filename="../../SaveAction.cpp" line="37"/>
        <source>Save all the top-level of the selected components</source>
        <translation>Sauver le top-level des components sélectionnés</translation>
    </message>
    <message>
        <location filename="../../SaveAction.cpp" line="43"/>
        <source>Save</source>
        <translation>Sauver</translation>
    </message>
</context>
<context>
    <name>SaveAllAction</name>
    <message>
        <location filename="../../SaveAllAction.cpp" line="43"/>
        <source>Saves all the currently loaded data</source>
        <translation>Sauver toutes les données chargées courantes</translation>
    </message>
    <message>
        <location filename="../../SaveAllAction.cpp" line="49"/>
        <source>Save All</source>
        <translation>Tout Sauver</translation>
    </message>
    <message>
        <location filename="../../SaveAllAction.cpp" line="64"/>
        <source>Saving all data...</source>
        <translation>Sauvegarde de toutes les données ...</translation>
    </message>
    <message>
        <location filename="../../SaveAllAction.cpp" line="73"/>
        <source>Ready.</source>
        <translation>Prêt.</translation>
    </message>
</context>
<context>
    <name>SaveAsAction</name>
    <message>
        <location filename="../../SaveAsAction.cpp" line="44"/>
        <source>Save the currently selected data under a different filename or format</source>
        <translation>Sauver les données sélectionnées courantes sous un fichier ou format différent</translation>
    </message>
    <message>
        <location filename="../../SaveAsAction.cpp" line="50"/>
        <source>Save As</source>
        <translation>Sauver Comme</translation>
    </message>
    <message>
        <location filename="../../SaveAsAction.cpp" line="71"/>
        <source>Saving currently selected component under new filename or format...</source>
        <translation>Sauvegarde du componet récemment selectionné sous un nouveau non de fichier ou format ...</translation>
    </message>
    <message>
        <location filename="../../SaveAsAction.cpp" line="172"/>
        <location filename="../../SaveAsAction.cpp" line="177"/>
        <source>Save File As...</source>
        <translation>Sauvegarde du Fichier sous ...</translation>
    </message>
    <message>
        <location filename="../../SaveAsAction.cpp" line="193"/>
        <source>Saving aborted</source>
        <translation>Sauvegarde abandonnée</translation>
    </message>
</context>
</TS>

/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef OPEN_WORKSPACE_H
#define OPEN_WORKSPACE_H

#include <Action.h>

class QFrame;
class QLineEdit;

/**
 * @ingroup group_sdk_actions_application
 *
 * @brief
 * When triggered, the OpenFile automatically opens the given file.
 */
class OpenWorkspace : public camitk::Action {
    Q_OBJECT
public:
    /// Default Constructor
    OpenWorkspace(camitk::ActionExtension*);

    /// Default Destructor
    virtual ~OpenWorkspace();

    /// Returns NULL: no permanent widget for this action. The GUI is shown as a one-shot dialog in apply
    virtual QWidget* getWidget();

public slots:
    /// apply the action i.e. show a file dialog and open the selected file(s)
    virtual camitk::Action::ApplyStatus apply();

    /// open a file dialog and set the corresponding property
    void selectFile();

private:
    QFrame* myWidget;
    QLineEdit* fileBrowserLineEdit;
};
#endif // OPEN_WORKSPACE_H

/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef SAVEWORKSPACE_H
#define SAVEWORKSPACE_H

#include <QObject>
#include <Action.h>
#include <Component.h>

/**
 * @ingroup group_sdk_actions_application
 *
 * @brief
 * Save the current workspace.
 */
class SaveWorkspace : public camitk::Action {
    Q_OBJECT

public:
    /// Default Constructor
    SaveWorkspace(camitk::ActionExtension*);

    /// Default Destructor
    virtual ~SaveWorkspace();

    /// Returns NULL: no permanent widget for this action. The GUI is run shown a one-shot dialog in apply
    virtual QWidget* getWidget();

public slots:
    /** this method is automatically called when the action is triggered.
      * It gets all the targets' top level components and save them.
      */
    virtual camitk::Action::ApplyStatus apply();

};
#endif // SAVEWORKSPACE_H

/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "SaveWorkspace.h"

#include <Application.h>
#include <Log.h>

#include <QFileDialog>

using namespace camitk;


// --------------- constructor -------------------
SaveWorkspace::SaveWorkspace(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Save Workspace");
    setEmbedded(false);
    setDescription(tr("Save the workspace"));
    setComponentClassName("");
    setIcon(QPixmap(":/fileSave"));

    // Setting classification family and tags
    setFamily("File");
    addTag(tr("Save"));
    addTag(tr("Workspace"));
}

// --------------- destructor -------------------
SaveWorkspace::~SaveWorkspace() {
    // do not delete the widget has it might have been used in the ActionViewer (i.e. the ownership might have been taken by the stacked widget)
}

// --------------- getWidget --------------
QWidget* SaveWorkspace::getWidget() {
    return nullptr;
}

// --------------- apply -------------------
Action::ApplyStatus SaveWorkspace::apply() {
    Application::showStatusBarMessage(tr("Saving the workspace..."));

    QString suggestedName = "Workspace.camitk";

    // get the file name
    QString filename = QFileDialog::getSaveFileName(nullptr, tr("Save Workspace As..."), suggestedName, "CamiTK file (*.camitk);;");

    if (!filename.isEmpty()) {
        // save the workspace
        bool saveStatus = Application::saveWorkspace(filename);
        if (saveStatus) {
            return SUCCESS;
        }
        else {
            return ABORTED;
        }
    }
    else {
        Application::showStatusBarMessage(tr("Saving workspace aborted"), 2000);
        // CCC Exception: just a trace message as the user voluntarily aborted the action
        CAMITK_TRACE(tr("A filename is required. Action aborted."))
        return ABORTED;
    }
}

/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "ApplicationFileActionExtension.h"

#include "OpenAction.h"
#include "OpenFile.h"
#include "OpenWorkspace.h"
#include "OpenWorkspaceAction.h"
#include "SaveAction.h"
#include "CloseAction.h"
#include "CloseAllAction.h"
#include "SaveAsAction.h"
#include "SaveAllAction.h"
#include "SaveWorkspace.h"
#include "QuitAction.h"

// --------------- init -------------------
void ApplicationFileActionExtension::init() {
    registerNewAction(OpenAction);
    registerNewAction(OpenFile);
    registerNewAction(OpenWorkspaceAction);
    registerNewAction(OpenWorkspace);
    registerNewAction(SaveAction);
    registerNewAction(SaveAsAction);
    registerNewAction(SaveAllAction);
    registerNewAction(SaveWorkspace);
    registerNewAction(CloseAction);
    registerNewAction(CloseAllAction);
    registerNewAction(QuitAction);
}

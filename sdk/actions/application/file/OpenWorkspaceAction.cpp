/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "OpenWorkspaceAction.h"

// CamiTK
#include <Application.h>
#include <Log.h>
#include <ExtensionManager.h>

// Qt
#include <QFileDialog>

using namespace camitk;

// --------------- constructor -------------------
OpenWorkspaceAction::OpenWorkspaceAction(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Open Workspace File");
    setEmbedded(false);
    setDescription(tr("Open workspace from a file"));
    setComponentClassName("");

    // Setting classification family and tags
    setFamily("File");
    addTag(tr("Open"));
    addTag(tr("Workspace"));

    // add a shortcut
    setIcon(QPixmap(":/fileOpen"));
}

// --------------- destructor -------------------
OpenWorkspaceAction::~OpenWorkspaceAction() {
}

// --------------- getWidget --------------
QWidget* OpenWorkspaceAction::getWidget() {
    return nullptr;
}

// --------------- apply -------------------
Action::ApplyStatus OpenWorkspaceAction::apply() {
    Application::showStatusBarMessage(tr("Opening workspace file..."));

    // filter: extension of known file format
    QString filter = "CamiTK file (*.camitk);;";
    // Open one file
    QString fileToOpen = QFileDialog::getOpenFileName(NULL, tr("Select One File to Open"), Application::getLastUsedDirectory().absolutePath(), filter);

    if (!fileToOpen.isEmpty()) {
        if (Application::loadWorkspace(fileToOpen)) {
            Application::showStatusBarMessage(tr("Workspace opened."));
            return SUCCESS;
        }
        else {
            Application::showStatusBarMessage(tr("Loading .camitk file failed. Action aborted."));
            return ABORTED;
        }
    }
    else {
        Application::showStatusBarMessage(tr("Open aborted."));
        return ABORTED;
    }
}

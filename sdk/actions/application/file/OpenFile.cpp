/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "OpenFile.h"

// CamiTK includes
#include <Application.h>
#include <ExtensionManager.h>
#include <Property.h>
#include <Log.h>

// Qt includes
#include <QFileDialog>
#include <QBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QFrame>

using namespace camitk;

// --------------- constructor -------------------
OpenFile::OpenFile(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Open File");
    setDescription(tr("Open data (component) from a given file"));
    setComponentClassName("");

    setIcon(QPixmap(":/fileOpen"));

    // Setting classification family and tags
    setFamily("File");
    addTag(tr("Open"));

    // add a shortcut
    getQAction()->setShortcut(QKeySequence::Open);
    getQAction()->setShortcutContext(Qt::ApplicationShortcut);

    // the unique property is the filename
    Property* fileNameProperty = new Property(tr("File Name"), "", tr("The name of the file to open."), "");
    addParameter(fileNameProperty);

    // lazy instantiation
    myWidget = NULL;
}

// --------------- destructor -------------------
OpenFile::~OpenFile() {
}

// --------------- getWidget --------------
QWidget* OpenFile::getWidget() {
    if (!myWidget) {
        //-- the frame
        myWidget = new QFrame();
        myWidget->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
        myWidget->setLineWidth(3);

        //-- the vertical layout: put every GUI elements in it
        QVBoxLayout* myWidgetLayout = new QVBoxLayout();

        //-- build file browsing widget
        QLabel* fileBrowserLabel = new QLabel(tr("File Name:"));
        fileBrowserLineEdit = new QLineEdit();
        fileBrowserLineEdit->setText(property("File Name").toString());
        fileBrowserLineEdit->setReadOnly(true);
        QPushButton* fileBrowserButton = new QPushButton(tr("Browse"));
        connect(fileBrowserButton, SIGNAL(clicked()), this, SLOT(selectFile()));

        //-- the horizontal layout for file browsing
        QHBoxLayout* fileBrowserLayout = new QHBoxLayout();
        fileBrowserLayout->addWidget(fileBrowserLabel);
        fileBrowserLayout->addWidget(fileBrowserLineEdit);
        fileBrowserLayout->addWidget(fileBrowserButton);
        myWidgetLayout->addLayout(fileBrowserLayout);

        //-- build the open button
        QPushButton* openSelected = new QPushButton(tr("Open Selected File"));
        QObject::connect(openSelected, SIGNAL(clicked()), this, SLOT(apply()));

        //-- the horizontal layout for the open button
        QHBoxLayout* buttonLayout = new QHBoxLayout;
        buttonLayout->addWidget(openSelected);
        myWidgetLayout->addLayout(buttonLayout);

        //-- set the layout for the action widget
        myWidget->setLayout(myWidgetLayout);
    }

    return myWidget;
}

// --------------- apply -------------------
Action::ApplyStatus OpenFile::apply() {
    QString fileToOpen = property("File Name").toString();

    if (!fileToOpen.isEmpty()) {
        Application::showStatusBarMessage(tr(QString("Opening file " + fileToOpen + "...").toStdString().c_str()));
        Application::open(fileToOpen);
        return SUCCESS;
    }
    else {
        CAMITK_WARNING(tr("A filename is required. Action aborted."))
        return ABORTED;
    }
}

// --------------- selectFile -------------------
void OpenFile::selectFile() {
    // filter: extension of known file format, example: (*.xml *.vtk *.wrl)
    QString filter;

    // first item = all known files
    filter += QString("All known files (*.") + ExtensionManager::getFileExtensions().join(" *.") + ");;";

    // add the extension of loaded and valid plugins
    foreach (ComponentExtension* ext, ExtensionManager::getComponentExtensionsList()) {
        filter += ext->getName() + " (*." + ext->getFileExtensions().join(" *.") + ");;";
    }

    // Open only one file
    QString fileName = QFileDialog::getOpenFileName(NULL, tr("Select One File to Open"), Application::getLastUsedDirectory().absolutePath(), filter);

    if (!fileName.isEmpty()) {
        fileBrowserLineEdit->setText(fileName);
        setProperty("File Name", fileName);
    }
}



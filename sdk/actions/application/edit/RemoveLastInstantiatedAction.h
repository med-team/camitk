/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef REMOVE_LAST_INSTANTIATED_ACTION_H
#define REMOVE_LAST_INSTANTIATED_ACTION_H

#include <Action.h>

/**
 * @ingroup group_sdk_actions_application
 *
 * @brief
 * Remove the last top-level selected component (if several top level components were selected, the latest one is removed).
 * Note that no prompt is asked to the user to save or not the component if unsaved.
 *
 * \note the name of this class and the name of the action is mispelled.
 * The spelling mistake is kept for API backward compatibility reason.
 */
class RemoveLastInstantiatedAction : public camitk::Action {
    Q_OBJECT

public:
    /// Default Constructor
    RemoveLastInstantiatedAction(camitk::ActionExtension*);

    /// Default Destructor
    virtual ~RemoveLastInstantiatedAction() = default;

    /// Returns NULL: no widget at all for this action
    virtual QWidget* getWidget();

public slots:
    /// apply the action select the last instantiated component
    virtual camitk::Action::ApplyStatus apply();
};

#endif // REMOVE_LAST_INSTANTIATED_ACTION_H

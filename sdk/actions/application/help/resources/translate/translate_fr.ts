<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>AboutAction</name>
    <message>
        <location filename="../../AboutAction.cpp" line="43"/>
        <source>Show a little dialog about the application</source>
        <translation>Montrer une petite information à propos de l&apos;application</translation>
    </message>
    <message>
        <location filename="../../AboutAction.cpp" line="49"/>
        <source>About</source>
        <translation>A propos</translation>
    </message>
    <message>
        <location filename="../../AboutAction.cpp" line="65"/>
        <source>About </source>
        <translation>A propos</translation>
    </message>
    <message>
        <location filename="../../AboutAction.cpp" line="76"/>
        <source>Build with </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../AboutAction.cpp" line="79"/>
        <source>Please visit &lt;a href=&apos;http://camitk.imag.fr&apos;&gt;camitk.imag.fr&lt;/a&gt; for more information.&lt;br/&gt;</source>
        <translation>Veuillez visiter &lt;a href=&apos;http://camitk.imag.fr&apos;&gt;camitk.imag.fr&lt;/a&gt; pour plus d&apos;informations.&lt;br/&gt;</translation>
    </message>
    <message>
        <location filename="../../AboutAction.cpp" line="80"/>
        <source>(c) UJF-Grenoble 1, CNRS, TIMC-IMAG UMR 5525</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>BugReportDialog</name>
    <message>
        <location filename="../../../../src/sdk/actions/application/BugReportDialog.ui" line="14"/>
        <source>Report Bug Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../src/sdk/actions/application/BugReportDialog.ui" line="32"/>
        <source>Generated bug report to complete and paste in the :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../src/sdk/actions/application/BugReportDialog.ui" line="39"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../src/sdk/actions/application/BugReportDialog.ui" line="74"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../src/sdk/actions/application/BugReportDialog.ui" line="81"/>
        <source>Copy to Clilpboard</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChangeLanguage</name>
    <message>
        <location filename="../../ChangeLanguage.cpp" line="145"/>
        <source>Information</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ChangeLanguage.cpp" line="145"/>
        <source>To apply translation, restart the application !</source>
        <translation>Pour appliquer la traduction, redémarrer votre application !</translation>
    </message>
</context>
<context>
    <name>ShowConsoleAction</name>
    <message>
        <location filename="../../ShowConsoleAction.cpp" line="37"/>
        <source>Show the redirection console window (view debugging information, cout, cerr,...)</source>
        <translation>Montrer la fenêtre de la redirection de la console  (view debugging information, cout, cerr,...)</translation>
    </message>
    <message>
        <location filename="../../ShowConsoleAction.cpp" line="43"/>
        <source>Console</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ShowConsoleAction.cpp" line="44"/>
        <source>Terminal</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ReportBugDialog</name>
    <message>
        <location filename="../../../../src/sdk/actions/application/ReportBugDialog.cpp" line="40"/>
        <source>&lt;a href=&apos;https://bugzilla-timc.imag.fr/&apos;&gt;Bugzilla Bug Report Tool&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../../../Dev/CamiTK/camitk/sdk/actions/application/ReportBugDialog.cpp" line="63"/>
        <source>&lt;b&gt;&lt;font color=&apos;red&apos;&gt;Impossible to load </source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>

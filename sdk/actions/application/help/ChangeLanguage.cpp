/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "ChangeLanguage.h"

// CamiTK
#include <Application.h>
#include <Log.h>

// Qt
#include <QPushButton>
#include <QFrame>
#include <QComboBox>
#include <QVBoxLayout>

using namespace camitk;

// --------------- constructor -------------------
ChangeLanguage::ChangeLanguage(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Change Language");
    setDescription("Change Language with a given .qm file ");
    setComponentClassName("");
    setIcon(QPixmap(""));
    // Setting classification family and tags
    setFamily("Help");
    addTag("Change");

    // the propertes are the language selected of Language and Flag
    setProperty("Language Selected", QVariant(""));
    setProperty("Flag File Name", QVariant(""));

    // lazy instantiation
    myWidget = NULL;

    // Get the path of the Language File in the setting file
    QSettings&   settings(Application::getSettings());
    settings.beginGroup(Application::getName() + ".Application");
    // Get the language and flag for the current Application instance
    QString language = settings.value("language").toString();
    ///QString languageAbreviation = settings.value("languageAbreviation").toString();
    QString flagFile = settings.value("flagFilename").toString();
    settings.endGroup();

    // install the flag
    if (!flagFile.isEmpty()) {
        setIcon(QPixmap(flagFile));
    }

    // the properties are the filename of Language and Flag
    setProperty("Language Selected", language);
    setProperty("Flag File Name", flagFile);

    // the list of Language and associed Flag known
    languageAndFlag l1 = { "french", "fr", ":/flags/flags/16/flag_fr.png" };
    listLanguageAndFlag.append(l1);

    languageAndFlag l2 = { "english", "en", ":/flags/flags/16/flag_us.png" };
    listLanguageAndFlag.append(l2);

}

// --------------- destructor -------------------
ChangeLanguage::~ChangeLanguage() {
}

// --------------- getWidget --------------
QWidget* ChangeLanguage::getWidget() {
    if (!myWidget) {
        //-- the frame
        myWidget = new QFrame();
        myWidget->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
        myWidget->setLineWidth(3);

        //-- the vertical layout: put every GUI elements in it
        QVBoxLayout* myWidgetLayout = new QVBoxLayout();

        languageList = new QComboBox();
        for (int i = 0; i < listLanguageAndFlag.size(); i++) {
            languageList->addItem(QIcon(listLanguageAndFlag[i].flagfilename), listLanguageAndFlag[i].language);
        }

        QHBoxLayout* fileBrowserLayout0 = new QHBoxLayout();
        fileBrowserLayout0->addWidget(languageList);
        myWidgetLayout->addLayout(fileBrowserLayout0);

        //-- build the open button
        QPushButton* openSelected = new QPushButton("Confirm Language Changement");
        QObject::connect(openSelected, SIGNAL(clicked()), this, SLOT(apply()));

        //-- the horizontal layout for the open button
        QHBoxLayout* buttonLayout = new QHBoxLayout;
        buttonLayout->addWidget(openSelected);
        myWidgetLayout->addLayout(buttonLayout);

        //-- set the layout for the action widget
        myWidget->setLayout(myWidgetLayout);
    }

    return myWidget;
}

// --------------- apply -------------------
Action::ApplyStatus ChangeLanguage::apply() {

    QString languageSelected = listLanguageAndFlag[languageList->currentIndex()].language;
    QString languageAbreviation = listLanguageAndFlag[languageList->currentIndex()].languageAbreviation;
    QString flagFileToOpen = listLanguageAndFlag[languageList->currentIndex()].flagfilename;

    // save settings (the filename of the language used)
    QSettings&   settings(Application::getSettings());
    settings.beginGroup(Application::getName() + ".Application");
    settings.setValue("language", languageSelected);
    settings.setValue("languageAbreviation", languageAbreviation);
    settings.setValue("flagFilename", flagFileToOpen);
    settings.endGroup();

    CAMITK_WARNING(tr("To apply the new language settings, you need to restart the application"))

    return SUCCESS;

}

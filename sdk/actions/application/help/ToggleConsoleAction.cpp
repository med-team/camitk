/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "ToggleConsoleAction.h"

#include <Application.h>
#include <MainWindow.h>

using namespace camitk;

// --------------- constructor -------------------
ToggleConsoleAction::ToggleConsoleAction(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Toggle Log Console");
    setEmbedded(false);
    setDescription(tr("Toggle the redirection console window that shows log and std output."));
    setComponentClassName("");
    setIcon(QPixmap(":/showConsole"));

    // Setting classification family and tags
    setFamily("Help");
    addTag(tr("Console"));
    addTag(tr("Terminal"));
    addTag(tr("Show Console"));
    addTag(tr("Log"));

    // can be checked or not (on/off), useful when group in a QActionGroup (radio buttons)
    getQAction()->setCheckable(true);
}


// --------------- destructor -------------------
ToggleConsoleAction::~ToggleConsoleAction() {
    // do not delete the widget has it might have been used in the ActionViewer (i.e. the ownership might have been taken by the stacked widget)
}

// --------------- getWidget --------------
QWidget* ToggleConsoleAction::getWidget() {
    if (Application::getMainWindow()) {
        getQAction()->setChecked(Application::getMainWindow()->getConsoleVisibility());
    }
    return nullptr;
}

// --------------- apply -------------------
Action::ApplyStatus ToggleConsoleAction::apply() {
    bool toggle = Application::getMainWindow()->getConsoleVisibility();
    Application::getMainWindow()->showConsole(!toggle);
    if (Application::getMainWindow()) {
        getQAction()->setChecked(Application::getMainWindow()->getConsoleVisibility());
    }
    return SUCCESS;
}

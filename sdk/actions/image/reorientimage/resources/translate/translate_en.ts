<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>ReorientImage</name>
    <message>
        <location filename="../../ReorientImage.ui" line="23"/>
        <source>Reorient Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ReorientImage.ui" line="26"/>
        <source>TransformDialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ReorientImage.ui" line="37"/>
        <source>Explanation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ReorientImage.ui" line="67"/>
        <source>Apply to the Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ReorientImage.ui" line="89"/>
        <source>How was your image acquired ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ReorientImage.ui" line="97"/>
        <source>Dicom RCS Label:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ReorientImage.ui" line="111"/>
        <source>Y Direction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ReorientImage.ui" line="118"/>
        <source>Right     (right of the patient) To Left        (left of the patient)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ReorientImage.ui" line="125"/>
        <source>X Direction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ReorientImage.ui" line="132"/>
        <source>Anterior (face of the patient) To Posterior (back of the patient)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ReorientImage.ui" line="139"/>
        <source>Z Direction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ReorientImage.ui" line="146"/>
        <source>Inferior  (feet of the patient)  To Superior  (head of the patient)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ReorientImage.ui" line="173"/>
        <source>Model Display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ReorientImage.ui" line="180"/>
        <source>Male</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ReorientImage.ui" line="187"/>
        <source>Female</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ReorientImage.ui" line="199"/>
        <source>Reset Image Origin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ReorientImage.ui" line="206"/>
        <source>Do not create new image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ReorientImage.cpp" line="79"/>
        <source>This action re-orients a medical image according to Dicom RCS (Reference Coordinates System).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ReorientImage.cpp" line="84"/>
        <source>Dicom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ReorientImage.cpp" line="85"/>
        <source>Orientation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ReorientImage.cpp" line="115"/>
        <source>Use male or female model for image orientation illustration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ReorientImage.cpp" line="117"/>
        <source>Reset the image frame so that the transform origin and the original origin are the same</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ReorientImage.cpp" line="119"/>
        <source>Do not create a new re-oriented image, but apply the transformations on the input image.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>

<?xml version="1" encoding="UTF-8"?>
<!-- from 
Works well for CT scan stored in short.

Article (SILVERSTEIN2008927)
Silverstein, J. C.; Parsad, N. M. & Tsirline, V.
Automatic perceptual color map generation for realistic volume visualization 
Journal of Biomedical Informatics, 2008, 41, 927 - 935 

Abstract: Advances in computed tomography imaging technology and inexpensive high performance computer graphics hardware are making high-resolution, full color (24-bit) volume visualizations commonplace. However, many of the color maps used in volume rendering provide questionable value in knowledge representation and are non-perceptual thus biasing data analysis or even obscuring information. These drawbacks, coupled with our need for realistic anatomical volume rendering for teaching and surgical planning, has motivated us to explore the auto-generation of color maps that combine natural colorization with the perceptual discriminating capacity of grayscale. As evidenced by the examples shown that have been created by the algorithm described, the merging of perceptually accurate and realistically colorized virtual anatomy appears to insightfully interpret and impartially enhance volume rendered patient data.

Air filled cavity	−1000	(0, 0, 0)
Lung parenchyma	−600 to −400	(194, 105, 82)
Fat	−100 to −60	(194, 166, 115)
Soft tissue	+40 to +80	(102 ↔ 153, 0, 0)
Bone	+400 to +1000	(255, 255, 255)

-->

<vrColorMap xmlns = "http://camitk.imag.fr/vrColormap"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
        xsi:schemaLocation="http://camitk.imag.fr/vrColormap vrColormap.xsd">
    <transparencies>
        <transparency gray="-1000" value="1.0"/>
        <transparency gray="-601"  value="0.9"/>
        <transparency gray="-101"  value="0.9"/>
        <transparency gray="-100"  value="0.0"/>
        <transparency gray="1000"  value="1.0"/> 
    </transparencies>
    <colors>
        <color gray="-1000" red="0" green="0"  blue="0"  alpha="255"/>
        <color gray="-601" red="0" green="0" blue="0" alpha="255"/>
        <color gray="-600" red="194" green="105" blue="85" alpha="255"/>
        <color gray="-150" red="194" green="105" blue="85" alpha="255"/>
        <color gray="-100" red="194" green="166" blue="115" alpha="255"/>
        <color gray="-60" red="194" green="166" blue="115" alpha="255"/>
        <color gray="40" red="102" green="0" blue="0" alpha="255"/>
        <color gray="80" red="153" green="0" blue="0" alpha="255"/>
        <color gray="400" red="255" green="255" blue="255" alpha="255"/>
        <color gray="1000" red="255" green="255" blue="255" alpha="255"/>
    </colors>
    <gradientOpacities>
        <gradientOpacity gray="0"   value="0.0"/>
        <gradientOpacity gray="255" value="1.0"/>
    </gradientOpacities>
    <shading>
        <ambient  value="0.5"/>
        <diffuse  value="0.8"/>
        <specular value="0.3"/>
    </shading>
</vrColorMap>


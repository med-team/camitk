/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef MULTIPLEPICKINGWIDGET_H
#define MULTIPLEPICKINGWIDGET_H

// Includes Qt
#include <QWidget>

// Includes CamiTK
#include <ImageComponent.h>

// local includes
#include "MultiPickingAPI.h"
#include "ui_MultiPickingWidget.h"
#include "PickedPixelMap.h"

/**
 * @ingroup group_sdk_actions_image_multipicking
 *
 * @brief
 * The multipicking action widget (a table with the pixel coordinates/3D indexes + some buttons to interact with it)
 *
 */
class MULTIPICKING_API MultiPickingWidget : public QWidget {
    Q_OBJECT

public:
    /// basic constructor
    MultiPickingWidget(QWidget* parent = nullptr);

    /// destructor
    ~MultiPickingWidget() override = default;

    /// Update the widget with the correct PickedPixelMap (ImageComponent + Qlist of the selected points)
    void updateComponent(camitk::ImageComponent* image);

    /// Give the picked pixel map (for other actions needing picking)
    PickedPixelMap* getPickedPixelMap(camitk::ImageComponent* image);


public slots:
    /// Slot called when the state of the radioButton pixel index changed
    void setPointTypeTable();

    /// Slot called when the button add is clicked
    void addPixelinTable();

    /// Slot called when remove button is clicked
    void removePixelFromTable();

    void removeSeedNumber(int, int);

    /// Slot called when save as button is clicked
    void savePixelList();

    /// Slot called when open CSV file button is clicked
    void openPixelList();

    /// Slot called when the user manually changes a value in the table
    void manuallyModified(int, int);

    /// Update the tableWidget when it's necessary
    void updateTable();


protected:
    Ui::MultiPickingWidget ui;

    /** allows one to construct the table
     * @param liste the * list to display in the QtableWidget
     * @param valueList the pointer to the values
     */
    void createItems(QList< QVector3D >* liste, QList<double>* valueList);

    /// the current Image Component
    camitk::ImageComponent* img;

    /// the current PickedPixelMap
    PickedPixelMap* pPM;

    QMap<camitk::ImageComponent*, PickedPixelMap*> map;

    /// allows one to resize the QtableWidget
    void resizeGraphicalTable();

};

#endif // MULTIPLEPICKINGWIDGET_H

/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef BOXVOIWIDGET_H
#define BOXVOIWIDGET_H

#include <QWidget>

#include <ImageComponent.h>
#include <MultiPickingWidget.h>
#include <Action.h>

#include "ui_BoxVOIWidget.h"

/**
 * @ingroup group_sdk_actions_image_cropvolume
 *
 * @brief
 * Crop volume action widget
 *
 */
class BoxVOIWidget : public QWidget {
    Q_OBJECT

public:
    /// Default construtor
    BoxVOIWidget(camitk::Action* action);

    /// destructor
    ~BoxVOIWidget() override = default;

    /// Update the widget with the correct PickedPixelMap (ImageComponent + Qlist of the selected points)
    void updateComponent(camitk::ImageComponent* image);

    /// List of seed points
    QList<QVector3D>* getSeedPoints(camitk::ImageComponent* image);

protected:
    Ui::BoxVOIWidget ui;

    /// Multiple picking widget
    MultiPickingWidget* pickingW;

    /// Connected Action
    camitk::Action* myAction;

};

#endif // BOXVOIWIDGET_H

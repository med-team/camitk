<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>BoxVOI</name>
    <message>
        <location filename="../../BoxVOI.cpp" line="51"/>
        <source>&lt;p&gt;This filter helps you to crop a volume to keep only an interesting subsample. To use it, select respectively the min and the max of each axe (i,j,k), i.e 6 points. It is possible to use only 2 points (the origin of parallelepiped and its opposite diagonal point).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../BoxVOI.cpp" line="58"/>
        <source>Crop Volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../BoxVOI.cpp" line="59"/>
        <source>Box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../BoxVOI.cpp" line="60"/>
        <source>Volume Of Interest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../BoxVOI.cpp" line="61"/>
        <source>Seed Point</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BoxVOIWidget</name>
    <message>
        <location filename="../../BoxVOIWidget.ui" line="14"/>
        <source>Crop Region of Interest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../BoxVOIWidget.ui" line="20"/>
        <source>Image Component</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../BoxVOIWidget.ui" line="26"/>
        <source>Original Image Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../BoxVOIWidget.ui" line="33"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../BoxVOIWidget.ui" line="49"/>
        <source>Box Points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../BoxVOIWidget.ui" line="63"/>
        <source>ROI properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../BoxVOIWidget.ui" line="88"/>
        <source>This filter helps you to crop a volume with a parallelepiped.

Two use cases are possible:
- Select respectively the min and the max of each axe (i,j,k), i.e 6 points.
- Select 2 points to construct the parallelepiped. In this case, given points must be the origin and opposite diagonal point of the parallelepiped. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../BoxVOIWidget.ui" line="113"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../../../../Dev/CamiTK/camitk/sdk/actions/image/cropvolume/BoxVOIWidget.ui" line="104"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>

#if defined(_WIN32) // MSVC and mingw
#ifdef COMPILE_BOXVOI_API
#define BOXVOI_API __declspec(dllexport)
#else
#define BOXVOI_API __declspec(dllimport)
#endif // COMPILE_BOXVOI_API
#else // for all other platforms BOXVOI_API is defined to be "nothing"
#define BOXVOI_API
#endif // MSVC and mingw
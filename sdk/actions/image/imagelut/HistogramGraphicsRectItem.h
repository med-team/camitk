/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef HistogramGraphicsRectItem_H
#define HistogramGraphicsRectItem_H

#include <QGraphicsRectItem>
#include <QGraphicsSceneHoverEvent>

#include "ImageLutWidget.h"

/**
 * @ingroup group_sdk_actions_image_lut
 *
 * @brief
 * The class HistogramGraphicsRectItem defines the histogram graphics item
 * and is required to manage the tooltip when the mouse moves over the histogram
 *
 */
class HistogramGraphicsRectItem : public QGraphicsRectItem {

public:
    /// Default construtor
    HistogramGraphicsRectItem(ImageLutWidget* lw, QRectF rect) : QGraphicsRectItem(rect), lutWidget(lw) {
    }

    /// destructor
    ~HistogramGraphicsRectItem() override = default;

protected:
    /// just send a the position of the mouse to the ImageLutWidget
    void hoverMoveEvent(QGraphicsSceneHoverEvent* event) override {
        lutWidget->showHistogramTooltip(event->pos());
    }

private:
    ImageLutWidget* lutWidget;
};

#endif

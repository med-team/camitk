/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef SHOWIN3DEXTENSION_H
#define SHOWIN3DEXTENSION_H

#include <ActionExtension.h>

/**
 * @ingroup group_sdk_actions_image_showin3d
 *
 * @brief
 * Show the selected @ref camitk::ImageComponent slices in the 3D viewer(s) action extension.
 *
 */
class ShowIn3DExtension : public camitk::ActionExtension {
    Q_OBJECT
    Q_INTERFACES(camitk::ActionExtension)
    Q_PLUGIN_METADATA(IID "fr.imag.camitk.sdk.action.showin3D")

public:
    /// Constructor
    ShowIn3DExtension() : camitk::ActionExtension() {}

    /// Destructor
    virtual ~ShowIn3DExtension() {}

    /// Method returning the action extension name
    virtual QString getName() override {
        return "ShowIn3DExtension";
    };

    /// Method returning the action extension description
    virtual QString getDescription() override {
        return "Toggle display of the image volume slices in the 3D viewer";
    };

    /// initialize all the actions
    virtual void init() override;

};

#endif // SHOWIN3DEXTENSION_H



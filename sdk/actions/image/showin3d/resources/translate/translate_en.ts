<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>ShowArbitrarySliceIn3D</name>
    <message>
        <source>Toggle display of the arbitrary slice(s) in 3D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>3D Viewer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShowAxialSliceIn3D</name>
    <message>
        <source>Toggle display of the axial slice in 3D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>3D Viewer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShowCoronalSliceIn3D</name>
    <message>
        <source>Toggle display of the coronal slice(s) in 3D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>3D Viewer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShowImageIn3D</name>
    <message>
        <source>Shows the Image Volume in 3D viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>3D Viewer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShowSagittalSliceIn3D</name>
    <message>
        <source>Toggle display of the sagittal slice(s) in 3D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>3D Viewer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>

/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef ELASTIXRUNNER_H
#define ELASTIXRUNNER_H

#include <QObject>
#include <QProcess>

#include "ElastixRegistrationAPI.h"

/**
 *
 * @ingroup group_sdk_actions_elastix_registration
 *
 * @brief
 * Worker that calls the elastix executable in a subprocess
 **/
class ELASTIX_REGISTRATION_API ElastixRunner : public QObject {
    Q_OBJECT

public:
    /// Default Constructor
    ElastixRunner(QString elastixPath, QStringList arguments, QString workingDirectory = "", QObject* parent = nullptr);

    // retrieve the exit status information
    int getExitCode() const;

    // retrieve the standard output
    QString getStdOutput() const;

    // retrieve the last process error as a message
    QString getProcessError() const;

public slots:
    void startProcess();

signals:
    // Signal to notify when output is available
    void outputReady(const QString& output);

    // Signal to notify when the process has finished
    void finished(int exitCode, const QString& output);

private slots:
    // Slot to process the standard output of the process
    void processOutput();

private:
    QString program;
    QStringList arguments;
    QString workingDirectory;
    int timeOut;
    QString output;
    int exitCode;
    QProcess::ProcessError processError;
};

#endif // ELASTIXRUNNER_H
/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef SIMPLEELASTIXREGISTRATIONACTION_H
#define SIMPLEELASTIXREGISTRATIONACTION_H

#include <Action.h>
#include <ImageComponent.h>

#include "ElastixRegistrationAPI.h"

/**
 *
 * @ingroup group_sdk_actions_elastix_registration
 *
 * @brief
 * Perform a registration
 **/
class ELASTIX_REGISTRATION_API SimpleElastixRegistrationAction : public camitk::Action {
    Q_OBJECT

public:
    /// Default Constructor
    SimpleElastixRegistrationAction(camitk::ActionExtension*);

    /// Default Destructor
    virtual ~SimpleElastixRegistrationAction();

    /// method called when the action when the action is triggered (i.e. started)
    virtual QWidget* getWidget() override;

    /// Possible Transformation
    enum TransformationType { TRANSLATION, AFFINE, B_SPLINE };
    Q_ENUM(TransformationType)

    enum MetricType { ADVANCED_MEAN_SQUARES, ADVANCED_NORMALIZED_CORRELATION, ADVANCED_MATTES_MUTUAL_INFORMATION, NORMALIZED_MUTUAL_INFORMATION };
    Q_ENUM(MetricType)

    /// manage property change immediately
    virtual bool event(QEvent* e) override;

public slots:
    /** this method is automatically called when the action is triggered.
      * Use getTargets() QList to get the list of component to use.
      * \note getTargets() is automatically filtered so that it only contains compatible components,
      * i.e., instances of ImageComponent (or a subclass).
      */
    virtual camitk::Action::ApplyStatus apply() override;

private:
    /// helper method to simplify the target component processing
//    virtual void process();

private:
    // last run results
    int lastExitCode;
    QString lastStdOutput;
    QString lastProcessError;

    // used to map item id with component for the "Fixed Image" and "Moving Image" combo box properties
    QList<camitk::ImageComponent*> imageComponentList;

    // update the elastix version property
    void updateElastixVersion();

    // ask the elastix executable path to the user, return true if the path is correct
    void defineElastixExecutable();

    // return the current test name using images names and parameter values
    QString defaultResultName() const;

    // name of the transformation computed by elastix
    // As the computed transformation deforms the moving image into the fixed image,
    // the transformation is a transform from moving to fixed, i.e., {}^{fixed} T_{moving}.
    // The name should therefore be "moving_to_fixed" where "moving" is the moving image
    // component name and "fixed" is the fixed image component name
    QString transformName() const;

    // save the given image component to the given path (preserving the filename)
    // returns the file name without the path if succeed, empty string otherwise
    QString saveComponentAs(camitk::ImageComponent* comp, QString path) const;

    // create the elastix parameter file from the current properties, return true if the parameter file was created without error
    bool createParameterFile(QFileInfo parameterFileInfo, QString initialization, QString transformType, QString similarityMetric) const;

    // run elastix with the given parameter in the given directory, return true if elastix exit with code 0
    bool runElastix(QStringList arguments, QString workingDirectory = "");

    // Copy the resulting image and logs to the moving image file directory
    // if everything went well returns the new mhd file to open
    QString copyResults(QString tempDir) const;

    // copy the given oldName file (name without dir) from source directory
    // to newName file in the destination directory
    bool copyFile(QString oldName, QDir source, QString newName, QDir destination) const;

    // replace searchString by replacementString in fileName (given as full absolute path)
    bool replaceInFile(const QString& fileName, const QString& searchString, const QString& replacementString) const;
};
#endif // SIMPLEELASTIXREGISTRATIONACTION_H

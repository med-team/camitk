<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>PixelColorChanger</name>
    <message>
        <location filename="../../PixelColorChanger.cpp" line="50"/>
        <source>&lt;br&gt;Sets clicked XxYxZ cube of voxels (by CTRL + Mouse Left) to a new defined color value.  &lt;ul&gt;                    &lt;li&gt;A size of 1 (X, Y or Z) corresponds to one pixel in the considered slice.&lt;/li&gt;                   &lt;li&gt;A size of 2 corresponds to 3 pixels in the slice, in the aim to always keep the picked pixel as the center.  &lt;br /&gt;(Size 3-&gt; 5pix; 4-&gt;7pix...)&lt;/li&gt;&lt;/ul&gt;&lt;br /&gt; Compatible with this pixel types formats :[unsigned] short / float / double / char</source>
        <translation>&lt;br&gt;Initialise les cubes XxYxZ  de voxels (par CTRL + Souris bouton Gauche) avec une nouvelle couleur définie.  &lt;ul&gt;                    &lt;li&gt;Une taille de 1 (X, Y ou Z) corresponds à un pixel dans la coupe traitée.&lt;/li&gt;                   &lt;li&gt;Une taille de 2 corresponds à 3 pixels dans la coupe, dans le but de toujours garder le pixel sélectionner comme centre.  &lt;br /&gt;(Taille 3-&gt; 5pix; 4-&gt;7pix...)&lt;/li&gt;&lt;/ul&gt;&lt;br /&gt; Compatible avec cesformats de type de pyxel :[unsigned] short / float / double / char</translation>
    </message>
    <message>
        <location filename="../../PixelColorChanger.cpp" line="59"/>
        <source>Color Changer</source>
        <translation>Chargeur de Couleur</translation>
    </message>
    <message>
        <location filename="../../PixelColorChanger.cpp" line="60"/>
        <source>Picked Pixel</source>
        <translation>Pixel  Sélectionné</translation>
    </message>
    <message>
        <location filename="../../PixelColorChanger.cpp" line="63"/>
        <source>New color</source>
        <translation>Nouvelle couleur</translation>
    </message>
    <message>
        <location filename="../../PixelColorChanger.cpp" line="63"/>
        <source>The new color of the voxel(s) picked.</source>
        <translation>La nouvelle couleur du voxel(s) selectionné(s).</translation>
    </message>
    <message>
        <location filename="../../PixelColorChanger.cpp" line="69"/>
        <source>Cube picking size in X</source>
        <translation>Taille du Cube de sélection en X</translation>
    </message>
    <message>
        <location filename="../../PixelColorChanger.cpp" line="69"/>
        <source>The cube picking width = 2xn + 1 where n is this property.</source>
        <translation>Largeur du Cube de sélection = 2xn + 1 où n est la propriété.</translation>
    </message>
    <message>
        <location filename="../../PixelColorChanger.cpp" line="74"/>
        <source>Cube picking size in Y</source>
        <translation>Taille du Cube de sélection en Y</translation>
    </message>
    <message>
        <location filename="../../PixelColorChanger.cpp" line="74"/>
        <source>The cube picking height = 2xn + 1 where n is this property.</source>
        <translation>Hauteur du Cube de sélection = 2xn + 1 où n est la propriété.</translation>
    </message>
    <message>
        <location filename="../../PixelColorChanger.cpp" line="79"/>
        <source>Cube picking size in Z</source>
        <translation>Taille du Cube de sélection en Z</translation>
    </message>
    <message>
        <location filename="../../PixelColorChanger.cpp" line="79"/>
        <source>The cube picking depth = 2xn + 1 where n is this property.</source>
        <translation>Profondeur du Cube de sélection = 2xn + 1 où n est la propriété.</translation>
    </message>
</context>
</TS>

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>PixelColorChanger</name>
    <message>
        <location filename="../../PixelColorChanger.cpp" line="50"/>
        <source>&lt;br&gt;Sets clicked XxYxZ cube of voxels (by CTRL + Mouse Left) to a new defined color value.  &lt;ul&gt;                    &lt;li&gt;A size of 1 (X, Y or Z) corresponds to one pixel in the considered slice.&lt;/li&gt;                   &lt;li&gt;A size of 2 corresponds to 3 pixels in the slice, in the aim to always keep the picked pixel as the center.  &lt;br /&gt;(Size 3-&gt; 5pix; 4-&gt;7pix...)&lt;/li&gt;&lt;/ul&gt;&lt;br /&gt; Compatible with this pixel types formats :[unsigned] short / float / double / char</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../PixelColorChanger.cpp" line="59"/>
        <source>Color Changer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../PixelColorChanger.cpp" line="60"/>
        <source>Picked Pixel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../PixelColorChanger.cpp" line="63"/>
        <source>New color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../PixelColorChanger.cpp" line="63"/>
        <source>The new color of the voxel(s) picked.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../PixelColorChanger.cpp" line="69"/>
        <source>Cube picking size in X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../PixelColorChanger.cpp" line="69"/>
        <source>The cube picking width = 2xn + 1 where n is this property.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../PixelColorChanger.cpp" line="74"/>
        <source>Cube picking size in Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../PixelColorChanger.cpp" line="74"/>
        <source>The cube picking height = 2xn + 1 where n is this property.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../PixelColorChanger.cpp" line="79"/>
        <source>Cube picking size in Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../PixelColorChanger.cpp" line="79"/>
        <source>The cube picking depth = 2xn + 1 where n is this property.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>

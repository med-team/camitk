/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef PIXELCOLORCHANGER_H
#define PIXELCOLORCHANGER_H

#include <QObject>
#include <qframe.h>
#include <Action.h>
#include <ImageComponent.h>

class PixelColorChanger : public camitk::Action {
    Q_OBJECT

public:
    /// Default Constructor
    PixelColorChanger(camitk::ActionExtension*);

    /// Default Destructor
    virtual ~PixelColorChanger();

    /// method called when the action when the action is triggered (i.e. started)
    virtual QWidget* getWidget();

public slots:
    /** this method is automatically called when the action is triggered.
      * Use getTargets() QList to get the list of component to use.
      * \note getTargets() is automatically filtered so that it only contains compatible components,
      * i.e., instances of ImageComponent (or a subclass).
      */
    virtual camitk::Action::ApplyStatus apply();
private:

    // once called, this action will be applied every time a picking or interaction is done in the axial/sagittal or coronal viewers
    void connectViewerInteraction();

    // disconnect signal (once called this action will NOT be applied every time a picking or interaction is done in the slice viewers)
    void disconnectViewerInteraction();

protected:
    /// new color given to picked pixel
    int newColorValue;
    /// size to draw pixels on i axis
    int sizei;
    /// size to draw pixels on j axis
    int sizej;
    /// size to draw pixels on k axis
    int sizek;
    /// is the widget automatically connected to image picking
    bool isConnected;
};
#endif // PIXELCOLORCHANGER_H

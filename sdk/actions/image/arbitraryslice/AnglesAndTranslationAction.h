/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef ANGLESANDTRANSLATIONACTION_H
#define ANGLESANDTRANSLATIONACTION_H

// CamiTK stuff
#include <QObject>
#include <Action.h>
#include <ArbitrarySingleImageComponent.h>
#include <MedicalImageViewer.h>

#include "ArbitrarySliceAPI.h"

/**
 * @ingroup group_sdk_actions_image_arbitraryslice
 *
 * @brief
 * This action simply display the widget allowing the user to select an angle to orientate the arbitrary slice.
 */
class ARBITRARY_SLICE_API AnglesAndTranslationAction : public camitk::Action {

    Q_OBJECT

public:

    /// Default Constructor
    AnglesAndTranslationAction(camitk::ActionExtension*);

    /// Default Destructor
    virtual ~AnglesAndTranslationAction();

    /// Return the arbitrary slice angles setter widget
    virtual QWidget* getWidget();

    /// manage change in the action parameters (angles and slice number)
    virtual bool event(QEvent* e);

    /// reset transform to identity
    void resetTransform();

public slots:
    /** This method returns always SUCCESS as the action aims at displaying its widget to be used in order to control the
     *  arbitrary slice display.
     */
    virtual camitk::Action::ApplyStatus apply() {
        return SUCCESS;
    }

    // Update the translation and the corresponding slider whenever the arbitrary slice viewer slider has changed
    void updateTranslation();

private:
    /// update action's parameter using the current image state + update widget GUI
    /// + update the central viewer depending on the current value of the "Visible Viewer" property
    void update();

    /// currently controled image
    camitk::ArbitrarySingleImageComponent* currentImageComp;

    /// block property changed event (temporarily)
    bool blockEvent;
};

#endif // ANGLESANDTRANSLATIONACTION_H


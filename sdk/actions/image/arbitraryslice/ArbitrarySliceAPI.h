#if defined(_WIN32) // MSVC and mingw
#ifdef COMPILE_ARBITRARY_SLICE_API
#define ARBITRARY_SLICE_API __declspec(dllexport)
#else
#define ARBITRARY_SLICE_API __declspec(dllimport)
#endif // COMPILE_ARBITRARY_SLICE_API
#else // for all other platforms ARBITRARY_SLICE_API is defined to be "nothing"
#define ARBITRARY_SLICE_API
#endif // MSVC and mingw
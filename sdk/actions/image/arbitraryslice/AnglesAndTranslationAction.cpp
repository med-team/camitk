/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "AnglesAndTranslationAction.h"

// includes from CamiTK
#include <Application.h>
#include <Property.h>
#include <MedicalImageViewer.h>
#include <ImageComponent.h>
#include <Log.h>

// to get the enum as a string
#include <QMetaEnum>

#include "AnglesAndTranslationWidget.h"

using namespace camitk;

// --------------- Constructor -------------------
AnglesAndTranslationAction::AnglesAndTranslationAction(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Arbitrary Slice");
    setDescription(tr("This action allows user to manually modify the orientation and translation of the arbitrary slice."));
    setComponentClassName("ImageComponent");

    // Setting classification family and tags
    setFamily("View");
    addTag(tr("arbitrary slice"));
    addTag(tr("arbitrary"));
    addTag(tr("modify angles"));
    addTag(tr("angle"));
    setEmbedded(true);

    currentImageComp = nullptr;
    blockEvent = true;

    // the AnglesAndTranslationWidget is going to modify these parameters using the GUI
    // but defining them as the action parameters also allows to set them programmatically
    // (for instance using another GUI or in a pipeline)
    // Setting parameters default values by using properties
    addParameter(new Property(tr("Translation"), 0.0, tr("Current translation inside the image"), ""));

    Property* actionProperty = new Property(tr("X Angle"), 0, tr("X Angle"), "degree");
    actionProperty->setAttribute("minimum", 0);
    actionProperty->setAttribute("maximum", 360);
    addParameter(actionProperty);

    actionProperty = new Property(tr("Y Angle"), 0, tr("Y Angle"), "degree");
    actionProperty->setAttribute("minimum", 0);
    actionProperty->setAttribute("maximum", 360);
    addParameter(actionProperty);

    actionProperty = new Property(tr("Z Angle"), 0, tr("Z Angle"), "degree");
    actionProperty->setAttribute("minimum", 0);
    actionProperty->setAttribute("maximum", 360);
    addParameter(actionProperty);

    actionProperty = new Property("Visible Viewer", MedicalImageViewer::VIEWER_ARBITRARY, "Visible viewer in the main window", "");
    // To set the enum type, use the same name of the enum as declared in the class
    actionProperty->setEnumTypeName("LayoutVisibility");
    // Add the property as an action parameter
    addParameter(actionProperty);

    // be notified automatically when the parameters change
    setAutoUpdateProperties(true);
}

// --------------- destructor -------------------
AnglesAndTranslationAction::~AnglesAndTranslationAction() {
    delete actionWidget;
}

// --------------- getWidget -------------------
QWidget* AnglesAndTranslationAction::getWidget() {
    if (!actionWidget) {
        actionWidget = new AnglesAndTranslationWidget(this);
        // update the translation and corresponding UI wheever the arbitrary viewer slider changes
        QObject::connect(Application::getViewer("Arbitrary Viewer"), SIGNAL(selectionChanged()), this, SLOT(updateTranslation()));
    }

    // update the pointer
    currentImageComp = dynamic_cast<ImageComponent*>(getTargets().last())->getArbitrarySlices();

    if (currentImageComp == nullptr) {
        CAMITK_WARNING(tr("Arbitrary Slice manipulation is not possible on 2D images. Aborting."))
        return nullptr;
    }
    else {
        // make sure the arbitrary slice is visible in 3D
        currentImageComp->setVisibility("3D Viewer", true);

        // update the properties
        update();

        return actionWidget;
    }
}

// ---------------------- updateParameters ----------------------------
void AnglesAndTranslationAction::update() {
    // update current action properties safely (i.e. without event(..) to be doing anything on the component)
    blockEvent = true;

    // current translation along the z axis of the arbitrary slice
    setProperty("Translation", currentImageComp->getTranslationInVolume() * 100.0);

    // update widget
    dynamic_cast<AnglesAndTranslationWidget*>(actionWidget)->updateGUI();

    // update central viewer
    MedicalImageViewer::LayoutVisibility visibleViewer = static_cast<MedicalImageViewer::LayoutVisibility>(property("Visible Viewer").toInt());

    MedicalImageViewer* medicalImageViewer = dynamic_cast<MedicalImageViewer*>(Application::getViewer("Medical Image Viewer"));
    if (medicalImageViewer != nullptr) {
        medicalImageViewer->setVisibleViewer(visibleViewer);
    }

    // keep property up-to-date when the GUI will change
    blockEvent = false;
}

// ---------------------- resetTransform ----------------------------
void AnglesAndTranslationAction::resetTransform() {
    currentImageComp->resetTransform();

    // Reset the angle to zero
    blockEvent = true;
    setProperty("X Angle", 0);
    setProperty("Y Angle", 0);
    setProperty("Z Angle", 0);
    blockEvent = false;

    update();
    currentImageComp->refresh();
}

// ---------------------- event ----------------------------
bool AnglesAndTranslationAction::event(QEvent* e) {
    if (currentImageComp == nullptr || blockEvent) {
        return QObject::event(e);
    }

    if (e->type() == QEvent::DynamicPropertyChange) {
        e->accept();
        QDynamicPropertyChangeEvent* changeEvent = dynamic_cast<QDynamicPropertyChangeEvent*>(e);

        if (!changeEvent) {
            return false;
        }

        // do something depending of the property that has changed
        if (changeEvent->propertyName() != "Visible Viewer") {
            // only translation and rotation angle are of interest here (the "Visible Viewer" changes will automatically be
            // taken into account in the update() method
            if (changeEvent->propertyName() == "Translation") {
                currentImageComp->setTransformTranslation(0.0, 0.0, property(changeEvent->propertyName()).toDouble() / 100.0);
            }
            else {
                currentImageComp->setTransformRotation(property("X Angle").toInt(), property("Y Angle").toInt(), property("Z Angle").toInt());
            }
        }

        // needed as rotation might change the translation value or translation might have been rejected
        update();

        currentImageComp->refresh();

        return true;
    }

    // this is important to continue the process if the event is a different one
    return QObject::event(e);
}

// ---------------------- updateTranslationSlider ----------------------------
void AnglesAndTranslationAction::updateTranslation() {
    // update the translation properties safely (i.e. without event(..) to be doing anything on the component)
    blockEvent = true;

    // current translation along the z axis of the arbitrary slice
    setProperty("Translation", currentImageComp->getTranslationInVolume() * 100.0);

    // update widget
    dynamic_cast<AnglesAndTranslationWidget*>(actionWidget)->updateGUI();

    blockEvent = false;
}

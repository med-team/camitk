/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "AnglesAndTranslationWidget.h"

// CamiTK stuff
#include <Property.h>
#include <AnglesAndTranslationAction.h>

using namespace camitk;

// -------------------- constructor --------------------
AnglesAndTranslationWidget::AnglesAndTranslationWidget(AnglesAndTranslationAction* a, QWidget* parent): QWidget(parent) {
    myAction = a;

    ui.setupUi(this);

    // Ui slice number change slot connection
    connect(ui.translationSpinBox, SIGNAL(valueChanged(double)), this, SLOT(translationSpinBoxChanged(double)));
    connect(ui.translationSlider, SIGNAL(valueChanged(int)), this, SLOT(translationSliderChanged(int)));

    // Ui angles slot connection
    connect(ui.xAngleDial, SIGNAL(valueChanged(int)), this, SLOT(xAngleDialValueChanged(int)));
    connect(ui.yAngleDial, SIGNAL(valueChanged(int)), this, SLOT(yAngleDialValueChanged(int)));
    connect(ui.zAngleDial, SIGNAL(valueChanged(int)), this, SLOT(zAngleDialValueChanged(int)));

    connect(ui.showArbitraryRadio, SIGNAL(clicked(bool)), this, SLOT(showArbitraryViewer(bool)));
    connect(ui.show3DRadio, SIGNAL(clicked(bool)), this, SLOT(show3DViewer(bool)));
    connect(ui.showAllRadio, SIGNAL(clicked(bool)), this, SLOT(showAllViewer(bool)));
    connect(ui.resetButton, SIGNAL(clicked()), this, SLOT(resetTransform()));
}

// -------------------- destructor --------------------
AnglesAndTranslationWidget::~AnglesAndTranslationWidget() {
}

// -------------------- updateGUI --------------------
void AnglesAndTranslationWidget::updateGUI() {
    ui.translationSpinBox->blockSignals(true);
    ui.translationSlider->blockSignals(true);
    ui.translationSpinBox->setValue(myAction->property("Translation").toDouble());
    ui.translationSlider->setValue(myAction->property("Translation").toDouble() * 10);
    ui.translationSpinBox->blockSignals(false);
    ui.translationSlider->blockSignals(false);

    ui.xAngleDial->blockSignals(true);
    ui.xAngleDial->setValue(myAction->property("X Angle").toInt());
    ui.xAngleDial->blockSignals(false);
    updateAngleSliderLabel(ui.xAngleDial);

    ui.yAngleDial->blockSignals(true);
    ui.yAngleDial->setValue(myAction->property("Y Angle").toInt());
    ui.yAngleDial->blockSignals(false);
    updateAngleSliderLabel(ui.yAngleDial);

    ui.zAngleDial->blockSignals(true);
    ui.zAngleDial->setValue(myAction->property("Z Angle").toInt());
    ui.zAngleDial->blockSignals(false);
    updateAngleSliderLabel(ui.zAngleDial);
}

// -------------------- showArbitraryViewer --------------------
void AnglesAndTranslationWidget::showArbitraryViewer(bool buttonState) {
    if (buttonState) {
        myAction->setProperty("Visible Viewer", MedicalImageViewer::VIEWER_ARBITRARY);
    }
}

// -------------------- show3DViewer --------------------
void AnglesAndTranslationWidget::show3DViewer(bool buttonState) {
    if (buttonState) {
        myAction->setProperty("Visible Viewer", MedicalImageViewer::VIEWER_3D);
    }
}

// -------------------- showAllViewer --------------------
void AnglesAndTranslationWidget::showAllViewer(bool buttonState) {
    if (buttonState) {
        myAction->setProperty("Visible Viewer", MedicalImageViewer::VIEWER_ALL);
    }
}

// -------------------- updateAngleSliderLabel --------------------
void AnglesAndTranslationWidget::resetTransform() {
    myAction->resetTransform();
}

// -------------------- translationSpinBoxChanged --------------------
void AnglesAndTranslationWidget::translationSpinBoxChanged(double value) {
    myAction->setProperty("Translation", value);
}

// -------------------- translationSliderChanged --------------------
void AnglesAndTranslationWidget::translationSliderChanged(int value) {
    // the slide is from 0 to 1000
    myAction->setProperty("Translation", value / 10);
}

// -------------------- xAngleDialValueChanged --------------------
void AnglesAndTranslationWidget::xAngleDialValueChanged(int value) {
    myAction->setProperty("X Angle", value);
    updateAngleSliderLabel(ui.xAngleDial);
}

// -------------------- yAngleDialValueChanged --------------------
void AnglesAndTranslationWidget::yAngleDialValueChanged(int value) {
    myAction->setProperty("Y Angle", value);
    updateAngleSliderLabel(ui.yAngleDial);
}

// -------------------- zAngleDialValueChanged --------------------
void AnglesAndTranslationWidget::zAngleDialValueChanged(int value) {
    myAction->setProperty("Z Angle", value);
    updateAngleSliderLabel(ui.zAngleDial);
}

// -------------------- updateAngleSliderLabel --------------------
void AnglesAndTranslationWidget::updateAngleSliderLabel(QDial* dial) {
    QString angleLetter;
    QLabel* label;

    if (dial == ui.xAngleDial) {
        angleLetter = "X";
        label = ui.xAngleValue;
    }
    else if (dial == ui.yAngleDial) {
        angleLetter = "Y";
        label = ui.yAngleValue;
    }
    else {
        // dial == ui.zAngleDial
        angleLetter = "Z";
        label = ui.zAngleValue;
    }

    label->setText(angleLetter + QString(" Angle: <tt>%1</tt>").arg(dial->value(), 3) + QChar(0x00B0));
    label->update();
}

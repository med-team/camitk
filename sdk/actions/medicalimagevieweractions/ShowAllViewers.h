/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef SHOWALLVIEWERS_H
#define SHOWALLVIEWERS_H

#include <Action.h>

/**
 * @ingroup group_sdk_actions_application
 *
 * @brief
 * Within the MedicalImageViewer, display all viewers (except arbitrary).
 *
 */
class ShowAllViewers : public camitk::Action {
    Q_OBJECT

public:
    /// Default Constructor
    ShowAllViewers(camitk::ActionExtension*);

    /// Default Destructor
    virtual ~ShowAllViewers();

    /// Returns NULL: no widget for this action.
    virtual QWidget* getWidget();

public slots:
    /** this method is automatically called when the action is triggered.
     * it calls setVisibility on the MedicalImageViewer instance
     */
    virtual camitk::Action::ApplyStatus apply();
};
#endif // SHOWALLVIEWERS_H

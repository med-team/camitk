/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef MEDICALIMAGEVIEWER_ACTION_EXTENSION_H
#define MEDICALIMAGEVIEWER_ACTION_EXTENSION_H

#include <ActionExtension.h>

/**
 * @ingroup group_sdk_actions_application
 *
 * @brief
 * The MedicalImageViewerActionExtension class features the actions to change the visibility of the viewers embedded in the Medical Image Viewer layout
 *
 **/
class MedicalImageViewerActionExtension : public camitk::ActionExtension {
    Q_OBJECT
    Q_INTERFACES(camitk::ActionExtension)
    Q_PLUGIN_METADATA(IID "fr.imag.camitk.sdk.action.medicalimagevieweractionextension")

public:
    /// the constructor (needed to initialize the icon resources)
    MedicalImageViewerActionExtension() = default;

    /// the destructor
    virtual ~MedicalImageViewerActionExtension() = default;

    /// initialize all the actions
    virtual void init() override;

    /// Method that return the action extension name
    virtual QString getName() override {
        return "Medical Image Viewer visibility";
    };

    /// Method that return the action extension description
    virtual QString getDescription() override {
        return "This extension provides six actions to change the visibility of the viewers embedded in the Medical Image Viewer layout";
    };

};

#endif // MEDICALIMAGEVIEWER_ACTION_EXTENSION_H

/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "ShowAllViewers.h"

#include <MedicalImageViewer.h>
using namespace camitk;

// --------------- constructor -------------------
ShowAllViewers::ShowAllViewers(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Show All Viewers");
    setEmbedded(false);
    setDescription(tr("Show Classical Medical Image Viewer (Axial, Coronal, Sagittal and 3D in a 4 panels)"));
    setComponentClassName("");

    // Setting classification family and tags
    setFamily("Application");
    addTag(tr("All Viewers"));
    addTag(tr("Medical Image Viewer"));

    // add a shortcut
    getQAction()->setShortcut(tr("Ctrl+0"));
    getQAction()->setShortcutContext(Qt::ApplicationShortcut);

    // can be checked or not (on/off), useful when group in a QActionGroup (radio buttons)
    getQAction()->setCheckable(true);
}

// --------------- destructor -------------------
ShowAllViewers::~ShowAllViewers() {
    // do not delete the widget has it might have been used in the ActionViewer (i.e. the ownership might have been taken by the stacked widget)
}

// --------------- getWidget --------------
QWidget* ShowAllViewers::getWidget() {
    return nullptr;
}

// --------------- apply -------------------
Action::ApplyStatus ShowAllViewers::apply() {
    MedicalImageViewer* medicalImageViewer = dynamic_cast<MedicalImageViewer*>(Application::getViewer("Medical Image Viewer"));
    if (medicalImageViewer != nullptr) {
        medicalImageViewer->setVisibleViewer(MedicalImageViewer::VIEWER_ALL);
    }
    Application::refresh();
    return SUCCESS;
}

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>StlExtension</name>
    <message>
        <source>Manage STL format, see &lt;a href=&quot;https://en.wikipedia.org/wiki/STL_%28file_format%29&quot;&gt;https://en.wikipedia.org/wiki/STL_%28file_format%29&lt;/a&gt;</source>
        <translation>Gérer le format STL, voir &lt;a href=&quot;https://en.wikipedia.org/wiki/STL_%28file_format%29&quot;&gt;https://en.wikipedia.org/wiki/STL_%28file_format%29&lt;/a&gt;</translation>
    </message>
</context>
</TS>

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>VtkMeshComponent</name>
    <message>
        <source>Ambient Lightning Coefficient. Warning: this cannot be saved in the VTK file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Diffuse Lightning Coefficient. Warning: this cannot be saved in the VTK file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Specular Lightning Coefficient. Warning: this cannot be saved in the VTK file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Specular Power. Warning: this cannot be saved in the VTK file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The object&apos;s opacity. 1.0 is totally opaque and 0.0 is completely transparent. Warning: this cannot be saved in the VTK file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Specular Surface Color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VtkMeshComponentExtension</name>
    <message>
        <source>Manage VTK &lt;em&gt;.vtk&lt;/em&gt; files in &lt;b&gt;CamiTK&lt;/b&gt;.&lt;br/&gt;CamiTK is based on vtk. Lots of things are possible with a vtk model!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>

/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef VTK_MESH_COMPONENT_H
#define VTK_MESH_COMPONENT_H

#include <MeshComponent.h>

#include "VtkMeshComponentAPI.h"
#include "VtkMeshUtil.h"

class VtkMeshComponentProperties;
class vtkDoubleArray;
class vtkPointData;

/**
 *  @ingroup group_sdk_components_vtkmesh
 *
 *  @brief
 *  Features various Vtk mesh for CamiTK.
 *
 **/
class VTK_COMPONENT_API VtkMeshComponent : public camitk::MeshComponent {
    Q_OBJECT

public:
    /** default constructor.
     *  This method may throw an AbortException if a problem occurs.
     */
    VtkMeshComponent(const QString& fileName);

    /// the virtual destructor
    virtual ~VtkMeshComponent();

    /// Export as MDL (see Geometry).
    bool exportMDL(std::string filename);

    /// the vtk logo (for the explorer viewer)
    virtual QPixmap getIcon();

    /// update a specific property
    virtual void updateProperty(QString name, QVariant value);

private:

    /// subtype of vtkPointSet
    VtkMeshUtil::VtkPointSetType whatIsIt;

    /// properties for vtk mesh type
    VtkMeshComponentProperties* myProperties;

    /// the vtk logo
    static QPixmap* myPixmap;

    // initialize the dynamic properties
    virtual void initDynamicProperties();

};




#endif

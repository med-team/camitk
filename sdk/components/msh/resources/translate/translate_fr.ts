<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>MshExtension</name>
    <message>
        <source>This simple MSH Component allows you to manipulate &lt;em&gt;.msh&lt;/em&gt; files (initially from gmsh software). See &lt;a href=&quot;http://www.geuz.org/gmsh/doc/texinfo/gmsh.html&quot;&gt;http://www.geuz.org/gmsh/doc/texinfo/gmsh.html&lt;/a&gt; for more information about this format</source>
        <translation>Ce component MSH simple vous permet de manipuler les fichiers &lt;em&gt;.msh&lt;/em&gt; (initialement à partir du logiciel gmsh). Voir &lt;a href=&quot;http://www.geuz.org/gmsh/doc/texinfo/gmsh.html&quot;&gt;http://www.geuz.org/gmsh/doc/texinfo/gmsh.html&lt;/a&gt; pour plus d&apos;informations a propos de ce format</translation>
    </message>
    <message>
        <source>The selected component does not have any points or cells. This is an invalid mesh.</source>
        <translation>Le component sélectionné n&apos;a aucun point ou cellule. Ce maillage est non valide.</translation>
    </message>
</context>
</TS>

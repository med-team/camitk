# nothing special for this component extension
camitk_extension(COMPONENT_EXTENSION
                 CEP_NAME SDK
                 DESCRIPTION "Support for .msh files"
                 DEFINES COMPILE_MSH_COMPONENT_API
                 HEADERS_TO_INSTALL MshComponent.h MshExtension.h MshComponentAPI.h
                 NEEDS_VIEWER_EXTENSION interactivegeometryviewer
                 ENABLE_AUTO_TEST
)

# Recursively update the shiboken path variable containing the CamiTK SDK tree structure
set(SHIBOKEN_CAMITK_SDK_PATH ${SHIBOKEN_CAMITK_SDK_PATH}:${CMAKE_CURRENT_SOURCE_DIR} CACHE INTERNAL "") 

/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// -- Core image component includes
#include "RawDataDialog.h"
#include "RawImageComponent.h"
#include <ImageOrientationHelper.h>

// -- QT includes
#include <QString>
#include <QMetaEnum>

// -- VTK includes
#include <vtkConfigure.h>


//--------------- Constructor ---------------------------------
RawDataDialog::RawDataDialog(QString filename)
    : QDialog() {
    this->filename = filename;
    ui.setupUi(this);
    init();
}

//--------------- Destructor ---------------------------------
RawDataDialog::~RawDataDialog() {
//     if(this->imgOrientation)
//         delete this->imgOrientation;
}

void RawDataDialog::init() {

    ui.filenameLineEdit->setText(filename);

    ui.voxelTypeCBox->addItem(QString("unsigned char"));
    ui.voxelTypeCBox->addItem(QString("char"));
    ui.voxelTypeCBox->addItem(QString("unsigned short"));
    ui.voxelTypeCBox->addItem(QString("short"));
    ui.voxelTypeCBox->addItem(QString("unsigned int"));
    ui.voxelTypeCBox->addItem(QString("int"));
    ui.voxelTypeCBox->addItem(QString("unsigned long"));
    ui.voxelTypeCBox->addItem(QString("long"));
    ui.voxelTypeCBox->addItem(QString("float"));
    ui.voxelTypeCBox->addItem(QString("double"));

    ui.nbScalarsLineEdit->setText(QString("1"));

    ui.voxelSizeXLineEdit->setText(QString("1.0"));
    ui.voxelSizeYLineEdit->setText(QString("1.0"));
    ui.voxelSizeZLineEdit->setText(QString("1.0"));

    ui.oxLineEdit->setText(QString("0.0"));
    ui.oyLineEdit->setText(QString("0.0"));
    ui.ozLineEdit->setText(QString("0.0"));

    ui.headerSizeLineEdit->setText(QString("0"));

    // fill the Image orientation combobox with the possible orientation
    ui.orientationComboBox->addItems(camitk::ImageOrientationHelper::getPossibleImageOrientations());
}


void RawDataDialog::voxelTypeChanged(int) {
    QString voxelType = ui.voxelTypeCBox->currentText();
    if ((voxelType.compare(QString("unsigned char")) == 0) ||
            (voxelType.compare(QString("char"))          == 0) ||
            (voxelType.compare(QString("float"))         == 0) ||
            (voxelType.compare(QString("double"))        == 0)) {
        ui.indianBox->setDisabled(true);
    }
    else {
        ui.indianBox->setEnabled(true);
    }

}

int RawDataDialog::getVoxelType() {
    int scalarType;
    switch (ui.voxelTypeCBox->currentIndex()) {
        default:
        case 0: // "unsigned char"
            scalarType = VTK_UNSIGNED_CHAR;
            break;
        case 1: // "char"
            scalarType = VTK_SIGNED_CHAR;
            break;
        case 2: // "unsigned short"
            scalarType = VTK_UNSIGNED_SHORT;
            break;
        case 3: // "short"
            scalarType = VTK_SHORT;
            break;
        case 4: // "unsigned int"
            scalarType = VTK_UNSIGNED_INT;
            break;
        case 5: // "int"
            scalarType = VTK_INT;
            break;
        case 6: // "unsigned long"
            scalarType = VTK_UNSIGNED_LONG;
            break;
        case 7: // float
            scalarType = VTK_FLOAT;
            break;
        case 8: // double
            scalarType = VTK_DOUBLE;
            break;

    }

    return scalarType;
}
int RawDataDialog::getDimX() {
    bool ok;
    int dimX = ui.dimXLineEdit->text().toInt(&ok);

    if (ok) {
        return dimX;
    }
    else {
        return -1;
    }
}

int RawDataDialog::getDimY() {
    bool ok;
    int dimY = ui.dimYLineEdit->text().toInt(&ok);

    if (ok) {
        return dimY;
    }
    else {
        return -1;
    }
}

int RawDataDialog::getDimZ() {
    bool ok;
    int dimZ = ui.dimZLineEdit->text().toInt(&ok);

    if (ok) {
        return dimZ;
    }
    else {
        return -1;
    }
}


bool RawDataDialog::isBigEndian() {
    return ui.indianBox->isChecked();
}


int RawDataDialog::getNbScalarComponents() {
    bool ok;
    int nbScalarComponents = ui.nbScalarsLineEdit->text().toInt(&ok);

    if (ok) {
        return nbScalarComponents;
    }
    else {
        return -1;
    }

}

double RawDataDialog::getVoxelSizeX() {
    bool ok;
    double voxelSizeX = ui.voxelSizeXLineEdit->text().toDouble(&ok);

    if (ok) {
        return voxelSizeX;
    }
    else {
        return -1.0;
    }
}

double RawDataDialog::getVoxelSizeY() {
    bool ok;
    double voxelSizeY = ui.voxelSizeYLineEdit->text().toDouble(&ok);

    if (ok) {
        return voxelSizeY;
    }
    else {
        return -1.0;
    }
}

double RawDataDialog::getVoxelSizeZ() {
    bool ok;
    double voxelSizeZ = ui.voxelSizeZLineEdit->text().toDouble(&ok);

    if (ok) {
        return voxelSizeZ;
    }
    else {
        return -1.0;
    }
}


double RawDataDialog::getOriginX() {
    bool ok;
    double Ox = ui.oxLineEdit->text().toDouble(&ok);

    if (ok) {
        return Ox;
    }
    else {
        return 0.0;
    }
}

double RawDataDialog::getOriginY() {
    bool ok;
    double Oy = ui.oyLineEdit->text().toDouble(&ok);

    if (ok) {
        return Oy;
    }
    else {
        return 0.0;
    }
}

double RawDataDialog::getOriginZ() {
    bool ok;
    double Oz = ui.ozLineEdit->text().toDouble(&ok);

    if (ok) {
        return Oz;
    }
    else {
        return 0.0;
    }
}



int RawDataDialog::getHeaderSize() {
    bool ok;
    int headerSize = ui.headerSizeLineEdit->text().toInt(&ok);

    if (ok) {
        return headerSize;
    }
    else {
        return 0;
    }
}

QString RawDataDialog::getFilename() {
    return filename;
}

bool RawDataDialog::isLowerLeftOrigin() {
    return ui.lloCheckBox->isChecked();
}

camitk::ImageOrientationHelper::PossibleImageOrientations RawDataDialog::getOrientation() {
    return (camitk::ImageOrientationHelper::PossibleImageOrientations) ui.orientationComboBox->currentIndex();
}





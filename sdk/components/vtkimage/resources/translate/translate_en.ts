<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>RawDataDialog</name>
    <message>
        <location filename="../../RawDataDialog.ui" line="17"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="64"/>
        <source>Raw data information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="88"/>
        <source>File name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="118"/>
        <source>Volume Dimensions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="138"/>
        <source>Dim X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="194"/>
        <source>Dim Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="244"/>
        <source>Dim Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="299"/>
        <source>Voxels Value Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="337"/>
        <source>Data Byte Order: Big Endian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="358"/>
        <source>Nb Scalar Components</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="415"/>
        <source>Voxels Spacing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="436"/>
        <source>Voxel size X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="487"/>
        <source>Voxel size Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="538"/>
        <source>Voxel size Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="595"/>
        <source>Image Origin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="616"/>
        <source>Ox</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="667"/>
        <source>Oy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="718"/>
        <source>Oz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="769"/>
        <source>Lower Left Origin ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="796"/>
        <source>Header </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="817"/>
        <source>Header size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../RawDataDialog.ui" line="871"/>
        <source>Image Orientation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VtkImageComponentExtension</name>
    <message>
        <location filename="../../VtkImageComponentExtension.cpp" line="58"/>
        <source>Manage any file type supported by vtk in &lt;b&gt;CamiTK&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../VtkImageComponentExtension.cpp" line="165"/>
        <source>Cannot save file: unrecognized extension &quot;.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>

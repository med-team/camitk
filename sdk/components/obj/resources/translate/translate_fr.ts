<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>ObjExtension</name>
    <message>
        <source>Manage Alias Wavefront OBJ &lt;em&gt;.obj&lt;/em&gt; files in &lt;b&gt;CamiTK&lt;/b&gt;.&lt;br/&gt;(very few support!)</source>
        <translation>Gérer les fichiers Alias Wavefront OBJ &lt;em&gt;.obj&lt;/em&gt; dans &lt;b&gt;CamiTK&lt;/b&gt;.&lt;br/&gt;(trés peu de support!)</translation>
    </message>
    <message>
        <source>The selected component does not have any points or cells. This is an invalid mesh.</source>
        <translation>Le component sélectionné n&apos;a pas de point ou de cellule. Ce maillage est non valide.</translation>
    </message>
</context>
</TS>

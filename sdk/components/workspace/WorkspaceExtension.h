/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef WORKSPACEEXTENSION_H
#define WORKSPACEEXTENSION_H

#include <ComponentExtension.h>

/**
 *  @ingroup group_sdk_components_workspace
 *
 *  @brief
 *  Manage workspace as a normal extension.
 *  Although camitk-imp has a specific menu, this extension simplifies the management
 *  of workspace .camitk files.
 *  .camitk documents can therefore be opened as any other component extension,
 *  although this extension does not instanciate any Component class.
 */
class WorkspaceExtension : public camitk::ComponentExtension {
    Q_OBJECT
    Q_INTERFACES(camitk::ComponentExtension)
    Q_PLUGIN_METADATA(IID "fr.imag.camitk.sdk.component.workspace")

public:
    /// Constructor
    WorkspaceExtension() : ComponentExtension() {}

    /// Method returning the component extension name
    virtual QString getName() const override;

    /// Method returning the component extension description
    virtual QString getDescription() const override;

    /** Get the list of managed extensions
     */
    virtual QStringList getFileExtensions() const override;

    /** .camitk may results in more than one instanciation.
     *  This method calls the OpenWorkspace action and returns nullptr.
     */
    camitk::Component* open(const QString&) override;

    /** .camitk should be saved by calling the save workspace. This method
     *  should never be available/called.
     *  @return false unconditionally
     */
    bool save(camitk::Component* component) const override;

protected:
    /// Destructor
    virtual ~WorkspaceExtension() = default;

};

#endif // WORKSPACEEXTENSION_H


camitk_extension(COMPONENT_EXTENSION
                 CEP_NAME SDK
                 DESCRIPTION "Open/Save Workspace"
                 # Default auto test behaviour cannot be used for this specific component:
                 # - As .camitk cannot saved by a component extension -> test level 2 will always fail
                 # - As this component extension does not create any Component instance, the open method
                 #   always returns nullptr, which is not an indicator of failure but will be considered
                 #   as so by the camitk-testcomponent -> test level 1 will always fail
                 #  ENABLE_AUTO_TEST
                 #  TEST_FILES simple-imp.camitk
                 #  AUTO_TEST_LEVEL 0
)

# TODO create a specific application + camitk_add_test in order to add test for Workspace I/O
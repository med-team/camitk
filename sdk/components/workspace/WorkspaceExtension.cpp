/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "WorkspaceExtension.h"

#include <Application.h>
#include <Action.h>
#include <Log.h>

using namespace camitk;

// --------------- getName -------------------
QString WorkspaceExtension::getName() const {
    return "CamiTK Workspace";
}

// --------------- getDescription -------------------
QString WorkspaceExtension::getDescription() const {
    return tr("Manage CamiTK Workspace format.");
}

// --------------- getFileExtensions -------------------
QStringList WorkspaceExtension::getFileExtensions() const {
    QStringList ext;
    ext << "camitk";

    return ext;
}

// --------------- open -------------------
Component* WorkspaceExtension::open(const QString& fileName) {
    // call OpenWorkspace action
    Action* openWorkspaceAction = Application::getAction("Open Workspace");

    if (openWorkspaceAction == nullptr) {
        CAMITK_ERROR("Cannot find the 'Open Workspace' action. Please check your configuration.")
        return nullptr;
    }
    else {
        // set the file name property
        openWorkspaceAction->setProperty("File Name", fileName);

        // apply the action
        Action::ApplyStatus status = openWorkspaceAction->applyInPipeline();

        // the Open Workspace action will show warning if there was any problem
        // with the file. Just return silently from here whatever happened.
        return nullptr;
    }
    return nullptr;
}

// --------------- save --------------------
bool WorkspaceExtension::save(Component*) const {
    return false;
}


camitk_application(NO_GUI
           NEEDS_XSD
           NEEDS_XERCESC
           NEEDS_CEP_LIBRARIES cepgenerator cepcoreschema
           CEP_NAME SDK
           ADDITIONAL_SOURCES CommandLineOptions.cxx CommandLineOptions.hxx 
           DESCRIPTION "Create CEP and file skeletons from XML files (no GUI)"
)
                      
#---------------------------------
# Testing command-line robustness
#---------------------------------
set(TEST_BASENAME ${APPLICATION_TARGET_NAME})
camitk_init_test(${TEST_BASENAME})
# should pass because invoking cepgenerator without arguments or with help arg shows usage and exit success
camitk_add_test(PROJECT_NAME ${TEST_BASENAME} TEST_SUFFIX "-")
camitk_add_test(EXECUTABLE_ARGS "--help" PROJECT_NAME ${TEST_BASENAME} TEST_SUFFIX "-")
camitk_add_test(EXECUTABLE_ARGS "-h"     PROJECT_NAME ${TEST_BASENAME} TEST_SUFFIX "-")

# test #4
# should pass because invoking cepgenerator with a faulty arguments results in printing
# an "Argument error" message (and exit failure)
camitk_add_test(EXECUTABLE_ARGS "-badarg" 
                PASS_REGULAR_EXPRESSION "unknown option '-badarg'"
                PROJECT_NAME ${TEST_BASENAME} TEST_SUFFIX "-")

if (WIN32) # needed as there is no "AND" operator in camitk_tests_requirement REQUIRES argument
    # see https://cmake.org/cmake/help/v3.11/variable/MSVC_VERSION.html for the list of version
    camitk_tests_requirement(TESTS application-cepgenerator-4
                             REQUIRES "${MSVC_VERSION} VERSION_EQUAL 1900"
                             REASON "xsdcxx compiled with a different version of MSVC
        XSDCXX was compiled with MSVC 14.0.
        The MSVC mismatch version of std C++ library generates a segfault during exit
        when freeing the xsdcxx cli_options_map.
        Therefore if the MSVC version is different than version 14.0, this test will unconditionnaly fail."
    )
endif()

camitk_add_test(EXECUTABLE_ARGS "-f missingdir" 
                PASS_REGULAR_EXPRESSION "Argument error: please provide an output directory."
                PROJECT_NAME ${TEST_BASENAME} TEST_SUFFIX "-")

camitk_add_test(EXECUTABLE_ARGS "-d missingfile" 
                PASS_REGULAR_EXPRESSION "Argument error: please provide an input cepcoreschema XML file."
                PROJECT_NAME ${TEST_BASENAME} TEST_SUFFIX "-")

camitk_add_test(EXECUTABLE_ARGS "-f badfile -d baddir" 
                PASS_REGULAR_EXPRESSION "File badfile does not exist or is not a file"
                PROJECT_NAME ${TEST_BASENAME} TEST_SUFFIX "-")

camitk_add_test(EXECUTABLE_ARGS "-f badfile -d ${CMAKE_CURRENT_BINARY_DIR}" 
                PASS_REGULAR_EXPRESSION "File badfile does not exist or is not a file"
                PROJECT_NAME ${TEST_BASENAME} TEST_SUFFIX "-")

camitk_add_test(EXECUTABLE_ARGS "-f ${CMAKE_CURRENT_SOURCE_DIR}/main.cpp -d ${CMAKE_CURRENT_BINARY_DIR}" 
                PASS_REGULAR_EXPRESSION "instance document parsing failed."
                PROJECT_NAME ${TEST_BASENAME} TEST_SUFFIX "-")

camitk_add_test(EXECUTABLE_ARGS "-f ${CMAKE_CURRENT_SOURCE_DIR}/main.cpp -d baddir" 
                PASS_REGULAR_EXPRESSION "Path baddir/ is not a directory"
                PROJECT_NAME ${TEST_BASENAME} TEST_SUFFIX "-")

# Add config test
find_program(BASH_PROGRAM bash)
if(BASH_PROGRAM)
    # use "ctest -VV -R cepgenerator-test" to run just this test
    add_test(NAME ${APPLICATION_TARGET_NAME}-bash-test 
             COMMAND ${BASH_PROGRAM} ${CMAKE_CURRENT_SOURCE_DIR}/testing/cepgenerator-test.sh -inbuild ${PROJECT_BINARY_DIR} ${PROJECT_SOURCE_DIR}
             WORKING_DIRECTORY ${CMAKE_BINARY_DIR} # needs to be at the top of the CamiTK module repository
    )
    # add test to this subproject
    set_tests_properties(${APPLICATION_TARGET_NAME}-bash-test PROPERTIES LABELS ${APPLICATION_TARGET_NAME})
endif()

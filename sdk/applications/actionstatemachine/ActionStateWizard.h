/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef ACTIONSTATEWIZARD_H
#define ACTIONSTATEWIZARD_H

#include "ui_ActionStateWizard.h"

/**
 * @ingroup group_sdk_application_asm
 *
 * @brief
 * This dialog is shown only if at least one of the two required input parameters (SCXML
 * and output directory) is missing when the application is started.
 *
 */
class ActionStateWizard : public QDialog {
    Q_OBJECT
public:
    /// Constructor (initialize the application parameters)
    ActionStateWizard(QString inputFileName, QString outputDir);

    /// Return the current filename of scxml file to use as input file
    QString getSCXMLFilename();

    /// Return the current directory name to use as output log directory
    QString getLogDirectory();

private slots:
    /// called when the user click on "Browse" to select an output directory
    void selectLogDirectory();

    /// called when the user click on "Browse" to select an SCXML input file
    void selectSCXMLFilename();

private:
    /// the Dialog GUI
    Ui::ActionStateWizard* ui;

    /// the current value of the SCXML input file
    QString SCXMLFileName;

    /// the current value of the output directory
    QString logDirectory;

    /// initialize the GUI to display the current value of SCXML input file
    void initSCXMLLabel();

    /// initialize the GUI to display the current value of output directory
    void initLogDirLabel();

    /// enable the Start button state only if both parameters are set
    void updateStartButtonState();
};
#endif // ACTIONSTATEWIZARD_H
/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "ActionStateWidget.h"
#include "ActionState.h"

#include <Action.h>

ActionStateWidget::ActionStateWidget(ActionState* actionState) {
    ui.setupUi(this);
    myActionState = actionState;
    ui.stateNameLabel->setText(myActionState->getName());
    ui.stateDescriptionLabel->setText(myActionState->getDescription());
    ui.actionFrame->hide();
}

QPushButton* ActionStateWidget::addTransitionButton(QString text) {
    QPushButton* button = new QPushButton(text, this);
    ui.buttonsLayout->addWidget(button);

    return button;
}

void ActionStateWidget::setActionWidget(QWidget* widget) {
    if (widget != nullptr) {
        ui.actionPropertyLayout->addWidget(widget);
        ui.actionFrame->show();
    }
}

void ActionStateWidget::setDescription(QString description) {
    ui.stateDescriptionLabel->setText(description);
}

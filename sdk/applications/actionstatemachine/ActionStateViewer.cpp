/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
// Qt Stuff
#include <QDialog>
#include <QVBoxLayout>
#include <QLabel>

// CamiTK includes
#include <Application.h>

// Local includes
#include "ActionStateViewer.h"

// ---------------- constructor ----------------
ActionStateViewer::ActionStateViewer(QString name) : Viewer(name, Viewer::DOCKED) {
    myWidget = nullptr;
    // empty the list as this is a state viewer, it has nothing to do with Components
    setComponentClassNames(QStringList());
}

// ---------------- getWidget ----------------
QWidget* ActionStateViewer::getWidget() {
    if (myWidget == nullptr) {
        myWidget = new QWidget();

        auto* actionStateWidgetLayout = new QVBoxLayout();

        //-- build the stackedWidget to the action widget layout
        actionStateWidgetStack = new QStackedWidget();
        actionStateWidgetStack->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
        actionStateWidgetStack->setLineWidth(3);
        // insert empty widget to fill the space by default
        emptyActionWidgetIndex = actionStateWidgetStack->addWidget(new QWidget());
        // init history insert new index in history for empty selection
        actionStateWidgetLayout->addWidget(actionStateWidgetStack);

        // set the action widget layout
        myWidget->setLayout(actionStateWidgetLayout);
        // Setting the ui layout
    }

    return myWidget;
}

// ---------------- setState ----------------
void ActionStateViewer::setState(ActionState* actionState) {
    //-- check history
    QWidget* actionStateWidget = actionState->getWidget();
    int actionStateWidgetIndex = actionStateWidgetStack->indexOf(actionStateWidget);

    if (actionStateWidgetIndex == -1 && actionStateWidget) {
        // add the widget (beware that actionWidgetStack then takes ownership of the widget!)
        actionStateWidgetIndex = actionStateWidgetStack->addWidget(actionStateWidget);
    }

    actionStateWidgetStack->setCurrentIndex(actionStateWidgetIndex);
    actionStateWidgetStack->update();

}


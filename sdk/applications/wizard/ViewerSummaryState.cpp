/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ViewerSummaryState.h"
#include "ViewerSummaryWidget.h"

#include "WizardMainWindow.h"

// includes from coreschema
#include <Viewer.hxx>
#include <ViewerExtension.hxx>

#include <iostream>

ViewerSummaryState::ViewerSummaryState(QString name, ViewerSummaryWidget* widget,
                                       WizardMainWindow* mainWindow, QState* parent)
    : WizardState(name, widget, mainWindow, parent) {
    this->domViewerExtension = nullptr;
    this->domViewer = nullptr;
}

void ViewerSummaryState::resetViewer(cepcoreschema::Viewer* domViewer) {
    this->domViewer = domViewer;
}

void ViewerSummaryState::onEntry(QEvent* event) {
    WizardState::onEntry(event);

    auto* viewerSummaryWidget = dynamic_cast<ViewerSummaryWidget*>(widget);
    if (viewerSummaryWidget != nullptr) {
        if (domViewer != nullptr) {
            QString name = domViewer->name().c_str();
            QString description = domViewer->description().c_str();
            cepcoreschema::ViewerType type = domViewer->type();

            viewerSummaryWidget->setSummary(name, description, type);
        }
    }

}

void ViewerSummaryState::onExit(QEvent* event) {
    WizardState::onExit(event);
}


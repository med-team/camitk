/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef COMPONENTEXTENSIONCREATIONSTATE_H
#define COMPONENTEXTENSIONCREATIONSTATE_H

#include <QState>
#include <QWidget>

class WizardMainWindow;
// Sub States
class ComponentExtensionDescriptionWidget;
class ComponentExtensionDescriptionState;
class ComponentsCreationWidget;
class ComponentsCreationState;
class ComponentCreationState;
class DependenciesWidget;
class ComponentExtensionDependenciesState;
class ExtensionSummaryWidget;
class ExtensionSummaryState;

// Dependency from cepcoreschema
// Declaration here to avoid declaration in dependant projects.
namespace cepcoreschema {
class Cep;
class ComponentExtension;
}

/**
 * @ingroup group_sdk_application_wizard
 *
 * @brief
 * State to create component extension.
 *
 */
class  ComponentExtensionCreationState : public QState  {

    Q_OBJECT;

public:
    /**  Constructor */
    ComponentExtensionCreationState(QString name, WizardMainWindow* mainWidnow, cepcoreschema::Cep* domCep);
    /**  Destructor */
    ~ComponentExtensionCreationState() override = default;

signals:
    void next();

public slots:
    virtual void extensionFinished();
    virtual void extensionCancelled();

protected:
    /// Reimplemented from QState
    /// @{
    void onEntry(QEvent* event) override;

    void onExit(QEvent* event) override;
    ///@}

private:

    bool cancelled;

    // Sub States
    ComponentExtensionDescriptionWidget*   componentExtensionDescriptionWidget;
    ComponentExtensionDescriptionState*    componentExtensionDescriptionState;
    ComponentsCreationWidget*              componentsCreationWidget;
    ComponentsCreationState*               componentsCreationState;
    ComponentCreationState*                componentCreationState;
    DependenciesWidget*                    componentExtensionDependenciesWidget;
    ComponentExtensionDependenciesState*   componentExtensionDependenciesState;
    ExtensionSummaryWidget*                componentExtensionSummaryWidget;
    ExtensionSummaryState*                 componentExtensionSummaryState;


    void createSubStates(WizardMainWindow* mainWindow);

    QString name;
    cepcoreschema::Cep* domCep;
    cepcoreschema::ComponentExtension* domComponentExtension;

};
#endif

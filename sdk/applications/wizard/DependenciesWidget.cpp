/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "DependenciesWidget.h"

#include "DefaultGUIText.h"

DependenciesWidget::DependenciesWidget(QWidget* parent) : QWidget(parent) {
    ui.setupUi(this);
}

void DependenciesWidget::setToDefault() {
    // Remove exisiting Libraries Check Boxes
    foreach (QCheckBox* cb, cepLibrariesCheckBox) {
        ui.cepLibsLayout->removeWidget(cb);
        delete cb;
    }
    cepLibrariesCheckBox.clear();

    // Remove exisiting Components Check Boxes
    foreach (QCheckBox* cb, cepComponentsCheckBox) {
        ui.cepLibsLayout->removeWidget(cb);
        delete cb;
    }
    cepComponentsCheckBox.clear();

    // Remove exisiting Actions Check Boxes
    foreach (QCheckBox* cb, cepActionsCheckBox) {
        ui.cepActionsLayout->removeWidget(cb);
        delete cb;
    }
    cepActionsCheckBox.clear();

    // Uncheck other libraries dependencies
    ui.itkCheckBox->setChecked(false);
    ui.opencvCheckBox->setChecked(false);
    ui.igstkCheckBox->setChecked(false);
    ui.gdcmCheckBox->setChecked(false);
    ui.xsdcxxCheckBox->setChecked(false);
    ui.libxml2CheckBox->setChecked(false);
    ui.qtmodulesCheckBox->setChecked(false);

}

void DependenciesWidget::nextButtonClicked() {
    emit next();
}

void DependenciesWidget::cancelButtonClicked() {
    emit cancel();
}

void DependenciesWidget::previousButtonClicked() {
    emit previous();
}

void DependenciesWidget::setItkDependency(bool checked) {
    ui.itkCheckBox->setChecked(checked);
}

void DependenciesWidget::setElement(QString element) {
    QString text;
    // Title Bar
    text = ui.labelExtension->text();
    text.replace(QRegExp("@ELEMENT@"), element);
    ui.labelExtension->setText(text);

    text = ui.labelElements->text();
    text.replace(QRegExp("@ELEMENTDETAILS@"), element.left(element.indexOf(" ")));
    ui.labelElements->setText(text);

    // Description Label
    text = defaultDependenciesExplanation;
    text.replace(QRegExp("@ELEMENT@"), element);
    ui.ExplanationLabel->setText(text);

    // Tool Tips
    QVector<QLabel*> questionMarks;
    questionMarks.append(ui.cepLibsQM);
    questionMarks.append(ui.cepComponentsQM);
    questionMarks.append(ui.cepActionsQM);
    questionMarks.append(ui.camitkLibrariesQM);
    questionMarks.append(ui.camitkComponentsQM);
    questionMarks.append(ui.camitkActionsQM);

    foreach (QLabel* qm, questionMarks) {
        text = qm->toolTip();
        text.replace(QRegExp("@ELEMENT@"), element);
        qm->setToolTip(text);
    }

}

void DependenciesWidget::updateCEPLibraries(QStringList cepLibraries) {
    // Remove exisiting Check Boxes
    foreach (QCheckBox* cb, cepLibrariesCheckBox) {
        ui.cepLibsLayout->removeWidget(cb);
        delete cb;
    }
    cepLibrariesCheckBox.clear();

    if (cepLibraries.isEmpty()) {
        ui.cepLibsQM->hide();
        ui.cepLibsLabel->hide();
    }
    else {
        int row = 0;
        int col = 0;
        foreach (QString lib, cepLibraries) {
            QCheckBox* checkBox = new QCheckBox();
            checkBox->setText(lib);
            checkBox->setChecked(false);

            this->cepLibrariesCheckBox.append(checkBox);
            ui.cepLibsLayout->addWidget(checkBox, col, row);
            row = (row + 1) % 3;
            col = row == 0 ? col + 1 : col;

        }
        ui.cepLibsQM->show();
        ui.cepLibsLabel->show();

    }
}

void DependenciesWidget::updateCEPComponents(QMap<QString, bool> cepComponents) {
    // Remove exisiting Check Boxes
    foreach (QCheckBox* cb, cepComponentsCheckBox) {
        ui.cepLibsLayout->removeWidget(cb);
        delete cb;
    }
    cepComponentsCheckBox.clear();

    if (cepComponents.isEmpty()) {
        ui.cepComponentsQM->hide();
        ui.cepComponentsLabel->hide();
    }
    else {
        int row = 0;
        int col = 0;
        QMapIterator<QString, bool> it(cepComponents);
        while (it.hasNext()) {
            it.next();
            QString comp = it.key();
            bool compDependency = it.value();
            QCheckBox* checkBox = new QCheckBox();
            checkBox->setText(comp);
            checkBox->setChecked(compDependency);
            checkBox->setEnabled(!compDependency);

            this->cepComponentsCheckBox.append(checkBox);
            ui.cepComponentsLayout->addWidget(checkBox, col, row);
            row = (row + 1) % 3;
            col = row == 0 ? col + 1 : col;

        }
        ui.cepComponentsQM->show();
        ui.cepComponentsLabel->show();

    }

}

void DependenciesWidget::updateCEPActions(QStringList cepActions) {
    // Remove exisiting Check Boxes
    foreach (QCheckBox* cb, cepActionsCheckBox) {
        ui.cepActionsLayout->removeWidget(cb);
        delete cb;
    }
    cepActionsCheckBox.clear();

    if (cepActions.isEmpty()) {
        ui.cepActionsQM ->hide();
        ui.cepActionsLabel->hide();
    }
    else {
        int row = 0;
        int col = 0;
        foreach (QString action, cepActions) {
            QCheckBox* checkBox = new QCheckBox();
            checkBox->setText(action);
            checkBox->setChecked(false);

            this->cepActionsCheckBox.append(checkBox);
            ui.cepActionsLayout->addWidget(checkBox, col, row);
            row = (row + 1) % 3;
            col = row == 0 ? col + 1 : col;

        }
        ui.cepActionsQM->show();
        ui.cepActionsLabel->show();
    }

}

QStringList DependenciesWidget::getCEPLibrariesDependencies() {
    QStringList cepLibs;
    foreach (QCheckBox* cb, cepLibrariesCheckBox) {
        if (cb->isChecked()) {
            cepLibs.append(cb->text());
        }
    }

    return cepLibs;
}

QStringList DependenciesWidget::getCEPComponentsDependencies() {
    QStringList cepComps;
    foreach (QCheckBox* cb, cepComponentsCheckBox) {
        if (cb->isChecked()) {
            cepComps.append(cb->text());
        }
    }

    return cepComps;
}

QStringList DependenciesWidget::getCEPActionsDependencies() {
    QStringList cepActions;
    foreach (QCheckBox* cb, cepActionsCheckBox) {
        if (cb->isChecked()) {
            cepActions.append(cb->text());
        }
    }
    return cepActions;
}


QStringList DependenciesWidget::getCamiTKLibrariesDependencies() {
    QStringList camitkLibraries;
    if (! ui.camitkLibrariesLineEdit->text().isEmpty()) {
        camitkLibraries = ui.camitkLibrariesLineEdit->text().split(" ");
    }
    return camitkLibraries;
}

QStringList DependenciesWidget::getCamiTKComponentsDependencies() {
    QStringList camitkComponents;
    if (! ui.camitkComponentsLineEdit->text().isEmpty()) {
        camitkComponents = ui.camitkComponentsLineEdit->text().split(" ");
    }

    return camitkComponents;
}

QStringList DependenciesWidget::getCamiTKActionsDependencies() {
    QStringList camitkActions;
    if (! ui.camitkActionsLineEdit->text().isEmpty()) {
        camitkActions = ui.camitkActionsLineEdit->text().split(" ");
    }

    return camitkActions;
}

QStringList DependenciesWidget::getExternalLibsDependencies() {
    QStringList externalLibs;
    if (ui.itkCheckBox->isChecked()) {
        externalLibs.append("itk");
    }
    if (ui.opencvCheckBox->isChecked()) {
        externalLibs.append("opencv");
    }
    if (ui.igstkCheckBox->isChecked()) {
        externalLibs.append("igstk");
    }
    if (ui.gdcmCheckBox->isChecked()) {
        externalLibs.append("gdcm");
    }
    if (ui.xsdcxxCheckBox->isChecked()) {
        externalLibs.append("xsd");
    }
    if (ui.libxml2CheckBox->isChecked()) {
        externalLibs.append("libxml2");
    }
    if (ui.qtmodulesCheckBox->isChecked()) {
        externalLibs.append("qt_modules");
    }

    return externalLibs;
}

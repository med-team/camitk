/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ActionsCreationWidget.h"

#include "DefaultGUIText.h"

// Qt files
#include <QMessageBox>

ActionsCreationWidget::ActionsCreationWidget(QWidget* parent) : QWidget(parent) {
    ui.setupUi(this);
}

void ActionsCreationWidget::setToDefault() {
    ui.requiredLabel->setStyleSheet(normalStyle);
    ui.label_Star->setStyleSheet(normalStyle);
    emptyExistingActions();
}

void ActionsCreationWidget::nextButtonClicked() {
    // check if at least an action has been created
#ifndef _WIZARD_QUESTIONS_SQUEEZE
    if (createdActions.isEmpty()) {
        ui.requiredLabel->setStyleSheet(enhancedStyle);
        ui.label_Star->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(NULL, "Actions Creation Widget", "Please create at least one action in this ActionExtension\n");
    }
    else {
        ui.requiredLabel->setStyleSheet(normalStyle);
        ui.label_Star->setStyleSheet(normalStyle);
        emit next();
    }
#else
    emit next();
#endif
}

void ActionsCreationWidget::addActionClicked() {
    ui.requiredLabel->setStyleSheet(normalStyle);
    ui.label_Star->setStyleSheet(normalStyle);

    emit newAction();
}

void ActionsCreationWidget::previousButtonClicked() {
    emit previous();
}

void ActionsCreationWidget::cancelButtonClicked() {
    emit cancel();
}

void ActionsCreationWidget::setGroupBoxTitle(QString text) {
    ui.actionsGroupBox->setTitle(text);
}

void ActionsCreationWidget::emptyExistingActions() {
    createdActions.clear();
    createdActionsString = defaultCreatedActionsString;
    ui.createdActionsTextEdit->setHtml(createdActionsString);
}

void ActionsCreationWidget::addActionName(QString actionName) {
    this->createdActions.append(actionName);

    QString toBeInserted = "<li>" + actionName + "</li>\n";
    int index = createdActionsString.lastIndexOf("</ul>");

    createdActionsString.insert(index, toBeInserted);
    ui.createdActionsTextEdit->setHtml(createdActionsString);
}

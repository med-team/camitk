/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "CepDescriptionState.h"
#include "CepDescriptionWidget.h"
#include "WizardMainWindow.h"

// includes from coreschema
#include <Cep.hxx>


CepDescriptionState::CepDescriptionState(QString name, CepDescriptionWidget* widget, WizardMainWindow* mainWindow, cepcoreschema::Cep* domCep)
    : WizardState(name, widget, mainWindow) {
    this->domCep = domCep;
}

void CepDescriptionState::onExit(QEvent* event) {
    WizardState::onExit(event);
    auto* cepDescriptionWidget = dynamic_cast<CepDescriptionWidget*>(widget);
    if (cepDescriptionWidget != nullptr) {
        QString cepName = cepDescriptionWidget->getCepName();
        QString cepDescription = cepDescriptionWidget->getCepDescription();
        domCep->name(cepName.toStdString());
        domCep->description(cepDescription.toStdString());
    }
}

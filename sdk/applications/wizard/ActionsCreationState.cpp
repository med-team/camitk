/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ActionsCreationState.h"
#include "ActionsCreationWidget.h"
#include "WizardMainWindow.h"
#include "ActionExtensionCreationState.h"

// includes from coreschema
#include <ActionExtension.hxx>
#include <Actions.hxx>


ActionsCreationState::ActionsCreationState(QString name, ActionsCreationWidget* widget, WizardMainWindow* mainWindow, ActionExtensionCreationState* parent)
    : WizardState(name, widget, mainWindow, parent) {
    this->domActionExtension = nullptr;
}

void ActionsCreationState::resetDomActionExtension(cepcoreschema::ActionExtension* domActionExtension) {
    this->domActionExtension = domActionExtension;
    auto* actionsCreationWidget = dynamic_cast<ActionsCreationWidget*>(widget);
    if (actionsCreationWidget != nullptr) {
        actionsCreationWidget->setToDefault();
    }
}

void ActionsCreationState::onEntry(QEvent* event) {
    WizardState::onEntry(event);

    auto* actionsCreationWidget = dynamic_cast<ActionsCreationWidget*>(widget);
    if (actionsCreationWidget != nullptr) {
        QString actionExtensionName = domActionExtension->name().c_str();
        actionsCreationWidget->setGroupBoxTitle(actionExtensionName);

        actionsCreationWidget->emptyExistingActions();
        cepcoreschema::Actions theActions = domActionExtension->actions();
        cepcoreschema::Actions::action_iterator it;
        for (it = theActions.action().begin(); it != theActions.action().end(); it++) {
            QString actionName = (*it).name().c_str();
            actionsCreationWidget->addActionName(actionName);
        }
    }
}

void ActionsCreationState::onExit(QEvent* event) {
    WizardState::onExit(event);
}

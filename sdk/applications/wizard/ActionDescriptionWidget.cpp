/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ActionDescriptionWidget.h"

#include "DefaultGUIText.h"

// Qt files
#include <QMessageBox>

ActionDescriptionWidget::ActionDescriptionWidget(QWidget* parent) : QWidget(parent) {
    ui.setupUi(this);
    setToDefault(defaultActionExtensionName);
}

void ActionDescriptionWidget::nextButtonClicked() {
    QString actionName = ui.actionNameItself->text();
    QString actionDescription = ui.actionGoalItself->toPlainText();
#ifndef _WIZARD_QUESTIONS_SQUEEZE
    if (actionName == actionExtensionName) {
        ui.actionNameStar->setStyleSheet(enhancedStyle);
        ui.actionGoalStar->setStyleSheet(normalStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultActionNameAndExtension);
    }
    else if (actionName.toUtf8() != actionName.toLatin1()) {
        ui.actionNameStar->setStyleSheet(enhancedStyle);
        ui.actionGoalStar->setStyleSheet(normalStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultAscii);
    }
    else if ((actionName.isEmpty()) || (actionName == defaultActionName)) {
        ui.actionNameStar->setStyleSheet(enhancedStyle);
        ui.actionGoalStar->setStyleSheet(normalStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultRealActionName);
    }
    else if (actionDescription.toUtf8() != actionDescription.toLatin1()) {
        ui.actionNameStar->setStyleSheet(normalStyle);
        ui.actionGoalStar->setStyleSheet(enhancedStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultAscii);
    }
    else if ((actionDescription.isEmpty()) || (actionDescription == defaultActionDescription)) {
        ui.actionNameStar->setStyleSheet(normalStyle);
        ui.actionGoalStar->setStyleSheet(enhancedStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultRealActionDescription);
    }
    else {
        emit next();
    }
#else
    emit next();
#endif
}

void ActionDescriptionWidget::cancelButtonClicked() {
    emit cancel();
}


QString ActionDescriptionWidget::getActionName() {
    return ui.actionNameItself->text();
}

QString ActionDescriptionWidget::getActionDescription() {
    return ui.actionGoalItself->toPlainText();
}

QString ActionDescriptionWidget::getComponentName() {
    return ui.componentComboBox->currentText();
}

bool ActionDescriptionWidget::isItkFilter() {
    return ui.itkFilterCheckBox->isChecked();
}

QString ActionDescriptionWidget::getOutputType() {
    return ui.itkOutputImageTypeComboBox->currentText();
}

void ActionDescriptionWidget::addPossibleComponent(QString possibleComponent) {
    possibleComponents << possibleComponent;
    ui.componentComboBox->clear();
    ui.componentComboBox->insertItems(0, possibleComponents);
}

void ActionDescriptionWidget::itkFilterClicked(bool checked) {
    if ((ui.componentComboBox->currentText() == "ImageComponent") && checked) {
        ui.itkOutputImageLabel->show();
        ui.itkOutputImageTypeComboBox->show();
    }
    else {
        ui.itkOutputImageLabel->hide();
        ui.itkOutputImageTypeComboBox->hide();

    }
}

void ActionDescriptionWidget::setToDefault(QString actionExtensionName) {

    this->actionExtensionName = actionExtensionName;

    ui.actionNameItself->setText(defaultActionName);
    ui.actionGoalItself->setPlainText(defaultActionDescription);
    ui.actionNameStar->setStyleSheet(normalStyle);
    ui.actionGoalStar->setStyleSheet(normalStyle);
    ui.requiredLabel->setStyleSheet(normalStyle);

    possibleComponents = defaultPossibleComponents;

    ui.componentComboBox->clear();
    ui.componentComboBox->insertItems(0, possibleComponents);
    ui.itkFilterCheckBox->setChecked(false);
    ui.itkOutputImageLabel->hide();
    ui.itkOutputImageTypeComboBox->hide();
    ui.componentComboBox->setCurrentIndex(possibleComponents.indexOf("ImageComponent"));
}

void ActionDescriptionWidget::setName(QString name) {
    ui.actionNameItself->setText(name);
}

void ActionDescriptionWidget::setDescription(QString description) {
    ui.actionGoalItself->setPlainText(description);
}

void ActionDescriptionWidget::setComponent(QString component) {
    ui.componentComboBox->setCurrentIndex(possibleComponents.indexOf(component));
}

void ActionDescriptionWidget::componentChanged(QString text) {
    if (ui.componentComboBox->currentText() == "ImageComponent") {
        ui.itkFilterCheckBox->show();
        if (ui.itkFilterCheckBox->isChecked()) {
            ui.itkOutputImageLabel->show();
            ui.itkOutputImageTypeComboBox->show();
        }
    }
    else {

        ui.itkFilterCheckBox->hide();
        ui.itkOutputImageLabel->hide();
        ui.itkOutputImageTypeComboBox->hide();
    }
}



/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef VIEWERCREATIONSTATE_H
#define VIEWERCREATIONSTATE_H

#include <QState>
#include <QWidget>

class WizardMainWindow;
class ViewerExtensionCreationState;

// Sub-states
class ViewerDescriptionWidget;
class ViewerDescriptionState;
class ViewerSummaryWidget;
class ViewerSummaryState;

// Dependency from cepcoreschema
// Declaration here to avoid declaration in dependant projects.
namespace cepcoreschema {
class ViewerExtension;
class Viewer;
class Cep;
}

/**
 * @ingroup group_sdk_application_wizard
 *
 * @brief
 * State to create one action.
 *
 * This state manages sub-state to create one action.
 *
 */
class ViewerCreationState : public QState  {

    Q_OBJECT;

public:
    /**  Constructor */
    ViewerCreationState(QString name, WizardMainWindow* mainWidnow, cepcoreschema::Cep* domCep, ViewerExtensionCreationState* parent);

    /**  Destructor */
    ~ViewerCreationState() override = default;

    void resetDomViewerExtension(cepcoreschema::ViewerExtension* domViewerExtension);

signals:
    void nextVCS();
    void nextVCS2();

public slots:
    virtual void viewerFinished();
    virtual void viewerCancelled();

protected:
    /// Reimplemented from QState
    /// @{
    void onEntry(QEvent* event) override;

    void onExit(QEvent* event) override;
    ///@}

    /// Substates (to be updated with domAction at each entry)
    ViewerDescriptionWidget*     viewerDescriptionWidget;
    ViewerDescriptionState*      viewerDescriptionState;
    ViewerSummaryWidget*         viewerSummaryWidget;
    ViewerSummaryState*          viewerSummaryState;


private:
    void createSubStates(WizardMainWindow* mainWindow);

    bool cancelled;
    QString name;
    cepcoreschema::ViewerExtension* domViewerExtension;
    cepcoreschema::Viewer* domViewer;
    cepcoreschema::Cep* domCep;

};
#endif

/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef ACTIONCLASSIFICATIONWIDGET_H
#define ACTIONCLASSIFICATIONWIDGET_H

// Include GUI  automatically generated file
#include "ui_ActionClassificationWidget.h"

// includes from Qt
#include <QWidget>

class ActionTagWidget;

/**
 * @ingroup group_sdk_application_wizard
 *
 * @brief
 * Widget to class the action and to attribute a tag to it.
 *
 */
class  ActionClassificationWidget : public QWidget {

    Q_OBJECT;

public:
    /**  Constructor */
    ActionClassificationWidget(QWidget* parent);

    /**  Destructor */
    ~ActionClassificationWidget() override = default;

    void setToDefault();

    QString getFamily();

    void removeTag(ActionTagWidget* widget);
    QList<ActionTagWidget*> getTagWidgets();


public slots:
    virtual void nextButtonClicked();
    virtual void previousButtonClicked();
    virtual void cancelButtonClicked();
    virtual void addTagClicked();

signals:
    void next();
    void previous();
    void cancel();

private:

    bool isOneNonAsciiTag();

    Ui::ActionClassificationWidget ui;
    QList<ActionTagWidget*> list;
};
#endif

/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef VIEWERDESCRIPTIONWIDGET_H
#define VIEWERDESCRIPTIONWIDGET_H

// Include GUI  automatically generated file
#include "ui_ViewerDescriptionWidget.h"

// includes from Qt
#include <QWidget>

#include <ViewerType.hxx>

/**
 * @ingroup group_sdk_application_wizard
 *
 * @brief
 * Widget to describe action
 *
 */
class ViewerDescriptionWidget : public QWidget {

    Q_OBJECT;

public:
    /**  Constructor * */
    ViewerDescriptionWidget(QWidget* parent);

    /**  Destructor */
    ~ViewerDescriptionWidget() override = default;

    void setToDefault(QString viewerExtensionName);

    void setName(QString name);
    void setDescription(QString description);

    QString getViewerName();
    QString getViewerDescription();
    cepcoreschema::ViewerType getType();

public slots:
    virtual void nextButtonClicked();
    virtual void cancelButtonClicked();

signals:
    void next();
    void cancel();

private:
    Ui::ViewerDescriptionWidget ui;

    QString viewerExtensionName;
};
#endif

/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ActionAddParameterState.h"
#include "ActionAddParameterWidget.h"
#include "WizardMainWindow.h"
#include "ActionCreationState.h"
#include "ActionParameterWidget.h"

// includes from coreschema
#include <Action.hxx>
#include <Parameters.hxx>
#include <Parameter.hxx>



ActionAddParameterState::ActionAddParameterState(QString name, ActionAddParameterWidget* widget, WizardMainWindow* mainWindow, ActionCreationState* parent)
    : WizardState(name, widget, mainWindow, parent) {
    this->domAction = nullptr;
}

void ActionAddParameterState::resetDomAction(cepcoreschema::Action* domAction) {
    this->domAction = domAction;
    auto* actionAddParameterWidget = dynamic_cast<ActionAddParameterWidget*>(widget);
    if (actionAddParameterWidget != nullptr) {
        actionAddParameterWidget->setToDefault();
    }
}

void ActionAddParameterState::onEntry(QEvent* event) {
    WizardState::onEntry(event);
}

void ActionAddParameterState::onExit(QEvent* event) {
    addDomParameters();
    WizardState::onExit(event);
}


void ActionAddParameterState::addDomParameters() {
    auto* addParameterWidget = dynamic_cast<ActionAddParameterWidget*>((this->widget));
    QList<ActionParameterWidget*> parameterWidgets = addParameterWidget->getActionParameterWidgets();

    if (parameterWidgets.size() > 0) {
        cepcoreschema::Parameters domParameters;

        foreach (ActionParameterWidget* p, parameterWidgets) {
            QString name = p->getName();
            QString type = p->getType();
            QString defValue = p->getDefaultValue();
            QString description = p->getDescription();
            QString unit = p->getUnit();
            cepcoreschema::Parameter domParam(name.toStdString().c_str(), type.toStdString().c_str(), description.toStdString().c_str());
            domParam.unit(unit.toStdString().c_str());
            domParam.defaultValue(defValue.toStdString().c_str());
            domParameters.parameter().push_back(domParam);
        }
        this->domAction->parameters(domParameters);
    }
}

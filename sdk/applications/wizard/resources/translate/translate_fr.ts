<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>ActionAddParameterWidget</name>
    <message>
        <location filename="../../ActionAddParameterWidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../ActionAddParameterWidget.ui" line="41"/>
        <source>Action-extension</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionAddParameterWidget.ui" line="66"/>
        <source>Action(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionAddParameterWidget.ui" line="90"/>
        <source>Dependencies</source>
        <translation>Dépendances</translation>
    </message>
    <message>
        <location filename="../../ActionAddParameterWidget.ui" line="106"/>
        <location filename="../../ActionAddParameterWidget.ui" line="403"/>
        <source>Summary</source>
        <translation>Sommaire</translation>
    </message>
    <message>
        <location filename="../../ActionAddParameterWidget.ui" line="165"/>
        <location filename="../../ActionAddParameterWidget.ui" line="478"/>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionAddParameterWidget.ui" line="212"/>
        <location filename="../../ActionAddParameterWidget.ui" line="524"/>
        <source>2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionAddParameterWidget.ui" line="249"/>
        <location filename="../../ActionAddParameterWidget.ui" line="561"/>
        <source>3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionAddParameterWidget.ui" line="272"/>
        <location filename="../../ActionAddParameterWidget.ui" line="584"/>
        <source>4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionAddParameterWidget.ui" line="339"/>
        <source>Description</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionAddParameterWidget.ui" line="363"/>
        <source>Parameter(s)</source>
        <translation>Paramètre(s)</translation>
    </message>
    <message>
        <location filename="../../ActionAddParameterWidget.ui" line="387"/>
        <source>Classification</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionAddParameterWidget.ui" line="650"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;These are the parameters you will enter to apply your action.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ceux sont les paramètres que vous entrerez pour appliquer votre action.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../ActionAddParameterWidget.ui" line="628"/>
        <source>Action input parameters</source>
        <translation>paramètres d&apos;entrée d&apos;une Action</translation>
    </message>
    <message>
        <location filename="../../ActionAddParameterWidget.ui" line="685"/>
        <source>Add a Parameter</source>
        <translation>Ajouter un paramètre</translation>
    </message>
    <message>
        <location filename="../../ActionAddParameterWidget.ui" line="733"/>
        <source>Previous</source>
        <translation>Précédent</translation>
    </message>
    <message>
        <location filename="../../ActionAddParameterWidget.ui" line="762"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../../ActionAddParameterWidget.ui" line="806"/>
        <source>Next</source>
        <translation>Suivant</translation>
    </message>
</context>
<context>
    <name>ActionClassificationWidget</name>
    <message>
        <location filename="../../ActionClassificationWidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../ActionClassificationWidget.ui" line="41"/>
        <source>Action-extension</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionClassificationWidget.ui" line="66"/>
        <source>Action(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionClassificationWidget.ui" line="90"/>
        <source>Dependencies</source>
        <translation>Dépendances</translation>
    </message>
    <message>
        <location filename="../../ActionClassificationWidget.ui" line="106"/>
        <location filename="../../ActionClassificationWidget.ui" line="403"/>
        <source>Summary</source>
        <translation>Sommaire</translation>
    </message>
    <message>
        <location filename="../../ActionClassificationWidget.ui" line="165"/>
        <location filename="../../ActionClassificationWidget.ui" line="478"/>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionClassificationWidget.ui" line="212"/>
        <location filename="../../ActionClassificationWidget.ui" line="524"/>
        <source>2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionClassificationWidget.ui" line="249"/>
        <location filename="../../ActionClassificationWidget.ui" line="573"/>
        <source>3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionClassificationWidget.ui" line="272"/>
        <location filename="../../ActionClassificationWidget.ui" line="596"/>
        <source>4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionClassificationWidget.ui" line="339"/>
        <source>Description</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionClassificationWidget.ui" line="363"/>
        <source>Parameter(s)</source>
        <translation>Paramètre(s)</translation>
    </message>
    <message>
        <location filename="../../ActionClassificationWidget.ui" line="387"/>
        <source>Classification</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionClassificationWidget.ui" line="643"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;You will be able to find your action in the imp application for example.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Vous devez être capable de trouver votre action dans l&apos;application imp par exemple.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../ActionClassificationWidget.ui" line="675"/>
        <source>To classify this action, please enter a family:</source>
        <translation>Ouur classer cette action, veuillez entrer une famille:</translation>
    </message>
    <message>
        <location filename="../../ActionClassificationWidget.ui" line="688"/>
        <source>To quickly retrieve your action, you can add some tags:</source>
        <translation>Pour retrouver rapidement votre action, vous pouvez ajouter des étiquettes:</translation>
    </message>
    <message>
        <location filename="../../ActionClassificationWidget.ui" line="707"/>
        <source>*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionClassificationWidget.ui" line="733"/>
        <source>Add a Tag</source>
        <translation>Ajouter une Etiquette</translation>
    </message>
    <message>
        <location filename="../../ActionClassificationWidget.ui" line="777"/>
        <source>* &lt;i&gt;required field&lt;/i&gt;</source>
        <translation>* &lt;i&gt;champ obligatoire&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../../ActionClassificationWidget.ui" line="806"/>
        <source>Previous</source>
        <translation>Précédent</translation>
    </message>
    <message>
        <location filename="../../ActionClassificationWidget.ui" line="835"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../../ActionClassificationWidget.ui" line="879"/>
        <source>Next</source>
        <translation>Suivant</translation>
    </message>
</context>
<context>
    <name>ActionDescriptionWidget</name>
    <message>
        <location filename="../../ActionDescriptionWidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../ActionDescriptionWidget.ui" line="41"/>
        <source>Action-extension</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionDescriptionWidget.ui" line="66"/>
        <source>Action(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionDescriptionWidget.ui" line="90"/>
        <source>Dependencies</source>
        <translation>Dépendances</translation>
    </message>
    <message>
        <location filename="../../ActionDescriptionWidget.ui" line="106"/>
        <location filename="../../ActionDescriptionWidget.ui" line="403"/>
        <source>Summary</source>
        <translation>Sommaire</translation>
    </message>
    <message>
        <location filename="../../ActionDescriptionWidget.ui" line="165"/>
        <location filename="../../ActionDescriptionWidget.ui" line="478"/>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionDescriptionWidget.ui" line="212"/>
        <location filename="../../ActionDescriptionWidget.ui" line="524"/>
        <source>2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionDescriptionWidget.ui" line="249"/>
        <location filename="../../ActionDescriptionWidget.ui" line="561"/>
        <source>3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionDescriptionWidget.ui" line="272"/>
        <location filename="../../ActionDescriptionWidget.ui" line="584"/>
        <source>4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionDescriptionWidget.ui" line="339"/>
        <source>Description</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionDescriptionWidget.ui" line="363"/>
        <source>Parameter(s)</source>
        <translation>Paramètre(s)</translation>
    </message>
    <message>
        <location filename="../../ActionDescriptionWidget.ui" line="387"/>
        <source>Classification</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionDescriptionWidget.ui" line="647"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Choose the type of component your action will use.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Choisir le type de component que votre action utilisera.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../ActionDescriptionWidget.ui" line="675"/>
        <source> Name of your action:</source>
        <translation> Nom de votre Action:</translation>
    </message>
    <message>
        <location filename="../../ActionDescriptionWidget.ui" line="696"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Give a precise description of what your action will do.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Donner une description précise de ce que votre action fera.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../../../src/sdk/applications/wizard/ActionDescriptionWidget.ui" line="747"/>
        <source>ITK Filter ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../src/sdk/applications/wizard/ActionDescriptionWidget.ui" line="759"/>
        <source>Output Image Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../src/sdk/applications/wizard/ActionDescriptionWidget.ui" line="775"/>
        <source>Same as Input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../src/sdk/applications/wizard/ActionDescriptionWidget.ui" line="780"/>
        <source>unsigned char</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../src/sdk/applications/wizard/ActionDescriptionWidget.ui" line="785"/>
        <source>char</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../src/sdk/applications/wizard/ActionDescriptionWidget.ui" line="790"/>
        <source>unsigned short</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../src/sdk/applications/wizard/ActionDescriptionWidget.ui" line="795"/>
        <source>short</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../src/sdk/applications/wizard/ActionDescriptionWidget.ui" line="800"/>
        <source>unsigned int</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../src/sdk/applications/wizard/ActionDescriptionWidget.ui" line="805"/>
        <source>int</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../src/sdk/applications/wizard/ActionDescriptionWidget.ui" line="810"/>
        <source>long</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../src/sdk/applications/wizard/ActionDescriptionWidget.ui" line="815"/>
        <source>double</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ActionDescriptionWidget.ui" line="831"/>
        <source> Component: </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionDescriptionWidget.ui" line="866"/>
        <location filename="../../ActionDescriptionWidget.ui" line="888"/>
        <location filename="../../ActionDescriptionWidget.ui" line="926"/>
        <source>*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionDescriptionWidget.ui" line="942"/>
        <source> Goal(s) of your action:  </source>
        <translation>But(s) de votre action: </translation>
    </message>
    <message>
        <location filename="../../ActionDescriptionWidget.ui" line="964"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Give a meaningful name to your action.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Donner un nom explicite à votre action.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../ActionDescriptionWidget.ui" line="1137"/>
        <source>* &lt;i&gt;required field&lt;/i&gt;</source>
        <translation>* &lt;i&gt;Champ obligatoire&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../../ActionDescriptionWidget.ui" line="1166"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../../ActionDescriptionWidget.ui" line="1210"/>
        <source>Next</source>
        <translation>Suivant</translation>
    </message>
</context>
<context>
    <name>ActionExtensionDescriptionWidget</name>
    <message>
        <location filename="../../ActionExtensionDescriptionWidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../ActionExtensionDescriptionWidget.ui" line="41"/>
        <source>Action-extension</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionExtensionDescriptionWidget.ui" line="66"/>
        <source>Action(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionExtensionDescriptionWidget.ui" line="90"/>
        <source>Dependencies</source>
        <translation>Dépendances</translation>
    </message>
    <message>
        <location filename="../../ActionExtensionDescriptionWidget.ui" line="106"/>
        <source>Summary</source>
        <translation>Sommaire</translation>
    </message>
    <message>
        <location filename="../../ActionExtensionDescriptionWidget.ui" line="165"/>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionExtensionDescriptionWidget.ui" line="212"/>
        <source>2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionExtensionDescriptionWidget.ui" line="249"/>
        <source>3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionExtensionDescriptionWidget.ui" line="272"/>
        <source>4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionExtensionDescriptionWidget.ui" line="360"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;Create your Action Extension&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;Créer votre Action Extension&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../../../src/sdk/applications/wizard/ActionExtensionDescriptionWidget.ui" line="407"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ActionExtensionDescriptionWidget.ui" line="429"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Give a meaningful name your action-extension. The name can corresponds to the overall processing performed by the extension.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Donner un nom explicite à votre action-extension. Le nom peut correspondre à l&apos;ensemble des opérations réalisées par l&apos;extension.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt; </translation>
    </message>
    <message>
        <location filename="../../../../src/sdk/applications/wizard/ActionExtensionDescriptionWidget.ui" line="508"/>
        <location filename="../../../../src/sdk/applications/wizard/ActionExtensionDescriptionWidget.ui" line="530"/>
        <source>*</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../src/sdk/applications/wizard/ActionExtensionDescriptionWidget.ui" line="546"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ActionExtensionDescriptionWidget.ui" line="567"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Give a precise description of what your action-extension will do.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Donner une description précise de ce que votre action fera.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../ActionExtensionDescriptionWidget.ui" line="631"/>
        <source>* &lt;i&gt;required field&lt;/i&gt;</source>
        <translation>* &lt;i&gt;champ obligatoire&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../../ActionExtensionDescriptionWidget.ui" line="660"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../../ActionExtensionDescriptionWidget.ui" line="704"/>
        <source>Next</source>
        <translation>Suivant</translation>
    </message>
</context>
<context>
    <name>ActionParameterWidget</name>
    <message>
        <location filename="../../ActionParameterWidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../ActionParameterWidget.ui" line="29"/>
        <source>Parameter</source>
        <translation>Paramètre</translation>
    </message>
    <message>
        <location filename="../../ActionParameterWidget.ui" line="51"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Please provide an &lt;span style=&quot; font-weight:600;&quot;&gt;explicite&lt;/span&gt; name to your parameter.&lt;/p&gt;&lt;p&gt;The name of a parameter is a string, so &lt;span style=&quot; font-style:italic;&quot;&gt;you can use space characters&lt;/span&gt; to give an explicit name to your parameter.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;If applicable, you should also indicate the &lt;span style=&quot; font-weight:600;&quot;&gt;unit&lt;/span&gt; of your parameter in its name.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Veuillez fournir un nom &lt;span style=&quot; font-weight:600;&quot;&gt;explicite&lt;/span&gt; to your parameter.&lt;/p&gt;&lt;p&gt;Le nom d&apos;un parametre est une string, donc &lt;span style=&quot; font-style:italic;&quot;&gt;vous devez utiliser des caractères espaces &lt;/span&gt;  pour donner un nom explicite à votre paramètre&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;Si possible, vous devrez aussi indiquer l&apos;&lt;span style=&quot; font-weight:600;&quot;&gt;Unité&lt;/span&gt; de votre paramètre dans son nom.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../ActionParameterWidget.ui" line="82"/>
        <source>Name </source>
        <translation>Nom </translation>
    </message>
    <message>
        <location filename="../../ActionParameterWidget.ui" line="205"/>
        <source>*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionParameterWidget.ui" line="224"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;By default, the type of parameters ar &lt;span style=&quot; font-style:italic;&quot;&gt;QVariant&lt;/span&gt;.&lt;/p&gt;&lt;p&gt;If you want to use other types for your parameter, provide a default one and then modify its type manually in the code, after having carefully read the Qt documentation on QVariant here:&lt;/p&gt;&lt;p&gt;http://qt-project.org/doc/qt-5.0/qtcore/qvariant.html&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Par défaut, le type des paramètres est &lt;span style=&quot; font-style:italic;&quot;&gt;QVariant&lt;/span&gt;.&lt;/p&gt;&lt;p&gt;Si vous voulez utiliser d&apos;autres types pour votre paramètre, fournissez en un par défaut et modifiez manuellement son type dans le code, aprés avoir précisèment lu la documention Qt sur QVariant ici:&lt;/p&gt;&lt;p&gt;http://qt-project.org/doc/qt-5.0/qtcore/qvariant.html&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../ActionParameterWidget.ui" line="249"/>
        <source>Type</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionParameterWidget.ui" line="301"/>
        <source>int</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionParameterWidget.ui" line="306"/>
        <source>bool</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionParameterWidget.ui" line="311"/>
        <source>double</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionParameterWidget.ui" line="316"/>
        <source>QString</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionParameterWidget.ui" line="321"/>
        <source>QDate</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionParameterWidget.ui" line="326"/>
        <source>QTime</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionParameterWidget.ui" line="331"/>
        <source>QColor</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionParameterWidget.ui" line="336"/>
        <source>QPoint</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionParameterWidget.ui" line="341"/>
        <source>QPointF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionParameterWidget.ui" line="346"/>
        <source>QVector3D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionParameterWidget.ui" line="366"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Generally, we give the most common used value for a parameter as default value.&lt;/p&gt;&lt;p&gt;The user will then be able to modify it before applying the action.&lt;/p&gt;&lt;p&gt;As parameter types are QVariant, the default value may explicitly express QVariant(...).&lt;/p&gt;&lt;p/&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;En général, nous donnons comme valeur par défaut, la vaeur la plus fréquement utilisée.&lt;/p&gt;&lt;p&gt;L&apos;utilisateur sera capable de le modifier avant d&apos;appliquer l&apos;action.&lt;/p&gt;&lt;p&gt; Si les types de parametres sont QVariant, la valeur par défaut devra  clairement être exprimé en QVariant(...).&lt;/p&gt;&lt;p/&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../ActionParameterWidget.ui" line="389"/>
        <source>Default Value</source>
        <translation>Valeur pare défaut</translation>
    </message>
    <message>
        <location filename="../../ActionParameterWidget.ui" line="523"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The description of your parameter will not be displaied directly with your action but will be added as a tool tip on your parameter.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;La description de votre paramètre ne sera pas affiché directement à votre action mais sera ajouté comme un commentaire surgissant à votre paramètre.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../ActionParameterWidget.ui" line="551"/>
        <source>Description</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionParameterWidget.ui" line="583"/>
        <source>Write a description about your parameter</source>
        <translation>Donner une description pour votre paramètre</translation>
    </message>
    <message>
        <location filename="../../ActionParameterWidget.ui" line="606"/>
        <source>Your property&apos;s unit (keep empty for no one / unknown unit)</source>
        <translation>Votre unité de la propriété (laisser vide pour aucune propriété /unité inconnue)</translation>
    </message>
    <message>
        <location filename="../../ActionParameterWidget.ui" line="634"/>
        <source>Unit</source>
        <translation>Unité</translation>
    </message>
    <message>
        <location filename="../../ActionParameterWidget.ui" line="689"/>
        <source>* required field</source>
        <translation>* champ obligatoire</translation>
    </message>
    <message>
        <location filename="../../ActionParameterWidget.ui" line="721"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Remove&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Retirer&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;
</translation>
    </message>
</context>
<context>
    <name>ActionSummaryWidget</name>
    <message>
        <location filename="../../ActionSummaryWidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../ActionSummaryWidget.ui" line="41"/>
        <source>Action-extension</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionSummaryWidget.ui" line="66"/>
        <source>Action(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionSummaryWidget.ui" line="90"/>
        <source>Dependencies</source>
        <translation>Dépendances</translation>
    </message>
    <message>
        <location filename="../../ActionSummaryWidget.ui" line="108"/>
        <location filename="../../ActionSummaryWidget.ui" line="362"/>
        <source>Summary</source>
        <translation>Sommaire</translation>
    </message>
    <message>
        <location filename="../../ActionSummaryWidget.ui" line="165"/>
        <location filename="../../ActionSummaryWidget.ui" line="414"/>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionSummaryWidget.ui" line="194"/>
        <location filename="../../ActionSummaryWidget.ui" line="437"/>
        <source>2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionSummaryWidget.ui" line="231"/>
        <location filename="../../ActionSummaryWidget.ui" line="460"/>
        <source>3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionSummaryWidget.ui" line="256"/>
        <location filename="../../ActionSummaryWidget.ui" line="485"/>
        <source>4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionSummaryWidget.ui" line="312"/>
        <source>Description</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionSummaryWidget.ui" line="328"/>
        <source>Parameter(s)</source>
        <translation>Paramètre(s)</translation>
    </message>
    <message>
        <location filename="../../ActionSummaryWidget.ui" line="344"/>
        <source>Classification</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionSummaryWidget.ui" line="542"/>
        <source>Previous</source>
        <translation>Précédent</translation>
    </message>
    <message>
        <location filename="../../ActionSummaryWidget.ui" line="571"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../../ActionSummaryWidget.ui" line="615"/>
        <source>Next</source>
        <translation>Suivant</translation>
    </message>
</context>
<context>
    <name>ActionTagWidget</name>
    <message>
        <location filename="../../ActionTagWidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../ActionTagWidget.ui" line="34"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;A tag is used to quickly look for an action thanks to keyworkds whithout the need of going through famillies.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Une étiquette est utilisée pour chercher rapidement une action grace aux mots clés sans avoir besoin de passer par les famillies.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../ActionTagWidget.ui" line="59"/>
        <source>Tag</source>
        <translation>Etiquette</translation>
    </message>
    <message>
        <location filename="../../ActionTagWidget.ui" line="180"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Remove&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Retirer&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>ActionsCreationWidget</name>
    <message>
        <location filename="../../ActionsCreationWidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../ActionsCreationWidget.ui" line="41"/>
        <source>Action-extension</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionsCreationWidget.ui" line="66"/>
        <source>Action(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionsCreationWidget.ui" line="90"/>
        <source>Dependencies</source>
        <translation>Dépendances</translation>
    </message>
    <message>
        <location filename="../../ActionsCreationWidget.ui" line="106"/>
        <source>Summary</source>
        <translation>Sommaire</translation>
    </message>
    <message>
        <location filename="../../ActionsCreationWidget.ui" line="165"/>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionsCreationWidget.ui" line="212"/>
        <source>2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionsCreationWidget.ui" line="249"/>
        <source>3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionsCreationWidget.ui" line="272"/>
        <source>4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionsCreationWidget.ui" line="360"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Create your Actions&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Créer vos Actions&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../ActionsCreationWidget.ui" line="382"/>
        <source>Actions</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionsCreationWidget.ui" line="415"/>
        <source>Add an Action</source>
        <translation>Ajouter une Action</translation>
    </message>
    <message>
        <location filename="../../ActionsCreationWidget.ui" line="433"/>
        <source>*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ActionsCreationWidget.ui" line="473"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;* Please, add at least an action in your extension&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;* Veuillez ajouterau moins une action dans votre extension&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../ActionsCreationWidget.ui" line="502"/>
        <source>Previous</source>
        <translation>Précédent</translation>
    </message>
    <message>
        <location filename="../../ActionsCreationWidget.ui" line="531"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../../ActionsCreationWidget.ui" line="575"/>
        <source>Next</source>
        <translation>Suivant</translation>
    </message>
</context>
<context>
    <name>CepContactWidget</name>
    <message>
        <location filename="../../CepContactWidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../CepContactWidget.ui" line="22"/>
        <location filename="../../CepContactWidget.ui" line="145"/>
        <source>*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../CepContactWidget.ui" line="31"/>
        <source>LGPL V3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../CepContactWidget.ui" line="41"/>
        <source>User defined Licence</source>
        <translation>Licence définie par l&apos;Utilisateur</translation>
    </message>
    <message>
        <location filename="../../CepContactWidget.ui" line="65"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Please provide an email contact.&lt;/p&gt;&lt;p&gt;Indeed if your CEP is destined to be maintained by CamiTK team, this latter should be able to contact a responsible for this project. &lt;/p&gt;&lt;p&gt;This email should &lt;span style=&quot; font-style:italic;&quot;&gt;preferably&lt;/span&gt; be the one of a permanant researcher (as a Professor or an internship advisor) able to take decision about copyright or diffusion rights for example.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Veuillez fournir un email de contact.&lt;/p&gt;&lt;p&gt;En effet, si votre CEP est destiné à être maintenu par l&apos;équipe CamiTK, celui-ci permettra d&apos;être contacter un responsable pour ce projet. &lt;/p&gt;&lt;p&gt;Cet email devrait être&lt;span style=&quot; font-style:italic;&quot;&gt;de preférence&lt;/span&gt;celui d&apos;un des chercheurs permanents (comme un Professeur ou un expert) autorisés à prendre des décisions sur le copyright ou des droits de diffusion par exemple.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../CepContactWidget.ui" line="87"/>
        <source>Licence</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../CepContactWidget.ui" line="100"/>
        <source>Contact</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../CepContactWidget.ui" line="122"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The licence of your particular CEP.&lt;/p&gt;&lt;p&gt;Indeed, although CamiTK is under LGPLV3 licence, you can decide any licence for your CEP.&lt;/p&gt;&lt;p&gt;If you do not know yet under which licence to distribute your CEP, let the LGPL licence without and modify it the day you begin to distribute it.&lt;/p&gt;&lt;p&gt;The person able to decide the licence should be the contact person.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;La licence de votre CEP.&lt;/p&gt;&lt;p&gt;En effet,bien que CamiTK soit sous LGPLV3 license, vous pouvez choisir une license pour votre CEP.&lt;/p&gt;&lt;p&gt;Si vous ne connaissez pas encore la licence à distribuer avec votre CEP ,laisser la licence LGPL telle quelle et modifier la le jour où vous commencerez à distribuer votre CEP.&lt;/p&gt;&lt;p&gt;La personne autorisée à choisir la licence devrait être la personne à contacter.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../CepContactWidget.ui" line="152"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:7pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Lucida Grande&apos;; font-size:13pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../CepContactWidget.ui" line="197"/>
        <source>Previous</source>
        <translation>Précédent</translation>
    </message>
    <message>
        <location filename="../../CepContactWidget.ui" line="227"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;* required fields&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;*champs obligatoires&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../CepContactWidget.ui" line="267"/>
        <source>Next</source>
        <translation>Suivant</translation>
    </message>
</context>
<context>
    <name>CepCreateOrModifyWidget</name>
    <message>
        <location filename="../../CepCreateOrModifyWidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../CepCreateOrModifyWidget.ui" line="53"/>
        <source>Do you want to </source>
        <translation>Voulez-vous</translation>
    </message>
    <message>
        <location filename="../../CepCreateOrModifyWidget.ui" line="82"/>
        <source>Add Components / Actions / Libraries to an existing CEP containing a Manifest.xml</source>
        <translation>Ajouter Components / Actions / Libraries à un CEP existant contenant un fichier Manifest.xml</translation>
    </message>
    <message>
        <location filename="../../CepCreateOrModifyWidget.ui" line="85"/>
        <source>Modify an existing CEP</source>
        <translation>Modifier un CEP existant</translation>
    </message>
    <message>
        <location filename="../../CepCreateOrModifyWidget.ui" line="97"/>
        <source>Create a new CEP from scratch and add Components, Actions, Libraries, etc.</source>
        <translation>Créer un nouveau CEP à partir de rienet ajouter des Components, Actions, Libraries, etc.</translation>
    </message>
    <message>
        <location filename="../../CepCreateOrModifyWidget.ui" line="100"/>
        <source>Create a new CEP</source>
        <translation>Créer un nouveau CEP</translation>
    </message>
    <message>
        <location filename="../../CepCreateOrModifyWidget.ui" line="132"/>
        <source>Previous</source>
        <translation>Précédent</translation>
    </message>
    <message>
        <location filename="../../CepCreateOrModifyWidget.ui" line="173"/>
        <source>Next</source>
        <translation>Suivant</translation>
    </message>
    <message>
        <location filename="../../CepCreateOrModifyWidget.cpp" line="44"/>
        <source>Feature not implemented</source>
        <translation>Caractéristique non implémentée</translation>
    </message>
    <message>
        <location filename="../../CepCreateOrModifyWidget.cpp" line="44"/>
        <source>This feature is not implemented yet, sorry...
</source>
        <translation>Cette caractéristique n&apos;est pas encore implémentée, désolé...</translation>
    </message>
</context>
<context>
    <name>CepCreateRecapWidget</name>
    <message>
        <location filename="../../CepCreateRecapWidget.ui" line="20"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../CepCreateRecapWidget.ui" line="28"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;CamiTK Extension Project&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;CamiTK Extension Project&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../CepCreateRecapWidget.ui" line="61"/>
        <source>Contact</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../CepCreateRecapWidget.ui" line="68"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Name&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Nom&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../CepCreateRecapWidget.ui" line="75"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Description&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../CepCreateRecapWidget.ui" line="119"/>
        <source>CEP Properties</source>
        <translation>Propriété du CEP</translation>
    </message>
    <message>
        <location filename="../../CepCreateRecapWidget.ui" line="154"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Extensions&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../CepCreateRecapWidget.ui" line="226"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Add An Internal Library&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Ajouter une Librairie Interne&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../CepCreateRecapWidget.ui" line="297"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Add An Action Extension&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Ajouter une Action Extension&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../CepCreateRecapWidget.ui" line="307"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Add A Component Extension&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Ajouter une Component Extension&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../CepCreateRecapWidget.ui" line="483"/>
        <source>Generate CEP</source>
        <translation>Générer un CEP</translation>
    </message>
</context>
<context>
    <name>CepCreationDirectoryWidget</name>
    <message>
        <location filename="../../CepCreationDirectoryWidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../CepCreationDirectoryWidget.ui" line="53"/>
        <source>First, you need to specify the destination for the project sources</source>
        <translation>Premièrement, vous avez besoin de préciser la destination de vos sources de projet</translation>
    </message>
    <message>
        <location filename="../../CepCreationDirectoryWidget.ui" line="90"/>
        <source>Please select a (&lt;i&gt;empty&lt;/i&gt;) development directory*</source>
        <translation>Veuillez sélectionner un répertoire de developpement (&lt;i&gt;vide&lt;/i&gt;)*</translation>
    </message>
    <message>
        <location filename="../../CepCreationDirectoryWidget.ui" line="130"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../CepCreationDirectoryWidget.ui" line="184"/>
        <source>Previous</source>
        <translation>Précédent</translation>
    </message>
    <message>
        <location filename="../../CepCreationDirectoryWidget.ui" line="214"/>
        <source>* &lt;i&gt;required field&lt;/i&gt;</source>
        <translation>* &lt;i&gt;champ obligatoire&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../../CepCreationDirectoryWidget.ui" line="254"/>
        <source>Next</source>
        <translation>Suivant</translation>
    </message>
    <message>
        <location filename="../../CepCreationDirectoryWidget.cpp" line="48"/>
        <source>Before going further...</source>
        <translation>Avant d&apos;aller plus loin...</translation>
    </message>
    <message>
        <location filename="../../CepCreationDirectoryWidget.cpp" line="48"/>
        <source>Please select a valid development directory
</source>
        <translation>Veuillez sélectionner un répertoire de développement valide</translation>
    </message>
    <message>
        <location filename="../../CepCreationDirectoryWidget.cpp" line="66"/>
        <source>Set the development directory</source>
        <translation>Sélectionner le répertoire de développement</translation>
    </message>
</context>
<context>
    <name>CepDescriptionWidget</name>
    <message>
        <location filename="../../CepDescriptionWidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../CepDescriptionWidget.ui" line="22"/>
        <source>Name of your CEP</source>
        <translation>nOM DE VOTRE cep</translation>
    </message>
    <message>
        <location filename="../../CepDescriptionWidget.ui" line="29"/>
        <location filename="../../CepDescriptionWidget.ui" line="78"/>
        <source>*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../CepDescriptionWidget.ui" line="51"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Please give &lt;span style=&quot; font-style:italic;&quot;&gt;an explicite&lt;/span&gt; name to your CEP.&lt;/p&gt;&lt;p&gt;It should make any new user understand the main theme of your project.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Veuillez donner &lt;span style=&quot; font-style:italic;&quot;&gt; un&lt;/span&gt; nom à votre CEP&lt;/p&gt;explicite.&lt;p&gt; Il devrait faire comprendre à un nouvel utilisateur l&apos;objet principal de votre projet.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../CepDescriptionWidget.ui" line="67"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:7pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Lucida Grande&apos;; font-size:13pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../CepDescriptionWidget.ui" line="113"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Lucida Grande&apos;; font-size:13pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Please provide a concise but precise description of your CEP:&lt;/p&gt;
&lt;ul style=&quot;margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;&quot;&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;What is its purpose ?   &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Which kind of data will it handle ?   &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Which kind of process wil be involved ?   &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Who are the authors ?   &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Who can it be useful to ?&lt;/li&gt;&lt;/ul&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Lucida Grande&apos;; font-size:13pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Veuuillez donner une description concise et précise de votre CEP:&lt;/p&gt;
&lt;ul style=&quot;margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;&quot;&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Quelle est son objectif ?   &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;Quelle sorte de données manipule t il ?   &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Quelles types de traitement seront ils effectués ?   &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Qui sont les auteurs ?   &lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;A qui peut il être utile ?&lt;/li&gt;&lt;/ul&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../CepDescriptionWidget.ui" line="139"/>
        <source>Description of your CEP</source>
        <translation>Description de votre CEP</translation>
    </message>
    <message>
        <location filename="../../CepDescriptionWidget.ui" line="180"/>
        <source>Previous</source>
        <translation>Précédent</translation>
    </message>
    <message>
        <location filename="../../CepDescriptionWidget.ui" line="210"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;* required fields&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;* champs obligatoires&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../CepDescriptionWidget.ui" line="250"/>
        <source>Next</source>
        <translation>Suivant</translation>
    </message>
</context>
<context>
    <name>ComponentAddPropertyWidget</name>
    <message>
        <location filename="../../ComponentAddPropertyWidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../ComponentAddPropertyWidget.ui" line="41"/>
        <source>Component-extension</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentAddPropertyWidget.ui" line="66"/>
        <source>Component(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentAddPropertyWidget.ui" line="90"/>
        <source>Dependencies</source>
        <translation>Dépendances</translation>
    </message>
    <message>
        <location filename="../../ComponentAddPropertyWidget.ui" line="106"/>
        <location filename="../../ComponentAddPropertyWidget.ui" line="379"/>
        <source>Summary</source>
        <translation>Sommaire</translation>
    </message>
    <message>
        <location filename="../../ComponentAddPropertyWidget.ui" line="165"/>
        <location filename="../../ComponentAddPropertyWidget.ui" line="454"/>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentAddPropertyWidget.ui" line="212"/>
        <location filename="../../ComponentAddPropertyWidget.ui" line="500"/>
        <source>2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentAddPropertyWidget.ui" line="249"/>
        <location filename="../../ComponentAddPropertyWidget.ui" line="523"/>
        <source>3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentAddPropertyWidget.ui" line="272"/>
        <source>4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentAddPropertyWidget.ui" line="339"/>
        <source>Description</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentAddPropertyWidget.ui" line="363"/>
        <source>Property(ies)</source>
        <translation>Propriété(s)</translation>
    </message>
    <message>
        <location filename="../../ComponentAddPropertyWidget.ui" line="567"/>
        <source>Component properties</source>
        <translation>Propriétés du Component</translation>
    </message>
    <message>
        <location filename="../../ComponentAddPropertyWidget.ui" line="589"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Your Component may have dynamic properties: a size, a number of points, etc.&lt;/p&gt;&lt;p&gt;Please add them here.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Il se peut que votre Component ait des propriéts dynamiques : une taille, un nombre de points, etc.&lt;/p&gt;&lt;p&gt;Veuillez les ajouter ici.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../ComponentAddPropertyWidget.ui" line="652"/>
        <source>Add a Property</source>
        <translation>Ajouter une propriété</translation>
    </message>
    <message>
        <location filename="../../ComponentAddPropertyWidget.ui" line="685"/>
        <source>Previous</source>
        <translation>Précédent</translation>
    </message>
    <message>
        <location filename="../../ComponentAddPropertyWidget.ui" line="714"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../../ComponentAddPropertyWidget.ui" line="758"/>
        <source>Next</source>
        <translation>Suivant</translation>
    </message>
</context>
<context>
    <name>ComponentDescriptionWidget</name>
    <message>
        <location filename="../../ComponentDescriptionWidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../ComponentDescriptionWidget.ui" line="41"/>
        <source>Component-extension</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentDescriptionWidget.ui" line="66"/>
        <source>Component(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentDescriptionWidget.ui" line="90"/>
        <source>Dependencies</source>
        <translation>Dépendances</translation>
    </message>
    <message>
        <location filename="../../ComponentDescriptionWidget.ui" line="106"/>
        <location filename="../../ComponentDescriptionWidget.ui" line="379"/>
        <source>Summary</source>
        <translation>Sommaire</translation>
    </message>
    <message>
        <location filename="../../ComponentDescriptionWidget.ui" line="165"/>
        <location filename="../../ComponentDescriptionWidget.ui" line="454"/>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentDescriptionWidget.ui" line="212"/>
        <location filename="../../ComponentDescriptionWidget.ui" line="500"/>
        <source>2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentDescriptionWidget.ui" line="249"/>
        <location filename="../../ComponentDescriptionWidget.ui" line="523"/>
        <source>3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentDescriptionWidget.ui" line="272"/>
        <source>4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentDescriptionWidget.ui" line="339"/>
        <source>Description</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentDescriptionWidget.ui" line="363"/>
        <source>Property(ies)</source>
        <translation>Propriété(s)</translation>
    </message>
    <message>
        <location filename="../../ComponentDescriptionWidget.ui" line="599"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;How will you visualize your component in a 3D viewer ?&lt;/p&gt;&lt;p&gt;Will it be a 3D (or 2D) image or a mesh (and points), or maybe, you do not need to visualize this component in the 3D viewer (only in the Explorer window) and only its sub-component will have 3D represenation (in this case, choose None).&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Comment visualiserez vous votre component dans un viewer 3D ?&lt;/p&gt;&lt;p&gt;Sera t il une image 3D (ou 2D) ou un maillage (ou points), ou peut-être, vous n&apos;avez pas besoin de visualiser ce component dans le viewer 3D (seulement dans la fenêtre Explorer) et seulement son sub-component aura une représentation 3D (dans ce cas, choisir Aucun).&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../ComponentDescriptionWidget.ui" line="629"/>
        <source> Name of your component:</source>
        <translation> Nom de votre component:</translation>
    </message>
    <message>
        <location filename="../../ComponentDescriptionWidget.ui" line="653"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Give a precise description of what kind of data and/or file our component will handle.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Donner une description précise de la nature des données et/ou fichier que votre component manipulera.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../ComponentDescriptionWidget.ui" line="693"/>
        <source>3D Representation</source>
        <translation>Représentation 3D</translation>
    </message>
    <message>
        <location filename="../../ComponentDescriptionWidget.ui" line="728"/>
        <location filename="../../ComponentDescriptionWidget.ui" line="750"/>
        <location filename="../../ComponentDescriptionWidget.ui" line="788"/>
        <source>*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentDescriptionWidget.ui" line="804"/>
        <source> Description of your component:  </source>
        <translation> Description de votrer component: </translation>
    </message>
    <message>
        <location filename="../../ComponentDescriptionWidget.ui" line="829"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Give a meaningful name to your component (related to the data it handles).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Donner un nom descriptif à votre component (en lien avec les données manipulées).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../ComponentDescriptionWidget.ui" line="866"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If your component can be read and/or write from/to a file, please provide the corresponding file suffix.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Note: Do not provide any file suffix if your component is only a sub-component from another.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Si votre component peut être lu et/ou écrit à partird&apos;/dans un fichier, veuillez donner le suffixe de fichier adéquate.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Note: Ne donnez pas un suffixe si votre component est juste un sub-component d&apos;un autre.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../ComponentDescriptionWidget.ui" line="890"/>
        <source>Corresponding File Suffix</source>
        <translation>Suffixe Fichier Correspondant</translation>
    </message>
    <message>
        <location filename="../../ComponentDescriptionWidget.ui" line="899"/>
        <source>Image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentDescriptionWidget.ui" line="906"/>
        <source>Mesh</source>
        <translation>Maillage</translation>
    </message>
    <message>
        <location filename="../../ComponentDescriptionWidget.ui" line="913"/>
        <source>None</source>
        <translation>Aucun</translation>
    </message>
    <message>
        <location filename="../../ComponentDescriptionWidget.ui" line="1005"/>
        <source>.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentDescriptionWidget.ui" line="1018"/>
        <source>example</source>
        <translation>exemple</translation>
    </message>
    <message>
        <location filename="../../ComponentDescriptionWidget.ui" line="1070"/>
        <source>* &lt;i&gt;required field&lt;/i&gt;</source>
        <translation>* &lt;i&gt;champ obligatoire&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../../ComponentDescriptionWidget.ui" line="1099"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../../ComponentDescriptionWidget.ui" line="1143"/>
        <source>Next</source>
        <translation>Suivant</translation>
    </message>
</context>
<context>
    <name>ComponentExtensionDescriptionWidget</name>
    <message>
        <location filename="../../ComponentExtensionDescriptionWidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../ComponentExtensionDescriptionWidget.ui" line="41"/>
        <source>Component-extension</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentExtensionDescriptionWidget.ui" line="66"/>
        <source>Component(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentExtensionDescriptionWidget.ui" line="90"/>
        <source>Dependencies</source>
        <translation>Dépendances</translation>
    </message>
    <message>
        <location filename="../../ComponentExtensionDescriptionWidget.ui" line="106"/>
        <source>Summary</source>
        <translation>Sommaire</translation>
    </message>
    <message>
        <location filename="../../ComponentExtensionDescriptionWidget.ui" line="165"/>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentExtensionDescriptionWidget.ui" line="212"/>
        <source>2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentExtensionDescriptionWidget.ui" line="250"/>
        <source>3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentExtensionDescriptionWidget.ui" line="276"/>
        <source>4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentExtensionDescriptionWidget.ui" line="353"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Create your Component Extension&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Créer votre Component Extension&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../ComponentExtensionDescriptionWidget.ui" line="390"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Several of your components (with the same theme) can be grouped within a folder named &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Component Extension&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;.&lt;br/&gt;Please fill-in the information requested for an &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Component Extension&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; on this form.&lt;br/&gt;Components will be added in the next form. &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Plusieurs de vos components (avec le même théme) peuvent être groupés à l&apos;intérieur d&apos;un dossier nommé &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Component Extension&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;.&lt;br/&gt;Veuillez remplir l&apos;information demandé pour un &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Component Extension&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; sur ce formulaire.&lt;br/&gt;Les Components seront ajoutés dans le prochain formulaire. &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../ComponentExtensionDescriptionWidget.ui" line="426"/>
        <source>Description</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentExtensionDescriptionWidget.ui" line="448"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Give a meaningful name your component-extension. The name can corresponds to the overall data types covered by the extension.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Donner un nom descriptif pour votre component-extension. Le nom peut correspondre à la totalité des types de données couvertes par l&apos;extension .&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../ComponentExtensionDescriptionWidget.ui" line="527"/>
        <location filename="../../ComponentExtensionDescriptionWidget.ui" line="549"/>
        <source>*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentExtensionDescriptionWidget.ui" line="565"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../ComponentExtensionDescriptionWidget.ui" line="586"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Give a precise description of what your component-extension will do.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Donner une description précise de ce que votre component-extension fera.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../ComponentExtensionDescriptionWidget.ui" line="650"/>
        <source>* &lt;i&gt;required field&lt;/i&gt;</source>
        <translation>* &lt;i&gt;champ obligatoire&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../../ComponentExtensionDescriptionWidget.ui" line="679"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../../ComponentExtensionDescriptionWidget.ui" line="723"/>
        <source>Next</source>
        <translation>Suivant</translation>
    </message>
</context>
<context>
    <name>ComponentPropertyWidget</name>
    <message>
        <location filename="../../ComponentPropertyWidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../ComponentPropertyWidget.ui" line="29"/>
        <source>Property</source>
        <translation>Propriété</translation>
    </message>
    <message>
        <location filename="../../ComponentPropertyWidget.ui" line="51"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Please provide an &lt;span style=&quot; font-weight:600;&quot;&gt;explicite&lt;/span&gt; name to your parameter.&lt;/p&gt;&lt;p&gt;The name of a parameter is a string, so &lt;span style=&quot; font-style:italic;&quot;&gt;you can use space characters&lt;/span&gt; to give an explicit name to your parameter.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;If applicable, you should also indicate the &lt;span style=&quot; font-weight:600;&quot;&gt;unit&lt;/span&gt; of your parameter in its name.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Veuillez donner un &lt;span style=&quot; font-weight:600;&quot;&gt;&lt;/span&gt; nom pour votre paramétre&lt;/p&gt; explicite.&lt;p&gt;Le nom d&apos;un paramètre est une string, donc &lt;span style=&quot; font-style:italic;&quot;&gt;vous pouvez utilisez des caractères espace&lt;/span&gt; pour donner un nom explicite à votre paramétre.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;Si possible , vous devrez aussi indiquer l&apos; &lt;span style=&quot; font-weight:600;&quot;&gt;unité&lt;/span&gt; de votre paramètre dans son nom.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../ComponentPropertyWidget.ui" line="80"/>
        <source>Name </source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../ComponentPropertyWidget.ui" line="203"/>
        <source>*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentPropertyWidget.ui" line="222"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;By default, the type of parameters ar &lt;span style=&quot; font-style:italic;&quot;&gt;QVariant&lt;/span&gt;.&lt;/p&gt;&lt;p&gt;If you want to use other types for your parameter, provide a default one and then modify its type manually in the code, after having carefully read the Qt documentation on QVariant here:&lt;/p&gt;&lt;p&gt;http://qt-project.org/doc/qt-5.0/qtcore/qvariant.html&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;By default, the type of parameters ar &lt;span style=&quot; font-style:italic;&quot;&gt;QVariant&lt;/span&gt;.&lt;/p&gt;&lt;p&gt;If you want to use other types for your parameter, provide a default one and then modify its type manually in the code, after having carefully read the Qt documentation on QVariant here:&lt;/p&gt;&lt;p&gt;http://qt-project.org/doc/qt-5.0/qtcore/qvariant.html&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../ComponentPropertyWidget.ui" line="245"/>
        <source>Type</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentPropertyWidget.ui" line="297"/>
        <source>int</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentPropertyWidget.ui" line="302"/>
        <source>bool</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentPropertyWidget.ui" line="307"/>
        <source>double</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentPropertyWidget.ui" line="312"/>
        <source>QString</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentPropertyWidget.ui" line="317"/>
        <source>QDate</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentPropertyWidget.ui" line="322"/>
        <source>QTime</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentPropertyWidget.ui" line="327"/>
        <source>QColor</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentPropertyWidget.ui" line="332"/>
        <source>QPoint</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentPropertyWidget.ui" line="337"/>
        <source>QPointF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentPropertyWidget.ui" line="342"/>
        <source>QVector3D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentPropertyWidget.ui" line="362"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Generally, we give the most common used value for a parameter as default value.&lt;/p&gt;&lt;p&gt;The user will then be able to modify it before applying the action.&lt;/p&gt;&lt;p&gt;As parameter types are QVariant, the default value may explicitly express QVariant(...).&lt;/p&gt;&lt;p/&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;En général, nous donnons comme valeur par défaut, la vaeur la plus fréquement utilisée.&lt;/p&gt;&lt;p&gt;L&apos;utilisateur sera capable de le modifier avant d&apos;appliquer l&apos;action.&lt;/p&gt;&lt;p&gt; Si les types de parametres sont QVariant, la valeur par défaut devra clairement être exprimé en QVariant(...).&lt;/p&gt;&lt;p/&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../ComponentPropertyWidget.ui" line="385"/>
        <source>Default Value</source>
        <translation>Valeur par Défaut</translation>
    </message>
    <message>
        <location filename="../../ComponentPropertyWidget.ui" line="519"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The description of your parameter will not be displaied directly with your action but will be added as a tool tip on your parameter.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;La description de votre paramètre ne sera pas affiché automatiquement avec votre action mais sera ajouté coome un commentaire surgissant sur votre paramètre.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../ComponentPropertyWidget.ui" line="547"/>
        <source>Description</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentPropertyWidget.ui" line="579"/>
        <source>Write a description about your property</source>
        <translation>Donner une description de votre propriété</translation>
    </message>
    <message>
        <location filename="../../ComponentPropertyWidget.ui" line="627"/>
        <source>Unit</source>
        <translation>Unité</translation>
    </message>
    <message>
        <location filename="../../ComponentPropertyWidget.ui" line="678"/>
        <source>required field *</source>
        <translation>champ obligatoire *</translation>
    </message>
    <message>
        <location filename="../../ComponentPropertyWidget.ui" line="710"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Remove&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Retirer&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>ComponentSummaryWidget</name>
    <message>
        <location filename="../../ComponentSummaryWidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../ComponentSummaryWidget.ui" line="41"/>
        <source>Component-extension</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentSummaryWidget.ui" line="66"/>
        <source>Component(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentSummaryWidget.ui" line="90"/>
        <source>Dependencies</source>
        <translation>Dépendances</translation>
    </message>
    <message>
        <location filename="../../ComponentSummaryWidget.ui" line="108"/>
        <location filename="../../ComponentSummaryWidget.ui" line="348"/>
        <source>Summary</source>
        <translation>Sommaire</translation>
    </message>
    <message>
        <location filename="../../ComponentSummaryWidget.ui" line="167"/>
        <location filename="../../ComponentSummaryWidget.ui" line="400"/>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentSummaryWidget.ui" line="196"/>
        <location filename="../../ComponentSummaryWidget.ui" line="423"/>
        <source>2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentSummaryWidget.ui" line="233"/>
        <source>3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentSummaryWidget.ui" line="258"/>
        <location filename="../../ComponentSummaryWidget.ui" line="448"/>
        <source>4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentSummaryWidget.ui" line="314"/>
        <source>Description</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentSummaryWidget.ui" line="330"/>
        <source>Property(ies)</source>
        <translation>Propriété(s)</translation>
    </message>
    <message>
        <location filename="../../ComponentSummaryWidget.ui" line="505"/>
        <source>Previous</source>
        <translation>Précédent</translation>
    </message>
    <message>
        <location filename="../../ComponentSummaryWidget.ui" line="534"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../../ComponentSummaryWidget.ui" line="578"/>
        <source>Next</source>
        <translation>Suivant</translation>
    </message>
</context>
<context>
    <name>ComponentsCreationWidget</name>
    <message>
        <location filename="../../ComponentsCreationWidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../ComponentsCreationWidget.ui" line="41"/>
        <source>Component-extension</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentsCreationWidget.ui" line="66"/>
        <source>Component(s)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentsCreationWidget.ui" line="90"/>
        <source>Dependencies</source>
        <translation>Dépendances</translation>
    </message>
    <message>
        <location filename="../../ComponentsCreationWidget.ui" line="106"/>
        <source>Summary</source>
        <translation>Sommaire</translation>
    </message>
    <message>
        <location filename="../../ComponentsCreationWidget.ui" line="165"/>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentsCreationWidget.ui" line="212"/>
        <source>2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentsCreationWidget.ui" line="249"/>
        <source>3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentsCreationWidget.ui" line="272"/>
        <source>4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentsCreationWidget.ui" line="362"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Create the Components&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Créer les Components&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../ComponentsCreationWidget.ui" line="384"/>
        <source>Components</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentsCreationWidget.ui" line="417"/>
        <source>Add a Component</source>
        <translation>Ajouter un Component</translation>
    </message>
    <message>
        <location filename="../../ComponentsCreationWidget.ui" line="435"/>
        <source>*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ComponentsCreationWidget.ui" line="475"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;* Please add at least a component in your extension&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;*Veuillez ajouter au moins un component dans votre extension&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../ComponentsCreationWidget.ui" line="504"/>
        <source>Previous</source>
        <translation>Précédent</translation>
    </message>
    <message>
        <location filename="../../ComponentsCreationWidget.ui" line="533"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../../ComponentsCreationWidget.ui" line="577"/>
        <source>Next</source>
        <translation>Suivant</translation>
    </message>
    <message>
        <location filename="../../ComponentsCreationWidget.cpp" line="54"/>
        <source>Components Creation Widget</source>
        <translation>Widget Création des Components</translation>
    </message>
    <message>
        <location filename="../../ComponentsCreationWidget.cpp" line="54"/>
        <source>Please create at least one Component in this ComponentExtension
</source>
        <translation>Veuillez créer au moins un Component dans cette ComponentExtension</translation>
    </message>
</context>
<context>
    <name>DependenciesWidget</name>
    <message>
        <location filename="../../DependenciesWidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../DependenciesWidget.ui" line="41"/>
        <source>@ELEMENT@</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../DependenciesWidget.ui" line="66"/>
        <source>@ELEMENTDETAILS@</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../DependenciesWidget.ui" line="90"/>
        <source>Dependencies</source>
        <translation>Dépendances</translation>
    </message>
    <message>
        <location filename="../../DependenciesWidget.ui" line="106"/>
        <source>Summary</source>
        <translation>Sommaire</translation>
    </message>
    <message>
        <location filename="../../DependenciesWidget.ui" line="165"/>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../DependenciesWidget.ui" line="212"/>
        <source>2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../DependenciesWidget.ui" line="249"/>
        <source>3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../DependenciesWidget.ui" line="272"/>
        <source>4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../DependenciesWidget.ui" line="309"/>
        <source>Internal CEP Dependencies</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../DependenciesWidget.ui" line="345"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Do your @ELEMENT@ depend on one of the following libraries ?&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;est ce que votre @ELEMENT@ dépend d&apos;une des librairies suivantes ?&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../DependenciesWidget.ui" line="361"/>
        <source>Internal CEP Libraries</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../DependenciesWidget.ui" line="400"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Does your @ELEMENT@ depend on one of the following Component(s) ?&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Est ce que votre @ELEMENT@ dépend d&apos;un des Component(s) suivant(s) ?&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../DependenciesWidget.ui" line="416"/>
        <source>Internal CEP Components</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../DependenciesWidget.ui" line="455"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Do your @ELEMENT@ depend on one or several of the following Actions ?&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Est ce que votre @ELEMENT@ dépend d&apos;une ou plusieurs Actions suivantes ?&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../DependenciesWidget.ui" line="471"/>
        <source>Internal CEP Actions</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../DependenciesWidget.ui" line="499"/>
        <source>CamiTK  Dependencies</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../DependenciesWidget.ui" line="519"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If your @ELEMENT@ depends on a CamiTK Library (e.g. cepcoreschema, pml, lml, mml, etc.), please indicate their name(s) separated by spaces.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Si votre @ELEMENT@ dépend d&apos;une librairie CamiTK (e.g. cepcoreschema, pml, lml, mml, etc.), veuillez indiquer leur(s) nom(s) séparé(s) par des espaces.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../DependenciesWidget.ui" line="535"/>
        <source>CamiTK Libraries</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../DependenciesWidget.ui" line="561"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If your @ELEMENT@ depends on a CamiTK Component (e.g. physicalmodel etc.), please indicate their name(s) separated by spaces.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Si votre @ELEMENT@ dépend d&apos;un Component CamiTK (e.g. physicalmodel etc.), veuillez indiquer leur(s) nom(s) séparé(s) par des espaces.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../DependenciesWidget.ui" line="577"/>
        <source>CamiTK Components</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../DependenciesWidget.ui" line="603"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If your @ELEMENT@ depends on a CamiTK Action (e.g. multipicking, etc.), please indicate their name(s) separated by spaces.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Si votre @ELEMENT@ dépend sur une Action CamiTK (e.g. multipicking, etc.), veuillez indiquer leur(s) nom(s) séparé(s) par des espaces.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../DependenciesWidget.ui" line="619"/>
        <source>CamiTK Actions</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../DependenciesWidget.ui" line="634"/>
        <source>External Libraries Dependencies</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../DependenciesWidget.ui" line="663"/>
        <source>XSD CXX</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../DependenciesWidget.ui" line="716"/>
        <source>LibXML2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../DependenciesWidget.ui" line="723"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;add this if your library depends on Qt modules, such as QtCore, QtGui, QtMultimedia, QtNetwork, QtOpenGL, QtScript, QtScriptTools, QtSql, QtSvg, QtWebkit, QtXml, QtXmlPatterns, QtDeclarative&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ajouter cela si votre librairie dépend d&apos;un module Qt, comme as QtCore, QtGui, QtMultimedia, QtNetwork, QtOpenGL, QtScript, QtScriptTools, QtSql, QtSvg, QtWebkit, QtXml, QtXmlPatterns, QtDeclarative&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../DependenciesWidget.ui" line="726"/>
        <source>Qt Modules</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../DependenciesWidget.ui" line="768"/>
        <source>Previous</source>
        <translation>Précedent</translation>
    </message>
    <message>
        <location filename="../../DependenciesWidget.ui" line="792"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../../DependenciesWidget.ui" line="831"/>
        <source>Next</source>
        <translation>Suivant</translation>
    </message>
</context>
<context>
    <name>ExtensionSummaryWidget</name>
    <message>
        <location filename="../../ExtensionSummaryWidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../ExtensionSummaryWidget.ui" line="41"/>
        <source>@ELEMENT@</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ExtensionSummaryWidget.ui" line="66"/>
        <source>@ELEMENTDETAILS@</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ExtensionSummaryWidget.ui" line="90"/>
        <source>Dependencies</source>
        <translation>Dépendances</translation>
    </message>
    <message>
        <location filename="../../ExtensionSummaryWidget.ui" line="108"/>
        <source>Summary</source>
        <translation>Sommaire</translation>
    </message>
    <message>
        <location filename="../../ExtensionSummaryWidget.ui" line="167"/>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ExtensionSummaryWidget.ui" line="214"/>
        <source>2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ExtensionSummaryWidget.ui" line="251"/>
        <source>3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ExtensionSummaryWidget.ui" line="276"/>
        <source>4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ExtensionSummaryWidget.ui" line="333"/>
        <source>Previous</source>
        <translation>Précédent</translation>
    </message>
    <message>
        <location filename="../../ExtensionSummaryWidget.ui" line="362"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../../ExtensionSummaryWidget.ui" line="406"/>
        <source>Next</source>
        <translation>Suivant</translation>
    </message>
</context>
<context>
    <name>GeneratingCEPWidget</name>
    <message>
        <location filename="../../GeneratingCEPWidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../GeneratingCEPWidget.ui" line="27"/>
        <source>Generating the following CEP:</source>
        <translation>Génération du CEP suivant:</translation>
    </message>
    <message>
        <location filename="../../GeneratingCEPWidget.ui" line="87"/>
        <source>Ok</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LibraryCopyFilesWidget</name>
    <message>
        <location filename="../../LibraryCopyFilesWidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../LibraryCopyFilesWidget.ui" line="38"/>
        <source>Internal Library</source>
        <translation>Librairie Interne</translation>
    </message>
    <message>
        <location filename="../../LibraryCopyFilesWidget.ui" line="63"/>
        <source>Copy File(s)</source>
        <translation>Copier Fichier(s)</translation>
    </message>
    <message>
        <location filename="../../LibraryCopyFilesWidget.ui" line="87"/>
        <source>Dependencies</source>
        <translation>Dépendances</translation>
    </message>
    <message>
        <location filename="../../LibraryCopyFilesWidget.ui" line="146"/>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../LibraryCopyFilesWidget.ui" line="193"/>
        <source>2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../LibraryCopyFilesWidget.ui" line="230"/>
        <source>3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../LibraryCopyFilesWidget.ui" line="271"/>
        <source>Library Files</source>
        <translation>Fichier Librairie</translation>
    </message>
    <message>
        <location filename="../../LibraryCopyFilesWidget.ui" line="317"/>
        <source>Add a File</source>
        <translation>Ajouter un Fichier</translation>
    </message>
    <message>
        <location filename="../../LibraryCopyFilesWidget.ui" line="340"/>
        <source>Previous</source>
        <translation>Précédent</translation>
    </message>
    <message>
        <location filename="../../LibraryCopyFilesWidget.ui" line="369"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../../LibraryCopyFilesWidget.ui" line="413"/>
        <source>Next</source>
        <translation>Suivant</translation>
    </message>
    <message>
        <location filename="../../LibraryCopyFilesWidget.cpp" line="73"/>
        <source>Select one or more files</source>
        <translation>Sélectionner un ou plusieurs fichiers</translation>
    </message>
</context>
<context>
    <name>LibraryDescriptionWidget</name>
    <message>
        <location filename="../../LibraryDescriptionWidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../LibraryDescriptionWidget.ui" line="41"/>
        <source>Internal Library</source>
        <translation>Librairie Interne</translation>
    </message>
    <message>
        <location filename="../../LibraryDescriptionWidget.ui" line="66"/>
        <source>Copy File(s)</source>
        <translation>Copier Fichier(s)</translation>
    </message>
    <message>
        <location filename="../../LibraryDescriptionWidget.ui" line="90"/>
        <source>Dependencies</source>
        <translation>Dépendances</translation>
    </message>
    <message>
        <location filename="../../LibraryDescriptionWidget.ui" line="149"/>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../LibraryDescriptionWidget.ui" line="196"/>
        <source>2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../LibraryDescriptionWidget.ui" line="233"/>
        <source>3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../LibraryDescriptionWidget.ui" line="308"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;Create your Library&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;Créer votre librairie&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../LibraryDescriptionWidget.ui" line="374"/>
        <source>Description</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../LibraryDescriptionWidget.ui" line="396"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Give a meaningful name your library. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Donner un nom descriptif pour votre librairie. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../LibraryDescriptionWidget.ui" line="419"/>
        <source>Static</source>
        <translation>Statique</translation>
    </message>
    <message>
        <location filename="../../LibraryDescriptionWidget.ui" line="429"/>
        <source>Dynamic</source>
        <translation>Dynamique</translation>
    </message>
    <message>
        <location filename="../../LibraryDescriptionWidget.ui" line="497"/>
        <location filename="../../LibraryDescriptionWidget.ui" line="519"/>
        <source>*</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../LibraryDescriptionWidget.ui" line="535"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../LibraryDescriptionWidget.ui" line="556"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Give a precise description of what your library can do.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Donner une description précise de ce que peut faire votre librairie.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../LibraryDescriptionWidget.ui" line="595"/>
        <source>Type</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../LibraryDescriptionWidget.ui" line="614"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Please choose whether your compiled library will be static or dynamic.&lt;/p&gt;&lt;p&gt;If you do not know yet, leave static.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Veuillez choisir si votre librairie compilée sera statique ou dynamique.&lt;/p&gt;&lt;p&gt;Si vous ne le savez pas encore, laisser statique.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../LibraryDescriptionWidget.ui" line="660"/>
        <source>* &lt;i&gt;required field&lt;/i&gt;</source>
        <translation>* &lt;i&gt;champ obligatoire&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../../LibraryDescriptionWidget.ui" line="689"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../../LibraryDescriptionWidget.ui" line="733"/>
        <source>Next</source>
        <translation>Suivant</translation>
    </message>
</context>
<context>
    <name>WelcomeWidget</name>
    <message>
        <location filename="../../WelcomeWidget.ui" line="26"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../WelcomeWidget.ui" line="72"/>
        <source>CamiTK Extension Wizard</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../WelcomeWidget.ui" line="93"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;This application will help you to dynamically create a CamiTK Extension Project.&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Before you begin, you should be familiar with CamiTK Extension Project, and with the notions of Components and Actions.&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;If not, please read more on http://camitk.imag.fr&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Cette application vous aidera à créer dynamiquement à CamiTK Extension Project.&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Avant de commencer, vous devrez être familiarisé avec CamiTK Extension Project, et avec les notions de Components et Actions.&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;justify&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Dans le cas inverse, veuillez en lire plus sur http://camitk.imag.fr&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../WelcomeWidget.ui" line="139"/>
        <source>Start</source>
        <translation>Démarrer</translation>
    </message>
    <message>
        <location filename="../../WelcomeWidget.ui" line="168"/>
        <source>* If you want to work from an existing XML file, the application you need is &lt;tt&gt;camitk-cepgenerator&lt;/tt&gt;, and not this one...</source>
        <translation>* Si vous voulez travailler à partir d&apos;un fichier XML existant, l&apos;application dont vous avez besoin est &lt;tt&gt;camitk-cepgenerator&lt;/tt&gt;, et pas celle-ci...</translation>
    </message>
</context>
<context>
    <name>WizardMainWindow</name>
    <message>
        <location filename="../../../../src/sdk/applications/wizard/WizardMainWindow.ui" line="26"/>
        <source>CamiTK-Wizard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../src/sdk/applications/wizard/WizardMainWindow.ui" line="152"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../../../Dev/CamiTK/camitk/sdk/applications/wizard/WizardMainWindow.ui" line="152"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RemovableLineEditWidget</name>
    <message>
        <location filename="../../RemovableLineEditWidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
</context>
<context>
    <name>DorothyMainWindow</name>
    <message>
        <location filename="../../DorothyMainWindow.ui" line="26"/>
        <source>CamiTK-Wizard</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../DorothyMainWindow.ui" line="152"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
</context>
</TS>

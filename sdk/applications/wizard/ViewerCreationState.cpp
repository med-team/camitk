/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ViewerCreationState.h"
#include "WizardMainWindow.h"
#include "ViewerExtensionCreationState.h"

#include "ViewerDescriptionWidget.h"
#include "ViewerDescriptionState.h"
#include "ViewerSummaryWidget.h"
#include "ViewerSummaryState.h"

#include <ViewerExtension.hxx>
#include <Viewer.hxx>
#include <ViewerType.hxx>
#include <Classification.hxx>

ViewerCreationState::ViewerCreationState(QString name, WizardMainWindow* mainWindow, cepcoreschema::Cep* domCep, ViewerExtensionCreationState* parent) : QState(parent) {
    this->name = name;
    this->cancelled = false;
    this->domViewer = nullptr;

    this->domViewerExtension = nullptr;
    this->domCep = domCep;

    createSubStates(mainWindow);
}

void ViewerCreationState::resetDomViewerExtension(cepcoreschema::ViewerExtension* domViewerExtension) {
    this->domViewerExtension = domViewerExtension;
}

void ViewerCreationState::onEntry(QEvent* event) {
    this->cancelled = false;
    cepcoreschema::ViewerType type(cepcoreschema::ViewerType::EMBEDDED);

    if (domViewer != nullptr) {
        delete domViewer;
        domViewer = nullptr;
    }

    domViewer = new cepcoreschema::Viewer("A Viewer",  "A viewer description", type);
    domViewer->component().push_back("Component");
    viewerDescriptionState->resetDomViewer(domViewer, domViewerExtension);
    viewerSummaryState->resetViewer(domViewer);
}

void ViewerCreationState::onExit(QEvent* event) {
    if (! cancelled) {
        domViewerExtension->viewer();
    }
    else {
        if (domViewer != nullptr) {
            delete domViewer;
            domViewer = nullptr;
        }
    }
}

void ViewerCreationState::viewerFinished() {
    cancelled = false;
    emit nextVCS2();
}

void ViewerCreationState::viewerCancelled() {
    cancelled = true;
    emit nextVCS();
}

void ViewerCreationState::createSubStates(WizardMainWindow* mainWindow) {
    viewerDescriptionWidget = new ViewerDescriptionWidget(nullptr);
    viewerDescriptionState = new ViewerDescriptionState("Viewer Description", viewerDescriptionWidget, mainWindow, domCep, this);

    viewerSummaryWidget = new ViewerSummaryWidget(nullptr);
    viewerSummaryState  = new ViewerSummaryState("Viewer Summary", viewerSummaryWidget, mainWindow, this);

    this->setInitialState(viewerDescriptionState);

    viewerDescriptionState->addTransition(viewerDescriptionWidget, SIGNAL(next()), viewerSummaryState);

    viewerSummaryState->addTransition(viewerSummaryWidget, SIGNAL(previous()), viewerDescriptionState);

    QObject::connect(viewerDescriptionWidget, SIGNAL(cancel()), this, SLOT(viewerCancelled()));
    QObject::connect(viewerSummaryWidget, SIGNAL(cancel()), this, SLOT(viewerCancelled()));
    QObject::connect(viewerSummaryWidget, SIGNAL(next()), this, SLOT(viewerFinished()));

}

/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ComponentExtensionCreationState.h"
#include "WizardMainWindow.h"

#include "ComponentExtensionDescriptionWidget.h"
#include "ComponentExtensionDescriptionState.h"
#include "ComponentsCreationWidget.h"
#include "ComponentsCreationState.h"
#include "ComponentCreationState.h"
#include "ComponentExtensionDependenciesState.h"
#include "DependenciesWidget.h"
#include "ExtensionSummaryWidget.h"
#include "ExtensionSummaryState.h"

#include <Cep.hxx>
#include <Components.hxx>
#include <ComponentExtension.hxx>

ComponentExtensionCreationState::ComponentExtensionCreationState(QString name, WizardMainWindow* mainWindow, cepcoreschema::Cep* domCep) : QState() {
    this->cancelled = false;
    this->name = name;
    this->domCep = domCep;
    this->domComponentExtension = nullptr;

    createSubStates(mainWindow);
}

void ComponentExtensionCreationState::onEntry(QEvent* event) {
    cancelled = false;
    if (domComponentExtension != nullptr) {
        delete domComponentExtension;
        domComponentExtension = nullptr;
    }

    cepcoreschema::Components theComponents;
    domComponentExtension = new cepcoreschema::ComponentExtension("A Component Extension", "A component extension description", theComponents);

    componentExtensionDescriptionState->resetDomComponentExtension(domComponentExtension);
    componentsCreationState->resetDomComponentExtension(domComponentExtension);
    componentCreationState->resetDomComponentExtension(domComponentExtension);
    componentExtensionDependenciesState->resetDomComponentExtension(domComponentExtension);
    componentExtensionSummaryState->setComponentExtension(domComponentExtension);
}


void ComponentExtensionCreationState::onExit(QEvent* event) {
    if (! cancelled) {
        cepcoreschema::Components someComponents;
        if (domCep->componentExtensions().present()) {
            domCep->componentExtensions().get().componentExtension().push_back((*domComponentExtension));
        }
        else {
            cepcoreschema::ComponentExtensions theExtensions;
            theExtensions.componentExtension().push_back((*domComponentExtension));
            domCep->componentExtensions(theExtensions);
        }
    }
    else {
        if (domComponentExtension != nullptr) {
            delete domComponentExtension;
            domComponentExtension = nullptr;
        }

    }

}


void ComponentExtensionCreationState::extensionFinished() {
    cancelled = false;
    emit next();
}

void ComponentExtensionCreationState::extensionCancelled() {
    cancelled = true;
    emit next();
}


void ComponentExtensionCreationState::createSubStates(WizardMainWindow* mainWindow) {
    componentExtensionDescriptionWidget = new ComponentExtensionDescriptionWidget(nullptr);
    componentExtensionDescriptionState  = new ComponentExtensionDescriptionState("Component Extension",  componentExtensionDescriptionWidget, mainWindow, this);

    componentsCreationWidget = new ComponentsCreationWidget(nullptr);
    componentsCreationState  = new ComponentsCreationState("Components Creation",  componentsCreationWidget, mainWindow, this);

    componentCreationState = new ComponentCreationState("Component Creation", mainWindow, this);

    componentExtensionDependenciesWidget = new DependenciesWidget(nullptr);
    componentExtensionDependenciesState = new ComponentExtensionDependenciesState("Component Extension Dependencies",  componentExtensionDependenciesWidget, mainWindow, domCep, this);

    componentExtensionSummaryWidget = new ExtensionSummaryWidget(nullptr);
    componentExtensionSummaryState  = new ExtensionSummaryState("Component Extension Summary", componentExtensionSummaryWidget, "Component", mainWindow, this);


    componentExtensionDescriptionState->addTransition(componentExtensionDescriptionWidget, SIGNAL(next()), componentsCreationState);

    componentsCreationState->addTransition(componentsCreationWidget, SIGNAL(newComponent()), componentCreationState);
    componentsCreationState->addTransition(componentsCreationWidget, SIGNAL(next()), componentExtensionDependenciesState);
    componentsCreationState->addTransition(componentsCreationWidget, SIGNAL(previous()), componentExtensionDescriptionState);

    componentCreationState->addTransition(componentCreationState, SIGNAL(nextCCS()), componentsCreationState);

    componentExtensionDependenciesState->addTransition(componentExtensionDependenciesWidget, SIGNAL(previous()), componentsCreationState);
    componentExtensionDependenciesState->addTransition(componentExtensionDependenciesWidget, SIGNAL(next()), componentExtensionSummaryState);

    componentExtensionSummaryState->addTransition(componentExtensionSummaryWidget, SIGNAL(previous()), componentExtensionDependenciesState);
    QObject::connect(componentExtensionSummaryWidget, SIGNAL(next()), this, SLOT(extensionFinished()));

    // Cancel
    QObject::connect(componentExtensionDescriptionWidget, SIGNAL(cancel()), this, SLOT(extensionCancelled()));
    QObject::connect(componentsCreationWidget, SIGNAL(cancel()), this, SLOT(extensionCancelled()));
    QObject::connect(componentExtensionDependenciesWidget, SIGNAL(cancel()), this, SLOT(extensionCancelled()));
    QObject::connect(componentExtensionSummaryWidget, SIGNAL(cancel()), this, SLOT(extensionCancelled()));

    this->setInitialState(componentExtensionDescriptionState);
}

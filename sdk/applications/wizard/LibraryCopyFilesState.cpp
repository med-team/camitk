/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "LibraryCopyFilesState.h"
#include "LibraryCopyFilesWidget.h"
#include "WizardMainWindow.h"
#include "LibraryCreationState.h"

// includes from coreschema
#include <Library.hxx>

LibraryCopyFilesState::LibraryCopyFilesState(QString name, LibraryCopyFilesWidget* widget, WizardMainWindow* mainWindow, QMap<QString, QStringList>* libraryFilesMap, LibraryCreationState* parent)
    : WizardState(name, widget, mainWindow, parent) {
    this->domLibrary = nullptr;
    this->libraryFilesMap = libraryFilesMap;
}

void LibraryCopyFilesState::resetDomLibrary(cepcoreschema::Library* domLibrary) {
    this->domLibrary = domLibrary;
    auto* libraryCopyFilesWidget = dynamic_cast<LibraryCopyFilesWidget*>(widget);
    if (libraryCopyFilesWidget != nullptr) {
        libraryCopyFilesWidget->emptyFileNames();
    }
}

void LibraryCopyFilesState::onEntry(QEvent* event) {
    WizardState::onEntry(event);

    auto* libraryCopyFilesWidget = dynamic_cast<LibraryCopyFilesWidget*>(widget);
    if (libraryCopyFilesWidget != nullptr) {
        libraryCopyFilesWidget->emptyFileNames();
        if (domLibrary != nullptr) {
            QString libraryName = domLibrary->name().c_str();
            QStringList files = libraryFilesMap->value(libraryName);
            foreach (QString file, files) {
                libraryCopyFilesWidget->addFile(file);
            }
        }
    }
}

void LibraryCopyFilesState::onExit(QEvent* event) {
    WizardState::onExit(event);
    auto* libraryCopyFilesWidget = dynamic_cast<LibraryCopyFilesWidget*>(widget);
    if (libraryCopyFilesWidget != nullptr) {
        QStringList filesList = libraryCopyFilesWidget->getFileNames();
        QString libraryName = QString(domLibrary->name().c_str());
        this->libraryFilesMap->insert(libraryName, filesList);

    }
}

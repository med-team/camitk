/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ActionParameterWidget.h"

// Local files
#include "ActionAddParameterWidget.h"
#include "DefaultGUIText.h"

// CepGenerator files
#include <ParameterGenerator.h>

ActionParameterWidget::ActionParameterWidget(ActionAddParameterWidget* parent) : QWidget(parent) {
    ui.setupUi(this);
    this->parent = parent;
    typeChanged(ui.typeComboBox->currentText());
}

void ActionParameterWidget::remove() {
    parent->removeParameter(this);

}

void ActionParameterWidget::nameChanged(QString name) {
    ui.groupBox->setTitle(name);
}

void ActionParameterWidget::typeChanged(QString typeName) {
    QString defValue;
    defValue = ParameterGenerator::getTypeDefaultValue(typeName);
    ui.defaultLineEdit->setText(defValue);
}

QString ActionParameterWidget::getName() {
    return ui.nameLineEdit->text();
}

QString ActionParameterWidget::getType() {
    return ui.typeComboBox->currentText();
}

QString ActionParameterWidget::getDefaultValue() {
    return ui.defaultLineEdit->text();
}

QString ActionParameterWidget::getDescription() {
    return ui.descriptionPlainTextEdit->toPlainText();
}

void ActionParameterWidget::setRequiredName() {
    ui.namLabelStar->setStyleSheet(enhancedStyle);
    ui.requiredFieldLabel->setStyleSheet(enhancedStyle);
}

void ActionParameterWidget::unsetRequiredName() {
    ui.namLabelStar->setStyleSheet(normalStyle);
    ui.requiredFieldLabel->setStyleSheet(normalStyle);
}

QString ActionParameterWidget::getUnit() {
    return ui.unitPlainTextEdit->toPlainText();
}


/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef CEPCREATIONDIRECTORYSTATE_H
#define CEPCREATIONDIRECTORYSTATE_H

#include "WizardState.h"

class CepCreationDirectoryWidget;

/**
 * @ingroup group_sdk_application_wizard
 *
 * @brief
 * State to define the directory of the CEP.
 *
 */
class  CepCreationDirectoryState : public WizardState  {

    Q_OBJECT;

public:
    /**  Constructor */
    CepCreationDirectoryState(QString name, CepCreationDirectoryWidget* widget, WizardMainWindow* mainWidnow, QString* devDirName);
    /**  Destructor */
    ~CepCreationDirectoryState() override = default;

protected:
    void onExit(QEvent* event) override;

private:
    QString* devDirName;
};
#endif

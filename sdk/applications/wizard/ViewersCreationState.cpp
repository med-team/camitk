/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ViewersCreationState.h"
#include "ViewersCreationWidget.h"
#include "WizardMainWindow.h"
#include "ViewerExtensionCreationState.h"

// includes from coreschema
#include <ViewerExtension.hxx>
#include <Viewer.hxx>

#include <iostream>

ViewersCreationState::ViewersCreationState(QString name, ViewersCreationWidget* widget, WizardMainWindow* mainWindow, ViewerExtensionCreationState* parent)
    : WizardState(name, widget, mainWindow, parent) {
    this->domViewerExtension = nullptr;
}

void ViewersCreationState::resetDomViewerExtension(cepcoreschema::ViewerExtension* domViewerExtension) {
    this->domViewerExtension = domViewerExtension;
    auto* viewersCreationWidget = dynamic_cast<ViewersCreationWidget*>(widget);
    domViewerExtension->registerDefaultViewer();
    viewersCreationWidget->resetDomViewerExtension(domViewerExtension);
    if (viewersCreationWidget != nullptr) {
        viewersCreationWidget->setToDefault();
    }
}

void ViewersCreationState::onEntry(QEvent* event) {
    WizardState::onEntry(event);
}

void ViewersCreationState::onExit(QEvent* event) {
    WizardState::onExit(event);
}



/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "CepContactWidget.h"

#include "DefaultGUIText.h"

// Qt files
#include <QMessageBox>

#include <iostream>

CepContactWidget::CepContactWidget(QWidget* parent) : QWidget(parent) {
    ui.setupUi(this);
    ui.cepContactLineEdit->setText(defaultContact);
    ui.userDefTextEdit->setHtml(defaultUserLicence);

    ui.userDefTextEdit->setVisible(false);
}

void CepContactWidget::nextButtonClicked() {
    QString contact = ui.cepContactLineEdit->text();
#ifndef _WIZARD_QUESTIONS_SQUEEZE
    if (contact.isEmpty() || contact == defaultContact || !contact.contains("@")) {
        ui.requiredLabel->setStyleSheet(enhancedStyle);
        ui.cepContactStar->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultRealContact);
    }
    else if (contact.toUtf8() != contact.toLatin1()) {
        ui.requiredLabel->setStyleSheet(enhancedStyle);
        ui.cepContactStar->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultAscii);
    }
    else {
        ui.requiredLabel->setStyleSheet(normalStyle);
        ui.cepContactStar->setStyleSheet(normalStyle);
        emit next();
    }
#else
    emit next();
#endif
}

void CepContactWidget::previousButtonClicked() {
    ui.requiredLabel->setStyleSheet(normalStyle);
    ui.cepContactStar->setStyleSheet(normalStyle);
    emit previous();
}

void CepContactWidget::setCepName(QString cepName) {
    QString userLicence = defaultUserLicence;
    userLicence = userLicence.replace(QRegExp("@CEP_NAME@"), cepName);

    ui.userDefTextEdit->setHtml(userLicence);
}

QString CepContactWidget::getMail() {
    QString res = ui.cepContactLineEdit->text();
    return res;
}

bool CepContactWidget::hasUserLicence() {
    return (! ui.lgplV3RadioButton->isChecked());
}

QString CepContactWidget::getUserLicence() {
    QString userLicence = ui.userDefTextEdit->toPlainText();
    std::cout << "licence: \n" << userLicence.toStdString() << std::endl;
    return userLicence;
}

/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef ACTIONDESCRIPTIONSTATE_H
#define ACTIONDESCRIPTIONSTATE_H

#include "WizardState.h"

class ActionDescriptionWidget;
class ActionCreationState;

// Dependency from cepcoreschema
// Declaration here to avoid declaration in dependant projects.
namespace cepcoreschema {
class Action;
class ActionExtension;
class Cep;
}

/**
 * @ingroup group_sdk_application_wizard
 *
 * @brief
 * State to describe action
 *
 */
class ActionDescriptionState : public WizardState  {

    Q_OBJECT;

public:
    /**  Constructor */
    ActionDescriptionState(QString name, ActionDescriptionWidget* widget, WizardMainWindow* mainWindow, cepcoreschema::Cep* domCep, ActionCreationState* parent);

    /**  Destructor */
    ~ActionDescriptionState() override = default;

    void resetDomAction(cepcoreschema::Action* domAction, cepcoreschema::ActionExtension* domActionExtension);

protected:
    void onExit(QEvent* event) override;
    void onEntry(QEvent* event) override;

private:
    // Dom element that will be completed with the name
    cepcoreschema::Action* domAction;
    // Just to check that the name of the Action is not the same of the Action Extension
    cepcoreschema::ActionExtension* domActionExtension;
    // To know which Components are available within the CEP (ot possibly apply the Action on).
    cepcoreschema::Cep* domCep;

};
#endif

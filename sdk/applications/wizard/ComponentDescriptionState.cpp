/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ComponentDescriptionState.h"
#include "ComponentDescriptionWidget.h"
#include "WizardMainWindow.h"
#include "ComponentCreationState.h"

// includes from coreschema
#include <Component.hxx>
#include <ComponentExtension.hxx>

// temporary
#include <iostream>

ComponentDescriptionState::ComponentDescriptionState(QString name, ComponentDescriptionWidget* widget, WizardMainWindow* mainWindow,  ComponentCreationState* parent)
    : WizardState(name, widget, mainWindow, parent) {
    this->domComponent = nullptr;
    this->domComponentExtension = nullptr;
}

void ComponentDescriptionState::resetDomComponent(cepcoreschema::Component* domComponent, cepcoreschema::ComponentExtension* domComponentExtension) {
    this->domComponent = domComponent;
    this->domComponentExtension = domComponentExtension;

    auto* componentDescriptionWidget = dynamic_cast<ComponentDescriptionWidget*>(widget);
    if (componentDescriptionWidget) {
        QString componentExtensionName = domComponentExtension->name().c_str();
        componentDescriptionWidget->setToDefault(componentExtensionName);
    }

}

void ComponentDescriptionState::onEntry(QEvent* event) {
    WizardState::onEntry(event);
    auto* componentDescriptionWidget = dynamic_cast<ComponentDescriptionWidget*>(widget);
    if (componentDescriptionWidget) {
        QString componentExtensionName = domComponentExtension->name().c_str();
        componentDescriptionWidget->setToDefault(componentExtensionName);
        // If the state is entered with a previous button (the component has already been created)
        if ((domComponent != NULL) && (QString("A Component") != QString(domComponent->name().c_str()))) {
            componentDescriptionWidget->setName(domComponent->name().c_str());
            componentDescriptionWidget->setDescription(domComponent->description().c_str());
        }
    }
}

void ComponentDescriptionState::onExit(QEvent* event) {
    WizardState::onExit(event);

    auto* componentDescriptionWidget = dynamic_cast<ComponentDescriptionWidget*>(widget);
    if (componentDescriptionWidget) {
        QString componentName = componentDescriptionWidget->getComponentName();
        QString componentDescription = componentDescriptionWidget->getComponentDescription();
        QString suffix = componentDescriptionWidget->getSuffix();
        QString representation = componentDescriptionWidget->get3DRepresentation();

        domComponent->name(componentName.toStdString());
        domComponent->description(componentDescription.toStdString());
        if (!suffix.isEmpty()) {
            std::cout << "File suffix: " << std::endl;
            domComponent->fileSuffix().push_back(suffix.toStdString());
        }
        domComponent->representation(representation.toStdString());
    }

}

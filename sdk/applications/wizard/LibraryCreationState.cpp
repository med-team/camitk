/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "LibraryCreationState.h"
#include "WizardMainWindow.h"

#include "LibraryDescriptionWidget.h"
#include "LibraryDescriptionState.h"
#include "LibraryCopyFilesWidget.h"
#include "LibraryCopyFilesState.h"
#include "LibraryDependenciesState.h"
#include "DependenciesWidget.h"

#include <Cep.hxx>
#include <Actions.hxx>
#include <ActionExtension.hxx>


LibraryCreationState::LibraryCreationState(QString name, WizardMainWindow* mainWindow, cepcoreschema::Cep* domCep, QMap<QString, QStringList>* libraryFilesMap) : QState() {
    this->name = name;
    this->domCep = domCep;
    this->libraryFilesMap = libraryFilesMap;
    this->domLibrary = nullptr;


    createSubStates(mainWindow);
}

void LibraryCreationState::onEntry(QEvent* event) {
    if (domLibrary != nullptr) {
        delete domLibrary;
        domLibrary = nullptr;
    }
    domLibrary = new cepcoreschema::Library("A Library", "A library description");

    libraryDescriptionState->resetDomLibrary(domLibrary);
    libraryCopyFilesState->resetDomLibrary(domLibrary);
    libraryDependenciesState->resetDomLibrary(domLibrary);

}

void LibraryCreationState::onExit(QEvent* event) {
    if (! cancelled) {
        cepcoreschema::Libraries someLibraries;
        if (domCep->libraries().present()) {
            domCep->libraries().get().library().push_back(*domLibrary);
        }
        else {
            cepcoreschema::Libraries theLibraries;
            theLibraries.library().push_back(*domLibrary);
            domCep->libraries(theLibraries);
        }
    }
    else {
        if ((domLibrary != NULL) && (QString("A Library") != QString(domLibrary->name().c_str()))) {
            QString libraryName = domLibrary->name().c_str();
            libraryFilesMap->remove(libraryName);
        }
    }

}


void LibraryCreationState::extensionFinished() {
    cancelled = false;
    emit next();
}

void LibraryCreationState::extensionCancelled() {
    cancelled = true;
    emit next();
}

void LibraryCreationState::createSubStates(WizardMainWindow* mainWindow) {
    libraryDescriptionWidget = new LibraryDescriptionWidget(nullptr);
    libraryDescriptionState = new LibraryDescriptionState("Library Description",  libraryDescriptionWidget, mainWindow, this);

    libraryCopyFilesWidget = new LibraryCopyFilesWidget(nullptr);
    libraryCopyFilesState  = new LibraryCopyFilesState("Library copy files",  libraryCopyFilesWidget, mainWindow, libraryFilesMap, this);

    libraryDependenciesWidget = new DependenciesWidget(nullptr);
    libraryDependenciesState = new LibraryDependenciesState("Library Dependencies",  libraryDependenciesWidget, mainWindow, domCep, this);


    this->setInitialState(libraryDescriptionState);

    libraryDescriptionState->addTransition(libraryDescriptionWidget, SIGNAL(next()), libraryCopyFilesState);
    libraryCopyFilesState->addTransition(libraryCopyFilesWidget, SIGNAL(next()), libraryDependenciesState);
    libraryCopyFilesState->addTransition(libraryCopyFilesWidget, SIGNAL(previous()), libraryDescriptionState);
    libraryDependenciesState->addTransition(libraryDependenciesWidget, SIGNAL(previous()), libraryCopyFilesState);

    QObject::connect(libraryDescriptionWidget, SIGNAL(cancel()), this, SLOT(extensionCancelled()));
    QObject::connect(libraryCopyFilesWidget, SIGNAL(cancel()), this, SLOT(extensionCancelled()));
    QObject::connect(libraryDependenciesWidget, SIGNAL(cancel()), this, SLOT(extensionCancelled()));


    QObject::connect(libraryDependenciesWidget, SIGNAL(next()), this, SLOT(extensionFinished()));



}

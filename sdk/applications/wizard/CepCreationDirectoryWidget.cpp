/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "CepCreationDirectoryWidget.h"

// Qt files
#include <QMessageBox>
#include <QFileDialog>

CepCreationDirectoryWidget::CepCreationDirectoryWidget(QWidget* parent) : QWidget(parent) {
    ui.setupUi(this);
}

void CepCreationDirectoryWidget::nextButtonClicked() {
    // Check if the dev. directory has been filled
    QString enteredDir = ui.inputDirectoryLineEdit->text();
    QFileInfo devDir(enteredDir);
    // Message if the field is empty (no development directory)

#ifndef _WIZARD_QUESTIONS_SQUEEZE
    if ((enteredDir == "") || (!devDir.isDir())) {

        ui.selectLabel->setStyleSheet("QLabel { background-color : red; color : yellow; }");
        ui.requiredLabel->setStyleSheet("QLabel { background-color : red; color : yellow; }");

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, tr("Before going further..."), tr("Please select a valid development directory\n"));
    }
    else {
        ui.selectLabel->setStyleSheet("QLabel { background-color : none; color : black; }");
        ui.requiredLabel->setStyleSheet("QLabel { background-color : none; color : black; }");
        emit next();

    }
#else
    emit next();
#endif

}

void CepCreationDirectoryWidget::previousButtonClicked() {
    ui.selectLabel->setStyleSheet("QLabel { background-color : none; color : black; }");
    ui.requiredLabel->setStyleSheet("QLabel { background-color : none; color : black; }");

    emit previous();
}

void CepCreationDirectoryWidget::chooseFileButtonClicked() {
    QString fileName = QFileDialog::getExistingDirectory(this, tr("Set the development directory"));
    ui.inputDirectoryLineEdit->setText(fileName);
}

QString CepCreationDirectoryWidget::getDirectoryName() {
    QString dir = ui.inputDirectoryLineEdit->text();
    return dir;
}

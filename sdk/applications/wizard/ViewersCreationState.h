/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef VIEWERSCREATIONSTATE_H
#define VIEWERSCREATIONSTATE_H

#include "WizardState.h"

class ViewersCreationWidget;
class ViewerExtensionCreationState;

// Dependency from cepcoreschema
// Declaration here to avoid declaration in dependant projects.
namespace cepcoreschema {
class ViewerExtension;
}

/**
 * @ingroup group_sdk_application_wizard
 *
 * @brief
 * State to create actions
 *
 */
class ViewersCreationState : public WizardState  {

    Q_OBJECT;

public:
    /**  Constructor */
    ViewersCreationState(QString name, ViewersCreationWidget* widget, WizardMainWindow* mainWindow, ViewerExtensionCreationState* parent);

    /**  Destructor */
    ~ViewersCreationState() override = default;

    void resetDomViewerExtension(cepcoreschema::ViewerExtension* domViewerExtension);

protected:
    void onEntry(QEvent* event) override;
    void onExit(QEvent* event) override;

private:
    cepcoreschema::ViewerExtension* domViewerExtension;

};
#endif

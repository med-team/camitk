/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef VIEWEREXTENSIONCREATIONSTATE_H
#define VIEWEREXTENSIONCREATIONSTATE_H

#include <QState>
#include <QWidget>

class WizardMainWindow;
// Sub states
class ViewerExtensionDescriptionWidget;
class ViewerExtensionDescriptionState;
class ViewersCreationWidget;
class ViewersCreationState;
class ViewerCreationState;
class DependenciesWidget;
class ViewerExtensionDependenciesState;
class ExtensionSummaryWidget;
class ExtensionSummaryState;

// Dependency from cepcoreschema
// Declaration here to avoid declaration in dependant projects.
namespace cepcoreschema {
class Cep;
class ViewerExtension;
}

/**
 * @ingroup group_sdk_application_wizard
 *
 * @brief
 * State to create an action extension. \n
 *
 * This state enables to create all states to generate one or more actions. \n
 * It manages a sub-states dedicated to the creation of actions.
 *
 */
class ViewerExtensionCreationState : public QState  {

    Q_OBJECT;

public:
    /**  Constructor */
    ViewerExtensionCreationState(QString name, WizardMainWindow* mainWidnow, cepcoreschema::Cep* domCep);

    /**  Destructor */
    ~ViewerExtensionCreationState() override = default;

signals:
    void next();

public slots:
    virtual void extensionFinished();
    virtual void extensionCancelled();

protected:
    /// Reimplemented from QState
    /// @{
    void onEntry(QEvent* event) override;

    void onExit(QEvent* event) override;
    ///@}

private:

    bool cancelled;

    // Sub States
    ViewerExtensionDescriptionWidget*   viewerExtensionDescriptionWidget;
    ViewerExtensionDescriptionState*    viewerExtensionDescriptionState;
    ViewersCreationWidget*              viewersCreationWidget;
    ViewersCreationState*               viewersCreationState;
    ViewerCreationState*                viewerCreationState;
    DependenciesWidget*                 viewerExtensionDependenciesWidget;
    ViewerExtensionDependenciesState*   viewerExtensionDependenciesState;
    ExtensionSummaryWidget*             viewerExtensionSummaryWidget;
    ExtensionSummaryState*              viewerExtensionSummaryState;

    void createSubStates(WizardMainWindow* mainWindow);

    QString name;
    cepcoreschema::Cep* domCep;
    cepcoreschema::ViewerExtension* domViewerExtension;

};
#endif

/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "CepCreateRecapState.h"
#include "CepCreateRecapWidget.h"
#include "WizardMainWindow.h"

// includes from coreschema
#include <Cep.hxx>
#include <Contact.hxx>
#include <ActionExtensions.hxx>
#include <ActionExtension.hxx>

// temporary include
#include <iostream>


CepCreateRecapState::CepCreateRecapState(QString name, CepCreateRecapWidget* widget, WizardMainWindow* mainWindow, cepcoreschema::Cep* domCep)
    : WizardState(name, widget, mainWindow) {
    this->domCep = domCep;
}

void CepCreateRecapState::onEntry(QEvent* event) {
    WizardState::onEntry(event);
    auto* cepCreateRecapWidget = dynamic_cast<CepCreateRecapWidget*>(widget);
    if (cepCreateRecapWidget != nullptr) {
        updateCepDescription(cepCreateRecapWidget);
        updateActionExtensions(cepCreateRecapWidget);
        updateViewerExtensions(cepCreateRecapWidget);
        updateComponentExtensions(cepCreateRecapWidget);
        updateCepLibraries(cepCreateRecapWidget);
    }
}

void CepCreateRecapState::updateCepDescription(CepCreateRecapWidget* cepCreateRecapWidget) {
    QString cepName = domCep->name().c_str();
    QString cepDescription = domCep->description().c_str();
    QString cepContact;
    cepcoreschema::Contact theContact = domCep->contact();
    cepcoreschema::Contact::email_iterator it;
    for (it = theContact.email().begin(); it != theContact.email().end(); it++) {
        cepContact += (*it).c_str();
        if ((it + 1) != theContact.email().end()) {
            cepContact += ", ";
        }
    }
    cepCreateRecapWidget->setNameItself(cepName);
    cepCreateRecapWidget->setDescriptionItself(cepDescription);
    cepCreateRecapWidget->setContactItself(cepContact);

}

void CepCreateRecapState::updateViewerExtensions(CepCreateRecapWidget* cepCreateRecapWidget) {
    cepCreateRecapWidget->emptyExistingViewerExtensions();
    if (domCep->viewerExtensions().present()) {
        cepcoreschema::ViewerExtensions cepViewerExtensions = domCep->viewerExtensions().get();
        cepcoreschema::ViewerExtensions::viewerExtension_iterator it;
        for (it = cepViewerExtensions.viewerExtension().begin(); it != cepViewerExtensions.viewerExtension().end(); it++) {
            cepcoreschema::ViewerExtension extension = (*it);
            QString viewerExtensionName = extension.name().c_str();
            cepCreateRecapWidget->addViewerExtension(viewerExtensionName);
        }
    }
}

void CepCreateRecapState::updateActionExtensions(CepCreateRecapWidget* cepCreateRecapWidget) {
    cepCreateRecapWidget->emptyExistingActionExtensions();
    if (domCep->actionExtensions().present()) {
        cepcoreschema::ActionExtensions cepActionExtensions  = domCep->actionExtensions().get();
        cepcoreschema::ActionExtensions::actionExtension_iterator it;
        for (it = cepActionExtensions.actionExtension().begin(); it != cepActionExtensions.actionExtension().end(); it++) {
            cepcoreschema::ActionExtension extension = (*it);
            QString actionExtensionName = extension.name().c_str();
            cepCreateRecapWidget->addActionExtension(actionExtensionName);
        }
    }
}

void CepCreateRecapState::updateComponentExtensions(CepCreateRecapWidget* cepCreateRecapWidget) {
    cepCreateRecapWidget->emptyExistingComponentExtensions();
    if (domCep->componentExtensions().present()) {
        cepcoreschema::ComponentExtensions cepComponentExtensions  = domCep->componentExtensions().get();
        cepcoreschema::ComponentExtensions::componentExtension_iterator it;
        for (it = cepComponentExtensions.componentExtension().begin(); it != cepComponentExtensions.componentExtension().end(); it++) {
            cepcoreschema::ComponentExtension extension = (*it);
            QString componentExtensionName = extension.name().c_str();
            cepCreateRecapWidget->addComponentExtension(componentExtensionName);
        }
    }

}

void CepCreateRecapState::updateCepLibraries(CepCreateRecapWidget* cepCreateRecapWidget) {
    cepCreateRecapWidget->emptyExistingLibraries();
    if (domCep->libraries().present()) {
        cepcoreschema::Libraries cepLibraries = domCep->libraries().get();
        cepcoreschema::Libraries::library_iterator it;
        for (it = cepLibraries.library().begin(); it != cepLibraries.library().end(); it++) {
            cepcoreschema::Library library = (*it);
            QString libraryName = library.name().c_str();
            cepCreateRecapWidget->addLibrary(libraryName);
        }

    }
}

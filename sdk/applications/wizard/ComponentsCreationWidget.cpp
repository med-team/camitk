/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ComponentsCreationWidget.h"

#include "DefaultGUIText.h"

// Qt files
#include <QMessageBox>

ComponentsCreationWidget::ComponentsCreationWidget(QWidget* parent) : QWidget(parent) {
    ui.setupUi(this);
}

void ComponentsCreationWidget::setToDefault() {
    ui.requiredLabel->setStyleSheet(normalStyle);
    ui.label_Star->setStyleSheet(normalStyle);
    emptyExistingComponents();
}

void ComponentsCreationWidget::nextButtonClicked() {
    // check if at least an action has been created
#ifndef _WIZARD_QUESTIONS_SQUEEZE
    if (createdComponents.isEmpty()) {
        ui.requiredLabel->setStyleSheet(enhancedStyle);
        ui.label_Star->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(NULL, tr("Components Creation Widget"), tr("Please create at least one Component in this ComponentExtension\n"));
    }
    else {
        ui.requiredLabel->setStyleSheet(normalStyle);
        ui.label_Star->setStyleSheet(normalStyle);

        emit next();
    }
#else
    emit next();
#endif
}

void ComponentsCreationWidget::addComponentClicked() {
    ui.requiredLabel->setStyleSheet(normalStyle);
    ui.label_Star->setStyleSheet(normalStyle);
    emit newComponent();
}

void ComponentsCreationWidget::previousButtonClicked() {
    emit previous();
}

void ComponentsCreationWidget::cancelButtonClicked() {
    emit cancel();
}

void ComponentsCreationWidget::setGroupBoxTitle(QString text) {
    ui.componentsGroupBox->setTitle(text);
}

void ComponentsCreationWidget::emptyExistingComponents() {
    createdComponents.clear();
    createdComponentsString = defaultCreatedComponentsString;
    ui.existingComponentsTextEdit->setHtml(createdComponentsString);
}

void ComponentsCreationWidget::addComponentName(QString componentName) {
    this->createdComponents.append(componentName);
    QString toBeInserted = "<li>" + componentName + "</li>\n";
    int index = createdComponentsString.lastIndexOf("</ul>");

    createdComponentsString.insert(index, toBeInserted);
    ui.existingComponentsTextEdit->setHtml(createdComponentsString);
}

/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef WizardMainWindow_H
#define WizardMainWindow_H

// Include GUI  automatically generated file
#include "ui_WizardMainWindow.h"

// includes from Qt
class QStackedWidget;
class QPixmap;
class QPalette;

/**
 * @ingroup group_sdk_application_wizard
 *
 * @brief
 * The main window of the Wizard. All state widgets are contained in this window.
 *
 */
class  WizardMainWindow : public QMainWindow  {
    Q_OBJECT;

public:
    /**  Constructor */
    WizardMainWindow();
    /**  Destructor */
    ~WizardMainWindow();

    void setCentralWidget(QWidget* widget);
    void setStateInfo(QString info);

public slots:
    virtual void welcomeHelp();


private:
    QStackedWidget* widgetStack;
    /// index of the empty widget,
    /// used when no state is active
    int emptyWidgetIndex;

    Ui::WizardMainWindow ui;

};
#endif

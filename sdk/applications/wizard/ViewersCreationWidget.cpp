/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ViewersCreationWidget.h"

#include "DefaultGUIText.h"

#include <ViewerExtension.hxx>
#include <Viewer.hxx>

// Qt files
#include <QInputDialog>
#include <QMessageBox>

ViewersCreationWidget::ViewersCreationWidget(QWidget* parent) : QWidget(parent) {
    ui.setupUi(this);
}

void ViewersCreationWidget::resetDomViewerExtension(cepcoreschema::ViewerExtension* domViewerExtension) {
    this->domViewerExtension = domViewerExtension;
}

void ViewersCreationWidget::setToDefault() {
    ui.requiredLabel->setStyleSheet(normalStyle);
    ui.label_Star->setStyleSheet(normalStyle);
    emptyExistingViewers();
}

void ViewersCreationWidget::nextButtonClicked() {
    // check if at least an action has been created
#ifndef _WIZARD_QUESTIONS_SQUEEZE
    if (createdViewers.isEmpty()) {
        ui.requiredLabel->setStyleSheet(enhancedStyle);
        ui.label_Star->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(NULL, "Viewers Creation Widget", "Please create at least default viewer in this ViewerExtension\n");
    }
    else {
        ui.requiredLabel->setStyleSheet(normalStyle);
        ui.label_Star->setStyleSheet(normalStyle);
        emit next();
    }

#else
    emit next();
#endif
}

void ViewersCreationWidget::registerDefaultViewerClicked() {
    addViewerName("default");
    //domViewerExtension->registerDefaultViewer(cepcoreschema::registerDefaultViewer::registerDefaultViewer());
    domViewerExtension->registerDefaultViewer(cepcoreschema::registerDefaultViewer());
}

void ViewersCreationWidget::registerNewViewerClicked() {
    QString viewerName = QInputDialog::getText(this, "ViewerName", "Get a name to the New Viewer:");

    if (!isViewerRegistered(viewerName)) {
        addViewerName(viewerName);
        domViewerExtension->registerNewViewer().push_back(viewerName.toStdString());
    }
    else {
        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(NULL, "Viewers Creation Widget", "A registered viewer with the same name already exist\n");
    }
}

void ViewersCreationWidget::previousButtonClicked() {
    emit previous();
}

void ViewersCreationWidget::cancelButtonClicked() {
    emit cancel();
}

void ViewersCreationWidget::setGroupBoxTitle(QString text) {
    ui.viewersGroupBox->setTitle(text);
}

void ViewersCreationWidget::emptyExistingViewers() {
    createdViewers.clear();
    createdViewersString = defaultCreatedViewersString;
    ui.createdViewersTextEdit->setHtml(createdViewersString);
}

void ViewersCreationWidget::addViewerName(QString viewerName) {
    this->createdViewers.append(viewerName);

    QString toBeInserted = "<li>" + viewerName + "</li>\n";
    int index = createdViewersString.lastIndexOf("</ul>");

    createdViewersString.insert(index, toBeInserted);
    ui.createdViewersTextEdit->setHtml(createdViewersString);
}

bool ViewersCreationWidget::isViewerRegistered(QString viewerName) {
    unsigned int i = 0;

    while (i < domViewerExtension->registerNewViewer().size() && domViewerExtension->registerNewViewer().at(i) != viewerName.toStdString()) {
        i++;
    }

    return i < domViewerExtension->registerNewViewer().size();
}

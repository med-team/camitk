/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ViewerExtensionCreationState.h"
#include "WizardMainWindow.h"

#include "ViewerExtensionDescriptionWidget.h"
#include "ViewerExtensionDescriptionState.h"
#include "ViewersCreationWidget.h"
#include "ViewersCreationState.h"
#include "ViewerCreationState.h"
#include "ViewerExtensionDependenciesState.h"
#include "DependenciesWidget.h"
#include "ExtensionSummaryWidget.h"
#include "ExtensionSummaryState.h"

#include <Cep.hxx>
#include <Viewer.hxx>
#include <ViewerExtension.hxx>

ViewerExtensionCreationState::ViewerExtensionCreationState(QString name, WizardMainWindow* mainWindow, cepcoreschema::Cep* domCep) : QState() {
    this->cancelled = false;
    this->name = name;
    this->domCep = domCep;
    this->domViewerExtension = nullptr;

    createSubStates(mainWindow);
}

void ViewerExtensionCreationState::onEntry(QEvent* event) {
    this->cancelled = false;
    if (this->domViewerExtension != nullptr) {
        delete domViewerExtension;
        domViewerExtension = nullptr;
    }

    cepcoreschema::Viewer theViewer("A Viewer", "A viewer description", cepcoreschema::ViewerType::EMBEDDED);
    this->domViewerExtension = new cepcoreschema::ViewerExtension("A Viewer Extension", "A viewer extension description", theViewer);

    viewerExtensionDescriptionState->resetDomViewerExtension(domViewerExtension);
    viewerExtensionDependenciesState->resetDomViewerExtension(domViewerExtension);
    viewerExtensionSummaryState->setViewerExtension(domViewerExtension);
    viewersCreationState->resetDomViewerExtension(domViewerExtension);
    viewerCreationState->resetDomViewerExtension(domViewerExtension);
}

void ViewerExtensionCreationState::onExit(QEvent* event) {
    if (! cancelled) {
        if (domCep->viewerExtensions().present()) {
            domCep->viewerExtensions().get().viewerExtension().push_back((*domViewerExtension));
        }
        else {
            cepcoreschema::ViewerExtensions theExtensions;
            theExtensions.viewerExtension().push_back((*domViewerExtension));
            domCep->viewerExtensions(theExtensions);
        }
    }
    else {
        if (domViewerExtension != nullptr) {
            delete domViewerExtension;
            domViewerExtension = nullptr;
        }
    }

}


void ViewerExtensionCreationState::extensionFinished() {
    cancelled = false;
    emit next();
}

void ViewerExtensionCreationState::extensionCancelled() {
    cancelled = true;
    emit next();
}

void ViewerExtensionCreationState::createSubStates(WizardMainWindow* mainWindow) {
    viewerExtensionDescriptionWidget = new ViewerExtensionDescriptionWidget(nullptr);
    viewerExtensionDescriptionState = new ViewerExtensionDescriptionState("Viewer Extension", viewerExtensionDescriptionWidget, mainWindow, this);

    viewersCreationWidget = new ViewersCreationWidget(nullptr);
    viewersCreationState = new ViewersCreationState("Viewers Creation",  viewersCreationWidget, mainWindow, this);

    viewerCreationState = new ViewerCreationState("Viewer Creation", mainWindow, domCep, this);

    viewerExtensionDependenciesWidget = new DependenciesWidget(nullptr);
    viewerExtensionDependenciesState = new ViewerExtensionDependenciesState("Viewer Extension Dependencies",  viewerExtensionDependenciesWidget, mainWindow, domCep, this);

    viewerExtensionSummaryWidget = new ExtensionSummaryWidget(nullptr);
    viewerExtensionSummaryState = new ExtensionSummaryState("Viewer Extension Summary", viewerExtensionSummaryWidget, "Viewer", mainWindow, this);

    viewerExtensionDescriptionState->addTransition(viewerExtensionDescriptionWidget, SIGNAL(next()), viewerCreationState);
    viewersCreationState->addTransition(viewersCreationWidget, SIGNAL(next()), viewerExtensionDependenciesState);
    viewersCreationState->addTransition(viewersCreationWidget, SIGNAL(previous()), viewerExtensionDescriptionState);

    viewerCreationState->addTransition(viewerCreationState, SIGNAL(nextVCS()), viewerExtensionDescriptionState);
    viewerCreationState->addTransition(viewerCreationState, SIGNAL(nextVCS2()), viewersCreationState);

    viewerExtensionDependenciesState->addTransition(viewerExtensionDependenciesWidget, SIGNAL(previous()), viewersCreationState);
    viewerExtensionDependenciesState->addTransition(viewerExtensionDependenciesWidget, SIGNAL(next()), viewerExtensionSummaryState);

    viewerExtensionSummaryState->addTransition(viewerExtensionSummaryWidget, SIGNAL(previous()), viewerExtensionDependenciesState);
    QObject::connect(viewerExtensionSummaryWidget, SIGNAL(next()), this, SLOT(extensionFinished()));

    // Cancel
    QObject::connect(viewerExtensionDescriptionWidget, SIGNAL(cancel()), this, SLOT(extensionCancelled()));
    QObject::connect(viewersCreationWidget, SIGNAL(cancel()), this, SLOT(extensionCancelled()));
    QObject::connect(viewerExtensionDependenciesWidget, SIGNAL(cancel()), this, SLOT(extensionCancelled()));
    QObject::connect(viewerExtensionSummaryWidget, SIGNAL(cancel()), this, SLOT(extensionCancelled()));

    this->setInitialState(viewerExtensionDescriptionState);
}

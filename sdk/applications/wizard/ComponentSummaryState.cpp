/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ComponentSummaryState.h"
#include "ComponentSummaryWidget.h"

#include "WizardMainWindow.h"

// includes from coreschema
#include <Component.hxx>

ComponentSummaryState::ComponentSummaryState(QString name, ComponentSummaryWidget* widget,
        WizardMainWindow* mainWindow, QState* parent)
    : WizardState(name, widget, mainWindow, parent) {
    this->domComponent = nullptr;
}

void ComponentSummaryState::resetComponent(cepcoreschema::Component* domComponent) {
    this->domComponent = domComponent;
}


void ComponentSummaryState::onEntry(QEvent* event) {
    WizardState::onEntry(event);

    auto* componentSummaryWidget = dynamic_cast<ComponentSummaryWidget*>(widget);
    if (componentSummaryWidget != nullptr) {
        if (domComponent != nullptr) {
            QString name = domComponent->name().c_str();
            QString description = domComponent->description().c_str();
            QString representation = domComponent->representation().c_str();

            QStringList properties;
            if (domComponent->properties().present()) {
                cepcoreschema::Parameters::parameter_const_iterator it;
                for (it = domComponent->properties().get().parameter().begin(); it != domComponent->properties().get().parameter().end(); it++) {
                    QString param = QString((*it).type().c_str()) + " " + (*it).name().c_str() + "(= " + (*it).defaultValue().get().c_str() + " )";
                    properties << param;
                }

            }

            componentSummaryWidget->setSummary(name, description, representation, properties);
        }
    }

}


void ComponentSummaryState::onExit(QEvent* event) {
    WizardState::onExit(event);
}


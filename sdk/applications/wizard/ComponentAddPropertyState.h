/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef COMPONENTADDPROPERTYSTATE_H
#define COMPONENTADDPROPERTYSTATE_H

#include "WizardState.h"

#include <QList>

class ComponentAddPropertyWidget;
class ComponentCreationState;


// Dependency from cepcoreschema
// Declaration here to avoid declaration in dependant projects.
namespace cepcoreschema {
class Component;
}

/**
 * @ingroup group_sdk_application_wizard
 *
 * @brief
 * State to add a property to a component
 *
 */
class ComponentAddPropertyState : public WizardState  {

    Q_OBJECT;

public:
    /**  Constructor */
    ComponentAddPropertyState(QString name, ComponentAddPropertyWidget* widget, WizardMainWindow* mainWindow, ComponentCreationState* parent);
    /**  Destructor */
    ~ComponentAddPropertyState() override = default;

    void resetDomComponent(cepcoreschema::Component* domComponent);

protected:
    void onEntry(QEvent* event) override;
    void onExit(QEvent* event) override;

    void addDomParameters();

private:
    cepcoreschema::Component* domComponent;

};
#endif

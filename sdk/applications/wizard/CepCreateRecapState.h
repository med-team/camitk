/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef CEPCREATERECAPSTATE_H
#define CEPCREATERECAPSTATE_H

#include "WizardState.h"

class CepCreateRecapWidget;

// Dependency from cepcoreschema
// Declaration here to avoid declaration in dependant projects.
namespace cepcoreschema {
class Cep;
}

/**
 * @ingroup group_sdk_application_wizard
 *
 * @brief
 * State to summarize the creation of the CEP
 *
 */
class CepCreateRecapState : public WizardState  {

    Q_OBJECT;

public:
    /**  Constructor */
    CepCreateRecapState(QString name, CepCreateRecapWidget* widget, WizardMainWindow* mainWindow, cepcoreschema::Cep* domCep);
    /**  Destructor */
    ~CepCreateRecapState() override = default;

protected:
    void onEntry(QEvent* event) override;


private:
    void updateCepDescription(CepCreateRecapWidget* cepCreateRecapWidget);
    void updateViewerExtensions(CepCreateRecapWidget* cepCreateRecapWidget);
    void updateActionExtensions(CepCreateRecapWidget* cepCreateRecapWidget);
    void updateComponentExtensions(CepCreateRecapWidget* cepCreateRecapWidget);
    void updateCepLibraries(CepCreateRecapWidget* cepCreateRecapWidget);

    cepcoreschema::Cep* domCep;

};
#endif

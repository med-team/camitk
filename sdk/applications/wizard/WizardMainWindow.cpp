/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "WizardMainWindow.h"

// includes from QT
#include <QUrl>
#include <QDesktopServices>
#include <QPalette>
#include <QPixmap>
#include <QFile>
#include <QStackedWidget>

WizardMainWindow::WizardMainWindow() : QMainWindow(NULL) {

    ui.setupUi(this);

    QFile stylesheetFile(":/resources/stylesheet.qss");
    stylesheetFile.open(QFile::ReadOnly);
    QString styleSheet = QString(stylesheetFile.readAll());
    ui.centralwidget->setStyleSheet(styleSheet);

    widgetStack = new QStackedWidget();
    widgetStack->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
    widgetStack->setLineWidth(3);
    // insert empty widget to fill the space by default
    emptyWidgetIndex = widgetStack->addWidget(new QWidget());
    // add widget stack to layout.
    ui.mainLayout->addWidget(widgetStack);

    // CONNECT
    //WelcomeWindow
    QObject::connect(ui.helpPushButton, SIGNAL(clicked()), this, SLOT(welcomeHelp()));
}


WizardMainWindow::~WizardMainWindow() {}

// Welcome buttons
/** When button Help of WelcomeWindow interface is clicked, the wiki page "New interfaces of Wizard" opens */
void WizardMainWindow::welcomeHelp() {
    QUrl helpUrl("https://camitk.imag.fr");
    QDesktopServices::openUrl(helpUrl);
}


void WizardMainWindow::setCentralWidget(QWidget* widget) {
    //-- check history
    int widgetIndex = widgetStack->indexOf(widget);

    if (widgetIndex == -1 && widget) {
        // add the widget (beware that actionWidgetStack then takes ownership of the widget!)
        widgetIndex = widgetStack->addWidget(widget);
    }

    widgetStack->setCurrentIndex(widgetIndex);

    widgetStack->update();
}

void WizardMainWindow::setStateInfo(QString info) {
//    ui.currentStateLabel->setText(info);
}

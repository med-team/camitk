/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ActionSummaryState.h"
#include "ActionSummaryWidget.h"

#include "WizardMainWindow.h"

// includes from coreschema
#include <Action.hxx>

#include <iostream>

ActionSummaryState::ActionSummaryState(QString name, ActionSummaryWidget* widget,
                                       WizardMainWindow* mainWindow, QState* parent)
    : WizardState(name, widget, mainWindow, parent) {
    this->domAction = nullptr;
}

void ActionSummaryState::resetAction(cepcoreschema::Action* domAction) {
    this->domAction = domAction;
}


void ActionSummaryState::onEntry(QEvent* event) {
    WizardState::onEntry(event);
    std::cout << "Action Summary State on Entry" << std::endl;

    auto* actionSummaryWidget = dynamic_cast<ActionSummaryWidget*>(widget);
    if (actionSummaryWidget != nullptr) {
        if (domAction != nullptr) {
            std::cout << "domAction non NULL" << std::endl;
            QString name = domAction->name().c_str();
            QString description = domAction->description().c_str();
            QString component = domAction->component().c_str();

            QStringList parameters;
            if (domAction->parameters().present()) {
                cepcoreschema::Parameters::parameter_const_iterator it;
                for (it = domAction->parameters().get().parameter().begin(); it != domAction->parameters().get().parameter().end(); it++) {
                    QString param = QString((*it).type().c_str()) + " " + (*it).name().c_str() + "(= " + (*it).defaultValue().get().c_str() + " )";
                    parameters << param;
                }

            }

            QString family = domAction->classification().family().c_str();
            QStringList tags;
            cepcoreschema::Classification::tag_iterator it;
            for (it = domAction->classification().tag().begin(); it != domAction->classification().tag().end(); it++) {
                tags << (*it).c_str();
            }

            actionSummaryWidget->setSummary(name, description, component, parameters, family, tags);
        }
    }

}


void ActionSummaryState::onExit(QEvent* event) {
    WizardState::onExit(event);
}


/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef COMPONENTEXTENSIONDESCRIPTIONWIDGET_H
#define COMPONENTEXTENSIONDESCRIPTIONWIDGET_H

// Include GUI  automatically generated file
#include "ui_ComponentExtensionDescriptionWidget.h"

// includes from Qt
#include <QWidget>

/**
 * @ingroup group_sdk_application_wizard
 *
 * @brief
 * Widget to describe component extension
 *
 */
class  ComponentExtensionDescriptionWidget : public QWidget {

    Q_OBJECT;

public:
    /**  Constructor */
    ComponentExtensionDescriptionWidget(QWidget* parent);
    /**  Destructor */
    ~ComponentExtensionDescriptionWidget() override = default;

    void setToDefault();

    QString getComponentExtensionName();
    QString getComponentExtensionDescription();

public slots:
    virtual void nextButtonClicked();
    virtual void cancelButtonClicked();

signals:
    void next();
    void cancel();

private:

    Ui::ComponentExtensionDescriptionWidget ui;
};
#endif

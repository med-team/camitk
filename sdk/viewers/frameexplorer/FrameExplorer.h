/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef FRAMEEXPLORER_H
#define FRAMEEXPLORER_H

#include "FrameExplorerAPI.h"

// -- Core stuff
#include <Viewer.h>

// -- QT stuff
#include <QTreeWidget>
#include <QTreeWidgetItem>

namespace camitk {
// -- Core stuff classes
class InterfaceFrame;
}

/**
 * @ingroup group_sdk_libraries_core_frameexplorer
 *
 * @brief
 * Frame Explorer window, display the hierachy of the frames of all the data currently opened in the application.
 * All frames are displayed in a QListView widget and can be selected
 * (single/multiple selection is available).
 *
 * It manages the camitk::InterfaceNode facet of components.
 *
 * If this viewer extension is loaded, the default instance of this viewer can be accessed directly
 * by Application::getViewer("Frame Explorer").
 *
 * @see camitk::InterfaceFrame
 * @see camitk::Viewer
 */
class FRAMEEXPLORER_API FrameExplorer : public camitk::Viewer {
    Q_OBJECT

public:
    /** @name General
      */
    ///@{
    /** Construtor */
    Q_INVOKABLE FrameExplorer(QString);

    /** Destructor */
    ~FrameExplorer() override = default;

    /** @name Inherited from Viewer
      */
    ///@{
    /// refresh the frame explorer (can be interesting to know which other viewer is calling this)
    void refresh(Viewer* whoIsAsking = nullptr) override;

    /// get the frame explorer widget (QTreeWidget)
    QWidget* getWidget() override;

private slots :

    /// slot called whenever the selection changed in the explorer
    void frameSelectionChanged();

private:

    /// @name QTreeWidget and QTreeWidgetItem management
    ///@{
    /// instantiate a new QTreeWidgetItem using names and properties from the InterfaceNode, and using parent
    QTreeWidgetItem* getNewItem(QTreeWidgetItem* parent, camitk::Component*);

    /// recursively add the Component in the tree explorer and return the QTreeWidgetItem of the InterfaceNode
    QTreeWidgetItem* add(QTreeWidgetItem*, camitk::Component*);

    /** Add the given Component to the explorer (at top level) and automatically create children Component items.
     * @param comp The Component to add in the tree view.
     */
    void add(camitk::Component* comp);

    /// clear the tree explorer
    void remove();


    /// the list view
    QTreeWidget* explorerTree;
    ///@}

};

#endif

# Call CamiTK CMake Macro to define the action
camitk_extension(VIEWER_EXTENSION
                 CEP_NAME SDK
                 DEFINES COMPILE_MEDICALIMAGEVIEWER_API
                 NEEDS_VIEWER_EXTENSION interactivesliceviewer interactivegeometryviewer
                 DESCRIPTION "An extension for the viewer that display GUI action"
                 INSTALL_ALL_HEADERS
)


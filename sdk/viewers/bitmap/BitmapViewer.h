/*****************************************************************************
* $CAMITK_LICENCE_BEGIN$
*
* CamiTK - Computer Assisted Medical Intervention ToolKit
* (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
*
* Visit http://camitk.imag.fr for more information
*
* This file is part of CamiTK.
*
* CamiTK is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License version 3
* only, as published by the Free Software Foundation.
*
* CamiTK is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License version 3 for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
*
* $CAMITK_LICENCE_END$
****************************************************************************/

#ifndef BITMAPVIEWER_H
#define BITMAPVIEWER_H

#include "BitmapViewerAPI.h"

//-- Core
#include <InteractiveViewer.h>

class QToolBar;

/**
 * @ingroup group_sdk_libraries_core_bitmapviewer
 *
 * @brief
 * InteractiveViewer specialized in 2D bitmap (images in jpeg, png...).
 * It is similar to InteractiveSliceViewer but has the proper orientation for 2D image and a gray background.
 *
 * It manages the camitk::InterfaceBitMap facet of components.
 *
 * All the logic is done by InteractiveViewer.
 *
 * If this viewer extension is loaded, the default instance of this viewer can be accessed directly
 * by Application::getViewer("Bitmap Explorer").
 *
 * @see camitk::InterfaceBitMap
 * @see camitk::ImageComponent
 * @see camitk::Viewer
 */
class BITMAP_VIEWER_API BitmapViewer : public camitk::InteractiveViewer {
    Q_OBJECT

public:
    Q_INVOKABLE BitmapViewer(QString name);

    virtual ~BitmapViewer() override;

    void refresh(camitk::Viewer* whoIsAsking = nullptr) override;

    QWidget* getWidget() override;

    QToolBar* getToolBar() override;

private:
    /// specific toolbar that show a screenshot + switch central viewer
    QToolBar* myToolbar;

    /// the bitmap viewer widget
    QWidget* myWidget;

    /// set visibility of all possible component (that is component that can be displayed in 2D)
    void updateVisibility();

};


#endif // BITMAPVIEWER_H


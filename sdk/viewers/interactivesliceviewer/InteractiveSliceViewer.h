/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef INTERACTIVESLICEVIEWER_H
#define INTERACTIVESLICEVIEWER_H

#include "InteractiveSliceViewerAPI.h"

// -- Core stuff
#include <InteractiveViewer.h>

// -- QT stuff
#include <QFrame>
#include <QGridLayout>
#include <QAction>
#include <QBoxLayout>

/**
  * @ingroup group_sdk_libraries_core_interactivesliceviewer
  *
  * @brief Interactive single slice viewer.
  *
  * InteractiveSliceViewer is a the InteractiveViewer subclass dedicated to manage
  * SingleImageComponent (a slice view of a medical image data).
  * It blocks 3D movement and only display a 2D slice
  *
  * It manages the camitk::InterfaceBitMap facet of components.
  *
  * All the logic is done by InteractiveViewer.
  *
  * If this viewer extension is loaded, there are four default instance of this viewer that can be accessed directly
  * by Application::getViewer("Axial Viewer"), Application::getViewer("Coronal Viewer"), Application::getViewer("Sagittal Viewer")
  * and Application::getViewer("Arbitrary Viewer").
  *
  * @see camitk::SingleImageComponent
  * @see camitk::InterfaceBitMap
  * @see camitk::Viewer
  *
  */
class INTERACTIVESLICEVIEWER_API InteractiveSliceViewer : public camitk::InteractiveViewer {
    Q_OBJECT

public:
    /// constructor
    Q_INVOKABLE InteractiveSliceViewer(QString);

    /** destructor */
    virtual ~InteractiveSliceViewer() override = default;

};


#endif // INTERACTIVESLICEVIEWER_H


/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef INTERACTIVESLICEVIEWEREXTENSION_H
#define INTERACTIVESLICEVIEWEREXTENSION_H

#include "InteractiveSliceViewerAPI.h"

//-- from CamiTK Core
#include <ViewerExtension.h>

/**
 * @ingroup group_sdk_libraries_core_interactivesliceviewer
 *
 * @brief Manage the InteractiveSliceViewe viewer, instanciates and register the default
 * "Axial Viewer", "Coronal Viewer" "Sagittal Viewer" and "Arbitrary Viewer".
 *
 */
class INTERACTIVESLICEVIEWER_API InteractiveSliceViewerExtension : public camitk::ViewerExtension {
    Q_OBJECT
    Q_INTERFACES(camitk::ViewerExtension);
    Q_PLUGIN_METADATA(IID "fr.imag.camitk.sdk.viewer.interactivesliceviewerextension")

public:
    /// Constructor
    InteractiveSliceViewerExtension() = default;

    /// Destructor
    virtual ~InteractiveSliceViewerExtension() = default;

    /// Method returning the viewer extension name
    virtual QString getName() override {
        return "Interactive Slice Viewer Extension";
    };

    /// Method returning the viewer extension description
    virtual QString getDescription() override {
        return "Interactive slice viewers are 2D viewers that show one specific image slice or 2D images";
    };

    /// initialize all the viewers
    virtual void init() override;

};

#endif // INTERACTIVESLICEVIEWEREXTENSION_H



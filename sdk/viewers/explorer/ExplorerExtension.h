/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef EXPLOREREXTENSION_H
#define EXPLOREREXTENSION_H

#include "ExplorerAPI.h"

//-- from CamiTK Core
#include <ViewerExtension.h>

/**
 * @ingroup group_sdk_libraries_core_explorer
 *
 * @brief Manage the Explorer viewer, instanciates and register the default "Explorer" instance.
 *
 */
class EXPLORER_API ExplorerExtension : public camitk::ViewerExtension {
    Q_OBJECT
    Q_INTERFACES(camitk::ViewerExtension);
    Q_PLUGIN_METADATA(IID "fr.imag.camitk.sdk.viewer.explorerextension")

public:
    /// Constructor
    ExplorerExtension() = default;

    /// Destructor
    virtual ~ExplorerExtension() = default;

    /// Method returning the viewer extension name
    virtual QString getName() override {
        return "Explorer Extension";
    };

    /// Method returning the viewer extension description
    virtual QString getDescription() override {
        return "The Explorer viewer shows all the instantiated components";
    };

    /// initialize all the viewers
    virtual void init() override;

};

#endif // EXPLOREREXTENSION_H



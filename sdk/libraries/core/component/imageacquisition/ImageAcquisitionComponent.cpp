/*****************************************************************************
* $CAMITK_LICENCE_BEGIN$
*
* CamiTK - Computer Assisted Medical Intervention ToolKit
* (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
*
* Visit http://camitk.imag.fr for more information
*
* This file is part of CamiTK.
*
* CamiTK is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License version 3
* only, as published by the Free Software Foundation.
*
* CamiTK is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License version 3 for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
*
* $CAMITK_LICENCE_END$
****************************************************************************/


#include "ImageAcquisitionComponent.h"

namespace camitk {

// -------------------- constructor --------------------
ImageAcquisitionComponent::ImageAcquisitionComponent(Component* comp, const QString& file) : Component(comp, file) {

    workingImageComponent2D = nullptr;
    workingImageComponent3D = nullptr;

    imaging2D = false;
    imaging3D = false;
}

// -------------------- constructor --------------------
ImageAcquisitionComponent::ImageAcquisitionComponent(const QString& file, const QString& name) : Component(file, name) {
    workingImageComponent2D = nullptr;
    workingImageComponent3D = nullptr;

    imaging2D = false;
    imaging3D = false;
}

//---------------- setImageComponent2D ----------------
void ImageAcquisitionComponent::setImageComponent2D(ImageComponent* input) {
    workingImageComponent2D = input;
}

//---------------- getImageComponent2D ----------------
ImageComponent* ImageAcquisitionComponent::getImageComponent2D() {
    return workingImageComponent2D;
}

//---------------- setImageComponent3D ----------------
void ImageAcquisitionComponent::setImageComponent3D(ImageComponent* input) {
    workingImageComponent3D = input;
}

//---------------- getImageComponent3D ----------------
ImageComponent* ImageAcquisitionComponent::getImageComponent3D() {
    return workingImageComponent3D;
}

//-------------------- isImaging2D --------------------
bool ImageAcquisitionComponent::isImaging2D() {
    return imaging2D;
}

//-------------------- isImaging3D --------------------
bool ImageAcquisitionComponent::isImaging3D() {
    return imaging3D;
}

//-------------------- startImaging2D --------------------
void camitk::ImageAcquisitionComponent::startImaging2D() {}

//-------------------- stopImaging2D --------------------
void camitk::ImageAcquisitionComponent::stopImaging2D() {}

//-------------------- startImaging3D --------------------
void camitk::ImageAcquisitionComponent::startImaging3D() {}

//-------------------- stopImaging3D --------------------
void camitk::ImageAcquisitionComponent:: stopImaging3D() {}

//-------------------- singleAcquisition2D --------------------
void camitk::ImageAcquisitionComponent::singleAcquisition2D() {}

//-------------------- singleAcquisition3D --------------------
void camitk::ImageAcquisitionComponent::singleAcquisition3D() {}

}

/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef IMAGEACQUISITION_COMPONENT_EXTENSION_H
#define IMAGEACQUISITION_COMPONENT_EXTENSION_H

// -- CamiTK Core stuff
#include "CamiTKAPI.h"
#include "ImageAcquisitionComponent.h"
#include "ComponentExtension.h"

//-- Qt Stuff
#include <QObject>

namespace camitk {

/**
  * @brief
  * This class describes what is a generic Image Acquisition Component extension.
  * To add a ImageAcquisitionComponentExtension write a new class that inherits from this class.
  * Do not forget to add the dependency to the library in your CMakeList (imageacquisitioninterface)
  *
  * This class derives from ComponentExtension and that is why a subclass of ImageAcquisitionComponentExtension
  * have to describe methods from ComponentExtension as well.
  *
  * The following methods HAVE to be redefined in your subclass:
  * - getName() (from ComponentExtension)
  * - getDescription() (from ComponentExtension)
  * - getFileExtensions() (from ComponentExtension)
  * - open() (from ComponentExtension)
  * - getInstance() (from ImageAcquisitionComponentExtension)
  *
  * \note
  * getInstance() method should just return a new instance of your ImageAcquisitionComponent subclass.
  *
  * \note
  * ImageAcquisitionComponentExtension works as a factory.
  * - getImagerList() automatically looks for all ImageAcquisitionComponentExtension subclass and add
  * them automatically to the list of possible instance produced by the factory.
  * This way a new device can be added on the fly by adding new extensions.
  *
  * The following methods can also be redefined (inherited from Component Extension):
  * - save: saving from a Component to one of the managed format
  * - hasDataDirectory: for directory type extension
  */
class CAMITK_API ImageAcquisitionComponentExtension : public ComponentExtension {

public:

    /// constructor
    ImageAcquisitionComponentExtension();

    /// static method to retrieve all classes that inherit from an ImageAcquisitionComponent
    static QStringList getImagerList();

    /// static method to create a generic instance of ImageAcquisitionComponent (such as a factory method)
    static ImageAcquisitionComponent* getInstance(QString imagerName, Component* parent, QString name);

    /// method to redefine in children classes to create an concrete instance of an ImageAcquisitionComponent
    virtual ImageAcquisitionComponent* getInstance(Component* parent, QString name) = 0;

protected:
    /// destructor
    ~ImageAcquisitionComponentExtension() = default;
};
}
#endif //IMAGEACQUISITION_COMPONENT_EXTENSION_H

/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef COMPONENT_EXTENSION_H
#define COMPONENT_EXTENSION_H

// -- Qt stuff
#include <QObject>

// -- CamiTK Core stuff
#include "CamiTKAPI.h"
#include "AbortException.h"

namespace camitk {
class Component;
/**
  * @ingroup group_sdk_libraries_core_component
  *
  * @brief
  * This class describes what is a generic Component extension.
  * To add a ComponentExtension to CamiTK core, write a new class that inherits from this class.
  *
  * There are two types of component extension: the classical one manages (mime type) file extension,
  * the other one manages all files in a given directory (e.g. Dicom images). For the latter you have
  * to redefine hasDataDirectory().
  *
  * The following methods HAVE to be redefined in your subclass:
  * - getName
  * - getDescription
  * - getFileExtensions
  * - open
  *
  * The following methods can be redefined:
  * - save: saving from a Component to one of the managed format
  * - hasDataDirectory: for directory type extension
  */
class CAMITK_API ComponentExtension : public QObject {

protected:

    /// protected constructor, \note never directly instantiate a ComponentExtension, use loadExtension(...) instead!
    ComponentExtension();

public:

    /// protected destructor, \note never directly delete a ComponentExtension, use UnloadExtension(...) instead!
    ~ComponentExtension() override = default;

    /// @name ComponentExtension plugin interface methods
    /// @{
    /// get the plugin name
    virtual QString getName() const = 0;

    /// get the plugin description
    virtual QString getDescription() const = 0;

    /// get the list of managed extensions (each file with an extension in the list can be loaded by this Component)
    virtual QStringList getFileExtensions() const = 0;

    /** get a new instance from data stored in a file (this is the most important method to redefine in your subclass)
     *
     *  This method may throw an AbortException if a problem occurs.
     *
     *  \note The parameter is a filename with an absolute path (from Qt's QFileInfo::absoluteFilePath method):
     *   On Unix (including Mac OS) this will always begin with the root, '/', directory.
     *   On Windows this will always begin 'D:/' where D is a drive letter, except for network shares that are not mapped to a drive letter,
     *   in which case the path will begin '//sharename/'
     */
    virtual Component* open(const QString&) = 0;

    /** save a given Component (does not have to be top-level) into one of the currently managed format (check
     *  the component QFileInfo(component->getFileName()).completeSuffix().
     *
     *  Redefine this method to extract all needed data/information from the Geometry or BitMap representation
     *  in order to export a given component to one of the file extension managed by this component extension.
     *
     *  \note this will enable to export to one of the managed filename extension at the CamiTK level (i.e. if
     *  you write this method, any compatible component can be saved to your managed format!
     *
     *  \note this method is called by CamiTK only if the filename extension is managed by this component extension.
     *  There should be no need to check it in the method.
     *
     *  The default behaviour is a "not implemented yet" message box.
     *
     *  @return false if the operation was not performed properly or not performed at all.
     */
    virtual bool save(Component* component) const;

    /// return true if this component manages directory instead of individual files (e.g. Dicom series are stored in directories, not files)
    virtual bool hasDataDirectory() const {
        return false;
    }

    /// get the file path (location of the .dll/.so/.dylib) of this plugin
    QString getLocation() const {
        return dynamicLibraryFileName;
    }

    /// set the file path (once loaded as a dynamic library)
    void setLocation(const QString loc) {
        dynamicLibraryFileName = loc;
    }

    /// Load, for the selected langage (asked to the Application), the associated .qm file
    void initResources();

    /// @}


private:
    /// the shared lib (.so, .dll or .dylib) used to instantiate the ComponentExtension subclass instance
    QString dynamicLibraryFileName;

    /// the autoload state
    bool autoload;
};



}
// -------------------- declare the interface for QPluginLoader --------------------
Q_DECLARE_INTERFACE(camitk::ComponentExtension, "TIMC-IMAG.ComponentExtension/2.1") //TODO use variable from CMake?


#endif //COMPONENT_EXTENSION_H

/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef MESH_DATA_VIEW_H
#define MESH_DATA_VIEW_H

#include <QTableView>
#include <QMouseEvent>

namespace camitk {

class MeshComponent;

/**
 * Qt view for mesh data.
 * This class use the Qt model/view design. This is the view.
 */
class MeshDataView : public QTableView {
    Q_OBJECT

public :
    /// by default extent cells to fit
    MeshDataView(QWidget* parent = nullptr);

protected :
    /// override to clear the selection if the user click outside the lines
    void mousePressEvent(QMouseEvent* event) override;

};

}

#endif

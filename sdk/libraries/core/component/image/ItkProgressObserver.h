/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef ITK_PROGRESS_OBSERVER
#define ITK_PROGRESS_OBSERVER

// -- Core image component stuff
#include "Application.h"

// -- itk stuff
#include <itkCommand.h>
#include <itkProcessObject.h>

namespace camitk {

/**
 *  @ingroup group_sdk_libraries_core_component_image
 *  @ingroup ImageFilters
 *
 *  @brief
 *  Allows showing a progress bar when using ITK methods.
 *
 */
class ItkProgressObserver : public itk::Command {
public:
    typedef ItkProgressObserver  Self;
    typedef itk::Command    Superclass;
    typedef itk::SmartPointer<Self> Pointer;
    itkNewMacro(Self);

protected:
    ItkProgressObserver();

public:
    void Execute(itk::Object* caller, const itk::EventObject& event);
    void Execute(const itk::Object* object, const itk::EventObject& event);
    void Reset();
    void SetCoef(double coef);
    void SetStartValue(double startValue);

private :
    double compteur;
    double coef;
    double startValue;
};


inline ItkProgressObserver::ItkProgressObserver() {
    coef = 1.0;
    compteur = 0.0;
    startValue = 0.0;
    // update the progress bar if there is one!
    Application::setProgressBarValue(compteur);
}

inline void ItkProgressObserver::Execute(itk::Object* caller, const itk::EventObject& event) {
    Execute((const itk::Object*)caller, event);
}

inline void ItkProgressObserver::Execute(const itk::Object* object, const itk::EventObject& event) {
    const itk::ProcessObject* filter =
        dynamic_cast< const itk::ProcessObject* >(object);
    if (! itk::ProgressEvent().CheckEvent(&event)) {
        return;
    }
    compteur = filter->GetProgress();
    // update the progress bar if there is one!
    Application::setProgressBarValue(startValue + compteur * coef);
}

inline void ItkProgressObserver::Reset() {
    compteur = 0.0;
    startValue = 0.0;
    // update the progress bar if there is one!
    Application::setProgressBarValue(compteur);
}

inline void ItkProgressObserver::SetCoef(double coef) {
    this->coef = coef;
}

inline void ItkProgressObserver::SetStartValue(double startValue) {
    this->startValue = startValue;
}

}

#endif //ITK_PROGRESS_OBSERVER

/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// -- Core image component stuff
#include "SingleImageComponent.h"
#include "ImageComponent.h"

// -- Core stuff
#include "Frame.h"
#include "Log.h"

// -- VTK stuff
// disable warning generated by clang about the surrounded headers
#include <CamiTKDisableWarnings>
#include <vtkProperty.h>
#include <CamiTKReEnableWarnings>

#include <vtkUnstructuredGrid.h>
#include <vtkImageClip.h>
#include <vtkImageChangeInformation.h>
#include <vtkMatrix4x4.h>

#include <cmath>

namespace camitk {
// -------------------- constructor  --------------------
SingleImageComponent::SingleImageComponent(Component* parentComponent, Slice::SliceOrientation sliceOrientation, const QString& name, vtkSmartPointer<vtkWindowLevelLookupTable> lut)
    : Component(parentComponent, name, Component::SLICE) {

    this->sliceOrientation = sliceOrientation;

    this->lut = lut;

    // set my parent image as my parent frame
    this->setParentFrame(parentComponent->getFrame());

    // build the slice 3D
    initRepresentation();
}

// -------------------- setSelected --------------------
void SingleImageComponent::setSelected(const bool b, const bool r) {
    dynamic_cast<ImageComponent*>(getParent())->setSelected(b, false);
}

// -------------------- singleImageSelected --------------------
void SingleImageComponent::singleImageSelected(const bool b) {
    Component::setSelected(b, false);
}

// -------------------- getViewSliceIn3D --------------------
bool SingleImageComponent::getViewSliceIn3D() const {
    CAMITK_WARNING(tr("getViewSliceIn3D() method is now deprecated and will be removed from CamiTK API, please use getVisibility(\"3D Viewer\") instead"))

    return getVisibility("3D Viewer");
}

// -------------------- setViewSliceIn3D --------------------
void SingleImageComponent::setViewSliceIn3D(bool toggle) {
    CAMITK_WARNING(tr("setViewSliceIn3D(bool) method is now deprecated and will be removed from CamiTK API, please use setVisibility(\"3D Viewer\",bool) instead"))

    setVisibility("3D Viewer", toggle);
}

// -------------------- initRepresentation --------------------
void SingleImageComponent::initRepresentation() {
    // initialize Slice
    mySlice = new Slice(dynamic_cast<ImageComponent*>(getParentComponent())->getImageData(), sliceOrientation, lut);

    // initialize 3D image actor to the proper position/orientation in the world reference frame
    mySlice->setImageWorldTransform(getTransformFromWorld());

    switch (sliceOrientation) {
        case Slice::AXIAL:
        case Slice::AXIAL_NEURO:
            setVisibility("Axial Viewer", true);
            break;
        case Slice::CORONAL:
            setVisibility("Coronal Viewer", true);
            break;
        case Slice::SAGITTAL:
            setVisibility("Sagittal Viewer", true);
            break;
        case Slice::ARBITRARY:
            setVisibility("Arbitrary Viewer", true);
            break;
    }
    setVisibility("3D Viewer", false);
}

// ---------------------- pixelPicked  ----------------------------
void SingleImageComponent::pixelPicked(double i, double j, double k) {
    ((ImageComponent*)getParent())->pixelPicked(i, j, k);
}

// All methods below are rewritten to forbid any transform from the ImageComponent,
// unless this is a arbitrary slice

// -------------------- setTransform --------------------
void SingleImageComponent::setTransform(vtkSmartPointer<vtkTransform> transform) {
    // Do nothing, my parent (the ImageComponent) should do that globally
}

// -------------------- resetTransform --------------------
void SingleImageComponent::resetTransform() {
    // Do nothing, my parent (the ImageComponent) should do that globally
}

// -------------------- rotate --------------------
void SingleImageComponent::rotate(double aroundX, double aroundY, double aroundZ) {
    // Do nothing, my parent (the ImageComponent) should do that globally
}

// -------------------- rotateVTK --------------------
void SingleImageComponent::rotateVTK(double aroundX, double aroundY, double aroundZ) {
    // Do nothing, my parent (the ImageComponent) should do that globally
}

// -------------------- setTransformRotation --------------------
void SingleImageComponent::setTransformRotation(double angleX, double angleY, double angleZ) {
    // Do nothing, my parent (the ImageComponent) should do that globally
}

// -------------------- setTransformRotationVTK --------------------
void SingleImageComponent::setTransformRotationVTK(double aroundX, double aroundY, double aroundZ) {
    // Do nothing, my parent (the ImageComponent) should do that globally
}

// -------------------- setTransformTranslation --------------------
void SingleImageComponent::setTransformTranslation(double x, double y, double z) {
    // Do nothing, my parent (the ImageComponent) should do that globally
}

// -------------------- setTransformTranslationVTK --------------------
void SingleImageComponent::setTransformTranslationVTK(double x, double y, double z) {
    // Do nothing, my parent (the ImageComponent) should do that globally
}

// -------------------- translate --------------------
void SingleImageComponent::translate(double x, double y, double z) {
    // Do nothing, my parent (the ImageComponent) should do that globally
}

// -------------------- getSliceOrientation --------------------
Slice::SliceOrientation SingleImageComponent::getSliceOrientation() {
    return sliceOrientation;
}

}


/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef ACTIONWIDGET_H
#define ACTIONWIDGET_H

// -- Core stuff
#include "CamiTKAPI.h"
#include "Action.h"

//-- Qt
#include <QFrame>
#include <QLabel>
#include <QScrollArea>
#include <QTextEdit>

namespace camitk {

// foward declaration
class ObjectController;

/**
 * @ingroup group_sdk_libraries_core_action
 *
 * @brief
 * Build a default widget for a given action using its Qt properties.
*  It should be good enough in most of the case.
*  The default widget contains a description, a reminder of the target component names,
*  and an applyable/revertable ObjectController that allows you to edit/modify properties
*  of the action.
*
*  The button frame contains an "Apply" and a "Revert" button. The Apply button updates
*  the action properties using the value entered by the user in the GUI. I.e., its clicked()
*  signal is connected the the action apply() slot.
*  The revert button resets the values in GUI to the initial values (all modification in the GUI
*  are cancelled).
*  You can easily show or hide the button frame using setButtonVisibility()
*
*  update() updates the widget using the actual/current properties and target of the action.
*  This method should be called by an action in the overriden getWidget method.
*  A typical getWidget() method of an action should use the lazy instantiation pattern to instantiate
*  ActionWidget the first time it is called, and call the ActionWidget instance update() method
*  for any subsequent calls.
*
*  If you cannot wait for the user to click on the apply button (or if this button is hidden)
*  and you want the value of the action's property/parameter to be updated everytime
*  there is a change in the GUI, call setAutoUpdateProperty(true)
*  (default is false);
*
*/
class CAMITK_API ActionWidget : public QFrame {
public:
    /// create a default action widget for the given action
    ActionWidget(Action*);

    ~ActionWidget() override;

    /// update display (target and property controller)
    void update();

    /// if false then the apply/revert buttons are shown
    void setButtonVisibility(bool);

    /// defines whether the name of the widget is visible or not
    /// true by default
    void setNameVisibility(bool visible);

    /// defines whether the description of the widget is visible or not
    /// true by default
    void setDescriptionVisibility(bool visible);

    /// automatically update the action properties whenever there is a change in the GUI
    void setAutoUpdateProperty(bool autoUpdate);

protected:
    /// return a text with the target component names followed by their class names
    QString getTargetLabel();

private:
    /// the managed action
    Action* myAction;

    /// the object controller with the properties of the default widget
    ObjectController* objectController;

    /// the target list label
    QLabel* targetLabel;

    /// the action name displayed in the widget
    QLabel* actionNameLabel;

    /// the description of the action displayed in the widget
    QTextEdit* descriptionTextEdit;

    /// this frame contains the Apply/Revert buttons and can be hide using setButtonVisibility()
    QFrame* buttonFrame;

};

}
#endif // ACTIONWIDGET_H

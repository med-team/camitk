/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// -- Core stuff
#include "ActionExtension.h"
#include "Action.h"
#include "Application.h"
#include "Core.h"
#include "Log.h"

// -- Qt stuff
#include <QDir>

namespace camitk {

// -------------------- constructor --------------------
ActionExtension::ActionExtension() {
    translator = nullptr;
}

// -------------------- destructor --------------------
ActionExtension::~ActionExtension() {
    while (!actions.empty()) {
        Action* toDelete = actions.takeFirst();

        // do not delete the "Quit" action: it is the action that triggers this delete!
        if (toDelete->getName() != "Quit") {
            delete toDelete;
        }
    }

    //delete internationalization instance
    if (translator) {
        delete translator;
    }
}

// -------------------- setLocation --------------------
void ActionExtension::setLocation(const QString loc) {
    dynamicLibraryFileName = loc;
}

// -------------------- getLocation --------------------
QString ActionExtension::getLocation() const {
    return dynamicLibraryFileName;
}

// -------------------- initResources --------------------
void ActionExtension::initResources() {
    // Get the selected language
    QString selectedLanguage = Application::getSelectedLanguage();

    // if a language is defined, then try to load the translation file
    if (!selectedLanguage.isEmpty()) {
        QString actionExtensionDirName = QDir(this->getLocation()).dirName();
        // remove any occurence of "-debug.dll" or ".so" or ".dylib" on the extension file name
        actionExtensionDirName.remove("-debug.dll").remove(".so").remove(".dylib");
        QString languageFile = ":/translate_" + actionExtensionDirName + "/translate/translate_" + selectedLanguage + ".qm";
        translator = new QTranslator();

        if (translator->load(languageFile)) {
            QCoreApplication::installTranslator(translator);
        }
        else {
            CAMITK_INFO(tr("Cannot load resource file: %1").arg(languageFile))
        }
    }
}

// -------------------- registerAction --------------------
void ActionExtension::registerAction(Action* action) {
    // simply add the action in the list
    actions.append(action);
}

// -------------------- getActions --------------------
const camitk::ActionList& ActionExtension::getActions() {
    return actions;
}


}

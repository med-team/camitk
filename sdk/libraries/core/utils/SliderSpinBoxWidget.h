/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef SLIDER_SPIN_BOX_WIDGET_H
#define SLIDER_SPIN_BOX_WIDGET_H

// -- Core stuff
#include "CamiTKAPI.h"

// -- QT stuff
#include <QSpinBox>
#include <QSlider>


namespace camitk {
/**
  * @ingroup group_sdk_libraries_core_utils
  *
  * @brief
  * A utility class to have QSpinBox and QSlider synchronized.
  * see for example InteractiveViewer (when it is used as 2D viewer)
  */
class CAMITK_API SliderSpinBoxWidget : public QWidget {
    Q_OBJECT

public:
    /// constructor (have to give the parent widget)
    SliderSpinBoxWidget(QWidget* parent = nullptr);

    /// destructor
    ~SliderSpinBoxWidget() override = default;

    /// get the current value
    int getValue() const;

    /// set the value (force)
    void setValue(int value);

    /// set the range
    void setRange(int min, int max);

    /// add 1 to the value
    void addSingleStep();

    /// subtract 1 to the value
    void subSingleStep();

    /// add a larger number of natural steps to the value, for information exact number of added step is min(1,(max-min)/10)
    void addPageStep();

    /// subtract a larger number of natural steps to the value, for information exact number of added step is min(1,(max-min)/10)
    void subPageStep();

    /// the QSpinBox instance
    QSpinBox* getSpinBox() {
        return spinBox;
    }

    /// the QSlider instance
    QSlider* getSlider() {
        return slider;
    }


signals:
    /** if the user change the value either in the slider or the spinBox,
     * this signal is emitted with the new value as parameter.
     */
    void valueChanged(int);

protected slots:
    /// any change in the spinBox value is connected to this slot (update the slider)
    void spinBoxValueChanged(int);

    /// any change in the slider value is connected to this slot (update the spinBox)
    void sliderValueChanged(int);

private:
    /// the QSpinBox instance
    QSpinBox* spinBox;

    /// the QSlider instance
    QSlider* slider;

    /// update the spinBox value (block signals)
    void updateSpinBoxValue(int);

    /// update the slider value (block signals)
    void updateSliderValue(int);

};

}

#endif

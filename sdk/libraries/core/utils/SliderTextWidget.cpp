/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// -- Core stuff
#include "SliderTextWidget.h"

// -- QT stuff
#include <QVBoxLayout>
#include <QHBoxLayout>


namespace camitk {
//------------ Constructor ------------------------
SliderTextWidget::SliderTextWidget(QWidget* parent, Qt::WindowFlags fl) : QWidget(parent, fl) {

    auto* vLayout = new QVBoxLayout(this);

    // the first line has 2 widgets -> a HBoxLayout is needed!
    auto* hLayout = new QHBoxLayout();
    vLayout->addLayout(hLayout);
    label = new QLabel;
    hLayout->addWidget(label);
    lineEdit = new QLineEdit;
    hLayout->addWidget(lineEdit);

    slider = new QSlider(Qt::Horizontal);
    vLayout->addWidget(slider);

    // connections
    connect(slider, SIGNAL(valueChanged(int)), this, SLOT(valueChanged(int)));
    connect(lineEdit, SIGNAL(textChanged(QString)), this, SLOT(textModified(QString)));
    connect(lineEdit, SIGNAL(returnPressed()), this, SLOT(returnPressed()));

    // initialize with parameters given in uic_SliderTextWidget
    setName(objectName());
    value = 50.0;
    min = 0.0;
    max = 100.0;
    QPalette p = lineEdit->palette();
    bgColor = p.color(lineEdit->backgroundRole());

    // block the signals, only if not already blocked to set the slider range
    bool alreadyBlocked = signalsBlocked();
    if (!alreadyBlocked) {
        blockSignals(true);
    }
    slider->setRange(0, 100);
    slider->setSingleStep(1);
    slider->setPageStep(10);
    if (!alreadyBlocked) {
        blockSignals(false);
    }

    updateLineEdit();
    updateSlider();
}

//------------ Destructor ------------------------
SliderTextWidget::~SliderTextWidget() {
    // no need to delete children widgets, Qt does it all for us
}

//------------ setNamethis ------------------------
void SliderTextWidget::setName(const QString& name) {
    label->setText(name);
}


//------------ setValue ------------------------
void SliderTextWidget::setValue(double val, bool emitValueChanged) {
    value = val;
    updateLineEdit();
    updateSlider();
    if (emitValueChanged) {
        emit valueChanged();
    }
}


//------------ init ------------------------
void SliderTextWidget::init(double min, double max, double value) {
    this->min = min;
    this->max = max;
    this->value = value;
    updateSlider();
    updateLineEdit();
}

//------------ valueChanged ------------------------
void SliderTextWidget::valueChanged(int val) {
    value = sliderToValue(val);
    updateLineEdit();
    emit valueChanged();
}

//------------ textModified ------------------------
void SliderTextWidget::textModified(QString) {
    QPalette p;
    p.setColor(lineEdit->backgroundRole(), QColor(255, 220, 168));
    lineEdit->setPalette(p);
}

//------------ returnPressed ------------------------
void SliderTextWidget::returnPressed() {
    QPalette p;
    p.setColor(lineEdit->backgroundRole(), bgColor);
    lineEdit->setPalette(p);

    value = lineEdit->text().toDouble();

    // update min/max if needed
    if (value < min) {
        min = value;
    }
    if (value > max) {
        max = value;
    }

    updateSlider();
    emit valueChanged();
}

//------------ updateLineEdit ------------------------
void SliderTextWidget::updateLineEdit() {
    // update line edit text
    // block the signals, only if not already blocked
    bool alreadyBlocked = lineEdit->signalsBlocked();
    if (!alreadyBlocked) {
        lineEdit->blockSignals(true);
    }
    lineEdit->setText(QString::number(value));
    if (!alreadyBlocked) {
        lineEdit->blockSignals(false);
    }
}

//------------ updateSlider ------------------------
void SliderTextWidget::updateSlider() {
    int v = valueToSlider(value);

    bool alreadyBlocked = slider->signalsBlocked();
    if (!alreadyBlocked) {
        slider->blockSignals(true);
    }
    // update slider value
    slider->setValue(v);
    if (!alreadyBlocked) {
        slider->blockSignals(false);
    }
}


//------------ sliderToValue ------------------------
double SliderTextWidget::sliderToValue(const int s) {
    return (min + double(s) * (max - min) / 100.0);
}

//------------ doubleToSlider ------------------------
int SliderTextWidget::valueToSlider(const double v) {
    return (int) 100.0 * (v - min) / (max - min);
}


}


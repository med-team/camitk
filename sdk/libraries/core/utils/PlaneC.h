/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef PLANE_C_H
#define PLANE_C_H

// -- VTK stuff
#include <vtkSmartPointer.h>

// -- VTK stuff classes
class vtkPlaneSource;
class vtkPolyDataMapper;
class vtkActor;
class vtkProperty;


namespace camitk {
/**
 * @ingroup group_sdk_libraries_core_utils
 *
 * @brief
 * This class allows you to transform a plane(translation,rotation around an axis)
 * and visualize it.
 *
 *  This is a pure vtk wrapper class (no Core stuff).
 *
 *
 */

class  PlaneC {
public:
    /** a planeC could represent a plane of a bounding box */
    enum PlaneCType {
        X_MIN, /**< the plane's normal is in X direction, the plane is at the min x */
        Y_MIN, /**< the plane's normal is in Y direction, the plane is at the min y */
        Z_MIN, /**< the plane's normal is in Z direction, the plane is at the min z */
        X_MAX, /**< the plane's normal is in X direction, the plane is at the max x */
        Y_MAX, /**< the plane's normal is in Y direction, the plane is at the max y */
        Z_MAX, /**< the plane's normal is in Z direction, the plane is at the max z */
        UNDEFINED, /**< the plane's normal is not defined yet */
    };
    /**Constructor by default*/
    PlaneC();
    /**Destructor*/
    ~PlaneC();

    /** Initialize the plane at a particular place of a bounding box
      * @param type the type of the plane (see enum PlaneCType)
      * @param bounds an array of 6 doubles representing the bounding box (the six values have
       to be put in the correct order : xmin, ymin, zmin, xmax, ymax, zmax)
      */
    void init(PlaneCType type, double* bounds);
    /**Visualization of the plane in wireframe*/
    void setVisuPlane();
    /**Visualization of the plane in red tone */
    void setVisuActivePlane();
    /**Visualization of the plane in blue tone */
    void setVisuActivePlaneOff();
    /**Delete the plane from the View*/
    void clear();
    /**Translate the plane on XAxis*/
    void translatePlaneX();
    /**Translate the plane on YAxis*/
    void translatePlaneY();
    /**Translate the plane on ZAxis*/
    void translatePlaneZ();
    /**Rotate the plane around Axe1*/
    void rotationAxe1();
    /**Rotate the plane around Axe2*/
    void rotationAxe2();
    /**get the actor of the plane*/
    vtkSmartPointer<vtkActor> getActor();

    /// set the transformation  (translation, rot1 and rot2)and convert percentage to real value
    void setTransfoPercentToRealValue(double* tab, double, double);
    /// set the translation value of the plane max to 100%
    void setTranslationMaxInPercent(double);
    /// get the transformation (translation, rot1 and rot2) in percentage
    void getTransformationInPercent(double*, double*, double*);
    /// Initilize the origin of the plane
    void setOrigin(double, double, double);
    /** The local x-axis vector will be set to (PointX - Origin) .
     The normal of the plane is defined by the cross product (x-axis, y-axis)
    */
    void setXAxisPoint(double, double, double);
    /** The local y-axis vector will be set to (PointY - Origin) .
     The normal of the plane is defined by the cross product (x-axis, y-axis)
    */
    void setYAxisPoint(double, double, double);
    ///get the origin of the plane
    void getOrigin(double* tab);
    ///get the normal of the plane
    void getNormal(double* tab);



private:
    vtkSmartPointer<vtkPlaneSource> plane;
    vtkSmartPointer<vtkPolyDataMapper> planeMapper;
    vtkSmartPointer<vtkActor> planeActor;
    vtkSmartPointer<vtkProperty> aProp;

    ///store the transformation
    double transformation[3];
    ///store the translation in percentage
    double translation;
    /// difference between current and initial position
    double angle1;
    double angle2;
    double translation1;
};

}

#endif

/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef ABORT_EXCEPTION_H
#define ABORT_EXCEPTION_H

// -- stl stuff
#include <string>
#include <exception>


namespace camitk {
/**
 * @ingroup group_sdk_libraries_core_utils
 *
 * @brief
 * Exception class to handle abortion in component instantiation.
 * Particularly useful to handle constructor's abortion.
 *
 */
class AbortException : public std::exception  {
public:
    /// default constructor: give the reason for the exception
    AbortException(std::string s) {
        reason = s;
    }

    ~AbortException() = default;

    /// get the detailed reason from the exception
    const char* what() const noexcept {
        return reason.c_str();
    }

private:
    std::string reason;
};

}

#endif

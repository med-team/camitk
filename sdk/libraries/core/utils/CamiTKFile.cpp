#include "CamiTKFile.h"
#include "Log.h"

#include <QVariant>
#include <QMap>
#include <QList>
#include <QString>
#include <QJsonDocument>
#include <QDateTime>
#include <QFile>
#include <QIODevice>
#include <QUrl>


namespace camitk {

const char* CamiTKFile::version = "camitk-5.2";
const int CamiTKFile::maxFileSize = 10000000; // Max 10 MB

CamiTKFile CamiTKFile::load(QString filepath) {
    CamiTKFile camiTKOut;
    QFile fileIn(filepath);
    QJsonParseError jsonParseError;

    if (fileIn.open(QIODevice::ReadOnly)) {
        // for security reason do not try to load files that are larger than the limit.
        if (fileIn.size() < CamiTKFile::maxFileSize) {
            // read first the file as byte array
            QByteArray rawJSon = fileIn.readAll();
            QJsonDocument jsonDoc = QJsonDocument::fromJson(rawJSon, &jsonParseError);
            if (jsonDoc.isNull()) {
                // extract the string from the error offset to the next return carriage
                QString jsonErrorData(rawJSon.mid(jsonParseError.offset, rawJSon.indexOf("\n", jsonParseError.offset) - jsonParseError.offset));
                int lineNr = rawJSon.left(jsonParseError.offset).count('\n');
                QString errorLine(rawJSon.split('\n')[lineNr - 1]);
                CAMITK_ERROR_ALT(
                    QObject::tr("Error reading json file %1:\n%2:\n%3\nOn line %4:\n%5")
                    .arg(filepath)
                    .arg(jsonParseError.errorString())
                    .arg(jsonErrorData)
                    .arg(lineNr)
                    .arg(errorLine));
                camiTKOut.content = {};
            }
            else {
                QVariantMap fullContent = jsonDoc.toVariant().toMap();

                if (fullContent.contains("camitk")) {
                    camiTKOut.content = fullContent.value("camitk").toMap();
                }
                else {
                    camiTKOut.content = {}; // Invalid (no "version")
                }
            }

        }
        else {
            CAMITK_WARNING_ALT(QObject::tr("Could not load CamiTK file \"%1\", it is over the maximum size limit (%2 > %3 bytes)! ").arg(filepath).arg(fileIn.size()).arg(CamiTKFile::maxFileSize));
        }
    }
    return camiTKOut;
}

CamiTKFile CamiTKFile::load(QUrl url) {
    if (url.isLocalFile()) { // file://
        return CamiTKFile::load(url.toLocalFile());
    }
    else { // Nothing else is supported (zip:// http:// ...)
        return CamiTKFile();
    }
}

CamiTKFile::CamiTKFile() {
    // Init header data
    content = {};
    setCurrentVersion();
    setCurrentTimestamp();

}

bool CamiTKFile::save(QString filepath) {
    QFile outFile(filepath);
    if (outFile.open(QIODevice::WriteOnly)) {
        setCurrentTimestamp();
        setCurrentVersion();
        QVariantMap fullContent = {{"camitk", content},};
        QJsonDocument jsonContent = QJsonDocument::fromVariant(fullContent);
        outFile.write(jsonContent.toJson(QJsonDocument::Indented));
        outFile.close();
        return true;
    }
    else {
        return false;
    }
}

bool CamiTKFile::save(QUrl url) {
    setCurrentTimestamp();
    if (url.isLocalFile()) {
        return save(url.toLocalFile());
    }
    else {
        return false; // Not supported
    }

}

bool CamiTKFile::isValid() {
    return content.contains("version");
}

QString CamiTKFile::getVersion() {
    return content["version"].toString();
}

QDateTime CamiTKFile::getTimestamp() {
    return QDateTime::fromString(content["timestamp"].toString(), Qt::ISODate);
}

void CamiTKFile::setCurrentVersion() {
    content["version"] = QString::fromStdString(CamiTKFile::version);
}

void CamiTKFile::setCurrentTimestamp() {
    content["timestamp"] = QDateTime::currentDateTime().toString(Qt::ISODate);
}

void CamiTKFile::addContent(QString key, const QVariant value) {
    content[key] = value;
}

bool CamiTKFile::hasContent(QString key) {
    return content.contains(key);
}

QVariant CamiTKFile::getContent(QString key) {
    return content.value(key);
}

} // namespace camitk

/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef VTKINTERACTORSTYLEPICK_H
#define VTKINTERACTORSTYLEPICK_H

#include "CamiTKAPI.h"

// disable warning generated by clang about the surrounded headers
#include <CamiTKDisableWarnings>
#include <vtkInteractorStyle.h>
#include <CamiTKReEnableWarnings>

#include <vtkSmartPointer.h>

class vtkUnsignedCharArray;

namespace camitk {

/// Specific backward compatible interactor for CamiTK RendererWidget
/// Interactor used when we are in picking mode
///
class CAMITK_API vtkInteractorStylePick : public vtkInteractorStyle {

public:

    static vtkInteractorStylePick* New();
    vtkTypeMacro(vtkInteractorStylePick, vtkInteractorStyle);

    void PrintSelf(ostream& os, vtkIndent indent) override;

    void SetAreaPicking(bool b);

    void OnLeftButtonDown() override;

    void OnLeftButtonUp() override;

    void OnMouseMove() override;

protected:

    vtkInteractorStylePick();
    ~vtkInteractorStylePick() override = default;

    virtual void Pick();

    void RedrawRubberBand();

    int startPosition[2];
    int endPosition[2];

    int moving;

    vtkSmartPointer<vtkUnsignedCharArray> pixelArray;

    bool areaPicking;
};

}

#endif //VTKINTERACTORSTYLEPICK_H

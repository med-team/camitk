/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef VIEWER_H
#define VIEWER_H

// -- Core stuff
#include "CamiTKAPI.h"

// -- QT stuff
#include <QObject>
#include <QPixmap>

// -- QT stuff classes
class QToolBar;
class QMenu;
class QPixMap;
class QWidget;
class QDockWidget;
class QLayout;

namespace camitk {
class Component;
class ViewerExtension;
class Property;
class PropertyObject;
class ViewerDockStyle;

/**
  * @ingroup group_sdk_libraries_core_viewer
  *
  * @brief
  * Viewer is an abstract class that is the base class for all viewers.
  *
  * There are two types of viewers:
  * - the embedded viewer (their widgets are inside a given layout), this is the default type
  * - the docked viewer (their widgets are inside a dock widget).
  *
  * The type of a viewer can be changed at any time using setType(..).
  * To define where is the viewer embedded or docked use setEmbedder(..) or setDockWidget(..)
  *
  * A viewer can embed other viewers in its own widget.
  * Example of viewer are: Explorer, PropertyExplorer, MedicalImageViewer (which embed the default InteractiveGeometryViewer
  * and default InteractiveSliceViewers).
  *
  * A viewer can be added to MainWindow either in the dock or the central viewer (depending on its type).
  * MainWindow will include its menu in the "View" menu and its toolbar in the application toolbar.
  *
  * The default viewed component are set so that the viewer is updated every time the current component selection
  * is modified.
  *
  * If your viewer does not view components (for instance, in the very rare/special case it views actions),
  * then you need to call the setComponentClassNames(...) method with an empty list.
  * An empty list means that this viewer is going to be also notified/refreshed every time the
  * current selected action is changed (i.e., anytime an action is triggered)
  *
  * In the scope of an Application, a viewer should be registered if you want to use it to display component facets.
  * Registered viewers are going to be refreshed automatically when Application::refresh() is called or when a new
  * top level component is opened/closed.
  *
  * The viewer's icon is displayed inside the dock window.
  *
  * Unregistered viewers are managed independently of the CamiTK Application logic. In this case it should be managed separately
  * and it is not connected to the automatic refresh chain. This is a special (rare) use case. If you think this is what you
  * need, then you will have to programmatically/specifically manage the content of the viewer and its refresh logic.
  *
  * The viewer's property object can be used to manage the properties of the viewer (for instance background colors or other
  * display options). (camitk-imp for instance presents the viewer properties in the setting dialog)
  *
  * The viewer's menu should present the properties in a QMenu to enable the user to easily modify the viewer properties
  * (camitk-imp for instance shows the viewer menus in the application "View" menu)
  *
  * A viewer toolbar is meant to be displayed in the main window toolbar area when the viewer's widget is visible (in a Dock
  * or as the central widget). The viewer toolbar is visible by default.
  * If you don't want to show the viewer toolbar in your main window, even if the viewer's widget is visible,
  * set its visibility to false.
  *
  * ### Using viewers
  *
  * In a viewer extension, the default behaviour is to have (at least) one instance of the new Viewer inheriting class.
  * It is very easy to access this instance with Application::getViewer()
  *
  * E.g. if FooBarViewer inherits from the Viewer class, then one default instance of the class, called
  * "Foo Bar Viewer", is available when the extension is loaded.
  *
  * To get the default FooBarViewer instance, use Application::getViewer("Foo Bar Viewer").
  *
  * One simple way of checking that a viewer extension is available, is to check the return value
  * of Application::getViewer(...), e.g. some code inside a MainWindow inherited class constructor:
  *
  * ```cpp
  * #include <FooBarViewer.h>
  * #include <Log.h>
  * ...
  *
  * FooBarViewer* fooBarViewer = dynamic_cast<FooBarViewer*>(Application::getViewer("Foo Bar Viewer"));
  * if (fooBarViewer != nullptr) {
  *    // everything is ok, the FooBar viewer extension is available, add it to the right docker, not visible by default
  *    addDockViewer(Qt::RightDockWidgetArea, fooBarViewer);
  *    showDockViewer(fooBarViewer, false);
  * }
  * else {
  *     CAMITK_ERROR(tr("Cannot find \"Foo Bar Viewer\". This viewer is mandatory for running this application."))
  *     // may be you wanted to create a new instance of FooBarViewer, just see below
  * }
  * ```
  *
  * \note
  * If your action or component requires a specific viewer, you need to add the following **argument** to the
  * `camitk_extension(..)` macro of your extension's `CMakeLists.txt`:
  * ```cmake
  * camitk_extension(...
  *              NEEDS_VIEWER_EXTENSION foobarviewer
  *                  ...
  * )
  * ```
  *
  * @see Application::getViewer()
  *
  * ### Creating new viewer instances
  *
  * It is very easy to create a new instance of a given viewer type (of a loaded viewer extension): just
  * use the viewer extension factory. See Application::getNewViewer().
  *
  * The following example creates a new instance of the `InteractiveGeometryViewer` viewer called "My Extra 3D Viewer" and embed it
  * in a specific layout:
  *
  * ```cpp
  * #include <InteractiveGeometryViewer.h>
  * ...
  * QVBoxLayout* myLayout;
  * ...
  * InteractiveGeometryViewer* extra3DViewer = dynamic_cast<InteractiveGeometryViewer*>(Application::getNewViewer("My Extra 3D Viewer", "InteractiveGeometryViewer"));
  * extra3DViewer->toggleLogo(false); // for instance: remove the CamiTK logo
  * // any other InteractiveGeometryViewer customization/tweaking goes here
  *
  * // embed the new viewer in custom layout (e.g. an Action widget)
  * extra3DViewer->setEmbedder(ui.illustrationLayout);
  * ...
  * // add a prop to the 3D viewer
  * extra3DViewer->getRendererWidget()->addProp(myVTKActor);
  *```
  *
  * \note
  * Application::getNewViewer() does not register the viewer's new instance automatically. Use Application::registerViewer() to register
  * your viewer if you want it to be refreshed automatically when Application::refresh() is called (e.g., when a new component is
  * opened or closed)
  *
  * @see ReorientImage gives an example of embedding an InteractiveGeometryViewer in an action widget
  * @see Application::getNewViewer()
  *
  * ### Creating a new viewer extension
  *
  * The simplest way to create a new viewer extension is to run camitk-wizard to obtain a working skeleton.
  * Feel free to check the two examples provided in the tutorial CEP (as well as the code of the viewer extensions provided
  * by the SDK).
  *
  */
class CAMITK_API Viewer : public QObject {
    Q_OBJECT

public:

    /// \enum ViewerType describes where this viewer should appear
    enum ViewerType {
        EMBEDDED,    ///< this viewer is meant to be embedded inside a layout (of another viewer or widget), use embedIn(..) to set the layout it is embedded in (this is the default viewer type)
        DOCKED,      ///< this viewer is meant to be docked, use dockIn() to get the dock widget
    };

    Q_ENUM(ViewerType)

    /// default constructor
    Viewer(QString name, ViewerType type = EMBEDDED);

    /// default destructor
    virtual ~Viewer() override;

    /// get the name of the viewer
    QString getName() const {
        return name;
    };

    /// get the name of the viewer
    QString getDescription() const {
        return description;
    };

    /// set the viewer layout (the type can be changed dynamically to fit the developer's purpose)
    void setType(ViewerType);

    /// get the viewer layout
    ViewerType getType();

    /// If the viewer type is DOCKED, dock the widget inside the given dock widget (do nothing if the type is EMBEDDED or
    /// if the viewer has already been docked before)
    /// Note that once set, the dock widget cannot be modified.
    /// The dock widget object's name, window title and icons are modified using the viewer's name and icon
    /// @return true if the docking operation was successful
    virtual bool setDockWidget(QDockWidget*);

    /// Get the QDockWidget* where this viewer is currently docked (or nullptr if it is not docked anywhere
    /// or if the viewer is of type EMBEDDED)
    virtual QDockWidget* getDockWidget();

    /// If the viewer type is EMBEDDED, embed the viewer widget in the given layout (do nothing if the type is DOCKED)
    /// Note that you can call this method any time you want to move the viewer's widget to another layout (but
    /// there is only one embedder at a time)
    /// @return true if the embedding operation was successful
    virtual bool setEmbedder(QLayout*);

    /// Get the QLayout* where this viewer is currently embedded (or nullptr if it is not embedded anywhere
    /// or if the viewer is of type DOCKED)
    virtual QLayout* getEmbedder();

    /// refresh the view (can be interesting to know which other viewer is calling this)
    virtual void refresh(Viewer* whoIsAsking = nullptr) = 0;

    /// get the viewer property object (returns nullptr by default, i.e. there are no property to edit)
    virtual PropertyObject* getPropertyObject() {
        return nullptr;
    };

    /// get the viewer menu (returns nullptr by default, i.e. there are no default edit menu)
    virtual QMenu* getMenu() {
        return nullptr;
    };

    /// get the viewer toolbar (returns nullptr by default, i.e. there are no default toolbar)
    virtual QToolBar* getToolBar() {
        return nullptr;
    };

    /// set the visibility of the viewer (show or hide its widget)
    virtual void setVisible(bool);

    /** set the visibility of the toolbar in the main window (true by default).
      * If the visibility is set to false, the next call to setCentralViewer(..) or addDockViewer(...) will remove it
      */
    virtual void setToolBarVisibility(bool);

    /// get the current value of the toolbar visibility
    virtual bool getToolBarVisibility();

    /// get the viewer icon
    virtual QPixmap getIcon();

    ///@cond
    /**
     * TODO CAMITK_API_DEPRECATED. This section list the methods marked as deprecated. They are to be removed in CamiTK 6.0
     * @deprecated
     *
     * DEPRECATED (CamiTK 6.0) -> to be removed
     * Please use getComponentClassNames() instead
     */
    /// get the list of Component class manages by this viewer
    /// (default is set to "Component", i.e. all type of Component)
    CAMITK_API_DEPRECATED("Please use getComponentClassNames() instead") QStringList getComponents();
    ///@endcond
    /// get the list of Component class manages by this viewer
    /// (default is set to "Component", i.e. all type of Component)
    QStringList getComponentClassNames();

signals:

    /// this signal is emitted when the current selection was changed by the viewer
    void selectionChanged();

protected:
    /// get the viewer widget.
    /// this method is protected and to be redefined in the inheriting class.
    /// \note to show the viewer's widget in the GUI, use dockIn() or embedIn(...)
    /// to set where the viewer is displayed
    virtual QWidget* getWidget() = 0;

    /** The selection has changed to the given ComponentList.
      * This method updates the Component::selection and emit the modified signal.
      * This method should be called by the inheriting class which can select Components (e.g.: Explorer).
     */
    void selectionChanged(ComponentList& compSet);

    /// the selection has changed to be just one comp
    void selectionChanged(Component* comp);

    /// clear the selection
    void clearSelection();

    /// set the default icon for the viewer extension
    void setIcon(QPixmap icon);

    ///@cond
    /**
     * TODO CAMITK_API_DEPRECATED. This section list the methods marked as deprecated. They are to be removed in CamiTK 6.0
     * @deprecated
     *
     * DEPRECATED (CamiTK 6.0) -> to be removed
     * Please use setComponentClassNames() instead
     */
    /// set the list of component class names managed by this viewer
    /// \note Default is set to "Component" (all type of components)
    CAMITK_API_DEPRECATED("Please use setComponentClassNames() instead") void setComponents(QStringList);
    ///@endcond
    /// set the list of component class names managed by this viewer
    /// \note Default is set to "Component" (all type of components)
    void setComponentClassNames(QStringList);

    /// set the viewer's description
    void setDescription(QString);

private:
    /// current viewer's name
    QString name;

    /// description of the viewer
    QString description;

    /// the current toolbar visibility
    bool toolbarVisibility;

    /// the Viewer pixmap icon
    QPixmap icon;

    /// the style for the dock widget
    ViewerDockStyle* dockWidgetStyle;

    /// list of Component class names managed by this viewer
    QStringList componentClassNames;

    /// this viewer's layout
    ViewerType type;

    /// if layout is of type DOCKED, then this is the widget where the viewer's widget is docked
    QDockWidget* dockWidget;

    /// if layout is of type EMBEDDED, this is the layout where the viewer's widget is embedded in
    QLayout* embedder;
};

}

#endif // VIEWER_H

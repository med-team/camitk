/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "ScreenshotFormatInfo.h"

namespace camitk {

//---------------------- getScreenshotMap ------------------------
const QMap<ScreenshotFormatInfo::ScreenshotFormat, ScreenshotFormatInfo*> ScreenshotFormatInfo::getMap() {
    // static instantiation (global variable)
    static QMap <ScreenshotFormatInfo::ScreenshotFormat, ScreenshotFormatInfo*> screenshotMap = initMap();

    return screenshotMap;
}

//---------------------- initScreenshotMap ------------------------
QMap<camitk::ScreenshotFormatInfo::ScreenshotFormat, camitk::ScreenshotFormatInfo*> ScreenshotFormatInfo::initMap() {
    QMap<ScreenshotFormat, ScreenshotFormatInfo*> screenshotMap;

    // NB: if you add something here, add something in the switch statement of the RendererWidget::screenshot method
    screenshotMap[PNG]  = new ScreenshotFormatInfo(PNG,  "png",  "Portable Network Graphics");
    screenshotMap[JPG]  = new ScreenshotFormatInfo(JPG,  "jpg",  "JPEG");
    screenshotMap[BMP]  = new ScreenshotFormatInfo(BMP,  "bmp",  "Bitmap");
    screenshotMap[PS]   = new ScreenshotFormatInfo(PS,   "ps",   "PostScript");
    screenshotMap[EPS]  = new ScreenshotFormatInfo(EPS,  "eps",  "Encapsulated PostScript");
    screenshotMap[PDF]  = new ScreenshotFormatInfo(PDF,  "pdf",  "Portable Document Format");
    screenshotMap[TEX]  = new ScreenshotFormatInfo(TEX,  "tex",  "LaTeX (text only)");
    screenshotMap[SVG]  = new ScreenshotFormatInfo(SVG,  "svg",  "Scalable Vector Graphics");
    screenshotMap[OBJ]  = new ScreenshotFormatInfo(OBJ,  "obj",  "Alias Wavefront .OBJ ");
    screenshotMap[RIB]  = new ScreenshotFormatInfo(RIB,  "rib",  "RenderMan/BMRT .RIB");
    screenshotMap[VRML] = new ScreenshotFormatInfo(VRML, "vrml", "VRML 2.0");
    screenshotMap[NOT_SUPPORTED] = new ScreenshotFormatInfo();

    return screenshotMap;
}

//---------------------- getScreenshotFormatInfo ------------------------
const ScreenshotFormatInfo* ScreenshotFormatInfo::get(unsigned int id) {
    if (id > NOT_SUPPORTED) {
        return get(NOT_SUPPORTED);
    }
    else {
        return get((ScreenshotFormat) id);
    }
}


const ScreenshotFormatInfo* ScreenshotFormatInfo::get(ScreenshotFormat f)  {
    return getMap().value(f);
}

const ScreenshotFormatInfo* ScreenshotFormatInfo::get(QString ext)  {
    unsigned int i = 0;
    bool found = false;

    while (i != NOT_SUPPORTED && !found) {
        found = (get(i)->extension == ext);
        i++;
    }

    if (found) {
        i--;
        return get(i);
    }
    else {
        return get(NOT_SUPPORTED);
    }
}

//---------------------- fileFilters ------------------------
const QString ScreenshotFormatInfo::fileFilters() {
    //-- build list of supported format
    QString filter("All supported image formats(");

    for (unsigned int i = 0; i < NOT_SUPPORTED; i++) {
        filter += " *." + get(i)->extension;
    }

    filter += QString(");;");

    // also add each entry individually
    for (unsigned int i = 0; i < NOT_SUPPORTED; i++) {
        filter += get(i)->description + "(*." + get(i)->extension + ");;";
    }

    return filter;
}

//---------------------- defaultFormat ------------------------
const camitk::ScreenshotFormatInfo* ScreenshotFormatInfo::defaultFormat() {
    return get(PNG);
}

} // namespace


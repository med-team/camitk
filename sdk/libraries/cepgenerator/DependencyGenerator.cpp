/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2024 Univ. Grenoble Alpes, CNRS, Grenoble INP - UGA, TIMC, 38000 Grenoble, France
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "DependencyGenerator.h"

#include <QStringList>
#include <QVector>

#include <Dependency.hxx>

using namespace cepcoreschema;

static const QString MACRO_CALL_CEP_LIBRARIES = "NEEDS_CEP_LIBRARIES";
static const QString MACRO_CALL_COMPONENTS  = "NEEDS_COMPONENT_EXTENSION";
static const QString MACRO_CALL_ACTIONS  = "NEEDS_ACTION_EXTENSION";
static const QString MACRO_CALL_VIEWERS  = "NEEDS_VIEWER_EXTENSION";


DependencyGenerator::DependencyGenerator(Dependency domDependency) {
    this->name = QString((domDependency.name()).c_str());

    switch (domDependency.type()) {
        default:
        case (DependencyType::library) :
            this->type = EXTERNAL_LIB;
            break;
        case (DependencyType::cepLibrary):
            this->type = CEP_LIB;
            break;
        case (DependencyType::component):
            this->type = NEEDED_COMPONENT;
            break;
        case (DependencyType::action):
            this->type = NEEDED_ACTION;
            break;
        case (DependencyType::viewer):
            this->type = NEEDED_VIEWER;
            break;
    }
}

DependencyGenerator::DependencyTypeGenerator DependencyGenerator::getType() {
    return this->type;
}

QString DependencyGenerator::getName() {
    return this->name;
}

QString DependencyGenerator::getExternalLibsString(QVector<DependencyGenerator* > dependencies) {
    QStringList externalLibsList;
    for (QVector<DependencyGenerator*>::const_iterator it = dependencies.constBegin(); it != dependencies.constEnd(); ++it) {
        if ((*it)->getType() == EXTERNAL_LIB) {
            externalLibsList << ("NEEDS_" + (*it)->getName().toUpper());
        }
    }
    QString result = "";
    if (! externalLibsList.isEmpty()) {
        for (int i = 0; i < externalLibsList.size(); i++) {
            result += externalLibsList.at(i) + "\n";
        }
    }

    return result;
}

QString DependencyGenerator::getCepLibsString(QVector<DependencyGenerator* > dependencies) {
    QStringList cepLibs;
    for (QVector<DependencyGenerator*>::const_iterator it = dependencies.constBegin(); it != dependencies.constEnd(); ++it) {
        if ((*it)->getType() == CEP_LIB) {
            cepLibs << (*it)->getName();
        }
    }
    QString result = "";
    if (! cepLibs.isEmpty()) {
        result = MACRO_CALL_CEP_LIBRARIES + " ";
        for (int i = 0; i < cepLibs.size(); i++) {
            result += cepLibs.at(i) + " ";
        }
    }
    return result;
}

QString DependencyGenerator::getNeededComponentsString(QVector<DependencyGenerator* > dependencies) {
    QStringList comps;
    for (QVector<DependencyGenerator*>::const_iterator it = dependencies.constBegin(); it != dependencies.constEnd(); ++it) {
        if ((*it)->type == NEEDED_COMPONENT) {
            comps << (*it)->getName();
        }
    }
    QString result = "";
    if (! comps.isEmpty()) {
        result = MACRO_CALL_COMPONENTS + " ";
        for (int i = 0; i < comps.size(); i++) {
            result += comps.at(i) + " ";
        }
    }
    return result;
}

QString DependencyGenerator::getNeededActionsString(QVector<DependencyGenerator* > dependencies) {
    QStringList actions;
    for (QVector<DependencyGenerator*>::const_iterator it = dependencies.constBegin(); it != dependencies.constEnd(); ++it) {
        if ((*it)->type == NEEDED_ACTION) {
            actions << (*it)->getName();
        }
    }
    QString result = "";
    if (! actions.isEmpty()) {
        result = MACRO_CALL_ACTIONS + " ";
        for (int i = 0; i < actions.size(); i++) {
            result += actions.at(i) + " ";
        }
    }
    return result;

}

QString DependencyGenerator::getNeededViewersString(QVector<DependencyGenerator* > dependencies) {
    QStringList actions;
    for (QVector<DependencyGenerator*>::const_iterator it = dependencies.constBegin(); it != dependencies.constEnd(); ++it) {
        if ((*it)->type == NEEDED_VIEWER) {
            actions << (*it)->getName();
        }
    }
    QString result = "";
    if (! actions.isEmpty()) {
        result = MACRO_CALL_VIEWERS + " ";
        for (int i = 0; i < actions.size(); i++) {
            result += actions.at(i) + " ";
        }
    }
    return result;

}



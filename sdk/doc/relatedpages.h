// Here are the related pages of the documention
/**
 * @page home-page CamiTK website
 * The home page of the project can be found at : <a href="http://camitk.imag.fr">camitk.imag.fr</a>
 *
 * @page wiki CamiTK documention
 * Installation instructions, many tips and tutorials are proposed in the <a href="https://camitk.imag.fr/docs">CamiTK documentation and wiki</a>
 * (you may want to edit it and share your knowledge).
 *
 **/

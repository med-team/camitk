// The following content describes the index.html page of the API documentation.
// It uses CMake variable (surrounded by the '@' pattern), replaced by their content using the CONFIGURE_FILE CMake command
// This file is configured in camitk/cmake/modules/CamiTKApiDoc.cmake into the CamiTKVersion.h file.

/** @mainpage CamiTK API Documentation

## Introduction

Welcome to the Computer Assisted Medical Intervention Tool kit (CamiTK) API Documentation.

These pages _only_ describes the API. Althoug there are a lot of interesting information these pages only provide technical information about the class and CMake macros and how to use them.

Please refer to the [main documentation page of the camitk website](https://camitk.imag.fr/docs/) to get a general overview of CamiTK, its architecture, how-tos, tutorials, in-depths and more...

\note
Use the white search box on the top most right (the one saying "Search") to look for something specific in the API doc.

#### Copyright (c) Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
*/




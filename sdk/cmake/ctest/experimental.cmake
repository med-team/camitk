# Driver instructions for Experimental build on a computer
# This script can be run locally to simulate what is going to happen when the nightly CI script is run on a VM
#
# Run this script to simulate a Nightly build similarly to:
#
# # define parameters
# SITE=$(hostname)                                        # the name of the current machine 
# CAMITK_SOURCE_DIR=~/Dev/CamiTK/src/camitk               # path to CamiTK code source directory
# CAMITK_BUILD_DIR=~/Dev/CamiTK/build/camitk-exp          # path to the intended build directory
# COMPILER_CONFIG="GCC-64bits-Debug"                      # compiler-arch-buildtype string
# # run the command
# ctest -VV \
#       -DCTEST_SITE="$SITE" \
#       -DCAMITK_CONTINUOUS_INTEGRATION="$COMPILER_CONFIG" \
#       -DCTEST_SOURCE_DIRECTORY="$CAMITK_SOURCE_DIR" \
#       -DCTEST_BINARY_DIRECTORY="$CAMITK_BUILD_DIR" \
#       -S $CAMITK_SOURCE_DIR/sdk/cmake/ctest/experimental.cmake > log.txt 2>&1
#
# It will submit a new report in the "experimental" section of the dashboard 
# identified as $SITE and $COMPILER_CONFIG.
#
# What this script does ?
# 1. INFORMATION STEP
#       Configure SITE and BUILD information to be correctly display on the dashboard
#       Loads information from the CTestConfig.cmake file.
# x. SKIP THE UPDATE STEP of nightly script
# 2. CONFIGURE STEP
#       configure the whole CamiTK project and create a new build directory 
# 3. BUILD STEP 
#       build each subproject of CamiTK
# 4. RUN TEST COVERAGE
#
# For each step a report is sent to the dashboard.
# This allows any developer to be informed (mailing list) of any problem, even BEFORE the script ends!

# ------------------------ STEP 1: information step ------------------------
message(STATUS "Step 1. Gather information about this test...")

# Need to be defined, for the build to run.
if(NOT DEFINED CTEST_SOURCE_DIRECTORY)
    message(FATAL_ERROR "Please provide the source directory of the continuous test with the CTEST_SOURCE_DIRECTORY argument")
endif()

if(NOT DEFINED CTEST_BINARY_DIRECTORY)
    message(FATAL_ERROR "Please provide the build directory of the continuous test with the CTEST_BINARY_DIRECTORY argument")
endif()

# Script configuration, depending of the build, computer running the script
# Update to feat each computer which runs this script

# Get VM compilation information given by ctest call command
if(CAMITK_CONTINUOUS_INTEGRATION)
    string(REGEX REPLACE "^(.*)-.*-.*" "\\1" COMPILER "${CAMITK_CONTINUOUS_INTEGRATION}")
    string(REGEX REPLACE "^.*-(.*)-.*" "\\1" ARCH "${CAMITK_CONTINUOUS_INTEGRATION}")
    string(REGEX REPLACE "^.*-.*-(.*)" "\\1" BUILDTYPE "${CAMITK_CONTINUOUS_INTEGRATION}")
else()
    message(FATAL_ERROR "CAMITK_CONTINUOUS_INTEGRATION value must be given as option of the ctest command calling this script.")
endif()

# Compose with those variables the CTest required ones.
site_name(CTEST_SITE)

# get the git hash
find_package(Git QUIET)
if(GIT_FOUND)
    include("${CTEST_SOURCE_DIRECTORY}/sdk/cmake/modules/macros/GetGitInfo.cmake")
    set(CMAKE_SOURCE_DIR ${CTEST_SOURCE_DIRECTORY})
    get_git_info(${CTEST_SOURCE_DIRECTORY})
    set(CURRENT_GIT_HASH ${CAMITK_GIT_ABBREVIATED_HASH})
else()
    set(CURRENT_GIT_HASH "???")
endif()

# set the build name using the compiler and commit hash
set(CTEST_BUILD_NAME ${CAMITK_CONTINUOUS_INTEGRATION}-${CURRENT_GIT_HASH})

if(UNIX)
    set( CTEST_CMAKE_GENERATOR  "Unix Makefiles" )
elseif(WIN32)
    if(COMPILER MATCHES "MinGW" OR "MINGW")
        set( CTEST_CMAKE_GENERATOR  "MinGW Makefiles" )
    elseif(COMPILER MATCHES "MSVC2008")
        set( CTEST_CMAKE_GENERATOR "Visual Studio 9 2008" )
    elseif(COMPILER MATCHES "MSVC2010" AND ARCH MATCHES "32bits")
        set( CTEST_CMAKE_GENERATOR  "Visual Studio 10" )
        set( CMAKE_MAKE_PROGRAM "C:/Program Files (x86)/Microsoft Visual Studio 10.0/Common7/IDE/VCExpress.exe") # Do not use MSBuild.exe as it won't work
    elseif(COMPILER MATCHES "MSVC2010" AND ARCH MATCHES "64bits")
        set( CTEST_CMAKE_GENERATOR  "Visual Studio 10 Win64" )
        set( CMAKE_MAKE_PROGRAM "C:/Program Files (x86)/Microsoft Visual Studio 10.0/Common7/IDE/VCExpress.exe") # Do not use MSBuild.exe as it won't work
    elseif(COMPILER MATCHES "MSVC2012" AND ARCH MATCHES "32bits")
        set( CTEST_CMAKE_GENERATOR  "Visual Studio 11" )
        set( CMAKE_MAKE_PROGRAM "C:/Program Files (x86)/Microsoft Visual Studio 11.0/Common7/IDE/WDExpress.exe") # Do not use MSBuild.exe as it won't work
    elseif(COMPILER MATCHES "MSVC2012" AND ARCH MATCHES "64bits")
        set( CTEST_CMAKE_GENERATOR  "Visual Studio 11 Win64" )
        set( CMAKE_MAKE_PROGRAM "C:/Program Files (x86)/Microsoft Visual Studio 11.0/Common7/IDE/WDExpress.exe") # Do not use MSBuild.exe as it won't work
    elseif(COMPILER MATCHES "MSVC2013" AND ARCH MATCHES "32bits")
        set( CTEST_CMAKE_GENERATOR  "Visual Studio 12" )
    elseif(COMPILER MATCHES "MSVC2013" AND ARCH MATCHES "64bits")
        set( CTEST_CMAKE_GENERATOR  "Visual Studio 12 Win64" )
    elseif(COMPILER MATCHES "MSVC2015" AND ARCH MATCHES "64bits")
        set( CTEST_CMAKE_GENERATOR  "Visual Studio 14 2015 Win64" )
    elseif(COMPILER MATCHES "MSVC2017" AND ARCH MATCHES "64bits")
        set( CTEST_CMAKE_GENERATOR "Visual Studio 15 2017 Win64" )
    else()
        message(FATAL_ERROR "CTEST COMPILER ERROR : No proper compiler found, please check ctest command syntax.")
    endif()
endif()

if(BUILDTYPE)
    set(CTEST_BUILD_CONFIGURATION ${BUILDTYPE})
    set(CTEST_CONFIGURATION_TYPE ${BUILDTYPE})
else()
    message(FATAL_ERROR "NO BUILD TYPE : Please provide a build type: Debug or Release")
endif()

# CMake configuration (put here all the configure flags)
set( CTEST_CONFIGURE_COMMAND "${CMAKE_COMMAND} -Wno-dev -G \"${CTEST_CMAKE_GENERATOR}\"")
set( CTEST_CONFIGURE_COMMAND "${CTEST_CONFIGURE_COMMAND} -DCMAKE_BUILD_TYPE:STRING=${CTEST_BUILD_CONFIGURATION}")
if(COMPILER MATCHES "MSVC2010" AND ARCH MATCHES "64bits") # Do not compile MML and PhysicalModel until libxml2 MSVC2010 x64 bug remains.
    set(CTEST_CONFIGURE_COMMAND "${CTEST_CONFIGURE_COMMAND} -DACTION_MML=FALSE -DCOMPONENT_MML=FALSE -DCOMPONENT_PHYSICALMODEL=FALSE")
endif()

set(CTEST_TEST_TIMEOUT 1500)
set( CTEST_CONFIGURE_COMMAND "${CTEST_CONFIGURE_COMMAND} -DCEP_IMAGING=TRUE -DCEP_MODELING=TRUE -DCEP_TUTORIALS=TRUE -DAPIDOC_SDK=TRUE -DCAMITK_DISABLE_TRANSLATION=TRUE -DCAMITK_TEST_COVERAGE=TRUE ${CTEST_SOURCE_DIRECTORY}")

# get CamiTK CDash server configuration
include("${CTEST_SOURCE_DIRECTORY}/CTestConfig.cmake")

# The type of build that this script will make 
# For this script we use Experimental build has it is designed to be run manually
ctest_start(Experimental)

# ------------------------ STEP 2: configure step ------------------------
message(STATUS "Step 2. Configure the CamiTK project...")

# Empty local installation directory
if(WIN32)
    # %APPDATA%\MySoft\Star Runner.ini
    set(CAMITK_USER_BASE_DIR_WINDOWS $ENV{APPDATA})
    file(TO_CMAKE_PATH "${CAMITK_USER_BASE_DIR_WINDOWS}" CAMITK_USER_BASE_DIR)
else()
    # (UNIX OR APPLE)
    # $HOME/.config/MySoft/Star Runner.ini 
    set(CAMITK_USER_BASE_DIR "$ENV{HOME}/.config")
endif()
set(CAMITK_USER_DIR "${CAMITK_USER_BASE_DIR}/CamiTK")
file(REMOVE_RECURSE ${CAMITK_USER_DIR})

# Configure whole project
ctest_empty_binary_directory(${CTEST_BINARY_DIRECTORY})
ctest_configure()
ctest_submit(PARTS Configure)

# Get the subprojects listing (automatically created at configuration step)
include("${CTEST_BINARY_DIRECTORY}/Subprojects.cmake")

# Update CDash configuration to the server
# The Project.xml file is automatically generated at configure time. 
# If a new extension is added to CamiTK, CDash will automatically be updated according to it.
# To do this, send this file to the server
ctest_submit(FILES "${CTEST_BINARY_DIRECTORY}/Project.xml") 

# ------------------------ STEP 3: build ------------------------

# Build each subprojects
message(STATUS "Step 3. Build all subprojects...")

foreach(subproject ${CAMITK_TARGETS})
        # tag sub project
        set_property(GLOBAL PROPERTY SubProject ${subproject})
        set_property(GLOBAL PROPERTY Label ${subproject})
        
        # build each sub project
        message(STATUS "- Building ${subproject}...")
        set(CTEST_BUILD_TARGET ${subproject})
        ctest_build()
        ctest_submit(PARTS Build)
endforeach()

# now that everything is build, run all the tests
foreach(subproject ${CAMITK_TARGETS})
        # run tests
        message(STATUS "- Running tests for ${subproject}...")
        ctest_test(BUILD "${CTEST_BINARY_DIRECTORY}" INCLUDE_LABEL "${subproject}")
        ctest_submit(PARTS Test)
endforeach()

# ------------------------ STEP 4: Test coverage  ------------------------
message(STATUS "- Running test coverage ${subproject}...")
set(CTEST_BUILD_TARGET camitk-ce-test-coverage)
ctest_build()

if(NOT DEFINED CTEST_COVERAGE_COMMAND)
    find_program(CTEST_COVERAGE_COMMAND NAMES gcov)
    if(CTEST_COVERAGE_COMMAND)
        message(STATUS "- ctest configured with gcov: ${CTEST_COVERAGE_COMMAND}")
    else()
        message(STATUS "- ctest not configured with gcov (gcov binary not found)")
    endif()
endif()

ctest_coverage()
ctest_submit(PARTS Coverage)

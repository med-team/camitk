# Driver instructions for the run test step of the continuous integration
#
# 1. Define parameters
# CAMITK_SITE="[Gitlab Runner] debian stable"                  # the name of the current machine 
# CAMITK_CI_MODE="Experimental"                                # ctest mode (Nightly, Continuous, Experimental...)
# CAMITK_CI_BRANCH="branch-name"                               # git branch name (check directly with git if not defined)
# CAMITK_CI_ID="Pipeline #$CI_PIPELINE_ID Job #$CI_BUILD_ID"   # unique id 
# CAMITK_SOURCE_DIR=~/Dev/CamiTK/src/camitk                    # path to CamiTK code source directory
# CAMITK_BUILD_DIR=~/Dev/CamiTK/build/camitk-exp               # path to the intended build directory
# CAMITK_BUILD_SETTINGS="GCC-64bits-Debug"                     # compiler-arch-buildtype string
#
# 2. run the command
# ctest -VV \
#       -DCTEST_SITE="$CAMITK_SITE" \
#       -DCI_MODE="$CAMITK_CI_MODE" \
#       -DCI_ID="$CAMITK_CI_ID" \
#       -DCI_BRANCH=$CAMITK_CI_BRANCH \
#       -DCI_BUILD_SETTINGS="$COMPILER_CONFIG" \
#       -DCTEST_SOURCE_DIRECTORY="$CAMITK_SOURCE_DIR" \
#       -DCTEST_BINARY_DIRECTORY="$CAMITK_BUILD_DIR" \
#       -S $CAMITK_SOURCE_DIR/sdk/cmake/ctest/ci-coverage.cmake > coverage.log 2>&1
#
# It will submit a new report in the "configure" section of the dashboard 
# identified as $SITE and $COMPILER_CONFIG.
#
# What this script does ?
# 1. INFORMATION STEP
#       Configure SITE and BUILD information to be correctly display on the dashboard
#       Loads information from the CTestConfig.cmake file.
# 2. RUN TEST COVERAGE
#

set(CI_STAGE "Coverage")

# ------------------------ STEP 1: information step ------------------------
include("${CTEST_SOURCE_DIRECTORY}/sdk/cmake/ctest/ci-setup.cmake")

# ------------------------ STEP 2: Test coverage  ------------------------
message(STATUS "- Running test coverage...")
set(CTEST_BUILD_TARGET camitk-ce-test-coverage)
ctest_build()
if(NOT DEFINED CTEST_COVERAGE_COMMAND)
    find_program(CTEST_COVERAGE_COMMAND NAMES gcov)
    if(CTEST_COVERAGE_COMMAND)
        message(STATUS "- ctest configured with gcov: ${CTEST_COVERAGE_COMMAND}")
    else()
        message(STATUS "- ctest not configured with gcov (gcov binary not found)")
    endif()
endif()
message(STATUS "- Running ctest_coverage()...")
ctest_coverage()
message(STATUS "- Submitting ctest coverage...")
ctest_submit(PARTS Coverage)

message(STATUS "Coverage done")

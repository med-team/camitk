# Driver instructions for Nightly build on a computer
# To configurat a Nightly build, create a cron job using a script that loads this ctest instruction (driver) using the 
# followind command line : ctest -S ctest-nightly-driver.cmake
#
# What this script does ?
# * INFORMATION STEP
#       Configure SITE and BUILD information to be correctly display on dashboard
#       Loads information from the CTestConfig.cmake file.
# * UPDATE STEP
#       use GIT to udpdate source code to the latest revision BEFORE Nightly start time, which is defined in the CTestConfig.cmake file.
# * CONFIGURE STEP
#       configure the whole CamiTK project and create a new build directory 
# * BUILD STEP 
#       build each subproject of CamiTK
# For each step a report is sent to the dashboard. This allows any developer to be informed (mailing list) of any problem, even BEFORE the script ends !

# Need to be defined, for the build to run.
if(NOT DEFINED CTEST_SOURCE_DIRECTORY)
    message(FATAL_ERROR "No source directory: please provide the path to the source directory with the CTEST_SOURCE_DIRECTORY argument")
endif()
if(NOT DEFINED CTEST_BINARY_DIRECTORY)
    message(FATAL_ERROR "No build directory: please provide the path to the build directory with the CTEST_BINARY_DIRECTORY argument")
endif()

# Script configuration, depending of the build, computer running the script
# Update to feat each computer which runs this script

# Get VM compilation information given by ctest call command
if(CAMITK_CONTINUOUS_INTEGRATION)
    string(REGEX REPLACE "^(.*)-.*-.*" "\\1" COMPILER "${CAMITK_CONTINUOUS_INTEGRATION}")
    string(REGEX REPLACE "^.*-(.*)-.*" "\\1" ARCH "${CAMITK_CONTINUOUS_INTEGRATION}")
    string(REGEX REPLACE "^.*-.*-(.*)" "\\1" BUILDTYPE "${CAMITK_CONTINUOUS_INTEGRATION}")
else()
    message(FATAL_ERROR "CAMITK_CONTINUOUS_INTEGRATION value must be given as option of the ctest command calling this script.")
endif()

# Compose with those variables the CTest required ones.
site_name(CTEST_SITE)

if(UNIX)
    set( CTEST_CMAKE_GENERATOR  "Unix Makefiles" )
elseif(WIN32)
    if(COMPILER MATCHES "MinGW" OR "MINGW")
        set( CTEST_CMAKE_GENERATOR  "MinGW Makefiles" )
    elseif(COMPILER MATCHES "MSVC2008")
        set( CTEST_CMAKE_GENERATOR "Visual Studio 9 2008" )
    elseif(COMPILER MATCHES "MSVC2010" AND ARCH MATCHES "32bits")
        set( CTEST_CMAKE_GENERATOR  "Visual Studio 10" )
        set( CMAKE_MAKE_PROGRAM "C:/Program Files (x86)/Microsoft Visual Studio 10.0/Common7/IDE/VCExpress.exe") # Do not use MSBuild.exe as it won't work
    elseif(COMPILER MATCHES "MSVC2010" AND ARCH MATCHES "64bits")
        set( CTEST_CMAKE_GENERATOR  "Visual Studio 10 Win64" )
        set( CMAKE_MAKE_PROGRAM "C:/Program Files (x86)/Microsoft Visual Studio 10.0/Common7/IDE/VCExpress.exe") # Do not use MSBuild.exe as it won't work
    elseif(COMPILER MATCHES "MSVC2012" AND ARCH MATCHES "32bits")
        set( CTEST_CMAKE_GENERATOR  "Visual Studio 11" )
        set( CMAKE_MAKE_PROGRAM "C:/Program Files (x86)/Microsoft Visual Studio 11.0/Common7/IDE/WDExpress.exe") # Do not use MSBuild.exe as it won't work
    elseif(COMPILER MATCHES "MSVC2012" AND ARCH MATCHES "64bits")
        set( CTEST_CMAKE_GENERATOR  "Visual Studio 11 Win64" )
        set( CMAKE_MAKE_PROGRAM "C:/Program Files (x86)/Microsoft Visual Studio 11.0/Common7/IDE/WDExpress.exe") # Do not use MSBuild.exe as it won't work
    elseif(COMPILER MATCHES "MSVC2013" AND ARCH MATCHES "32bits")
        set( CTEST_CMAKE_GENERATOR  "Visual Studio 12" )
    elseif(COMPILER MATCHES "MSVC2013" AND ARCH MATCHES "64bits")
        set( CTEST_CMAKE_GENERATOR  "Visual Studio 12 Win64" )
    elseif(COMPILER MATCHES "MSVC2015" AND ARCH MATCHES "64bits")
        set( CTEST_CMAKE_GENERATOR  "Visual Studio 14 2015 Win64" )
    elseif(COMPILER MATCHES "MSVC2017" AND ARCH MATCHES "64bits")
        set( CTEST_CMAKE_GENERATOR "Visual Studio 15 2017 Win64" )
    else()
        message(FATAL_ERROR "CTEST COMPILER ERROR : No proper compiler found, please check ctest command syntax.")
    endif()
endif()

if(BUILDTYPE)
    set(CTEST_BUILD_CONFIGURATION ${BUILDTYPE})
    set(CTEST_CONFIGURATION_TYPE ${BUILDTYPE})
else()
    message(FATAL_ERROR "NO BUILD TYPE : Please provide a build type: Debug or Release")
endif()

# get the git hash
find_package(Git QUIET)
if(GIT_FOUND)
    include("${CTEST_SOURCE_DIRECTORY}/sdk/cmake/modules/macros/GetGitInfo.cmake")
else()
    message(FATAL_ERROR "Git command not found: please install git.")
endif()

# set the build name using the compiler and commit hash of the origin/develop branch
# CTEST_BUILD_NAME needs to be set before ctest_start, which in turns is called before ctest_update
# therefore the current git hash should be fetch fro origin/develop
get_git_info(${CTEST_SOURCE_DIRECTORY})
set(CTEST_BUILD_NAME ${CAMITK_CONTINUOUS_INTEGRATION}-${CAMITK_ORIGIN_DEVELOP_GIT_ABBREVIATED_HASH})

# Update source code and send reports
set(CTEST_GIT_COMMAND ${GIT_EXECUTABLE})

if(NOT EXISTS "${CTEST_SOURCE_DIRECTORY}")
  set(CTEST_CHECKOUT_COMMAND "${GIT_EXECUTABLE} clone https://gricad-gitlab.univ-grenoble-alpes.fr/CamiTK/CamiTK.git ${CTEST_SOURCE_DIRECTORY}")
endif()

set(CTEST_TEST_TIMEOUT 1500)
set(CTEST_UPDATE_COMMAND ${GIT_EXECUTABLE})

# To use deprecated flag only if it is gcc compiler
IF (WIN32)
    set(COVERAGE_OPTION "FALSE")
ELSE()
    set(COVERAGE_OPTION "TRUE")
ENDIF()

# CMake configuration (put here all the configure flags)
set(CTEST_CONFIGURE_COMMAND "${CMAKE_COMMAND} -Wno-dev -G \"${CTEST_CMAKE_GENERATOR}\"")
set(CTEST_CONFIGURE_COMMAND "${CTEST_CONFIGURE_COMMAND} -DCMAKE_BUILD_TYPE:STRING=${CTEST_BUILD_CONFIGURATION}")
if(COMPILER MATCHES "MSVC2010" AND ARCH MATCHES "64bits") # Do not compile MML and PhysicalModel until libxml2 MSVC2010 x64 bug remains.
    set(CTEST_CONFIGURE_COMMAND "${CTEST_CONFIGURE_COMMAND} -DACTION_MML=FALSE -DCOMPONENT_MML=FALSE -DCOMPONENT_PHYSICALMODEL=FALSE")
endif()
set(CTEST_CONFIGURE_COMMAND "${CTEST_CONFIGURE_COMMAND} -DCEP_IMAGING=TRUE -DCEP_MODELING=TRUE -DCEP_TUTORIALS=TRUE -DAPIDOC_SDK=TRUE -DCAMITK_TEST_COVERAGE=${COVERAGE_OPTION} ${CTEST_SOURCE_DIRECTORY}")
# -DCAMITK_INSTALL_ROOT=${CTEST_BINARY_DIRECTORY}

# to get CDash server configuration :
include("${CTEST_SOURCE_DIRECTORY}/CTestConfig.cmake")

# The type of build that this script will make 
# For this script we use Nightly build has it is designed to be run once a day
ctest_start(Nightly)

# Update source code
ctest_update(SOURCE ${CTEST_SOURCE_DIRECTORY} RETURN_VALUE count)

if(count GREATER 0)
    # We only show update when there is change (avoid the CDash u=1 bug with no errors report)
    ctest_submit(PARTS Update Notes)
endif()

# Configure whole project
ctest_empty_binary_directory(${CTEST_BINARY_DIRECTORY})
ctest_configure()
ctest_submit(PARTS Configure)

#Empty local installation directory
if(WIN32)
    # %APPDATA%\MySoft\Star Runner.ini
    set(CAMITK_USER_BASE_DIR_WINDOWS $ENV{APPDATA})
    file(TO_CMAKE_PATH "${CAMITK_USER_BASE_DIR_WINDOWS}" CAMITK_USER_BASE_DIR)
else()
    # (UNIX OR APPLE)
    # $HOME/.config/MySoft/Star Runner.ini 
    set(CAMITK_USER_BASE_DIR "$ENV{HOME}/.config")
endif()
set(CAMITK_USER_DIR "${CAMITK_USER_BASE_DIR}/CamiTK")
file(REMOVE_RECURSE ${CAMITK_USER_DIR})

# to get subprojects listing (automatically created at configuration step)
include("${CTEST_BINARY_DIRECTORY}/Subprojects.cmake")

# Update CDash configuration to the server
# Project.xml file is automatically generated. If someone added an extension to CamiTK, CDash will automatically be updated according to it.
# To do this, send this file to the server
ctest_submit(FILES "${CTEST_BINARY_DIRECTORY}/Project.xml") 

# Build each subprojects
foreach(subproject ${CAMITK_TARGETS})
        # tag sub project
        set_property(GLOBAL PROPERTY SubProject ${subproject})
        set_property(GLOBAL PROPERTY Label ${subproject})

        # build each sub project
        set(CTEST_BUILD_TARGET ${subproject})
        ctest_build()
        ctest_submit(PARTS Build)
endforeach()

# build the api-doc
set(CTEST_BUILD_TARGET camitk-ce-api-doc)
ctest_build()
# TODO zip it and download it on the website every so often (as `develop api-doc`)

# now that everything is build, run all the tests
foreach(subproject ${CAMITK_TARGETS})
        # Run tests
        ctest_test(BUILD "${CTEST_BINARY_DIRECTORY}" INCLUDE_LABEL "${subproject}")
        ctest_submit(PARTS Test)
endforeach() 

# Compute test coverage
message(STATUS "- Running test coverage...")
set(CTEST_BUILD_TARGET camitk-ce-test-coverage)
ctest_build()

if(NOT DEFINED CTEST_COVERAGE_COMMAND)
    find_program(CTEST_COVERAGE_COMMAND NAMES gcov)
    if(CTEST_COVERAGE_COMMAND)
        message(STATUS "- ctest configured with gcov: ${CTEST_COVERAGE_COMMAND}")
    else()
        message(STATUS "- ctest not configured with gcov (gcov binary not found)")
    endif()
endif()

ctest_coverage()
ctest_submit(PARTS Coverage)

# Install locally this build version of CamiTK
set(CTEST_BUILD_TARGET camitk-ce-local-install)
ctest_build()
ctest_submit(PARTS Install)

# Empty local installation directory
file(REMOVE_RECURSE ${CAMITK_USER_DIR})


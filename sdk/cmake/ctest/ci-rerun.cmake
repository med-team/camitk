# Driver instructions for the reran a failed test during continuous integration
#
# 1. Define parameters
# CAMITK_SITE="[Gitlab Runner] debian stable"                  # the name of the current machine 
# CAMITK_CI_MODE="Experimental"                                # ctest mode (Nightly, Continuous, Experimental...)
# CAMITK_CI_BRANCH="branch-name"                               # git branch name (check directly with git if not defined)
# CAMITK_CI_ID="Pipeline #$CI_PIPELINE_ID Job #$CI_BUILD_ID"   # unique id 
# CAMITK_SOURCE_DIR=~/Dev/CamiTK/src/camitk                    # path to CamiTK code source directory
# CAMITK_BUILD_DIR=~/Dev/CamiTK/build/camitk-exp               # path to the intended build directory
# CAMITK_BUILD_SETTINGS="GCC-64bits-Debug"                     # compiler-arch-buildtype string
# RERUN_TESTNAME=..."                                          # name of the test
#
# 2. run the command
# ctest -VV \
#       -DCTEST_SITE="$CAMITK_SITE" \
#       -DCI_MODE="$CAMITK_CI_MODE" \
#       -DCI_ID="$CAMITK_CI_ID" \
#       -DCI_BRANCH=$CAMITK_CI_BRANCH \
#       -DCI_BUILD_SETTINGS="$COMPILER_CONFIG" \
#       -DCTEST_SOURCE_DIRECTORY="$CAMITK_SOURCE_DIR" \
#       -DCTEST_BINARY_DIRECTORY="$CAMITK_BUILD_DIR" \
#       -DRERUN_TESTNAME=name-of-the-test-to-run \
#       -S $CAMITK_SOURCE_DIR/sdk/cmake/ctest/ci-rerun.cmake > test.log 2>&1
#
# It will submit a new report in the "configure" section of the dashboard 
# identified as $SITE and $COMPILER_CONFIG.
#
# What this script does ?
# 1. INFORMATION STEP
#       Configure SITE and BUILD information to be correctly display on the dashboard
#       Loads information from the CTestConfig.cmake file.
# 2. RUN TEST
#       Re-Run test RERUN_TESTNAME

set(CI_STAGE "Test")

# ------------------------ STEP 1: information step ------------------------
include("${CTEST_SOURCE_DIRECTORY}/sdk/cmake/ctest/ci-setup.cmake")

# ------------------------ STEP 2: test ------------------------
message(STATUS "Step 2. Rerun only test ${RERUN_TESTNAME}... Using ${NUMBER_OF_PROC} tests in parallel")

# set extra time for test to 30 min = 1800 s (default is 10 min = 600 s)
set(CTEST_TEST_TIMEOUT 1800)

# run test
message(STATUS "- Running test ${RERUN_TESTNAME}...")
# When testname is something-1, adding a "$" at the end, avoid rerunning the test names something-10 to something-19
set(TESTNAME_REGEX "${RERUN_TESTNAME}$") 

ctest_test(BUILD "${CTEST_BINARY_DIRECTORY}" APPEND
           PARALLEL_LEVEL ${NUMBER_OF_PROC}    # Run test in parallel
           RETURN_VALUE TEST_RES
           INCLUDE ${TESTNAME_REGEX}
)
       
if (NOT TEST_RES EQUAL 0)
    message(STATUS "- Error: Test ${RERUN_TESTNAME} failed with return value ${TEST_RES}")
else()
    message(STATUS "- Test ${RERUN_TESTNAME} passed")
endif()
        
message(STATUS "- Submitting test ${RERUN_TESTNAME} results...")

ctest_submit(PARTS Test RETURN_VALUE TEST_RES)
        
if (NOT TEST_RES EQUAL 0)
    message(STATUS "- Error: Submitting test ${RERUN_TESTNAME} results failed with return value ${TEST_RES}")
else()
    message(STATUS "- Submitting test ${RERUN_TESTNAME} results passed")
endif()

message(STATUS "Test ${RERUN_TESTNAME} done")

# find xml2 using xml2-config (cmake is not really elegant for that)
if(NOT CMAKE_CROSSCOMPILING)
  # just ask CMake
  find_package( LibXml2 REQUIRED )
  add_definitions(${LIBXML2_DEFINITIONS})
else()
  find_program(XML2_CONFIG
    NAMES xml2-config
    PATHS "/usr/bin" ${CMAKE_FIND_ROOT_PATH}/bin #Add paths here
  )
  if (NOT XML2_CONFIG)
    message(FATAL_ERROR "For crosscompiling, you need to define XML2_CONFIG")
  else()
    set(LIBXML2_FOUND 1)
    exec_program(${XML2_CONFIG}
                  ARGS --libs
                  OUTPUT_VARIABLE LIBXML2_LIBRARIES
    )
    exec_program(${XML2_CONFIG}
                  ARGS --cflags
                  OUTPUT_VARIABLE LIBXML2_INCLUDE_DIR)
    include_directories(${LIBXML2_INCLUDE_DIR})
    add_definitions(${LIBXML2_INCLUDE_DIR})    
  endif()
endif()

# --------------------------
# Include all CamiTK macros
# --------------------------

# -- generic useful macros
include(GetSubdirectories)
include(GatherHeadersAndSources)
include(ParseArguments)
include(GetDirectoryName)
include(GetSubdirectoryFiles)
include(ExportHeaders)

# -- CamiTK specific macros
include(camitk/CamiTKAddSubDirectory)
include(camitk/CamiTKInstallAllHeaders)

# -- CamiTK extensions and library macro
include(camitk/CamiTKExtensionProject)
include(camitk/CamiTKExtension)
include(camitk/CamiTKLibrary)
include(camitk/CamiTKApplication)

# -- CamiTK subproject gathering
include(camitk/manifest/CamiTKInitManifestData)
include(camitk/manifest/CamiTKAddSubProject)
include(camitk/manifest/CamiTKWriteManifestData)

# -- CamiTK test macros (see also http://www.org/cmake/help/v2.8.8/cmake.html#section_PropertiesonTests)
include(camitk/test/CamiTKInitTest)
include(camitk/test/CamiTKAddTest)
include(camitk/test/CamiTKAddIntegrationTest)
include(camitk/test/CamiTKAdditionalActionTest)
include(camitk/test/CamiTKDisableTests)
include(camitk/test/CamiTKTestsRequirement)

# -- packaging macros
include(camitk/packaging/CamiTKCommunityEditionPackaging)
include(camitk/packaging/CamiTKCEPPackaging)

# -- CamiTK actions / components parsing test macros
include(camitk/test/level/CamiTKParseTestInit)
include(camitk/test/level/CamiTKParseTestAdd)
include(camitk/test/level/CamiTKParseTestAddSeparator)
include(camitk/test/level/CamiTKParseTestValidate)

# -- Translations
include(camitk/CamiTKTranslate)

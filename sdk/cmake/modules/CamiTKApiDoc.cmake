#------------------------------------------
# To generate CamiTK the api documentation
#-------------------------------------------
#
# To generate the documentation for a new version and update the website:
# - set the APIDOC_SDK option to ON
# - make camitk-ce-api-doc
# - cd ${CAMITK_DOCSY_ROOT}/static/api
# - cp ${CMAKE_BUILD_DIR}/share/camitk-${version}/apidoc/html ${version}
# and update the config.toml of the docsy accordingly to ${version}
#
# If respectively doxygen changed dramatically its css, or docsy its header, tweaks have to be made
# camitk_doxygen_stylesheet.css or (camitk.css and camitk_doxygen_header.html) respectively.
#
# ------------------------------------------------
# Note about custom header, footer and stylesheet
# ------------------------------------------------
#
# When the doxygen version changes, the header, footer and stylesheet might also change
# Update camitk_doxygen_footer.html  camitk_doxygen_header.html  camitk_doxygen_stylesheet.css
# by first ask doxygen to generate default versions using:
# doxygen -w html new_header.html new_footer.html new_stylesheet.css YourConfigFile
#

set(APIDOC_SDK FALSE CACHE BOOL "Generate API documentation for CamiTK Community Edition")
if(APIDOC_SDK)
    include(FindDoxygen)
    if (DOXYGEN)
        # ---------------------------------
        # Doxygen requirement
        # ---------------------------------
        find_package(HTMLHelp)

        if (HTML_HELP_COMPILER)
            set(DOXYGEN_HTMLHELP YES)
        else (HTML_HELP_COMPILER)
            set(DOXYGEN_HTMLHELP NO)
        endif (HTML_HELP_COMPILER)

        if (DOT)
            set(HAVE_DOT YES)
        else (DOT)
            set(HAVE_DOT NO)
        endif (DOT)

        # ---------------------------------
        # Doxygen CMake configuration
        # ---------------------------------
        set(DOXYGEN_LANGUAGE "English" CACHE STRING "Language used by doxygen")
        mark_as_advanced(DOXYGEN_LANGUAGE)

        # define where to put the camitk-ce-api-doc at build time
        set(CAMITK_APIDOC_DIR ${CMAKE_BINARY_DIR}/share/${CAMITK_SHORT_VERSION_STRING}/apidoc)
        set(DOXYGEN_OUTPUT_DIR ${CAMITK_APIDOC_DIR})
        
        # path to the logo
        set(PROJECT_LOGO ${CMAKE_CURRENT_SOURCE_DIR}/libraries/core/resources/camitk-small.png)
                
        message(STATUS "Generating build target 'camitk-ce-api-doc' for CamiTK SDK documentation in ${CAMITK_APIDOC_DIR}")
        
        # ---------------------------------
        # List of source dirs to use
        # ---------------------------------
        set(DOXYGEN_SUBDIRS)
        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_CURRENT_SOURCE_DIR}/doc)
        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_CURRENT_SOURCE_DIR}/libraries/core)
        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_CURRENT_SOURCE_DIR}/libraries/cepgenerator)
        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_CURRENT_SOURCE_DIR}/libraries/qtpropertybrowser)
        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_CURRENT_SOURCE_DIR}/viewers)
        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_CURRENT_SOURCE_DIR}/actions)
        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_CURRENT_SOURCE_DIR}/applications)
        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_CURRENT_SOURCE_DIR}/cmake)
        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_CURRENT_SOURCE_DIR}/components)
        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_BINARY_DIR}/doxygencmake)

        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_SOURCE_DIR}/imaging/doc)
        #do not exist yet        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_SOURCE_DIR}/imaging/viewers)
        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_SOURCE_DIR}/imaging/actions)
        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_SOURCE_DIR}/imaging/libraries)
        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_SOURCE_DIR}/imaging/components)

        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_SOURCE_DIR}/modeling/doc)
        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_SOURCE_DIR}/modeling/libraries)
        #do not exist yet        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_SOURCE_DIR}/modeling/viewers)
        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_SOURCE_DIR}/modeling/actions)
        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_SOURCE_DIR}/modeling/components)
        
        string(REGEX REPLACE ";" " " DOXYGEN_INPUT_LIST "${DOXYGEN_SUBDIRS}")
                
        # -------------------------------
        # CMake macro doxygen generation 
        # -------------------------------
        # This is a great trick that will read all the CMake macros source and generate corresponding 
        # pseudo-header files with doxygen documentation tags.
        # It create pseudo-header source file by converting all the #! comment in the macro source into doxygen comments
        # These pseudo header files are written in ${CMAKE_BINARY_DIR}/doxygencmake (which path is included in the 
        # DOXYGEN_INPUT_LIST). They will therefore generate corresponding API doc web pages.
        file(MAKE_DIRECTORY ${CMAKE_BINARY_DIR}/doxygencmake)
        
        file(GLOB_RECURSE CMAKE_MACROS ${CMAKE_SOURCE_DIR}/sdk/cmake/modules/macros/*.cmake)
        
        foreach(CMAKE_MACRO ${CMAKE_MACROS})
            # message(STATUS "Generating doxygen documentation for CMake macro ${CMAKE_MACRO}")
            # read to a string buffer
            file(READ ${CMAKE_MACRO} cmakeMacroText)
            # replace on each line the specific macro doxygen code #! by C++ header doxygecn code ///
            string(REGEX REPLACE "#!" "///" doxygenDoc "${cmakeMacroText}")
            # replace the string "macro(name_of_camitk_macro..." by "name_of_camitk_macro(...)" in order to have an nice doxygen output
            string(REGEX REPLACE "macro\\(([a-z_]*)" "\\1(){" doxygenDocWithName "${doxygenDoc}")
            # name of the file
            get_filename_component(doxygenDocFilename ${CMAKE_MACRO} NAME_WE)
            file(WRITE ${CMAKE_BINARY_DIR}/doxygencmake/${doxygenDocFilename}.h ${doxygenDocWithName})
        endforeach()
  
        # ---------------------------------
        # Configure doxyfile from template
        # ---------------------------------
        # Configure doxyfile, doxygen project configuration file.
        configure_file(${CMAKE_CURRENT_SOURCE_DIR}/doc/doxygen.conf.in ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile @ONLY)

        # Force configure file CamiTKVersion.h.in to generate index.html page for API documentation
        configure_file(${CMAKE_CURRENT_SOURCE_DIR}/libraries/core/CamiTKVersion.h.in ${CMAKE_CURRENT_BINARY_DIR}/core/CamiTKVersion.h @ONLY)

        # --------------------------------------------------------
        # Add the corresponding target and configure installation
        # --------------------------------------------------------
        add_custom_target(camitk-ce-api-doc
                          COMMAND ${DOXYGEN_EXECUTABLE} ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile
                          # copy custom css
                          COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_CURRENT_SOURCE_DIR}/doc/resources/camitk.css ${CAMITK_APIDOC_DIR}/html
                          COMMENT "Generating CamiTK SDK API documentation using Doxygen"
                          VERBATIM
        )

        # create directory if it does not exist
        if (NOT EXISTS ${CAMITK_APIDOC_DIR})
            message(STATUS "Creating api-doc directory: ${CAMITK_APIDOC_DIR}")
            file(MAKE_DIRECTORY ${CAMITK_APIDOC_DIR})
        endif()

        # doc installation
        install(DIRECTORY ${CAMITK_APIDOC_DIR}
                DESTINATION share/${CAMITK_SHORT_VERSION_STRING}
                COMPONENT camitk-ce-api-doc
        )
    else()
        message(WARNING "Doxygen not found - CamiTK SDK API documentation and reference manual will not be created")
    endif()
endif()

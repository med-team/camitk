#!
#! @ingroup group_sdk_cmake
#!
#! macro get_subdirectoryfiles get the names of all the files (and not directories) contained in a defined directory
#! in case insensitive alphabetical order
#!
#! \note
#! This macro sorts the filename alphabetically without considering the case (case insensitive sort)
#! This sort is needed to always get the filenames in the same order, on all plateforms (Linux, Windows,...) thanks
#! to the insensitive sort.
#!
#! Usage:
#! \code
#! get_subdirectoryfiles(Path Filenames)
#! \endcode
#!
#! \param Path (required)          input directory path
#! \param Name (required)          the name of the resulting variable containing all the file names of the given directory sorted alphabetically (case insensitive)
#!            
macro(get_subdirectoryfiles Path Filenames)
  set(Filenames)
  file(GLOB children ${Path}/*)

  # continue process only if there is some file to process
  if (children)
    # create the copy of children
    set(childrenSorted)
    foreach(FILENAME ${children})
      string(TOLOWER "${FILENAME}" filenameLower)
      # build a new variable (map) to associate the key filenameLower to the real filename FILENAME
      set("map_${filenameLower}" "${FILENAME}")
      # add the key to a specific list
      list(APPEND childrenSorted "${filenameLower}")
    endforeach()
    
    # sort the key list (all lowercase)
    list(SORT childrenSorted)

    # loop on all keys
    foreach(filenameLower ${childrenSorted})
      # get the filename from the lower case name
      set(realFilename ${map_${filenameLower}})
      if(NOT IS_DIRECTORY ${realFilename})
          set(${Filenames} ${${Filenames}} ${realFilename})
      endif()
    endforeach()
  endif()
endmacro()

#!
#! This CMake file run a command, put the command output in a given file
#! and check if the command output is the same as the given input file.
#! Use this CMake file to test if a command returns the same log as a given expected log.
#! 
#! \note
#! You have to call camitk_test_init(..) first to prepare all variables
#!
#! Inspired from http://stackoverflow.com/questions/3305545/how-to-adapt-my-unit-tests-to-cmake-and-ctest
#! and http://www.cmake.org/pipermail/cmake/2009-July/030619.html

# declare outputfiles
set(CAMITK_TEST_COMMAND_FILE ${CAMITK_TEST_OUTPUT_DIR}/command) # which command is run to test the exectable
set(CAMITK_TEST_COMMAND_RESULT_FILE ${CAMITK_TEST_OUTPUT_DIR}/command-result) # the exit result (0=success, 1=failure) of the tested command goes in this file
set(CAMITK_TEST_COMMAND_OUTPUT_FILE ${CAMITK_TEST_OUTPUT_DIR}/command-output) # the output of the tested command goes in this files
set(CAMITK_TEST_PASS_FILE_COMMAND_FILE ${CAMITK_TEST_OUTPUT_DIR}/test) # which command is run to diff the files
set(CAMITK_TEST_PASS_FILE_OUTPUT_FILE ${CAMITK_TEST_OUTPUT_DIR}/test-output) # output of the diff cmake command
set(CAMITK_TEST_PASS_FILE_RESULT_FILE ${CAMITK_TEST_OUTPUT_DIR}/test-result) # exit result (0=success, 1=failure)

# remove all
execute_process(COMMAND ${CMAKE_COMMAND} -E remove -f ${CAMITK_TEST_COMMAND_FILE} ${CAMITK_TEST_COMMAND_RESULT_FILE} ${CAMITK_TEST_COMMAND_OUTPUT_FILE} ${CAMITK_TEST_PASS_FILE_COMMAND_FILE} ${CAMITK_TEST_PASS_FILE_OUTPUT_FILE} ${CAMITK_TEST_PASS_FILE_RESULT_FILE})
# First run the executable
file(WRITE ${CAMITK_TEST_COMMAND_FILE} "${CAMITK_TEST_COMMAND} ${CAMITK_TEST_COMMAND_ARG}")

# expands all arguments
string(REPLACE " " ";" CAMITK_TEST_COMMAND_ARG_LIST ${CAMITK_TEST_COMMAND_ARG})

# Run test and get output
execute_process(
  COMMAND ${CAMITK_TEST_COMMAND} ${CAMITK_TEST_COMMAND_ARG_LIST}
  RESULT_VARIABLE CAMITK_TEST_COMMAND_RESULT
  OUTPUT_VARIABLE CAMITK_TEST_COMMAND_OUTPUT
)
file(WRITE ${CAMITK_TEST_COMMAND_RESULT_FILE} ${CAMITK_TEST_COMMAND_RESULT})
file(WRITE ${CAMITK_TEST_COMMAND_OUTPUT_FILE} ${CAMITK_TEST_COMMAND_OUTPUT})

# Then compare output with input
file(WRITE ${CAMITK_TEST_PASS_FILE_COMMAND_FILE} "${CMAKE_COMMAND} -E compare_files ${CAMITK_TEST_PASS_FILE} ${CAMITK_TEST_COMMAND_OUTPUT_FILE}")


# check file exists
if (NOT EXISTS ${CAMITK_TEST_PASS_FILE})
  message(STATUS "[FAIL]")
  message(FATAL_ERROR "${CAMITK_TEST_NAME}: input file ${CAMITK_TEST_PASS_FILE} not found." )
endif()

if (NOT EXISTS ${CAMITK_TEST_COMMAND_OUTPUT_FILE})
  message(STATUS "[FAIL]")
  message(FATAL_ERROR "${CAMITK_TEST_NAME}: output file ${CAMITK_TEST_COMMAND_OUTPUT_FILE} not found." )
endif()


message(STATUS "Comparing file \"${CAMITK_TEST_PASS_FILE}\" to \"${CAMITK_TEST_COMMAND_OUTPUT_FILE}\"...")
execute_process(
  COMMAND ${CMAKE_COMMAND} -E compare_files ${CAMITK_TEST_PASS_FILE} ${CAMITK_TEST_COMMAND_OUTPUT_FILE}
  RESULT_VARIABLE CAMITK_TEST_PASS_FILE_RESULT
  OUTPUT_VARIABLE CAMITK_TEST_PASS_FILE_OUTPUT
  OUTPUT_QUIET
  ERROR_QUIET
)
file(WRITE ${CAMITK_TEST_PASS_FILE_OUTPUT_FILE} ${CAMITK_TEST_PASS_FILE_OUTPUT})
file(WRITE ${CAMITK_TEST_PASS_FILE_RESULT_FILE} ${CAMITK_TEST_PASS_FILE_RESULT})

# check result
if( CAMITK_TEST_PASS_FILE_RESULT )
  message(STATUS "[FAIL]")
  message(FATAL_ERROR "${CAMITK_TEST_NAME}: ${CAMITK_TEST_COMMAND_OUTPUT_FILE} does not match ${CAMITK_TEST_PASS_FILE}" )
else()
  message(STATUS "[OK]")
endif()

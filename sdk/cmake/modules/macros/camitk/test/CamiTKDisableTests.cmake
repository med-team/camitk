#!
#! @ingroup group_sdk_cmake_camitk_test
#!
#! camitk_disable_tests is a macro to unconditionally disable one or more tests.
#! 
#! It encapsulates CMake set_tests_properties in order to manage version older that CMake 3.9
#! and to force the developper to give a (good) reason for bypassing tests.
#! If CMake version is lower than 3.9, the test(s) is/are executed but with the WILL_FAIL (i.e., a
#! failed test is considered as passed).
#!
#! Use in conjunction with the camitk_extension macro. In camitk_extension using ENABLE_AUTO_TEST
#! automatically generate a number of tests. Some of these tests might need to be unconditionally disabled 
#! during the development process, in this case use this macro.
#!
#! Usage:
#! \code
#! camitk_disable_tests(TESTS test1 [test2...]
#!                      REASON "reason"
#! )
#! \endcode
#!
#! \param TESTS         Names of the test(s) to disable (names separated by space)
#! \param REASON        A text to explain why the tests is/are unconditionally disabled. 
#!
#!
#! Example invocation:
#!
#! \code
#!
#! camitk_extension(ACTION_EXTENSION
#!                 ...
#!                 ENABLE_AUTO_TEST
#!                 ...
#! )
#!
#! ...
#! # Specific test management
#! camitk_disable_tests(TESTS action-myaction-level1-10
#!                      REASON "This test is expected to fail until such and such is implemented. Disable for now"
#! )
#! \endcode
#
#! @sa camitk_tests_requirement
macro(camitk_disable_tests)
    set(options "")
    set(oneValueArgs REASON)
    set(multiValueArgs TESTS)
    cmake_parse_arguments(CAMITK_DISABLE_TESTS "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})
    
    foreach(TEST_NAME ${CAMITK_DISABLE_TESTS_TESTS})
        # CMake < 3.9 did not have the DISABLED test property. Use WILL_FAIL instead
        if(${CMAKE_VERSION} VERSION_LESS "3.9")
            set_tests_properties(${TEST_NAME} PROPERTIES WILL_FAIL true)
        else()
            set_tests_properties(${TEST_NAME} PROPERTIES DISABLED true)
        endif()
    endforeach()
    
    # message to the developper
    string(REPLACE ";" " " CAMITK_DISABLE_TESTS_TESTS_STRING "${CAMITK_DISABLE_TESTS_TESTS}")
    set(DISABLE_MESSAGE "Unconditionally disabling tests:\n   Disabled tests: ${CAMITK_DISABLE_TESTS_TESTS_STRING}\n")
    if(${CMAKE_VERSION} VERSION_LESS "3.9")
        set(DISABLE_MESSAGE ${DISABLE_MESSAGE} "   (using WILL_FAIL property because CMake < 3.9)\n")
    endif()
    set(DISABLE_MESSAGE ${DISABLE_MESSAGE} "   Reason: ${CAMITK_DISABLE_TESTS_REASON}")
    message(STATUS ${DISABLE_MESSAGE})
    
endmacro()

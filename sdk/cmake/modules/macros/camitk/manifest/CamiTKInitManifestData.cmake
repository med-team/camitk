#!
#! @ingroup group_sdk_cmake_camitk_cdash_projects
#!
#! MACRO camitk_init_manifest_data initializes the global variables that will
#! gather all the information about all the subprojects.
#!
#! There are 7 types of subprojects:
#! - the corelib (only present in the SDK)
#! - the cep library (one subproject per library of the CEP)
#! - the application action (only present in the SDK for application actions)
#! - the component (one subproject per component extension of the CEP)
#! - the action (one subproject per action extension of the CEP)
#! - the viewer (one subproject per viewer extension of the CEP)
#! - the application (one subproject per application of the CEP)
#!
#! Note: application actions are special actions grouped into a specific action extension.
#! This unique action extension is mandatory for any CamiTK application to run properly
#! (they include basic actions such as "save", "open"...)
#!
#! macro camitk_init_manifest_data is called only once per CEP or per CEP Set and
#! reset the global cache variables that are filled with XML fragment.
#!
#! The CAMITK_SUBPROJECTS variable is a XML document that concatenate all
#! these variable. CAMITK_SUBPROJECTS is used to produced the "Project.xml" file
#! in the build directory. The "Project.xml" file is send and later parse by CDash to
#! present Continuous Integration information by subproject.
#!
#! This macro clears all the following global cache variables:
#! - CAMITK_CORE_LIBRARY_SUBPROJECTS is the XML fragment describing the CamiTK Core library (only found in the SDK)
#! - CAMITK_CEP_LIBRARY_SUBPROJECTS is the XML fragment describing the library created by the CEP
#! - CAMITK_APPLICATION_ACTION_SUBPROJECTS is the XML fragment describing the application actions (only found in the SDK)
#! - CAMITK_COMPONENT_SUBPROJECTS is the XML fragment describing the component extensions created by the CEP
#! - CAMITK_ACTION_SUBPROJECTS is the XML fragment describing the action extensions created by the CEP
#! - CAMITK_VIEWER_SUBPROJECTS is the XML fragment describing the viewer extensions created by the CEP
#! - CAMITK_APPLICATION_SUBPROJECTS is the XML Fragment describing applications created by the CEP
#!
#! The CAMITK_TARGETS variable list all the targets. It is also used by CDash.
#! Targets are also organized into sub categies:
#!
#! This macro also clears all these global cache variables.
#! - CAMITK_CEP_LIBRARY_TARGETS
#! - CAMITK_COMPONENT_TARGETS
#! - CAMITK_ACTION_TARGETS
#! - CAMITK_VIEWER_TARGETS
#! - CAMITK_APPLICATION_TARGETS
#!
#! usage:
#! \code
#! camitk_init_manifest_data()
#! \endcode
macro(camitk_init_manifest_data)

    if(CAMITK_EXTENSION_PROJECT_SET)
        if(NOT CAMITK_SUBPROJECTS)
            # This is a call to camitk_init_manifest_data from the first CEP of a CEP Set
            camitk_init_manifest_data_reset_variables(${CAMITK_EXTENSION_PROJECT_SET_NAME})
        endif()
        # else: if CAMITK_SUBPROJECTS cache variable is defined, this means the variables were already reset
    else()
        # This is a call to camitk_init_manifest_data from a standalone CEP
        camitk_init_manifest_data_reset_variables(${CMAKE_PROJECT_NAME})
    endif()

endmacro()

# Intern macro to simplify camitk_init_manifest_data
macro(camitk_init_manifest_data_reset_variables NAME)

    # list of all subprojects
    set(CAMITK_TARGETS             "" CACHE INTERNAL "List of all compilation targets")

    # Targets by category
    set(CAMITK_CEP_LIBRARY_TARGETS "" CACHE STRING "List of library extension targets"     FORCE)
    set(CAMITK_COMPONENT_TARGETS   "" CACHE STRING "List of component extension targets"   FORCE)
    set(CAMITK_ACTION_TARGETS      "" CACHE STRING "List of action extension targets"      FORCE)
    set(CAMITK_VIEWER_TARGETS      "" CACHE STRING "List of viewer extension targets"      FORCE)
    set(CAMITK_APPLICATION_TARGETS "" CACHE STRING "List of application targets"           FORCE)

    # Subproject list (XML document)
    set(CAMITK_SUBPROJECTS "<?xml version=\"1.0\"?><Project name=\"${NAME}\">" CACHE INTERNAL "XML description of all subprojects")

    # Subprojects by category (XML fragment)
    set(CAMITK_CORE_LIBRARY_SUBPROJECTS ""       CACHE INTERNAL "XML description of core library subprojects")
    set(CAMITK_CEP_LIBRARY_SUBPROJECTS ""        CACHE INTERNAL "XML description of library subprojects")
    set(CAMITK_APPLICATION_ACTION_SUBPROJECTS "" CACHE INTERNAL "XML description of application action extension subprojects")
    set(CAMITK_COMPONENT_SUBPROJECTS ""          CACHE INTERNAL "XML description of component extension subprojects")
    set(CAMITK_ACTION_SUBPROJECTS ""             CACHE INTERNAL "XML description of action extension subprojects")
    set(CAMITK_VIEWER_SUBPROJECTS ""             CACHE INTERNAL "XML description of viewer extension subprojects")
    set(CAMITK_APPLICATION_SUBPROJECTS ""        CACHE INTERNAL "XML description of application subprojects")

endmacro()

#!
#! @ingroup group_sdk_cmake
#!
#! macro get_subdirectories get the names of all the subdirectories that
#! contains a CMakeLists.txt in case insensitive alphabetical order
#!
#! \note
#! This macro sorts the directory alphabetically without considering the case (case insensitive sort)
#! This sort is needed to always get the subdirectories in the same order, on all plateforms (Linux, Windows,...) thanks
#! to the insensitive sort.
#!
#! Usage:
#! \code
#! get_subdirectories(Name)
#! \endcode
#!
#! \param Name (required)          the name of the resulting variable containing all the subdirectories names sorted alphabetically (case insensitive)
#!
macro(get_subdirectories Name)
  set(Name)
  file(GLOB LS *)
  
  # continue process only if there is some file to process
  if (LS)
    foreach(FILENAME ${LS})
      if(IS_DIRECTORY ${FILENAME} AND EXISTS "${FILENAME}/CMakeLists.txt")
        get_directory_name(${FILENAME} DIRNAME)
        # ignore .svn
        if (NOT DIRNAME STREQUAL ".svn")
	        set(${Name} ${${Name}} ${DIRNAME})
        endif()
      endif()
    endforeach()

    # create the copy of Name list
    set(NameSorted)
    foreach(filename ${Name})
      string(TOLOWER "${filename}" filenameLower)
      # build a new variable (map) to associate the key filenameLower to the real filename
      set("map_${filenameLower}" "${filename}")
      # add the key to a specific list
      list(APPEND NameSorted "${filenameLower}")
    endforeach()

    # sort the key list (all lowercase)
    list(SORT NameSorted)
    
    foreach(filenameLower ${NameSorted})    
      # get the filename from the lower case name
      set(Name ${map_${filenameLower}})
    endforeach()  
  endif()
  
endmacro()
